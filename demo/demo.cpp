//------------------------------------//
//
// demo.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: demo
// Date: 2006-7-11
//
//------------------------------------//

#include "../src/ayuine2.hpp"

using namespace ayuine;

void site_main() {
	FreeCamera camera;
	ALight light1, light2, light3, light4, light5, light6, light7;
	//AEmitter emitter(50);
	ASkyDome sky;
	AMesh lamp, lamp2, lamp3;

	//// Ustaw �wiat�a
	//light1.Origin = vec3(0, -3, 3);
	//light1.Color = vec4(1.0, 0.5, 0.5, 1.5);
	//light1.Range = 8.0f;
	//light1.link(Engine::World);

	//light2.Origin = vec3(9, 3, 3);
	//light2.Color = vec4(1.0, 0.5, 0.5, 1.5);
	//light2.Range = 8.0f;
	//light2.link(Engine::World);

	//light3.Origin = vec3(9, -8, 3);
	//light3.Color = vec4(0.5, 1.0, 0.5, 1.5);
	//light3.Range = 8.0f;
	//light3.link(Engine::World);

	//light4.Origin = vec3(1.36f, -7.16f, 0.23f);
	//light4.Color = vec4(0.5, 0.5, 1.0, 1.5);
	//light4.Range = 8.0f;
	//light4.link(Engine::World);

	//light5.Origin = vec3(-10, -8, 1);
	//light5.Color = vec4(0.5, 1.0, 1.0, 1.5);
	//light5.Range = 8.0f;
	//light5.link(Engine::World);

	//light6.Origin = vec3(-9, 3.38f, 2.39f);
	//light6.Color = vec4(1.0, 1.0, 0.5, 1.5);
	//light6.Range = 8.0f;
	//light6.link(Engine::World);

	//light7.Origin = vec3(-8, -3, 4/*-30, -2, 9*/);
	//light7.Color = vec4(1.0f, 1.0f, 0.1f, 1.5f);
	//light7.Range = 8.0f;
	//light7.link(Engine::World);

	// Ustaw niebo
	sky.Sun = true;
	sky.SunArea = 2.0f;
	sky.SunColor = vec4(0.8f, 0.8f, 0.8f, 3.0f);
	sky.SunDir = vec3(1, 1, 1).normalize();
	sky.link(Engine::World);

	// Lampa
	lamp.Mesh = Mesh::get("lamp_shade");
	lamp.Origin = vec3(0, -3, 4);
	lamp.Angles.pitch() = 30.0f;
	lamp.rigidBody(8.0f);
	lamp.link(Engine::World);

	// Lampa 2
	lamp2.Mesh = Mesh::get("lamp_shade");
	lamp2.Origin = vec3(0, -2.5, 3);
	lamp2.Angles.pitch() = -20.0f;
	lamp2.rigidBody(8.0f);
	lamp2.link(Engine::World);

	//// Ustaw emitter cz�steczek
	//emitter.Type = etSphere;
	//emitter.Origin = vec3(1.65f, -8.65f, 0.6f);
	//emitter.Size = vec3(0.08f, 0.08f, 0.08f);
	//emitter.Radius = 0.25f;
	//emitter.RadiusDerivative = 0.5f;
	//emitter.StartColor = vec4(0.05f, 0.05f, 0.05f, 0.3f);
	//emitter.EndColor = vec4(0.05f, 0.05f, 0.05f, 0.1f);
	//emitter.LifeTime = 1.6f;
	//emitter.Enabled = true;
	//emitter.Material = "default_particle";
	//emitter.Force = vec3(0, 0, 1);
	//emitter.StartVelocity = 0.05f;
	//emitter.link(Engine::World);
	
	// Skonfiguruj silnik
	Engine::Camera = &camera;

	// Wczytaj materia�y i tekstury
	Material::cache();
	Texture::cache();

	// Uruchom silnik
	Engine::run();

	// Wywal kamer�
	Engine::Camera = nullptr;
}

int main(int argc, char *argv[]) {
	// W��cz silnik
	Engine::init();

	// Wczytaj �wiat
	Engine::World->load("site");

	// Uruchom silnik
	site_main();

	// Wy��cz silnik
	Engine::release();
	return 0;
}