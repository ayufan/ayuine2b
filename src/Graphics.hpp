//------------------------------------//
//
// Graphics.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-14
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// VertexShaderConst enumeration

	enum VertexShaderConst
	{
		vscNone = -1,

		// Material Consts
		vscColor						= 0,
		vscParam						= 1,
		vscFrame						= 2,

		// Light Consts
		vscLightColor				= 3,
		vscLightParam				= 4,
		
		// Transform Consts
		vscTransform				= 5,
		vscObject						= 9,

		// Light Consts
		vscLightTransform		= 12,
		vscLightOrigin			= 16,
		vscLightDir					= 17,

		// Camera Consts
		vscCameraOrigin			= 18,
		vscCameraAt					= 19,
		vscCameraUp					= 20,
		vscCameraRight			= 21,

		// Skinning Consts
		vscSkinning					= 22
	};

	//------------------------------------//
	// PixelShaderConst enumeration

	enum PixelShaderConst
	{
		pscNone = -1,

		// Material Consts
		pscColor						= 0,
		pscParam						= 1,
		pscFrame						= 2,

		// Light Consts
		pscLightColor				= 3,
		pscLightParam				= 4,

		// PRT Consts
		pscPCA							= 5,

		// Bloom Consts
		pscTaps							= 9
	};

	//------------------------------------//
	// SamplerTypes enumeration

	enum SamplerTypes
	{
		// Material Samplers
		stTexture0					= 0,
		stTexture1					= 1,
		stTexture2					= 2,
		stTexture3					= 3,

		// Ambient/Light/Refraction Samplers
		stPRT								= 4,
		stShadow						= 5,
		stProjection				= 6,
		stRefractive				= 6,
		stReflective				= 7,

		// PostProcess Samplers
		stScene							= 6,
		stBlur							= 7
	};

	//------------------------------------//
	// BlendMode enumeration
	
	enum BlendMode
	{
		bmNone,
		bmBlend,
		bmAdditive,
		bmDecal,
		bmOverlay
	};

	//------------------------------------//
	// TextureMode enumeration

	enum TextureMode
	{
		tmConst,
		tmDiffuse,
		tmTexture,
		tmTextureConst,
		tmTextureDiffuse,
		tmMaskedColor
	};

	//------------------------------------//
	// TextureFilter enumeration

	enum TextureFilter
	{
		tfPoint,
		tfLinear,
		tfMipmaps
	};

	//------------------------------------//
	// TextureAddress enumeration

	enum TextureAddress
	{
		taWrap,
		taClamp,
		taMirror,
		taBorder
	};

	//------------------------------------//
	// LockFlags enumeration

	enum LockFlags 
	{
		lfNone,
		lfReadOnly,
		lfNoOverwrite
	};
};

#include "DeviceObject.hpp"
#include "IndexBuffer.hpp"
#include "VertexBuffer.hpp"
#include "VertexType.hpp"
#include "Texture.hpp"
#include "Shader.hpp"
#include "Font.hpp"
#include "Draw.hpp"
#include "Device.hpp"
