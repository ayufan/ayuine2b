//------------------------------------//
//
// SmartPtr.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-2
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// BaseSmartPtr class

	template<typename _Type>
	class SmartPtr
	{
		ValueType(SmartPtr, 0);

		// Typedefs
	public:
		typedef _Type					Type;
		typedef	const	_Type		ConstType;

		// Fields
	public:
		Type*		Value;

		// Constructor
	public:
		SmartPtr() {
			Value = nullptr;
		}
		explicit SmartPtr(Type *value) {
			Value = value;
		}
	private:
		SmartPtr(const SmartPtr<_Type> &other) {
		}

		// Destructor
	public:
		~SmartPtr() {
			reset();
		}

		// Operators
	public:
		operator Type* () {
			return Value;
		}
		operator ConstType* () const {
			return Value;
		}

		// Accessor Operators
	public:
		Type *operator -> () {
			return Value;
		}
		const Type *operator -> () const {
			return Value;
		}

		// Unary Operators
	public:
		Type &operator * () {
			return *Value;
		}
		const Type &operator * () const {
			return Value;
		}
		bool operator ! () {
			return Value == nullptr;
		}

		// Assign Operators
	public:
		//SmartPtr<_Type> &operator = (Type *value) {
		//	reset(value);
		//	return (_Base&)*this;
		//}

		// Methods
	public:
		void reset(Type *value = nullptr) {
			// Zniszcz star� warto��
			if(Value) {
				IUnknown* i = dynamic_cast<IUnknown*>(Value);
				if(i) {
					i->Release();
				}
				else {
					delete Value;
				}
			}

			// Ustaw now� warto��
			Value = value;
		}
		Type *release() {
			Type *value;

			// Zapisz warto��
			value = Value;

			// Zwolnij warto��
			Value = nullptr;

			// Zwr�� warto��
			return Value;
		}
	};
};
