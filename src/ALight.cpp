//------------------------------------//
//
// ALight.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-1
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// ALight: Constructor

	ALight::ALight() {
		Range = 50.0f;
		Color = vec4(1, 0, 0, 1);

		_shadowFlags = 0;
		
		// Flags
		Flags = afCastShadows | afInvalidate | afUpdate | afZones | afArea | afPrepare | afTransform;
	}

	//------------------------------------//
	// ALight: Destructor

	ALight::~ALight() {
	}

	//------------------------------------//
	// ALight: Methods

	void ALight::invalidate(const bbox &box, Actor *owner) {
		// Sprawd� czy obiekt rzuca cie�
		if(owner) {
			if(~owner->Flags & afCastShadows)
				return;
		}

		static vec4 planes[] = {
			vec4(+1, -1,  0, 0).normalize(),
			vec4(+1, +1,  0, 0).normalize(),
			vec4( 0, +1, -1, 0).normalize(),
			vec4( 0, +1, +1, 0).normalize(),
			vec4(+1,  0, -1, 0).normalize(),
			vec4(+1,  0, +1, 0).normalize()
		};
		u32 frustum[6][4][2] = {
			{{0, 1}, {1, 1}, {4, 1}, {5, 1}},		// X-
			{{0, 2}, {1, 2}, {4, 2}, {5, 2}},		// X+
			{{0, 2}, {1, 1}, {2, 1}, {3, 1}},		// Y-
			{{0, 1}, {1, 2}, {2, 2}, {3, 2}},		// Y+ 
			{{4, 2}, {5, 1}, {2, 2}, {3, 1}},		// Z-
			{{4, 1}, {5, 2}, {2, 1}, {3, 2}}		// Z+ 
		}; 

		// Sprawd� czy obszar pokrywa si� z zasi�giem �wiat�a
		if(!Box.test(box))
			return;

		// Przebuduj wszystko
		if(box.test(Origin)) {
			_shadowFlags = 0;
			return;
		}

		u32 sides[6];
		bbox lbox(box.Mins - Origin, box.Maxs - Origin);

		// Wyznacz pozycje bboxa dla ka�dej p�aszczyzny frustuma
		for(u32 i = 0; i < 6; i++) {
			sides[i] = planes[i].test(lbox);
		}

		// Sprawd� ka�d� �ciank�
		for(u32 i = 0; i < 6; i++) {
			for(u32 j = 0; j < 4; j++) {
				u32 side = sides[frustum[i][j][0]];
				u32 test = 3 - frustum[i][j][1];

				if(~side & test)
					goto next;
			}

			// Oznacz �ciank� do od�wie�enia
			_shadowFlags &= ~(1 << i);
next:;
		}
	}

	void ALight::preupdate(f32 dt) {
		// Wywo�aj g��wn� metod�
		Actor::preupdate(dt);

		// Ustaw boxa
		Box = bbox(Origin - Range, Origin + Range);
	}

	void ALight::postupdate(f32 dt) {
		// Wywo�aj g��wn� metod�
		Actor::postupdate(dt);

		// Sprawd� czy �wiat�o zmieni�o swoje parametry
		if(Changed)
			_shadowFlags = 0;
	}
	
	void ALight::prepare(RenderFrame &frame) {
		// Sprawd� typ wy�wietlanej sceny
		if(frame.Mode != rmShaded)
			return;

		if(Engine::Config->Shadows && ~_shadowFlags & 0x3F) {
			if(!_shadowMap)
				_shadowMap.reset(new CubeTarget(256, D3DFMT_A16B16G16R16));

			// TODO: Wygeneruj shadow map�
			CubeCamera camera;

			// Ustaw kamer�
			camera.Origin = Origin;
			camera.Distance = Range;

			// Wygeneruj ka�d� �ciank�
			for(u32 i = 0; i < 6; i++) {
				camera.Face = (CubeMapFace)i;

				// Sprawd� sciank�
				if(_shadowFlags & (1 << i))
					continue;
				_shadowFlags |= (1 << i);

				// Uaktualnij kamer�
				camera.update(0);

				// Utw�rz ramk� do wy�wietlania
				RenderFrame frame;

				// Skonfiguruj ramk�
				frame.camera(&camera);
				frame.Mode = rmDepth;
				frame.Width = _shadowMap->info().Width;
				frame.Height = _shadowMap->info().Height;

				// Przygotuj ramk�
				Engine::World->prepare(frame);

				// Podepnij shadow map�
				_shadowMap->begin(0, camera.Face);

				// Wy�wietl ramk�
				Engine::Render->frame(frame);

				// Odepnij shadow map�
				_shadowMap->end(0);
			}
		}

		// Utw�rz nowe �wiat�o
		RenderLight *light = new RenderLight();
		mat4 temp;

		// Wype�nij struktur� �wiat�a
		light->Box = Box;
		light->Sphere = bsphere(Origin, Range);
		light->Origin = Origin;
		light->Range = Range;
		light->Color = Color;
		light->Param = vec4(0.5f, 0.5f, 1.0f, 1.0f);
		light->PRT = Engine::Config->PRT > 1;
		light->ShadowMap = _shadowMap;

		// Oblicz �wiat�o
		if(light->PRT)
			//SphericalHarmonics::sphericalLight(light->Origin, light->Range, light->Color.W, light->PCA);
			dxe(D3DXSHEvalSphericalLight(4, (D3DXVECTOR3*)&light->Origin, light->Range, light->Color.W * 4, 1.0f, 1.0f, &light->PCA->X, nullptr, nullptr));

		// Oblicz transformacj�
		mat4::translation(light->Transform, -Origin);
		mat4::scaling(temp, vec3(1.0f / light->Range));
		mat4::multiply(light->Transform, temp, light->Transform);

		vec3 i = light->Transform.transformCoord(vec3(0, 0, 30));

		// Dodaj �wiat�o
		frame.addLight(light);
	}
}