//------------------------------------//	
//
// Camera.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-1
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Camera: Static Fields

	const mat4 Camera::BaseView(0,0,1,0, -1,0,0,0, 0,1,0,0, 0,0,0,1);

	//------------------------------------//
	// Camera: Constructor

	Camera::Camera() {
	}

	//------------------------------------//
	// Camera: Destructor

	Camera::~Camera() {
		dGuard {
		}
		dUnguard;
	}

	//------------------------------------//
	// Camera: Properties

	const vec3 &Camera::origin() const {
		return _origin;
	}

	const vec3 &Camera::at() const {
		return _at;
	}

	const vec3 &Camera::up() const {
		return _up;
	}

	const vec3 &Camera::right() const {
		return _right;
	}

	f32 Camera::distance() const {
		return _distance;
	}

	const mat4 &Camera::projection() const {
		return _projection;
	}

	const mat4 &Camera::view() const {
		return _view;
	}

	const ayuine::frustum& Camera::frustum() const {
		return _frustum;
	}

	//------------------------------------//
	// Camera: Methods

	void Camera::update(f32 dt) {
	}

	//------------------------------------//
	// FreeCamera: Constructor

	FreeCamera::FreeCamera() {
		_maxVelocity = 5.0f;
		_maxAngular = 180.0f;
		_velocityAccel = 2.0f;
		_angularAccel = 90.0f;
		_dumping = 0.95f;
		_controlled = true;
		Origin = vec3(0, 0, 3);
		Angles = angles(0, 0, 0);
	}

	//------------------------------------//
	// FreeCamera: Destructor

	FreeCamera::~FreeCamera() {
	}

	//------------------------------------//
	// FreeCamera: Methods

	void FreeCamera::update(f32 dtime) {
		Guard {
			// Sprawd� czy ta kamera jest kontrolowana...
			if(_controlled && Engine::Input) {
				// Warto�ci tymczasowe
				f32 dvel = _velocityAccel * dtime;
				f32	davel = _angularAccel * dtime;
				f32 l;
				vec3 v(0, 0, 0);
				angles a(0, 0, 0);

				// Obs�uga pr�dko�ci liniowej
				if(Engine::Input->keyState(DIK_UP, false))			
					v += Angles.at() * dvel;
				if(Engine::Input->keyState(DIK_DOWN, false))		
					v -= Angles.at() * dvel;
				if(Engine::Input->keyState(DIK_LEFT, false))		
					v -= Angles.right() * dvel;
				if(Engine::Input->keyState(DIK_RIGHT, false))		
					v += Angles.right() * dvel;
				if(Engine::Input->keyState(DIK_Z, false))			
					v += Angles.up() * dvel;
				if(Engine::Input->keyState(DIK_X, false))		
					v -= Angles.up() * dvel;
				if(Engine::Input->keyState(DIK_W, false))
					a.pitch() += davel;
				if(Engine::Input->keyState(DIK_S, false))
					a.pitch() -= davel;
				if(Engine::Input->keyState(DIK_A, false))
					a.yaw() -= davel;
				if(Engine::Input->keyState(DIK_D, false))
					a.yaw() += davel;
				if(Engine::Input->keyState(DIK_Q, false))
					a.roll() -= davel;
				if(Engine::Input->keyState(DIK_E, false))
					a.roll() += davel;

				// Obs�uga pr�dko�ci k�towej
				//a.yaw() += _angularAccel * (f32)Engine::Input->mouseX(true) / Engine::Device->width();
				//a.pitch() -= _angularAccel * (f32)Engine::Input->mouseY(true) / Engine::Device->height();

				// Oblicz pr�dko��
				_velocity += v;
				_angular += a;

				// Przytnij wektor pr�dko�ci liniowej
				l = _velocity.length();
				if(l > _maxVelocity)
					_velocity *= (_maxVelocity / l);

				// Przytnij wektor pr�dko�ci k�towej
				l = _angular.length();
				if(l > _maxAngular)
					_angular *= (_maxAngular / l);

				// Oblicz nowe parametry ruchu
				Origin += _velocity * dtime;
				Angles += _angular * dtime;

				// Przytnij g�ra/d�
				Angles.pitch() = clamp(Angles.pitch(), -90.0f, 90.0f);

				// Dumping pr�dko�ci k�towej i liniowej
				_velocity -= _velocity * _dumping * dtime;
				_angular -= _angular * _dumping * dtime;
			}

			mat4 temp;

			// Zapisz macierze dla kamery
			mat4::perspective(_projection, 65.0f, Engine::Device->aspect(), 0.05f, 1000.0f);
			mat4::rotation(temp, Angles);
			mat4::multiply(_view, BaseView, temp);
			mat4::translation(temp, -Origin);
			mat4::multiply(_view, _view, temp);

			// Zapisz parametry widoku
			_frustum = ayuine::frustum(_projection, _view);
			_origin = Origin;
			_at = Angles.at();
			_up = Angles.up();
			_right = Angles.right();
			_distance = 1000.0f;
		}
		Unguard;
	}

	//------------------------------------//
	// CubeCamera: Constructor

	CubeCamera::CubeCamera() {
		Face = cmfPositiveX;
		Distance = 1000.0f;
	}

	//------------------------------------//
	// CubeCamera: Update Methods

	void CubeCamera::update(f32 dtime) {
		switch(Face) {
			case cmfPositiveX:
				_at = vec3(-1, 0, 0);
				_up = vec3(0, -1, 0);
				break;

			case cmfNegativeX:
				_at = vec3(1, 0, 0);
				_up = vec3(0, -1, 0);
				break;

			case cmfPositiveY:
				_at = vec3(0, -1, 0);
				_up = vec3(0, 0, 1);
				break;

			case cmfNegativeY:
				_at = vec3(0, 1, 0);
				_up = vec3(0, 0, -1);
				break;

			case cmfPositiveZ:
				_at = vec3(0, 0, -1);
				_up = vec3(0, -1, 0);
				break;

			case cmfNegativeZ:
				_at = vec3(0, 0, 1);
				_up = vec3(0, -1, 0);
				break;
		}
		_right = (_up % _at).normalize();
		_origin = Origin;
		_distance = Distance;

		// Wyznacz macierz widoku
		mat4 temp(
			_at.X, _right.X, _up.X, 0,
			_at.Y, _right.Y, _up.Y, 0, 
			_at.Z, _right.Z, _up.Z, 0,
			0, 0, 0, 1);
		mat4::multiply(_view, BaseView, temp);
		mat4::translation(temp, -_origin);
		mat4::multiply(_view, _view, temp);

		// Wyznacz macierz transformacji
		mat4::perspective(_projection, 90.0f, 1.0f, 0.1f, _distance);

		// Oblicz p�aszczyzny widoku
		_frustum = ayuine::frustum(_projection, _view);
	}
};
