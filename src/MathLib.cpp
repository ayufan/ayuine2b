//------------------------------------//
//
// MathLib.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-24
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// vec4: Methods

	Side vec4::test(const class bsphere &sphere) const {
		f32 d = *this ^ sphere.Center;

		if(d - sphere.Radius > MathErr)
			return sFront;
		if(d + sphere.Radius < MathErr)
			return sBack;
		if(sphere.Radius > 0)
			return sSplit;
		return sOn;
	}

	Side vec4::test(const class bbox &box) const {
		vec3 mins, maxs;

		// Wyznacz wektory
		if(X < 0) {
			mins.X = box.Mins.X;
			maxs.X = box.Maxs.X;
		}
		else {
			maxs.X = box.Mins.X;
			mins.X = box.Maxs.X;
		}

		if(Y < 0) {
			mins.Y = box.Mins.Y;
			maxs.Y = box.Maxs.Y;
		}
		else {
			maxs.Y = box.Mins.Y;
			mins.Y = box.Maxs.Y;
		}

		if(Z < 0) {
			mins.Z = box.Mins.Z;
			maxs.Z = box.Maxs.Z;
		}
		else {
			maxs.Z = box.Mins.Z;
			mins.Z = box.Maxs.Z;
		}

		// Oblicz dystans
		f32 a = *this ^ mins;
		f32 b = *this ^ maxs;

		// Sprawd� wynik
		if(a >= MathErr && b < -MathErr)
			return sSplit;
		if(a > MathErr)
			return sFront;
		if(b < -MathErr)
			return sBack;
		return sOn;
	}

	//------------------------------------//
	// angles: Constructor

	angles::angles(const vec3 &dir, f32 r) {
		if(dir.X == 0 && dir.Y == 0) {
			yaw() = 0;
			pitch() = (f32)(dir.Z > 0 ? 90.0 : 270.0);
		}
		else {
			yaw() = arcTan(dir.Y, dir.X);
			if(yaw() < 0)
				yaw() += 360;

			pitch() = arcTan(dir.Z, vec2(dir.X, dir.Y).length());
			if(pitch() < 0)
				pitch() += 360;
		}
		roll() = r;
	}

	angles::angles(const vec3 &dir, const vec3 &up) {
		if(dir.X == 0 && dir.Y == 0) {
			yaw() = 0;
			pitch() = (f32)(dir.Z > 0 ? 90.0 : 270.0);
		}
		else {
			yaw() = arcTan(dir.Y, dir.X);
			if(yaw() < 0)
				yaw() += 360;

			pitch() = arcTan(dir.Z, vec2(dir.X, dir.Y).length());
			if(pitch() < 0)
				pitch() += 360;
		}
		roll() = 0.0f;
	}

	//------------------------------------//
	// angles: Methods

	vec3 angles::at() const {
		f32 sp, sy, cp, cy;

		sincos(-yaw(), sy, cy);
		sincos(-pitch(), sp, cp);

		return vec3(cp * cy, cp * sy, -sp);
	}

	vec3 angles::right() const {
		f32 sp, sy, sr, cp, cy, cr;

		sincos(-yaw(), sy, cy);
		sincos(-pitch(), sp, cp);
		sincos(-roll(), sr, cr);

		return vec3(- sr * sp * cy + cr * sy, - sr * sp * sy - cr * cy, -sr * cp);
	}

	vec3 angles::up() const {
		f32 sp, sy, sr, cp, cy, cr;

		sincos(-yaw(), sy, cy);
		sincos(-pitch(), sp, cp);
		sincos(-roll(), sr, cr);

		return vec3(cr * sp * cy + sr * sy, cr * sp * sy - sr * cy, cr * cp);
	}

	vec3 angles::rotate(const vec3 &in) const {
		vec3 in2(in);

		if(yaw() != 0)
			in2 = in2.rotateZ(yaw());

		if(pitch() != 0)
			in2 = in2.rotateY(pitch());

		if(roll() != 0)
			in2 = in2.rotateX(roll());

		return in2;
	}

	vec3 angles::rotate(const vec3 &in, const vec3 &around) const {
		vec3 in2(in - around);

		if(yaw() != 0)
			in2 = in2.rotateZ(yaw());

		if(pitch() != 0)
			in2 = in2.rotateY(pitch());

		if(roll() != 0)
			in2 = in2.rotateX(roll());

		return in2 + around;
	}

	//------------------------------------//
	// mat4: Constructors

	//NEW ORDER: mat4(	f32 a11, f32 a12, f32 a13, f32 a14,
	//			f32 a21, f32 a22, f32 a23, f32 a24,
	//			f32 a31, f32 a32, f32 a33, f32 a34,
	//			f32 a41, f32 a42, f32 a43, f32 a44)

	mat4::mat4(	
		f32 a11, f32 a21, f32 a31, f32 a41,
		f32 a12, f32 a22, f32 a32, f32 a42,
		f32 a13, f32 a23, f32 a33, f32 a43,
		f32 a14, f32 a24, f32 a34, f32 a44)
	{
		m11 = a11; m21 = a21; m31 = a31; m41 = a41;
		m12 = a12; m22 = a22;	m32 = a32; m42 = a42;
		m13 = a13; m23 = a23; m33 = a33; m43 = a43;
		m14 = a14; m24 = a24; m34 = a34; m44 = a44;
	}

	mat4::mat4(	
		f32 a11, f32 a21, f32 a31,
		f32 a12, f32 a22, f32 a32,
		f32 a13, f32 a23, f32 a33)
	{
		m11 = a11; m21 = a21; m31 = a31; m41 = 0;
		m12 = a12; m22 = a22;	m32 = a32; m42 = 0;
		m13 = a13; m23 = a23; m33 = a33; m43 = 0;
		m14 = 0;   m24 = 0;   m34 = 0;   m44 = 1;
	}

	//------------------------------------//
	// mat4: Properties

	vec4 mat4::column(u32 index) const {
		Assert(index < 4);
		switch (index) {
			default:
			case 0:		return vec4(m11, m21, m31, m41);
			case 1:		return vec4(m12, m22, m32, m42);
			case 2:		return vec4(m13, m23, m33, m43);
			case 3:		return vec4(m14, m24, m34, m44);
		}
	}

	vec4 mat4::row(u32 index) const {
		Assert(index < 4);
		switch (index) {
			default:
			case 0:		return vec4(m11, m12, m13, m14);
			case 1:		return vec4(m21, m22, m23, m24);
			case 2:		return vec4(m31, m32, m33, m34);
			case 3:		return vec4(m41, m42, m43, m44);
		}
	}

	void mat4::column(u32 index, const vec4 &value) {
		Assert(index < 4);
		switch(index) {
			case 0:		m11 = value.X; m21 = value.Y; m31 = value.Z; m41 = value.W; break;
			case 1:		m12 = value.X; m22 = value.Y; m32 = value.Z; m42 = value.W; break;
			case 2:		m13 = value.X; m23 = value.Y; m33 = value.Z; m43 = value.W; break;
			case 3:		m14 = value.X; m24 = value.Y; m34 = value.Z; m44 = value.W; break;
		}
	}

	void mat4::row(u32 index, const vec4 &value) {
		Assert(index < 4);
		switch(index) {
			case 0:		m11 = value.X; m12 = value.Y; m13 = value.Z; m14 = value.W; break;
			case 1:		m21 = value.X; m22 = value.Y; m23 = value.Z; m24 = value.W; break;
			case 2:		m31 = value.X; m32 = value.Y; m33 = value.Z; m34 = value.W; break;
			case 3:		m41 = value.X; m42 = value.Y; m43 = value.Z; m44 = value.W; break;
		}
	}

	vec3 mat4::origin() const {
		return vec3(m14, m24, m34);
	}

	vec3 mat4::originInv() const {
		return vec3(
			-(m14 * m11 + m24 * m21 + m34 * m31),
			-(m14 * m12 + m24 * m22 + m34 * m32),
			-(m14 * m13 + m24 * m23 + m34 * m33));
	}

	ayuine::angles mat4::angles() const {
		f32 xy = sqrtf(m11 * m11 + m12 * m12);

		if(xy > MathErr) {
			return ayuine::angles(
				-rad2deg(atan2f(m12, m11)),
				-rad2deg(atan2f(-m13, xy)),
				-rad2deg(atan2f(m23, m33)));
		}
		else {
			return ayuine::angles(
				-rad2deg(atan2f(-m21, m22)),
				-rad2deg(atan2f(-m13, xy)),
				0.0f);
		}
	}

	vec3 mat4::axis(u32 i) const {
		Assert("Out of range" && i > 3);
		return (vec3&)m[i][0];
	}

	vec3 mat4::scale() const {
		// Oblicz skal� macierzy
		return vec3(
			((vec3&)m11).length(),
			((vec3&)m21).length(),
			((vec3&)m31).length());
	}

	void mat4::scale(const vec3 &scale, bool origin) {
		// Przeskaluj macierz
		m11 *= scale.X;
		m12 *= scale.X;
		m13 *= scale.X;
		m21 *= scale.Y;
		m22 *= scale.Y;
		m23 *= scale.Y;
		m31 *= scale.Z;
		m32 *= scale.Z;
		m33 *= scale.Z;

		if(origin) {
			m14 *= scale.X;
			m24 *= scale.Y;
			m34 *= scale.Z;
		}
	}

	//------------------------------------//
	// mat4: Methods

	vec4 mat4::transform(const vec4 &i) const {
		return vec4(
			m11 * i.X + m12 * i.Y + m13 * i.Z + m14 * i.W,
			m21 * i.X + m22 * i.Y + m23 * i.Z + m24 * i.W,
			m31 * i.X + m32 * i.Y + m33 * i.Z + m34 * i.W,
			m41 * i.X + m42 * i.Y + m43 * i.Z + m44 * i.W);
	}

	vec4 mat4::transform(const vec3 &i) const {
		return vec4(
			m11 * i.X + m12 * i.Y + m13 * i.Z + m14 * 1.0f,
			m21 * i.X + m22 * i.Y + m23 * i.Z + m24 * 1.0f,
			m31 * i.X + m32 * i.Y + m33 * i.Z + m34 * 1.0f,
			m41 * i.X + m42 * i.Y + m43 * i.Z + m44 * 1.0f);
	}

	vec3 mat4::transformCoord(const vec3 &i) const {
		f32 invW = 1.0f / (m41 * i.X + m42 * i.Y + m43 * i.Z + m44 * 1.0f);
		return vec3(
			(m11 * i.X + m12 * i.Y + m13 * i.Z + m14 * 1.0f) * invW,
			(m21 * i.X + m22 * i.Y + m23 * i.Z + m24 * 1.0f) * invW,
			(m31 * i.X + m32 * i.Y + m33 * i.Z + m34 * 1.0f) * invW);
	}

	vec3 mat4::transformNormal(const vec3 &i) const {
		return vec3(
			m11 * i.X + m12 * i.Y + m13 * i.Z,
			m21 * i.X + m22 * i.Y + m23 * i.Z,
			m31 * i.X + m32 * i.Y + m33 * i.Z);
	}

	vec4 mat4::transformPlane(const vec4 &i) const {
		return vec4(transformNormal(i.normal()).normalize(), transformCoord(i.normal() * (-i.W)));
	}

	//------------------------------------//
	// mat4: Functions

	void mat4::lookAt(mat4 &m, const vec3 &eye, const vec3 &at, const vec3 &up) {
		vec3 x, y, z;

		z = (at - eye).normalize();
		x = (up % z).normalize();
		y = (z % x).normalize();

		m.m11 = x.X;	m.m12 = x.Y;	m.m13 = x.Z;	m.m14 = -(x ^ eye);
		m.m21 = y.X;	m.m22 = y.Y;	m.m23 = y.Z;	m.m24 = -(y ^ eye);
		m.m31 = z.X;	m.m32 = z.Y;	m.m33 = z.Z;	m.m34 = -(z ^ eye);
		m.m41 = 0;		m.m42 = 0;		m.m43 = 0;		m.m44 = 1;
	}
	
	void mat4::frustum(mat4 &m, const rect &bounds, f32 _near, f32 _far) {
		// Col 1
		m.m11 = (2 * _near) / (bounds.Right - bounds.Left);
		m.m12 = 0;
		m.m13 = (bounds.Left + bounds.Right) / (bounds.Left - bounds.Right);
		m.m14 = 0;

		// Col 2
		m.m21 = 0;
		m.m22 = (2 * _near) / (bounds.Top - bounds.Bottom);
		m.m23 = (bounds.Top + bounds.Bottom) / (bounds.Bottom - bounds.Top);
		m.m24 = 0;

		// Col 3
		m.m31 = 0;
		m.m32 = 0;
		m.m33 = _far / (_far - _near);
		m.m34 = (_near * _far) / (_near - _far);

		// Col 4
		m.m41 = 0;
		m.m42 = 0;
		m.m43 = 1;
		m.m44 = 0;
	}
	
	void mat4::perspective(mat4 &m, f32 fovy, f32 aspect, f32 _near, f32 _far) {
		f32 y = 1.0f / tan(fovy / 2.0f);
		f32 x = y / aspect;

		// Col 1
		m.m11 = x;
		m.m12 = 0;
		m.m13 = 0;
		m.m14 = 0;

		// Col 2
		m.m21 = 0;
		m.m22 = y;
		m.m23 = 0;
		m.m24 = 0;

		// Col 3
		m.m31 = 0;
		m.m32 = 0;
		m.m33 = _far / (_far - _near);
		m.m34 = (_near * _far) / (_near - _far);

		// Col 4
		m.m41 = 0;
		m.m42 = 0;
		m.m43 = 1;
		m.m44 = 0;
	}

	void mat4::clip(mat4 &m, const vec4 &plane) {
		// Oblicz wsp�czynniki
		f32 z = ((vec3&)m.m31).length();
		f32 a = -m.m34 / z;

		// Ustaw przedni� p�aszczyzn�
		m.m31 = plane.X * z;
		m.m32 = plane.Y * z;
		m.m33 = plane.Z * z;
		m.m34 = (plane.W - a) * z;
	}

	void mat4::ortho(mat4 &m, const rect &bounds, f32 _near, f32 _far)
	{
		// Col 1
		m.m11 = 2 / (bounds.Right - bounds.Left);
		m.m12 = 0;
		m.m13 = 0;
		m.m14 = (bounds.Left + bounds.Right) / (bounds.Left - bounds.Right);

		// Col 2
		m.m21 = 0;
		m.m22 = 2 / (bounds.Top - bounds.Bottom);
		m.m23 = 0;
		m.m24 = (bounds.Top + bounds.Bottom) / (bounds.Bottom - bounds.Top);

		// Col 3
		m.m31 = 0;
		m.m32 = 0;
		m.m33 = 1 / (_far - _near);
		m.m34 = _near / (_near - _far);

		// Col 4
		m.m41 = 0;
		m.m42 = 0;
		m.m43 = 0;
		m.m44 = 1;
	}
	
	void mat4::reflection(mat4 &m, const vec4 &plane)
	{
		// Row 1
		m.m11 = -2 * plane.X * plane.X + 1;
		m.m21 = -2 * plane.Y * plane.X;
		m.m31 = -2 * plane.Z * plane.X;
		m.m41 = 0;

		// Row 2
		m.m12 = -2 * plane.X * plane.Y;
		m.m22 = -2 * plane.Y * plane.Y + 1;
		m.m32 = -2 * plane.Z * plane.Y;
		m.m42 = 0;

		// Row 3
		m.m13 = -2 * plane.X * plane.Z;
		m.m23 = -2 * plane.Y * plane.Z;
		m.m33 = -2 * plane.Z * plane.Z + 1;
		m.m43 = 0;

		// Row 4
		m.m14 = -2 * plane.X * plane.W;
		m.m24 = -2 * plane.Y * plane.W;
		m.m34 = -2 * plane.Z * plane.W;
		m.m44 = 1;
	}
	
	void mat4::multiply(mat4 &m, const mat4 &a, const mat4 &b)
	{
		// Sprawdz wskazniki
		if(&m == &a)
			return multiply(m, mat4(a), b);
		if(&m == &b)
			return multiply(m, a, mat4(b));

		// TODO: a = b, b = a ???

		// Row 1
		m.m11 = a.m11 * b.m11 + a.m12 * b.m21 + a.m13 * b.m31 + a.m14 * b.m41;
		m.m21 = a.m21 * b.m11 + a.m22 * b.m21 + a.m23 * b.m31 + a.m24 * b.m41;
		m.m31 = a.m31 * b.m11 + a.m32 * b.m21 + a.m33 * b.m31 + a.m34 * b.m41;
		m.m41 = a.m41 * b.m11 + a.m42 * b.m21 + a.m43 * b.m31 + a.m44 * b.m41;

		// Row 2
		m.m12 = a.m11 * b.m12 + a.m12 * b.m22 + a.m13 * b.m32 + a.m14 * b.m42;
		m.m22 = a.m21 * b.m12 + a.m22 * b.m22 + a.m23 * b.m32 + a.m24 * b.m42;
		m.m32 = a.m31 * b.m12 + a.m32 * b.m22 + a.m33 * b.m32 + a.m34 * b.m42;
		m.m42 = a.m41 * b.m12 + a.m42 * b.m22 + a.m43 * b.m32 + a.m44 * b.m42;

		// Row 3
		m.m13 = a.m11 * b.m13 + a.m12 * b.m23 + a.m13 * b.m33 + a.m14 * b.m43;
		m.m23 = a.m21 * b.m13 + a.m22 * b.m23 + a.m23 * b.m33 + a.m24 * b.m43;
		m.m33 = a.m31 * b.m13 + a.m32 * b.m23 + a.m33 * b.m33 + a.m34 * b.m43;
		m.m43 = a.m41 * b.m13 + a.m42 * b.m23 + a.m43 * b.m33 + a.m44 * b.m43;

		// Row 4
		m.m14 = a.m11 * b.m14 + a.m12 * b.m24 + a.m13 * b.m34 + a.m14 * b.m44;
		m.m24 = a.m21 * b.m14 + a.m22 * b.m24 + a.m23 * b.m34 + a.m24 * b.m44;
		m.m34 = a.m31 * b.m14 + a.m32 * b.m24 + a.m33 * b.m34 + a.m34 * b.m44;
		m.m44 = a.m41 * b.m14 + a.m42 * b.m24 + a.m43 * b.m34 + a.m44 * b.m44;
	}
	
	void mat4::firstPersonCamera(mat4 &m, const vec3 &o, const ayuine::angles &a, f32 z) {
		lookAt(m, o, o + a.at() * z, a.up());
	}
	
	void mat4::modelViewCamera(mat4 &m, const vec3 &o, const ayuine::angles &a, f32 z) {
		lookAt(m, o + a.at() * z, o, a.up());
	}
	
	void mat4::fromCols(mat4 &m, const vec4 &col1, const vec4 &col2, const vec4 &col3, const vec4 &col4)
	{
		m = mat4(
			col1.X, col2.X, col3.X, col4.X,
			col1.Y, col2.Y, col3.Y, col4.Y,
			col1.Z, col2.Z, col3.Z, col4.Z,
			col1.W, col2.W, col3.W, col4.W);
	}
	
	void mat4::fromRows(mat4 &m, const vec4 &row1, const vec4 &row2, const vec4 &row3, const vec4 &row4)
	{
		m = mat4(
			row1.X, row1.Y, row1.Z, row1.W,
			row2.X, row2.Y, row2.Z, row2.W,
			row3.X, row3.Y, row3.Z, row3.W,
			row4.X, row4.Y, row4.Z, row4.W);
	}
	
	void mat4::scaling(mat4 &m, const vec3 &scale) {
		// Row 1
		m.m11 = scale.X;
		m.m21 = 0;
		m.m31 = 0;
		m.m41 = 0;

		// Row 2
		m.m12 = 0;
		m.m22 = scale.Y;
		m.m32 = 0;
		m.m42 = 0;

		// Row 3
		m.m13 = 0;
		m.m23 = 0;
		m.m33 = scale.Z;
		m.m43 = 0;

		// Row 4
		m.m14 = 0;
		m.m24 = 0;
		m.m34 = 0;
		m.m44 = 1.0f;
	}
	
	void mat4::translation(mat4 &m, const vec3 &value) {
		// Row 1
		m.m11 = 1;
		m.m21 = 0;
		m.m31 = 0;
		m.m41 = 0;

		// Row 2
		m.m12 = 0;
		m.m22 = 1;
		m.m32 = 0;
		m.m42 = 0;

		// Row 3
		m.m13 = 0;
		m.m23 = 0;
		m.m33 = 1;
		m.m43 = 0;

		// Row 4
		m.m14 = value.X;
		m.m24 = value.Y;
		m.m34 = value.Z;
		m.m44 = 1;
	}
	
	void mat4::rotation(mat4 &m, const ayuine::angles &a) {
		vec3 tmp;

		// Col 1
		tmp = a.at();
		m.m11 = tmp.X;
		m.m12 = tmp.Y;
		m.m13 = tmp.Z;
		m.m14 = 0;

		// Col 2
		tmp = -a.right();
		m.m21 = tmp.X;
		m.m22 = tmp.Y;
		m.m23 = tmp.Z;
		m.m24 = 0;

		// Col 3
		tmp = a.up();
		m.m31 = tmp.X;
		m.m32 = tmp.Y;
		m.m33 = tmp.Z;
		m.m34 = 0;

		// Col 4
		m.m41 = 0;
		m.m42 = 0;
		m.m43 = 0;
		m.m44 = 1;
	}

	void mat4::rotation(mat4 &m, const quat &q) {
		f32 x2 = q.X * 2, y2 = q.Y * 2, z2 = q.Z * 2;
		f32 wx = x2 * q.W, wy = y2 * q.W, wz = z2 * q.W;
		f32 xx = x2 * q.X, xy = y2 * q.X, xz = z2 * q.X;
		f32 yy = y2 * q.Y, yz = z2 * q.Y, zz = z2 * q.Z;

		m.m11 = 1 - (yy + zz); m.m21 = xy + wz; m.m31 = xz - wy; m.m41 = 0;
		m.m12 = xy - wz; m.m22 = 1 - (xx + zz); m.m32 = yz + wx; m.m42 = 0;
		m.m13 = xz + wy; m.m23 = yz - wx; m.m33 = 1 - (xx + yy); m.m43 = 0;
		m.m14 = 0; m.m24 = 0; m.m34 = 0; m.m44 = 1;
	}

	void mat4::invert(mat4 &m, const mat4 &i) {
		m.m11 = (i.m22 * (i.m33 * i.m44 - i.m34 * i.m43) - i.m23 * (i.m32 * i.m44 - i.m34 * i.m42) + i.m24 * (i.m32 * i.m43 - i.m33 * i.m42));
		m.m21 = -(i.m21 * (i.m33 * i.m44 - i.m34 * i.m43) - i.m23 * (i.m31 * i.m44 - i.m34 * i.m41) + i.m24 * (i.m31 * i.m43 - i.m33 * i.m41));
		m.m31 = (i.m21 * (i.m32 * i.m44 - i.m34 * i.m42) - i.m22 * (i.m31 * i.m44 - i.m34 * i.m41) + i.m24 * (i.m31 * i.m42 - i.m32 * i.m41));
		m.m41 = -(i.m21 * (i.m32 * i.m43 - i.m33 * i.m42) - i.m22 * (i.m31 * i.m43 - i.m33 * i.m41) + i.m23 * (i.m31 * i.m42 - i.m32 * i.m41));

		m.m12 = -(i.m12 * (i.m33 * i.m44 - i.m34 * i.m43) - i.m13 * (i.m32 * i.m44 - i.m34 * i.m42) + i.m14 * (i.m32 * i.m43 - i.m33 * i.m42));
		m.m22 = (i.m11 * (i.m33 * i.m44 - i.m34 * i.m43) - i.m13 * (i.m31 * i.m44 - i.m34 * i.m41) + i.m14 * (i.m31 * i.m43 - i.m33 * i.m41));
		m.m32 = -(i.m11 * (i.m32 * i.m44 - i.m34 * i.m42) - i.m12 * (i.m31 * i.m44 - i.m34 * i.m41) + i.m14 * (i.m31 * i.m42 - i.m32 * i.m41));
		m.m42 = (i.m11 * (i.m32 * i.m43 - i.m33 * i.m42) - i.m12 * (i.m31 * i.m43 - i.m33 * i.m41) + i.m13 * (i.m31 * i.m42 - i.m32 * i.m41));

		m.m13 = (i.m12 * (i.m23 * i.m44 - i.m24 * i.m43) - i.m13 * (i.m22 * i.m44 - i.m24 * i.m42) + i.m14 * (i.m22 * i.m43 - i.m23 * i.m42));
		m.m23 = -(i.m11 * (i.m23 * i.m44 - i.m24 * i.m43) - i.m13 * (i.m21 * i.m44 - i.m24 * i.m41) + i.m14 * (i.m21 * i.m43 - i.m23 * i.m41));
		m.m33 = (i.m11 * (i.m22 * i.m44 - i.m24 * i.m42) - i.m12 * (i.m21 * i.m44 - i.m24 * i.m41) + i.m14 * (i.m21 * i.m42 - i.m22 * i.m41));
		m.m43 = -(i.m11 * (i.m22 * i.m43 - i.m23 * i.m42) - i.m12 * (i.m21 * i.m43 - i.m23 * i.m41) + i.m13 * (i.m21 * i.m42 - i.m22 * i.m41));

		m.m14 = -(i.m12 * (i.m23 * i.m34 - i.m24 * i.m33) - i.m13 * (i.m22 * i.m34 - i.m24 * i.m32) + i.m14 * (i.m22 * i.m33 - i.m23 * i.m32));
		m.m24 = (i.m11 * (i.m23 * i.m34 - i.m24 * i.m33) - i.m13 * (i.m21 * i.m34 - i.m24 * i.m31) + i.m14 * (i.m21 * i.m33 - i.m23 * i.m31));
		m.m34 = -(i.m11 * (i.m22 * i.m34 - i.m24 * i.m32) - i.m12 * (i.m21 * i.m34 - i.m24 * i.m31) + i.m14 * (i.m21 * i.m32 - i.m22 * i.m31));
		m.m44 = (i.m11 * (i.m22 * i.m33 - i.m23 * i.m32) - i.m12 * (i.m21 * i.m33 - i.m23 * i.m31) + i.m13 * (i.m21 * i.m32 - i.m22 * i.m31));

		f32 invDet = 1.0f / ((i.m11 * m.m11) + (i.m12 * m.m21) + (i.m13 * m.m31) + (i.m14 * m.m41));

		m.m11 *= invDet; m.m21 *= invDet; m.m31 *= invDet; m.m41 *= invDet;
		m.m12 *= invDet; m.m22 *= invDet; m.m32 *= invDet; m.m42 *= invDet;
		m.m13 *= invDet; m.m23 *= invDet; m.m33 *= invDet; m.m43 *= invDet;
		m.m14 *= invDet; m.m24 *= invDet; m.m34 *= invDet; m.m44 *= invDet;
	}
	
	void mat4::transpose(mat4 &m, const mat4 &i) {
		// Row 1
		m.m11 = i.m11;
		m.m21 = i.m12;
		m.m31 = i.m13;
		m.m41 = i.m14;

		// Row 2
		m.m12 = i.m21;
		m.m22 = i.m22;
		m.m32 = i.m23;
		m.m42 = i.m24;

		// Row 3
		m.m13 = i.m31;
		m.m23 = i.m32;
		m.m33 = i.m33;
		m.m43 = i.m34;

		// Row 4
		m.m14 = i.m41;
		m.m24 = i.m42;
		m.m34 = i.m43;
		m.m44 = i.m44;
	}
	
	void mat4::transpose(mat4 &m)	{
		f32 tmp;

		tmp = m.m12; m.m12 = m.m21; m.m21 = tmp;
		tmp = m.m13; m.m13 = m.m31; m.m31 = tmp;
		tmp = m.m14; m.m14 = m.m41; m.m41 = tmp;
		tmp = m.m23; m.m23 = m.m32; m.m32 = tmp;
		tmp = m.m24; m.m24 = m.m42; m.m42 = tmp;
		tmp = m.m34; m.m34 = m.m43; m.m43 = tmp;
	}

	//------------------------------------//
	// quat: Constructor

	quat::quat(f32 x, f32 y, f32 z) {
		X = x;
		Y = y;
		Z = z;
		f32 temp = 1.0f - ((vec3&)X).lengthSq();
		if(temp >= 0)
			W = -sqrt(temp);
		else
			W = 0.0f;
	}

	quat::quat(const vec3 &axis, f32 angle) {
		if(!axis.empty()) {
			((vec3&)X) = axis * (sin(angle / 2) / axis.length());
			W = cos(angle / 2);
		}
		else {
			X = 0; Y = 0; Z = 0; W = 1;
		}
	}
	
	quat::quat(const mat4 &m) {
		f32 trace = m.m11 + m.m22 + m.m33;

		if(trace > 0)
		{
			f32 scale = sqrt(trace + 1.0f);
			f32 invHalfScale = 0.5f / scale;

			X = invHalfScale * (m.m32 - m.m23);
			Y = invHalfScale * (m.m13 - m.m31);
			Z = invHalfScale * (m.m21 - m.m12);
			W = scale * 0.5f;
		}
		else
		{
			switch ((m.m22 > m.m11) ? (m.m33 > m.m22 ? 2 : 1) : (m.m33 > m.m11 ? 2 : 0))
			{
				case 0:
				{
					f32 scale = sqrt(m.m11 - m.m22 - m.m33 + 1);
					f32 invHalfScale = 0.5f / scale;

					X = 0.5f * scale;
					Y = invHalfScale * (m.m21 - m.m12);
					Z = invHalfScale * (m.m31 - m.m13);
					W = invHalfScale * (m.m32 - m.m23);
				}
				break;

				case 1:
				{
					f32 scale = sqrt(m.m22 - m.m33 - m.m11 + 1);
					f32 invHalfScale = 0.5f / scale;

					X = invHalfScale * (m.m12 - m.m21);
					Y = 0.5f * scale;
					Z = invHalfScale * (m.m32 - m.m23);
					W = invHalfScale * (m.m13 - m.m31);
				}
				break;

				case 2:
				default:
				{
					f32 scale = sqrt(m.m33 - m.m11 - m.m22 + 1);
					f32 invHalfScale = 0.5f / scale;

					X = invHalfScale * (m.m13 - m.m31);
					Y = invHalfScale * (m.m23 - m.m32);
					Z = 0.5f * scale;
					W = invHalfScale * (m.m21 - m.m12);
				}
				break;
			}
		}
	}
	
	quat::quat(const vec3 &at, const vec3 &up) {
		mat4 temp;

		mat4::lookAt(temp, vec3(0, 0, 0), at, up);
		this[0] = temp;
	}
	
	quat::quat(const angles &a) {
		mat4 temp;

		mat4::rotation(temp, a);
		this[0] = temp;
	}

	//------------------------------------//
	// quat: Methods

	quat quat::normalize() const {
		if(empty())
			return quat(1, 0, 0, 0);
		f32 inv = 1.0f / length();
		return quat(W * inv, X * inv, Y * inv, Z * inv);
	}

	quat quat::slerp(const quat &a, const quat &b, f32 t) {
		f32 c = clamp(a ^ b, -1.0f, 1.0f);

		if(c != 0) {
			f32 angle = cos(c);
			f32 s = sin(angle);
			f32 invs = 1.0f / s;
			f32 c1 = sin((1.0f - t) * angle) * invs;
			f32 c2 = sin(t * angle) * invs;
			return quat(c1 * a.W + c2 * b.W, c1 * a.X + c2 * b.X, c1 * a.Y + c2 * b.Y, c1 * a.Z + c2 * b.Z);
		}
		return quat();
	}

	//------------------------------------//
	// ray: Methods

	void ray::assign(const vec3 &origin, const vec3 &at) {
		Origin = origin;
		At = at;
	}

	ray ray::transform(const mat4 &m) const {
		return ray(m.transformCoord(Origin), m.transformNormal(At));
	}

	vec3 ray::hit(f32 time) const {
		return Origin + At * time;
	}

	//------------------------------------//
	// bsphere: Methods

	void bsphere::assign(const vec3 &center, f32 radius) {
		Center = center;
		Radius = radius;
	}

	void bsphere::assign(const vec3 *v, u32 count, u32 stride) {
		for(size_t i = count; i > 0; ) {
			Center += *(vec3*)(((u8*)v) + --i * stride);
		}
		Center /= (f32)count;
		Radius = 0;
		for(size_t i = count; i > 0; )
			Radius = max(Radius, (*(vec3*)(((u8*)v) + --i * stride) - Center).length());
	}

	void bsphere::assign(const Array<vec3> &verts) {
		if(verts.size()) {
			assign(&verts.at(0), verts.size());
		}
		else {
			Radius = 0;
		}
	}

	bsphere bsphere::transform(const mat4 &m) const {
		return bsphere(m.transformCoord(Center), m.transformNormal(Radius).length());
	}

	bsphere bsphere::scale(const vec3 &scale) const {
		return bsphere(Center, (scale * Radius).length());
	}

	bool bsphere::test(const vec3 &v) const {
		return (v - Center).lengthSq() <= sq(Radius);
	}

	bool bsphere::test(const bsphere &s) const {
		return (s.Center - Center).lengthSq() <= sq(Radius + s.Radius);
	}

	f32 bsphere::collide(const ray &ray) const {
		vec3 q = Center - ray.Origin;
		f32 c = q.length();
		f32 v = ray.At ^ q;
		f32 d = sq(Radius) - (sq(c) - sq(v));

		if (d < 0)
			return -1.0f;

		return v - sq(d);
	}

	//------------------------------------//
	// bbox: Static Fields

	const u32 bbox::Quads[6][4] = {
		{3, 6, 7, 5},
		{0, 2, 4, 1},
		{1, 4, 7, 6},
		{0, 3, 5, 2},
		{2, 5, 7, 4},
		{0, 1, 6, 3}
	};

	const u32 bbox::Edges[12][2] = {
		{0, 1}, {0, 2}, {0, 3},
		{1, 4}, {1, 6},
		{2, 4}, {2, 5},
		{3, 5}, {3, 6},
		{4, 7},
		{5, 7},
		{6, 7},
	};

	//------------------------------------//
	// bbox: Methods

	void bbox::assign(const vec3 &mins, const vec3 &maxs) {
		Mins = mins;
		Maxs = maxs;
	}

	void bbox::assign(const vec3 *v, u32 count, u32 stride) {
		if(v && count >= 1) {
			// Pierwszy wierzcho�ek zapisz jako granice bboxa
			Mins = Maxs = *v;

			// UNDONE(Stats): Dla kolejnych testuj
			for(u32 i = 1; i < count; i++) {
				vec3 &vv = (vec3&)*((u8*)v + i * stride);
				Mins = vec3::minimize(Mins, vv);
				Maxs = vec3::maximize(Maxs, vv);
			}
		}
		else {
			clear();
		}
	}

	void bbox::assign(const Array<vec3> &verts) {
		if(verts.size()) {
			assign(verts, verts.size());
		}
		else {
			clear();
		}
	}

	void bbox::add(const vec3 &v) {
		Mins = vec3::minimize(Mins, v);
		Maxs = vec3::maximize(Maxs, v);
	}

	void bbox::add(const vec3 *v, size_t count) {
		while(count-- > 0)
			add(*v++);
	}

	bbox bbox::intersect(const bbox &box) const {
		if(box.empty() || empty())
			return bbox();
		return bbox(vec3::maximize(Mins, box.Mins), vec3::minimize(Maxs, box.Maxs));
	}

	bbox bbox::merge(const bbox &box) const {
		if(box.empty())
			return *this;
		if(empty())
			return box;
		return bbox(vec3::minimize(Mins, box.Mins), vec3::maximize(Maxs, box.Maxs));
	}

	void bbox::clear() {
		Mins.X = Mins.Y = Mins.Z = +FLT_MAX;
		Maxs.X = Maxs.Y = Maxs.Z = -FLT_MAX;
	}

	void bbox::vertexes(vec3 vertexes[8]) const {
		vertexes[0] = vec3(Mins.X, Mins.Y, Mins.Z);
		vertexes[1] = vec3(Mins.X, Mins.Y, Maxs.Z);
		vertexes[2] = vec3(Mins.X, Maxs.Y, Mins.Z);
		vertexes[3] = vec3(Maxs.X, Mins.Y, Mins.Z);
		vertexes[4] = vec3(Mins.X, Maxs.Y, Maxs.Z);
		vertexes[5] = vec3(Maxs.X, Maxs.Y, Mins.Z);
		vertexes[6] = vec3(Maxs.X, Mins.Y, Maxs.Z);
		vertexes[7] = vec3(Maxs.X, Maxs.Y, Maxs.Z);
	}

	void bbox::planes(vec4 planes[6]) const {
		planes[0] = vec4( 1, 0, 0, -Maxs.X);
		planes[1] = vec4(-1, 0, 0,  Mins.X);
		planes[2] = vec4(0,  1, 0, -Maxs.Y);
		planes[3] = vec4(0, -1, 0,  Mins.Y);
		planes[4] = vec4(0, 0,  1, -Maxs.Z);
		planes[5] = vec4(0, 0, -1,  Mins.Z);
	}

	bbox bbox::transform(const mat4 &m) const {
		vec3 v[8];

		// Pobierz wierzcho�ki boxa
		vertexes(v);

		// Przetransformuj wierzcho�ki
		m.transformCoord(v, v, CountOf(v));

		// Utw�rz nowego boxa
		return bbox(v, CountOf(v));
	}
	bool bbox::test(const vec3 &v) const {
		return	Mins.X <= v.X && v.X <= Maxs.X &&
						Mins.Y <= v.Y && v.Y <= Maxs.Y &&
						Mins.Z <= v.Z && v.Z <= Maxs.Z;
	}

	bool bbox::test(const bbox &box) const {
		// TODO : Do sprawdzenia !
		return
			(Mins.X <= box.Mins.X && box.Mins.X <= Maxs.X || Mins.X <= box.Maxs.X && box.Maxs.X <= Maxs.X || box.Mins.X <= Mins.X && Mins.X <= box.Maxs.X || box.Mins.X <= Maxs.X && Maxs.X <= box.Maxs.X) &&
			(Mins.Y <= box.Mins.Y && box.Mins.Y <= Maxs.Y || Mins.Y <= box.Maxs.Y && box.Maxs.Y <= Maxs.Y || box.Mins.Y <= Mins.Y && Mins.Y <= box.Maxs.Y || box.Mins.Y <= Maxs.Y && Maxs.Y <= box.Maxs.Y) &&
			(Mins.Z <= box.Mins.Z && box.Mins.Z <= Maxs.Z || Mins.Z <= box.Maxs.Z && box.Maxs.Z <= Maxs.Z || box.Mins.Z <= Mins.Z && Mins.Z <= box.Maxs.Z || box.Mins.Z <= Maxs.Z && Maxs.Z <= box.Maxs.Z);
	}

	bool bbox::test(const bsphere &bsphere) const {
		return	Mins.X <= bsphere.Center.X + bsphere.Radius && bsphere.Center.X - bsphere.Radius <= Maxs.X &&
						Mins.Y <= bsphere.Center.Y + bsphere.Radius && bsphere.Center.Y - bsphere.Radius <= Maxs.Y &&
						Mins.Z <= bsphere.Center.Z + bsphere.Radius && bsphere.Center.Z - bsphere.Radius <= Maxs.Z;
	}

	f32 bbox::collide(const ray &ray) const {
		// TODO : Kolizja Ray->Box !
/*
		f32* vx = stackalloc f32[3];
		f32* maxT = stackalloc f32[3];
		s32* vxp = stackalloc s32[3];

		vx[0] = vx[1] = vx[2] = 0;
		maxT[0] = maxT[1] = maxT[2] = 0;
		vxp[0] = vxp[1] = vxp[2] = 0;

		for (s32 i = 0; i < 3; i++)
		{
			if (ray.Origin[i] <= mins[i])
			{
				vx[i] = mins[i];
				vxp[i] = -1;
			}
			else if (maxs[i] <= ray.Origin[i])
			{
				vx[i] = maxs[i];
				vxp[i] = -1;
			}
			else
				vx[i] = 0;
		}

		if (vxp[0] == 0 && vxp[1] == 0 && vxp[2] == 0)
		{
			return 0; // Mean's inside
		}

		for (s32 i = 0; i < 3; i++)
		{
			if (vxp[i] != 0 && MathLib.Abs(ray.Direction[i]) > MathLib.Ep)
			{
				maxT[i] = (vx[i] - ray.Origin[i]) / ray.Direction[i];
				vxp[i] = 1;
			}
			else
			{
				maxT[i] = 0;
				vxp[i] = 0;
			}
		}

		s32 whichPlane = 0;

		if (vxp[1] != 0 && (vxp[whichPlane] == 0 || MathLib.Abs(maxT[whichPlane]) < MathLib.Abs(maxT[1])))
			whichPlane = 1;
		if (vxp[2] != 0 && (vxp[whichPlane] == 0 || MathLib.Abs(maxT[whichPlane]) < MathLib.Abs(maxT[2])))
			whichPlane = 2;

		if (maxT[whichPlane] < 0)
			return -1;

		for (s32 i = 0; i < 3; i++)
		{
			if (whichPlane == i)
				continue;

			vx[i] = ray.Origin.X + maxT[whichPlane] * ray.Direction.X;
			if (vx[i] < mins[i] || maxs[i] < vx[i])
				return -1;
		}
		return maxT[whichPlane];*/
		return -1;
	}

	//------------------------------------//
	// frustum: Constructor

	frustum::frustum() {
	}

	frustum::frustum(const mat4 &object) {
		// Zapisz i oblicz macierz odwrotn�
		mat4::invert(_invObject, _object = object);

		// Oblicz p�aszczyzny widoku
		_planes[0] = vec4(_object.m41 - _object.m11, _object.m42 - _object.m12, _object.m43 - _object.m13, _object.m44 - _object.m14).normalizeNormal();
		_planes[1] = vec4(_object.m41 + _object.m11, _object.m42 + _object.m12, _object.m43 + _object.m13, _object.m44 + _object.m14).normalizeNormal();
		_planes[2] = vec4(_object.m41 - _object.m21, _object.m42 - _object.m22, _object.m43 - _object.m23, _object.m44 - _object.m24).normalizeNormal();
		_planes[3] = vec4(_object.m41 + _object.m21, _object.m42 + _object.m22, _object.m43 + _object.m23, _object.m44 + _object.m24).normalizeNormal();
		_planes[4] = vec4(_object.m41 - _object.m31, _object.m42 - _object.m32, _object.m43 - _object.m33, _object.m44 - _object.m34).normalizeNormal();
		_planes[5] = vec4(_object.m41 + _object.m31, _object.m42 + _object.m32, _object.m43 + _object.m33, _object.m44 + _object.m34).normalizeNormal();
	}

	frustum::frustum(const mat4 &projection, const mat4 &view) {
		mat4 transform;
	
		// Oblicz transformacje
		mat4::multiply(transform, projection, view);

		// Utw�rz transformacje
		*this = frustum(transform);
	}

	//------------------------------------//
	// frustum: Methods

	bool frustum::test(const vec3 &point) const {
		for(u32 i = 0; i < fpCount; i++) {
			if((_planes[i] ^ point) < -MathErr)
				return false;
		}
		return true;
	}

	bool frustum::test(const vec4 &plane) const {
		// TODO !
		return true;
	}

	bool frustum::test(const vec3 *verts, u32 count, u32 stride) const {
		u32 map = 0;

		// Sprawd� ka�dy wierzcho�ek
		for(u32 i = 0; i < count; i++, next(verts, stride)) {
			for(u32 j = 0; j < fpCount; j++) {
				if((_planes[j] ^ *verts) < -MathErr) {
					map |= 1 << j;
					goto next;
				}
			}
			return true;
next:;
		}
		if((map & 3) == 3 || (map & 12) == 12 || (map & 48) == 48)
			return true;
		return false;
	}

	bool frustum::test(const Array<vec3> &verts) const {
		if(verts.empty())
			return false;
		return test(verts, verts.size(), sizeof(vec3));
	}

	bool frustum::test(const bbox &box) const {
		vec3 verts[8];
		u32 map = 0;

		// Je�li bbox jest pusty to nic nie r�b
		if(box.empty())
			return false;

		// Pobierz wszystkie wierzcho�ki bboxa
		box.vertexes(verts);
		return test(verts, CountOf(verts), sizeof(vec3));
	}

	bool frustum::test(const bsphere &sphere) const {
		for(u32 j = 0; j < fpCount; j++) {
			if((_planes[j] ^ sphere.Center) < -(MathErr + sphere.Radius))
				return false;
		}
		return true;
	}

	vec3 frustum::screen(const vec3 &point) const {
		return _object.transformCoord(point);
	}

	vec3 frustum::world(const vec3 &point) const {
		return _invObject.transformCoord(point);
	}

	rect frustum::screenRect(const bbox &box) const {
		vec3 verts[8];
	
		// Wyznacz wierzcho�ki bboxa
		box.vertexes(verts);

		// Przetransformuj wierzcho�ki
		for(u32 i = 0; i < CountOf(verts); i++) {
			vec4 v = _object.transform(verts[i]);

			// Sprawd� W, je�li < 0 tzn. �e wierzcho�ek znajduje si� z ty�u frustuma
			if(v.W < MathErr) {
				verts[i].X = (v.X >= 0 ? 1.0f : -1.0f);
				verts[i].Y = (v.Y >= 0 ? 1.0f : -1.0f);
				verts[i].Z = 0.0f;
			}
			else {
				(vec2&)verts[i] = (vec2&)v / v.W;
			}
		}

		// Zapisz warto�� domy�ln� dla min i max
		vec2 min = (vec2&)verts[0];
		vec2 max = (vec2&)verts[0];

		// Por�wnaj reszt� warto�ci
		for(u32 i = 1; i < CountOf(verts); i++) {
			min = vec2::minimize(min, (vec2&)verts[i]);
			max = vec2::maximize(max, (vec2&)verts[i]);
		}
		return rect(min, max);
	}

	//------------------------------------//
	// poly functions

	vec4 poly::plane(const polyVerts &poly) {
		return vec4(poly.at(0), poly.at(1), poly.at(2));
	}

	polyVerts poly::build(const polyVerts &points) {
		throw(std::runtime_error("not yet supported"));
	}

	polyVerts poly::portal(const vec4 &plane, float size) {
		vec3 origin, right, up;
		vec3 verts[4];

		// Pobierz wektory normalne
		plane.normal().normalVectors(right, up);

		// Oblicz wektory
		origin = plane.normal() * (-plane.W);
		right *= abs(size);
		up *= abs(size);

		// Oblicz wierzcho�ki
		if(size > 0) {
			verts[0] = origin - right - up;
			verts[1] = origin + right - up;
			verts[2] = origin + right + up;
			verts[3] = origin - right + up;
		}
		else {
			verts[0] = origin - right + up;
			verts[1] = origin + right + up;
			verts[2] = origin + right - up;
			verts[3] = origin - right - up;
		}

		return polyVerts(verts, 3);
	}

	Side poly::split(const polyVerts &poly, const vec4 &plane, polyVerts *front, polyVerts *back) {
		float *dists = nullptr, *distsIter;
		u8 *sides = nullptr, *sidesIter;
		const vec3 *polyIter;
		int side[3] = {0, 0, 0};

		// Sprawd� argumenty
		Assert(poly.size() >= 3);

		// Zaalokuj dane
		distsIter = dists = (float*)_malloca(sizeof(float) * (poly.size() + 1));
		sidesIter = sides = (u8*)_malloca(sizeof(u8) * (poly.size() + 1));
		polyIter = &poly[0];

		// Sprawd�, ka�dy wierzcho�ek i zapisz informacje o nim
		for(int i = (int)poly.size() - 1; i >= 0; i--) {
			side[*sidesIter++ = plane.test(*polyIter++, distsIter++)]++;
		}

		// Wszystkie wierzcho�ki z przodu (czyli zwracamy przedni� cz��)
		if(side[sFront] != 0 && side[sBack] == 0) {
			if(front)
				*front = poly;
			if(back)
				back->clear();
			return sFront;
		}

		// Wszystkie wierzcho�ki z ty�u (czyli zwracamy tylni� cz��)
		if(side[sBack] != 0 && side[sFront] == 0) {
			if(back)
				*back = poly;
			if(front)
				front->clear();
			return sBack;
		}

		// Wszystkie wierzcho�ki na p�aszczyznie
		if(side[sFront] == 0 && side[sBack] == 0) {
			if(front)
				front->clear();
			if(back)
				back->clear();
			return sFront;
		}

		// Wierzcho�ki po obu stronach p�aszczyzny
		vec3 *frontTemp = front != nullptr ? (vec3*)_malloca(sizeof(vec3) * (poly.size() + 4)) : nullptr;
		vec3 *frontIter = frontTemp;
		vec3 *backTemp = back != nullptr ? (vec3*)_malloca(sizeof(vec3) * (poly.size() + 4)) : nullptr;
		vec3 *backIter = backTemp;

		// Popraw dane
		sides[poly.size()] = sides[0];
		dists[poly.size()] = dists[0];
		sidesIter = sides;
		distsIter = dists;
		polyIter = &poly[0];

		// Dodaj wierzcho�ki do odpowiednich nowych polign�w
		for(int i = (int)poly.size() - 1; i >= 0; i--, sidesIter++, distsIter++, polyIter++) {
			// Prz�d
			if((*sidesIter == sFront || *sidesIter == sOn) && frontIter != nullptr)
				*frontIter++ = *polyIter;

			// Ty�
			if((*sidesIter == sBack || *sidesIter == sOn) && backIter != nullptr)
				*backIter++ = *polyIter;

			// Na
			if(sidesIter[0] == sidesIter[1] || sidesIter[1] == sOn)
				continue;

			// Przecina
			vec3 temp = vec3::lerp(polyIter[0], i > 0 ? polyIter[1] : poly[0], distsIter[0] / (distsIter[0] - distsIter[1]));
			
			// Dodaj do przodu i do ty�u
			if(frontIter != nullptr)
				*frontIter++ = temp;
			if(backIter != nullptr)
				*backIter++ = temp;
		}
		// Zapisz podzielone poligony
		if(front)
			*front = polyVerts(frontTemp, frontIter - frontTemp);
		if(back)
			*back = polyVerts(backTemp, backIter - backTemp);
		return sSplit;
	}

	polyVerts poly::add(u32 count, ...) {
		throw(std::runtime_error("not yet supported"));
	}

	polyVerts poly::merge(u32 count, ...) {
		throw(std::runtime_error("not yet supported"));
	}

	void poly::removeCollinear(polyVerts &poly) {
/*
			if (poly.Length < 3)
				throw new InvalidpolyException("Not enough poly vertices");

			Vector3 prev = (poly[poly.Length - 1] - poly[0]).Normalize;
			int count = 0;
			int* indices = stackalloc int[poly.Length];
			float epsilon = 1.0f - MathLib.Ep;

			for(int i = 0; i < poly.Length; i++)
			{
				Vector3 curr = (poly[i == poly.Length - 1 ? 0 : i + 1] - poly[i]);
				float length = curr.Length;

				// Sprawdz odleglosc miedzy wierzcholkami
				if(length < MathLib.Ep)
					continue;

				// Znormalizuj wektor
				curr /= length;

				// Wierzcholek lezy na tej samej prostej
				if(MathLib.Abs(curr ^ prev) > epsilon)
					continue;

				indices[count++] = i;
				prev = curr;
			}

			if(count < 3)
				return null;
			if(count == poly.Length)
				return poly;

			Vector3[] newpoly = new Vector3[count];

			for(int i = count - 1; i >= 0; i--)
				newpoly[i] = poly[indices[i]];

			return newpoly;
*/
		throw(std::runtime_error("not yet supported"));
	}

	f32 poly::area(const polyVerts &poly) {
		// Sprawd� argumenty
		Assert(poly.size() >= 3);

		f32 area = 0;

		for(int i = (int)poly.size() - 1; i >= 2; i--) {
			float a = (poly[0] - poly[i - 1]).length();
			float b = (poly[0] - poly[i - 0]).length();
			float c = (poly[i] - poly[i - 1]).length();
			float p = (a + b + c) / 2.0f;
			
			area += sqrt(p * (p - a) * (p - b) * (p - c));
		}
		return area;
	}

	bool poly::isTiny(const polyVerts &poly, float size) {
		// Sprawd� argumenty
		Assert(poly.size() >= 3);
		
		const vec3*	curr = &poly[0];
		vec3	prev = poly[poly.size() - 1];

		// Kwadrat
		size *= size;

		// Sprawd� ka�d� kraw�d�
		for(int i = (int)poly.size() - 1; i >= 0; i--, prev = *curr++) {
			if((*curr - prev).lengthSq() < size)
				return true;
		}
		return false;
	}

	bool poly::isHuge(const ayuine::polyVerts &poly, float size) {
		// Sprawd� argumenty
		Assert(poly.size() >= 3);
		
		const vec3*	curr = &poly[0];
		vec3	prev = poly[poly.size() - 1];

		// Kwadrat
		size *= size;

		// Sprawd� ka�d� kraw�d�
		for(int i = (int)poly.size() - 1; i >= 0; i--, prev = *curr++) {
			if((*curr - prev).lengthSq() > size)
				return true;
		}
		return false;	
	}

	bool poly::isPlanar(const polyVerts &poly) {
		// Sprawd� argumenty
		Assert(poly.size() >= 3);
		
		const vec3*	curr = &poly[0];
		vec4	plane = poly::plane(poly);

		// Sprawd� ka�d� kraw�d�
		for(int i = (int)poly.size() - 1; i >= 0; i--, curr++) {
			if(abs(plane ^ *curr) > MathErr)
				return false;
		}
		return true;	
	}

	bool poly::isConvex(const polyVerts &poly) {
		// Sprawd� argumenty
		Assert(poly.size() >= 3);
		
		const vec3*	curr = &poly[0];
		vec3	prev = poly[poly.size() - 1];
		vec4	plane = poly::plane(poly);

		// Sprawd� ka�d� kraw�d�
		for(int i = (int)poly.size() - 1; i >= 0; i--, prev = *curr++) {
			vec4	edge(plane, prev, *curr);
			const vec3*	curr2 = &poly[i];

			// Sprawd� wszystkie wierzcho�ki
			for(int j = i - 1; j >= 0; j--, curr2--) {
				if((edge ^ *curr2--) > MathErr)
					return false;
			}
		}
		return true;
	}

	polyEdges poly::edges(const polyVerts &poly) {
		// Sprawd� argumenty
		Assert(poly.size() >= 3);

		const vec3*	curr = &poly[0];
		vec3	prev = poly[poly.size() - 1];
		vec4	plane = poly::plane(poly);
		vec4*	edgesTemp = (vec4*)_malloca(sizeof(vec4) * poly.size());
		vec4*	edgesItor = edgesTemp;

		// Wyznacz ka�d� kraw�d�
		for(int i = (int)poly.size() - 1; i >= 0; i--, prev = *curr++) {
			*edgesItor++ = vec4(plane, prev, *curr);
		}
		return polyEdges(edgesTemp, edgesItor - edgesTemp);
	}

	Side poly::test(const polyVerts &verts, const vec4 &plane) {
		// Sprawd� argumenty
		Assert(verts.size() >= 3);

		const vec3*	curr = &verts[0];
		u32 	side = sOn;

		// Sprawd� ka�dy wierzcho�ek
		for(int i = (int)verts.size() - 1; i >= 0; i--) 
			side |= plane.test(*curr++);
		return (Side)side;
	}

	bool poly::test(const polyVerts &verts, const vec3 &point) {
		// Sprawd� argumenty
		Assert(verts.size() >= 3);

		const vec3*	curr = &verts[0];
		vec3	prev = verts[verts.size() - 1];
		vec4	plane = poly::plane(verts);

		// Sprawd� ka�d� kraw�d�
		for(int i = (int)verts.size() - 1; i >= 0; i--, prev = *curr++) {
			if((vec4(plane, prev, *curr) ^ point) > MathErr)
				return false;
		}
		return true;
	}

	bool poly::test(const polyVerts &a, const polyVerts &b) {
		throw(std::runtime_error("not yet supported"));
	}

	//------------------------------------//
	// SphericalHarmonics: Static Fields

	Array<SphericalHarmonics::Sample>			SphericalHarmonics::_samples;

	//------------------------------------//
	// SphericalHarmonics: Functions

	static f64 factorial(s32 x) {
		static f64 F[33] = {0.0};

		// Sprawd� czy mamy tyle warto�ci
		if(x < 0 || x >= CountOf(F))
			return 0;

		// Oblicz warto�ci
		if(!F[0]) {
			F[0] = 1;
			for(u32 i = 1; i < CountOf(F); i++) 
				F[i] = F[i - 1] * i;
		}

		// Zwr�� warto��
		return F[x];
	}

	static f64 P(s32 l, s32 m, f64 x) {
		f64 pmm = 1.0;
		if (m > 0) {
			f64 h = ::sqrt((1.0-x)*(1.0+x)),
						 f = 1.0;
			for (s32 i = 1; i <= m; i++) {
				pmm *= -f * h;
				f += 2.0; 
			}
		}
		if (l == m)
			return pmm;
		else {
			f64 pmmp1 = x * (2 * m + 1) * pmm;
			if (l == (m+1))
				return pmmp1;
			else {
				f64 pll = 0.0;
				for (s32 ll = m+2; ll <= l; ll++) {
					pll = (x * (2 * ll - 1) * pmmp1 - (ll + m - 1) * pmm) / (ll - m);
					pmm = pmmp1;
					pmmp1 = pll;
				}
				return pll;
			}
		}
	}

	static f64 K(s32 l, s32 m) {
		return ::sqrt((2 * l + 1) / (Pi * 4) * factorial(l - m) / factorial(l + m));
	}

	static f64 sphericalHarmonicY(s32 l, s32 m, f64 theta, f64 phi) {
		f64 a;
		s32 sm;

		// Warto�� absolutna
		sm = abs(m);

		// Wyznacz k�t
		if(m > 0)
			a = ::cos(sm * phi) * ::sqrt(2.0);
		else if(m < 0)
			a = ::sin(sm * phi) * ::sqrt(2.0);
		else
			a = 1.0f;

		// Wynik
		return P(l, sm, ::cos(theta)) * K(l, sm) * a; 
	}

	vec3 SphericalHarmonics::fromSphere(const vec2 &v) {
		f32 stheta, ctheta;
		f32 sphi, cphi;
		
		// Oblicz k�ty
		stheta = sinf(v.X);
		ctheta = cosf(v.X);
		sphi = sinf(v.Y);
		cphi = cosf(v.Y);

		// Zwr�� wektor
		return vec3(cphi * stheta, sphi * stheta, ctheta);
	}
	
	vec2 SphericalHarmonics::toSphere(const vec3 &v) {
		// Oblicz k�ty
		return vec2(acosf(v.Z), atan2f(v.Y, v.X));
	}

	void SphericalHarmonics::computeSamples(u32 quality) {
		f64 coeffs[16] = {0};

		// Utw�rz sample
		_samples.resize(quality * quality);

		// Oblicz skal�
		f64 invQuality = 1.0 / quality;
		f64 invQuality2 = 2.0 * invQuality;

		vec3 v;

		// Zainiciuj losowanie
		srand(time(nullptr));

		// Wype�nij �rodowisko
		for(u32 i = 0; i < quality; i++) {
			for(u32 j = 0; j < quality; j++) {
				// Oblicz parametry
				f64 r1 = 0;//(f64)rand() / (f64)RAND_MAX;
				f64 r2 = 0;//(f64)rand() / (f64)RAND_MAX;
				f64 theta = 2.0 * ::acos(::sqrt(1 - (j + r2) * invQuality));		// polar: 0 to PI
				f64 phi = 2.0 * Pi * (i + r1) * invQuality;											// azimuth: 0 to 2PI
				
				// Pobierz sampla
				Sample &sample = _samples.at(i * quality + j);

				// Skonfiguruj sampla
				sample.Normal = fromSphere(vec2(theta, phi));
				sample.Sphere = toSphere(sample.Normal);
				sample.Normal = sample.Normal.normalize();

				// Oblicz wsp�czynniki
				for(s32 l = 0; l < 4; l++) {
					for(s32 m = -l; m <= l; m++) {
						sample.Coeffs[l * (l + 1) + m] = sphericalHarmonicY(l, m, theta, phi);
					}
				}
				v += sample.Normal;
			}
		}

		//for(u32 i = 0; i < quality; i++) {
		//	for(u32 j = 0; j < quality; j += 2) {
		//		// Oblicz parametry
		//		f64 r1 = 0.5f;//(f64)rand() / (f64)RAND_MAX;
		//		f64 r2 = 0.5f;//(f64)rand() / (f64)RAND_MAX;
		//		f64 theta = Pi * (j + r2) * invQuality;							// polar: 0 to PI
		//		f64 phi = 2.0 * Pi * (i + r1) * invQuality;					// azimuth: 0 to 2PI
		//		
		//		// Pobierz sampla
		//		Sample &sample = _samples.at(i * (quality / 2) + j / 2);

		//		// Skonfiguruj sampla
		//		sample.Normal = fromSphere(vec2(theta, phi));
		//		sample.Sphere = toSphere(sample.Normal);
		//		sample.Normal = sample.Normal.normalize();

		//		// Oblicz wsp�czynniki
		//		for(s32 l = 0; l < 4; l++) {
		//			for(s32 m = -l; m <= l; m++) {
		//				coeffs[l * (l + 1) + m] += sample.Coeffs[l * (l + 1) + m] = sphericalHarmonicY(l, m, theta, phi);
		//			}
		//		}
		//		v += sample.Normal;
		//	}
		//}
	}

	void SphericalHarmonics::directionalLight(const vec3 &dir, f32 intensity, vec4 out[4]) {
		f64 coeffs[16];

		// Wyzeruj wsp�czynniki
		memset(coeffs, 0, sizeof(coeffs));
		vec2 s = toSphere(dir);

		for(s32 l = 0; l < 4; l++) {
			for(s32 m = -l; m <= l; m++) {
				(&out[0].X)[l * (l + 1) + m] = intensity * sphericalHarmonicY(l, m, s.X, s.Y);
			}
		}

		// Oblicz ka�dy sampel
		//foreach(vector<Sample>, i, _samples) {
		//	f64 scalar = intensity * clamp(dir ^ i->Normal);

		//	// Dodaj wsp�czynniki
		//	for(u32 j = 0; j < 16; j++)
		//		coeffs[j] += scalar * i->Coeffs[j];
		//}

		//// Znormalizuj wsp�czynniki
		//f64 scalar = 2 * Pi / (f64)_samples.size();
		//for(u32 j = 0; j < 16; j++)
		//	(&out[0].X)[j] = coeffs[j] * scalar;
	}
	
	void SphericalHarmonics::sphericalLight(const vec3 &origin, f32 radius, f32 intensity, vec4 out[4]) {
		vec3 normal;
		f32 length;
		f64 coeffs[16];

		// Wyzeruj wsp�czynniki
		memset(coeffs, 0, sizeof(coeffs));

		// Znormalizuj pozycje
		normal = origin.normalize(length);

		// Sprawd� czy �wiat�o znajduje si� w zasi�gu
		if(length > radius)
			return;

		// Oblicz sta��
		f64 constant = (intensity * 1) / (length * ::sqrt(2.0 * Pi));

		// Oblicz ka�dy sampel
		ArrayEach(Sample, i, _samples) {
			f64 h = i->Normal ^ normal;
			f64 x = ::acos(h);
			f64 scalar = constant * ::exp(-(x * x * x) / (2 * length * length)) * h;

			// Dodaj wsp�czynniki
			for(u32 j = 0; j < 16; j++)
				coeffs[j] += scalar * i->Coeffs[j];
		}

		// Znormalizuj wsp�czynniki
		f64 scalar = 4.0 * Pi / (f64)_samples.size();
		for(u32 j = 0; j < 16; j++)
			(&out[0].X)[j] = coeffs[j] * scalar;
	}
	
	void SphericalHarmonics::coneLight(const vec3 &origin, const vec3 &dir, f32 radius, f32 intensity, vec4 out[4]) {
		f64 coeffs[16];
		
		// Wyzeruj wsp�czynniki
		memset(coeffs, 0, sizeof(coeffs));

		// Znormalizuj pozycje
		f32 length = origin.length();

		// Sprawd� czy �wiat�o znajduje si� w zasi�gu
		if(length > radius)
			return;

		// Oblicz sta��
		f32 constant = (intensity * 1) / (length * sqrt(2.0f * Pi));

		// Oblicz ka�dy sampel
		ArrayEach(Sample, i, _samples) {
			f64 h = i->Normal ^ dir;

			if(h > 0.0f) { 
				f64 x = ::acos(h);
				f64 scalar = constant * ::exp(-(x * x * x) / (2 * length * length)) * h;

				// Dodaj wsp�czynniki
				for(u32 j = 0; j < 4; j++)
					coeffs[j] += scalar * i->Coeffs[j];
			}
		}

		// Znormalizuj wsp�czynniki
		f64 scalar = 4.0 * Pi / (f64)_samples.size();
		for(u32 j = 0; j < 16; j++)
			(&out[0].X)[j] = coeffs[j] * scalar;
	}

	static f64 clearSkyLight(const vec2 &sample, const vec2 &sun, f32 zenithIntensity) {
		if(sample.X > Pi / 2.0f || ::abs(::cos(sample.X)) < 0.01f)
			return zenithIntensity * (1.0 + 2.0 * ::cos(sample.X)) / 3.0;

		f64 Az = ::abs(sample.Y - sun.Y);
		f64 xi = ::acos(cos(sun.X) * ::cos(sample.X) + ::sin(sun.X) * ::sin(sample.X) * ::cos(Az));
		f64 top = 1.0 - ::exp(-0.32 / ::cos(sample.X)) * (0.91 + 10 * ::exp(-3 * xi) + 0.45 * ::pow(::cos(xi), 2));
		f64 bottom = (0.91 + 10 * ::exp(-3 * sun.X) + 0.45 * ::pow(::cos(sun.X), 2)) * 0.274;
		return zenithIntensity * top / bottom;
	}
	
	void SphericalHarmonics::clearSkyLight(const vec2 &sun, f32 zenithIntensity, vec4 out[4]) {
		f64 coeffs[16];

		// Wyzeruj wsp�czynniki
		memset(coeffs, 0, sizeof(coeffs));

		// Oblicz ka�dy sampel
		ArrayEach(Sample, i, _samples) {
			f64 scalar = ayuine::clearSkyLight(i->Sphere, sun, zenithIntensity);

			// Dodaj wsp�czynniki
			for(u32 j = 0; j < 4; j++)
				coeffs[j] += scalar * i->Coeffs[j];
		}

		// Znormalizuj wsp�czynniki
		f64 scalar = 4.0 * Pi / (f64)_samples.size();
		for(u32 j = 0; j < 16; j++)
			(&out[0].X)[j] = coeffs[j] * scalar;
	}
	
	static f64 overcastLight(const vec2 &sample, f32 zenithIntensity) {
		return zenithIntensity * (1.0 + 2.0 * ::cos(sample.Y)) / 3.0;
	}
	
	void SphericalHarmonics::overcastLight(f32 zenithIntensity, vec4 out[4]) {
		f64 coeffs[16];

		// Wyzeruj wsp�czynniki
		memset(coeffs, 0, sizeof(coeffs));

		// Oblicz ka�dy sampel
		ArrayEach(Sample, i, _samples) {
			f64 scalar = ayuine::overcastLight(i->Sphere, zenithIntensity);

			// Dodaj wsp�czynniki
			for(u32 j = 0; j < 4; j++)
				coeffs[j] += scalar * i->Coeffs[j];
		}

		// Znormalizuj wsp�czynniki
		f64 scalar = 4.0f * Pi / (f64)_samples.size();
		for(u32 j = 0; j < 16; j++)
			(&out[0].X)[j] = coeffs[j] * scalar;
	}
};
