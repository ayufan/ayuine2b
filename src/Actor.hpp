//------------------------------------//
//
// Actor.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// ChangedFlags enumeration

	enum ChangedFlags
	{
		cfNone		= 0,
		cfArea		= 1,
		cfForce		= 2
	};

	//------------------------------------//
	// ActorFlags enumeration

	enum ActorFlags
	{
		afNone						= 0,
		afCastShadows			= 1,
		afReceiveShadows	= 2,
		afInvalidate			= 4,
		afInvalidated			= 8,
		afUpdate					= 16,
		afPrepare					= 32,
		afZones						= 64,
		afArea						= 128,
		afTransform				= 256,
		afChilds					= 512,
	};

	//------------------------------------//
	// Actor class

	class Actor : public Object
	{
		ObjectType(Actor, Object, 0);

		// Fields
	public:
		vec3							Origin;
		angles						Angles;
		bool							Local;
		bool							Destroyable;
		Actor*						Parent;
		Actor*						Childs;
		Actor*						Next;
		Actor*						NextChanged;
		Actor*						NextUpdate;
		Actor*						NextPrepare;
		Actor*						NextInvalidate;
		Actor*						NextVisible;
		u32								Flags;

		// Transform Fields
	public:
		bbox							Box;
		mat4							Transform, InvTransform;
		vec3							WorldOrigin;
		angles						WorldAngles;

		// Physics Fields
	public:
		SmartPtr<RigidBody>	Body;

		// Visibility Fields
	public:
		u32								Visible;
		rect							VisibleRect;
		u32								Changed;

		// Bsp Fields
	protected:
		u32								_frame;
		Array<u32>				_zones, _oldZones;
		vec3							_oldOrigin;
		angles						_oldAngles;
		bbox							_oldBox;

		// Constructor
	public:
		AYUAPI Actor();

		// Destructor
	public:
		AYUAPI virtual ~Actor();

		// Methods
	public:
		AYUAPI virtual void invalidate(const bbox &box, Actor *owner);
		AYUAPI virtual void preupdate(f32 dt);
		AYUAPI virtual void postupdate(f32 dt);
		AYUAPI virtual void prepare(RenderFrame &frame);

		// Hierarchy methods
	public:
		AYUAPI virtual bool	link(Actor *newParent);
		AYUAPI virtual bool unlink();

		// Friends
		friend class World;
	};
};

#include "AMesh.hpp"
#include "ASkyDome.hpp"
#include "ALight.hpp"
#include "AEmitter.hpp"
#include "World.hpp"
