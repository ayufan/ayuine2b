//------------------------------------//
//
// Reader.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-25
//
//------------------------------------//

#include "StdAfx.hpp"

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace ayuine
{
	//------------------------------------//
	// Useful Structures

	struct WhitespaceIf
	{
		// Fields
		bool _crossline;

		// Constructor
		WhitespaceIf(bool crossline) {
			_crossline = crossline;
		}

		// Operator
		bool operator () (c8 value) {
			if(value > 32)
				return true;

			if(value == '\n' && !_crossline)
				return true;
			return false;
		}
	};

	struct CommandIf
	{
		// Fields
		bool _first;

		// Constructor
		CommandIf() {
			_first = true;
		}

		// Operators
		bool operator () (c8 value) {
			if(_first) {
				_first = false;
				return !isalpha(value);
			}
			else {
				return !isalnum(value);
			}
		}
	};

	struct QuoteIf
	{
		// Fields
		bool escape;

		// Constructor
		QuoteIf() {
			escape = false;
		}

		// Operator
		bool operator () (c8 value) {
			if(escape) {
				escape = false;
			}
			else {
				if(value == '\"')
					return true;
				else if(value == '\"')
					escape = true;
			}
			return false;
		}
	};

	struct StringIf
	{
		// Operator
		bool operator () (c8 value) {
			return value <= 32;
		}
	};

	struct SectionIf
	{
		// Fields
		s32 level;

		// Constructor
		SectionIf(bool in = true) {
			level = in;
		}

		// Operator
		bool operator () (c8 value) {
			switch(value) {
				case '}':
					return --level <= 0;

				case '{':
					level++;

				default:
					return false;
			}
		}
	};

	struct RSectionIf
	{
		// Fields
		s32 level;

		// Constructor
		RSectionIf(bool in) {
			level = in;
		}

		// Operator
		bool operator () (c8 value) {
			switch(value) {
				case '{':
					return --level <= 0;

				case '}':
					level++;

				default:
					return false;
			}
		}
	};

	//------------------------------------//
	// Constructor

	Reader::Reader(const string &data) {
		_data = data;
		_at = _data.begin();
	}

	Reader::Reader(const Reader &reader) {
		_data = reader._data;
		_at = _data.begin() + (_at - reader._data.begin());
	}

	//------------------------------------//
	// Destructor

	Reader::~Reader() {
	}

	//------------------------------------//
	// Helper Methods

	bool Reader::skip(bool crossline) {
		// Omi� wszystkie bia�e znaki
		_at = std::find_if(_at, _data.end(), WhitespaceIf(crossline));

		// Zwr�c warto��
		return _at != _data.end() && (!crossline || *_at != '\n');
	}

	u32 Reader::at() {
		return _at - _data.begin();
	}

	u32 Reader::atline() {
		return std::count(_data.begin(), _at, '\n');
	}

	void Reader::at(u32 at) {
		Assert(0 <= at && at < _data.size());

		_at = _data.begin() + at;
	}

	//------------------------------------//
	// Methods

	u32 Reader::line(string &out) {
		string::iterator start, end;

		// Sprawd� czy osi�gni�to koniec pliku
		if(_at == _data.end())
			return -1;

		// Wyszukaj koniec linji
		start = _at;
		end = std::find(_at, _data.end(), '\n');

		// W przypadku osi�gni�cia ko�ca linji, omi� znak ko�ca linji
		if(end != _data.end())
			_at = end + 1;
		else
			_at = end;

		// Zwr�� ci�g znak�w
		out = string(start, end);
		return end - start;
	}

	string Reader::command(bool crossline) {
		string::iterator curr = _at;

		try {
			// Omin biale znaki
			if(skip(crossline)) {
				string::iterator start, end;

				// Wyszukaj koniec ci�gu
				start = _at;
				end = std::find_if(start, _data.end(), CommandIf());

				if(start != end) {
					// Zapisz koniec
					_at = end;

					// Zwr�� ci�g znak�w
					return string(start, end);
				}
			}
			Assert("Expected command" && false);
		}
		catch(...) {
			_at = curr;
			throw;
		}
		return "";
	}

	string Reader::quote(bool crossline, u32 flags) {
		string::iterator curr = _at;

		try {
			// Omin biale znaki
			if(skip(crossline)) {
				string::iterator start, end;

				// Sprawd� typ ci�gu
				switch(*_at) {
					// Sekcja w cudzys�owach
					case '\"':
						if((flags & qfQuote) == 0)
							goto error;

						// Znajdz koniec
						start = _at+1;
						end = std::find_if(start, _data.end(), QuoteIf());

						// Zapisz koniec
						_at = end+1;
						break;

					// Sekcja w klamerkach
					case '{':
						if((flags & qfSection) == 0)
							goto error;

						// Znajdz koniec
						start = _at+1;
						end = std::find_if(start, _data.end(), SectionIf());

						// Zapisz koniec
						_at = end+1;
						break;

					// Jaki� ci�g znak�w do bia�ej spacji
					default:
						if((flags & qfString) == 0)
							goto error;

						// Znajdz koniec
						start = _at;
						end = std::find_if(start, _data.end(), StringIf());

						// Sprawd� czy odczytali�my co�
						if(start == end)
							goto error;

						// Zapisz koniec
						_at = end;
						break;
				}

				// Przytnij bia�e znaki z lewej
				while(start != end) {
					if(start[0] > 32)
						break;
					start++;
				}

				// Przytnij bia�e znaki z prawej
				while(start != end) {
					if(end[-1] > 32)
						break;
					end--;
				}

				// Zwr�c wynik
				return string(start, end);
			}

	error:
			Assert("Expected string" && false);
		}
		catch(...) {
			_at = curr;
			throw;
		}
		return "";
	}

	bool Reader::boolean(bool crossline) {
		string::iterator curr = _at;
		try {
			string value = quote(crossline, qfString);

			// Sprawd� ci�g znak�w
			if(!strcmpi(value.c_str(), "true") || !strcmpi(value.c_str(), "yes") || !strcmpi(value.c_str(), "1"))
				return true;
			if(!strcmpi(value.c_str(), "false") || !strcmpi(value.c_str(), "no") || !strcmpi(value.c_str(), "0"))
				return false;
			Assert("Expected boolean" && false);
		}
		catch(...) {
			_at = curr;
			throw;
		}
		return false;
	}

	s32 Reader::integer(bool crossline) {
		string::iterator curr = _at;
		try {
			string value = quote(crossline, qfString);
			c8 *end;

			// Wczytaj warto��
			s32 ret = strtol(value.c_str(), &end, 10);

			// Sprawd� czy wczytano warto��
			Assert("Expected integer" && *end == '\0');
			return ret;
		}
		catch(...) {
			_at = curr;
			throw;
		}
	}

	f32 Reader::value(bool crossline) {
		string::iterator curr = _at;
		try {
			string value = quote(crossline, qfString);
			c8 *end;

			// Wczytaj warto��
			f32 ret = (f32)strtod(value.c_str(), &end);

			// Sprawd� czy wczytano warto��
			Assert("Expected value" && *end == '\0');
			return ret;
		}
		catch(...) {
			_at = curr;
			throw;
		}
	}

	bool Reader::match(const string &pattern, bool whitespace, bool crossline) {
		string::iterator curr = _at;
		try {
			if(pattern.empty())
				return false;

			// Omin biale spacje
			if(skip(crossline)) {
				u32 length = pattern.length();

				// Sprawd� d�ugo�� ci�gu
				if(_data.end() - _at >= length) {
					// Por�wnaj dwa ci�gi
					if(std::equal(&pattern[0], &pattern[length], _at)) {
						// Sprawd� ko�c�wk�
						if(!whitespace || _data.end() == _at || *_at <= 32) {
							// Zapisz nowy podci�g
							_at += length;
							return true;
						}
					}
				}
			}

			// B��d
			return false;
		}
		catch(...) {
			_at = curr;
			throw;
		}
	}

	vec2 Reader::vector2(bool crossline) {
		string::iterator curr = _at;
		try {
			vec2 v;

			// Oczekuj nawiasu
			if(match("(", false, crossline)) {
				// Wczytaj sk�adniki wektora
				v.X = value(false);
				v.Y = value(false);

				// Oczekuj nawiasu
				if(match(")", false, false))
					return v;
			}
			Assert("Expected vec2" && false);
		}
		catch(...) {
			_at = curr;
			throw;
		}
		return vec2();
	}

	vec3 Reader::vector3(bool crossline) {
		string::iterator curr = _at;
		try {
			vec3 v;

			// Oczekuj nawiasu
			if(match("(", false, crossline)) {
				// Wczytaj sk�adniki wektora
				v.X = value(false);
				v.Y = value(false);
				v.Z = value(false);

				// Oczekuj nawiasu
				if(match(")", false, false))
					return v;
			}
			Assert("Expected vec3" && false);
		}
		catch(...) {
			_at = curr;
			throw;
		}
		return vec3();
	}

	vec4 Reader::vector4(bool crossline) {
		string::iterator curr = _at;
		try {
			vec4 v;

			// Oczekuj nawiasu
			if(match("(", false, crossline)) {
				// Wczytaj sk�adniki wektora
				v.X = value(false);
				v.Y = value(false);
				v.Z = value(false);
				v.W = value(false);

				// Oczekuj nawiasu
				if(match(")", false, false))
					return v;
			}
			Assert("Expected vec4" && false);
		}
		catch(...) {
			_at = curr;
			throw;
		}
		return vec4();
	}

	mat4 Reader::matrix4(bool crossline) {
		string::iterator curr = _at;
		try {
			mat4 m = mat4Identity;

			// Oczekuj nawiasu
			if(!match("[", false, crossline))
				goto error;

			// Wczytaj macierz
			for(s32 i = 0; i < 4; i++) {
				// Oczekuj nawiasu
				if(!match("(", false, true))
					goto error;

				// Wczytaj wiersz
				m[i].X = value(false);
				m[i].Y = value(false);
				m[i].Z = value(false);

				try {
					m[i].W = value(false);
				}
				catch(...) {
				}

				// Oczekuj nawiasu
				if(!match("]", false, crossline))
					goto error;
			}

			// Oczekuj nawiasu
			if(!match("]", false, crossline))
				goto error;

			// Zwr�c macierz
			return m;

error:
			Assert("Expected mat4" && false);
		}
		catch(...) {
			_at = curr;
			throw;
		}
		return mat4();
	}

	u32 Reader::enumeration(const EnumValue *enums, bool crossline) {
		// Sprawd� argumenty
		Assert(enums);

		// Wczytaj ci�g znak�w	
		string value = quote(crossline);

		// Wyszukaj warto��
		for(u32 i = 0; enums[i].Name.size(); i++) {
			if(!strcmpi(enums[i].Name.c_str(), value.c_str()))
				return enums[i].Value;
		}
		Assert("Expected enum" && false);
		return 0;
	}

	//------------------------------------//
	// Expect Methods

	void Reader::expectBeginOfSection() {
		string::iterator curr = _at;
		try {
			if(beginOfSection(true)) {
				return;
			}
			Assert("Expected '{'" && false);
		}
		catch(...) {
			_at = curr;
			throw;
		}
	}

	void Reader::expectEndOfSection() {
		string::iterator curr = _at;
		try {
			if(endOfSection(true)) {
				return;
			}
			Assert("Expected '}'" && false);
		}
		catch(...) {
			_at = curr;
			throw;
		}
	}

	void Reader::expectEndOfCommand() {
		string::iterator curr = _at;
		try {
			if(endOfLine(true)) {
				return;
			}
			Assert("Expected ';'" && false);
		}
		catch(...) {
			_at = curr;
			throw;
		}
	}

	//------------------------------------//
	// Move Methods

	void Reader::moveToStartOfSection(bool in) {
	// Szukaj ko�ca sekcji
		string::reverse_iterator at = std::find_if(_data.rend() - (_at - _data.begin()), _data.rend(), RSectionIf(in));

		// Sprawd� czy znaleziono koniec
		Assert("Expected '{'" && at != _data.rend());

		// Zapisz pocz�tek sekcji
		_at = _data.begin() + (_data.rend() - at) + 1;
	}

	void Reader::moveToEndOfSection(bool in) {
		// Szukaj ko�ca sekcji
		string::iterator at = std::find_if(_at, _data.end(), SectionIf(in));

		// Sprawd� czy znaleziono koniec
		Assert("Expected '}'" && at != _data.end());

		// Zapisz koniec sekcji
		_at = at + 1;
	}

	void Reader::moveToStartOfCommand() {
		// Przesu� si� do znalezienia \n
		string::reverse_iterator at = std::find(_data.rend() - (_at - _data.begin()), _data.rend(), '\n');

		// Sprawdz czy znaleziono \n
		if(at != _data.rend())
			_at = _data.begin() + (_data.rend() - at) + 1;
		else
			_at = _data.begin();
	}

	void Reader::moveToEndOfCommand() {
		// Przesu� si� do znalezienia \n
		string::iterator at = std::find(_at, _data.end(), '\n');

		// Sprawdz czy znaleziono \n
		if(at != _data.end())
			_at = at + 1;
		else
			_at = _data.end();
	}

	//------------------------------------//
	// Query Methods

	bool Reader::endOfLine(bool omit) {
		if(skip(false) && *_at == '\n') {
			if(omit)
				_at++;
			return true;
		}
		return false;
	}

	bool Reader::endOfFile() {
		return !skip(true);
	}

	bool Reader::beginOfSection(bool omit) {
		if(skip(true) && *_at == '{') {
			if(omit)
				_at++;
			return true;
		}
		return false;
	}

	bool Reader::endOfSection(bool omit) {
		if(skip(true) && *_at == '}') {
			if(omit)
				_at++;
			return true;
		}
		return false;
	}

	//------------------------------------//
	// Read Methods

	bool Reader::readProperty(const Property &property, void *data) {
		// Sprawd� w�a�ciwo�ci w�a�ciwo�ci
		Assert(~property.Flags & pfReadOnly);

		// Spr�buj odczyta�
		switch(property.Type) {
			case ptBoolean: 
				offset<bool>(data, property.Offset, 1) = boolean();
				break;

			case ptInteger:
				offset<s32>(data, property.Offset, 1) = integer();
				break;

			case ptValue:
				offset<f32>(data, property.Offset, 1) = value();
				break;

			case ptString:
				offset<string>(data, property.Offset, 1) = quote();
				break;

			case ptVec3:
				offset<vec3>(data, property.Offset, 1) = vector3();
				break;

			case ptVec4:
				offset<vec4>(data, property.Offset, 1) = vector4();
				break;

			case ptMat4:
				offset<mat4>(data, property.Offset, 1) = matrix4();
				break;

			case ptResource:
				offset<string>(data, property.Offset, 1) = quote();
				break;

			case ptNone:
				if(property.Enums)
					offset<u32>(data, property.Offset, 1) = enumeration(property.Enums);
				else if(property.Properties)
					readProperties(property.Properties, &offset<u32>(data, property.Offset, 1));
				break;

			default:
				throw(std::logic_error("invalid property type"));
		}
		return true;
	}

	void Reader::readProperties(Property* properties, void *data, bool s) {
		// Sprawd� argumenty
		Assert(data);
		Assert(properties);

		// Sprawd� czy mamy do czynienia z sekcj�
		bool section = s ? beginOfSection(true) : false;

		// Sprawd� rezultat
		Assert("Expected '{'" && (!s || section))

		// Wczytuj do ko�ca sekcji lub pliku
		while(section ? !endOfSection() : !endOfFile()) {
			// Pobierz komend�
			string key = command();

			// Wyszukaj komend�
			for(u32 i = 0; !properties[i].end(); i++) {
				// Pobierz w�a�ciwo��
				const Property *property = &properties[i];

				// Sprawd� czy dobrze pobrali�my
				if(!property)
					continue;

				// Sprawd� nazw�
				if(!strcmpi(key.c_str(), property->Name.c_str())) {
					// Wczytaj warto��
					readProperty(*property, data);

					// Sprawd� czy mamy doczynienia z ko�cem komendy
					expectEndOfCommand();
					goto next;
				}
			}
			Throw(AssertException(va("%s not found", key.c_str())));
next:;
		}
	}
};