//------------------------------------//
//
// Stats.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-13
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Stats: Constructor

	Stats::Stats() {
		// Wyczy�� wszystkie dane
		memset(this, 0, sizeof(*this));
	}

	//------------------------------------//
	// Stats: Methods

	void Stats::reset() {
		// Wyczy�� wszystkie dane
		memset(this, 0, sizeof(*this));
	}

	void Stats::show(Font *font) {
		if(!font)
			return;

		u32 line = 0;

		// Sprawd� czy mamy kamer�
		if(Engine::Camera) {
			vec3 eye = Engine::Camera->origin();
			vec3 at = Engine::Camera->at();

			// Wy�wietl pozycje kamery
			font->draw(vec2(0, font->height() * line++), va("Eye: %5.2f %5.2f %5.2f", eye.X, eye.Y, eye.Z));
			font->draw(vec2(0, font->height() * line++), va("At: %1.3f %1.3f %1.3f", at.X, at.Y, at.Z));
		}

		// Wy�wietl statystyki
		font->draw(vec2(0, font->height() * line++), va("FPS: %.2f", (Engine::Input->keyState(DIK_CAPSLOCK, false) + 1) * Engine::Time->fps()));
		font->draw(vec2(0, font->height() * line++), va("Draws: %i", Draws));
		font->draw(vec2(0, font->height() * line++), va("Primitives: %i", Primitives));
		font->draw(vec2(0, font->height() * line++), va("Textures: %i", Textures));
		font->draw(vec2(0, font->height() * line++), va("Buffers: %i", Buffers));
		font->draw(vec2(0, font->height() * line++), va("States: %i", States));
		font->draw(vec2(0, font->height() * line++), va("Consts: %i", Consts));
		font->draw(vec2(0, font->height() * line++), va("Shaders: %i", Shaders));
		font->draw(vec2(0, font->height() * line++), va("Clips: %i", Clips));
	}
};
