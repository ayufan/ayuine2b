//------------------------------------//
//
// Time.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Time: Constructor

	Time::Time() {
		_current = 0;
		_delta = 0;
		_last = 0;
		_fps = 0;
		_fpsStart = 0;
		_fpsCount = 0;

		// Pobierz cz�stotliwo��
		QueryPerformanceFrequency((LARGE_INTEGER*)&_freq);

		// Pobierz pocz�tkowy czas
		QueryPerformanceCounter((LARGE_INTEGER*)&_first);
	}
	
	//------------------------------------//
	// Time: Destructor

	Time::~Time() {
	}

	//------------------------------------//
	// Time: Methods

	void Time::reset() {
		// Wyzeruj liczniki
		_current = 0;
		_delta = 0;
		_last = 0;
		_fps = 0;
		_fpsStart = 0;
		_fpsCount = 0;
		
		// Pobierz pocz�tkowy czas
		QueryPerformanceCounter((LARGE_INTEGER*)&_first);
	}

	u64 Time::cycles() {
		u64 cycles;

		// Pobierz ilo�� cykli
		QueryPerformanceCounter((LARGE_INTEGER*)&cycles);
		return cycles - _first;
	}

	void Time::update() {
		u64 current, delta;

		// Pobierz ilo�� cykli
		current = cycles();

		// Oblicz r�nic� ilo�ci cykli
		delta = current - _last;

		// Oblicz parametry dla danej klatki
		_delta = (f64)delta / _freq;
		_current = (f64)current / _freq;

		// Dodaj klatk�
		_fpsCount++;

		// Zmiana sekundy
		if((_fpsStart / _freq) < (current / _freq)) {
			// Oblicz ilo�� klatek
			_fps = _fpsCount * f64(current - _fpsStart) / _freq;

			// Wyzeruj licznik klatek
			_fpsStart = current;
			_fpsCount = 0;
		}

		// Zapisz ilo�� cykli
		_last = current;
	}

	f32 Time::current() {
		return _current;
	}

	f32 Time::delta() {
		return _delta;
	}

	f32 Time::fps() {
		return _fps;
	}
};
