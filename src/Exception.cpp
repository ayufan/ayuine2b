//------------------------------------//
//
// Exception.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-30
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Exception: Constructor

	Exception::Exception(const string &message) {
		strcpy_s(Message, sizeof(Message), message.c_str());
	}

	//------------------------------------//
	// Exception: Destructor

	Exception::~Exception() {

	}

	//------------------------------------//
	// UnhandledException: Constructor

	UnhandledException::UnhandledException(const string &message) {
		strcpy_s(Message, sizeof(Message), message.c_str());
	}

	//------------------------------------//
	// AssertException: Constructor

	AssertException::AssertException(const string &message) {
		strcpy_s(Message, sizeof(Message), message.c_str());
	}
};
