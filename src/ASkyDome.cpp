//------------------------------------//
//
// ASkyDome.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-16
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// ASkyDome: Constructor

	ASkyDome::ASkyDome() {
		const u32 Levels = 3;
		const f32 Size = 10;
		const vec3 verts0[8] = {
			vec3(-Size, -Size, -Size),		// 0
			vec3(-Size, -Size,  Size),		// 1
			vec3(-Size,  Size, -Size),		// 2
			vec3( Size, -Size, -Size),		// 3
			vec3(-Size,  Size,  Size),		// 4
			vec3( Size,  Size, -Size),		// 5
			vec3( Size, -Size,  Size),		// 6
			vec3( Size,  Size,  Size)			// 7
		};
		const u16 indices0[20] = {
			3, 6, 7, 5,
			0, 2, 4, 1,
			1, 4, 7, 6,
			//0, 3, 5, 2,
			2, 5, 7, 4,
			0, 1, 6, 3,
		};

		// Ustaw domy�lne dane
		Material = "default_sky";
		Sun = false;
		SunArea = 0;
		Box = bbox(-4096, 4096);

		// Flags
		Flags = afPrepare;

		// Wygeneruj SkyDome
		Array<vec3> verts(CountOf(indices0));

		// Ustaw poziom 0 
		for(u32 i = 0; i < CountOf(indices0); i++) {
			verts.at(i) = verts0[indices0[i]];
		}

		// Teseluj nast�pne poziomy
		for(u32 i = 0; i < Levels; i++) {
			u32 count = verts.size();

			// Zwi�ksz rozmiar
			verts.resize(verts.size() * 4);

			// Pobierz quada
			for(u32 j = 0; j < count; j += 4) {
				vec3 *p = &verts.at(j);
				vec3 v[4];
				vec3 vv[5];

				// Zapisz star� pozycj�
				v[0] = p[0];
				v[1] = p[1];
				v[2] = p[2];
				v[3] = p[3];

				// Oblicz wszystkie 5 nowych wierzcho�k�w
				vv[0] = vec3::lerp(v[0], v[1]);
				vv[1] = vec3::lerp(v[1], v[2]);
				vv[2] = vec3::lerp(v[2], v[3]);
				vv[3] = vec3::lerp(v[3], v[0]);
				vv[4] = vec3::lerp(v[0], v[2]);

				// 0-------3-------3
				// |       |       |
				// |   0   |   1   |
				// |       |       |
				// 0-------4-------2
				// |       |       |
				// |   2   |   3   |
				// |       |       |
				// 1-------1-------2

				// Dodaj quada 0
				p[count * 0 + 0] = v[0];
				p[count * 0 + 1] = vv[0];
				p[count * 0 + 2] = vv[4];
				p[count * 0 + 3] = vv[3];

				// Dodaj quada 1 
				p[count * 1 + 0] = vv[3];
				p[count * 1 + 1] = vv[4];
				p[count * 1 + 2] = vv[2];
				p[count * 1 + 3] = v[3];

				// Dodaj quada 2
				p[count * 2 + 0] = vv[0];
				p[count * 2 + 1] = v[1];
				p[count * 2 + 2] = vv[1];
				p[count * 2 + 3] = vv[4];

				// Dodaj quada 3
				p[count * 3 + 0] = vv[4];
				p[count * 3 + 1] = vv[1];
				p[count * 3 + 2] = v[2];
				p[count * 3 + 3] = vv[2];
			}
		}

		// Zoptymilizuj list� wierzcho�k�w
		Array<u16> map = optimize<u16>(verts);
		
		// Utw�rz tablic� indeks�w
		Array<u16> indices(map.size() * 3 / 2);

		// Sprawd� czy nie przekroczyli�my 16 bit�w
		assert(indices.size() < 65536);

		// Wygeneruj indeksy
		for(Array<u16>::Iterator s = map.begin(), d = indices.begin(); s != map.end(); s += 4) {
			// Tr�jk�t 1
			*d++ = s[2];
			*d++ = s[1];
			*d++ = s[0];

			// Tr�jk�t 2
			*d++ = s[3];
			*d++ = s[2];
			*d++ = s[0];
		}

		// Znormalizuj wierzcho�ki
		ArrayEach(vec3, i, verts) {
			*i *= Size / i->length();
		}

		// Zbuduj potrzebne dane
		_verts = verts.size();
		_indices = indices.size();
		_vertexType.reset(new VertexType(D3DFVF_XYZ));
		_vertexBuffer.reset(new VertexBuffer(verts.size(), sizeof(vec3), false));
		_indexBuffer.reset(new IndexBuffer(indices.size(), if16, false));

		// Uaktualnij dane
		_vertexBuffer->update(&verts.at(0), verts.size());
		_indexBuffer->update(&indices.at(0), indices.size());
	}

	//------------------------------------//
	// ASkyDome: Methods

	void ASkyDome::preupdate(f32 dt) {
		Actor::preupdate(dt);
	}

	void ASkyDome::postupdate(f32 dt) {
		Actor::postupdate(dt);
	}

	void ASkyDome::prepare(RenderFrame &frame) {
		if(frame.Mode == rmDepth)
			return;

		RenderMesh *mesh = new RenderMesh();

		// Przygotuj ramk�
		mat4::translation(mesh->Transform, frame.CameraOrigin);
 		mesh->Material = Material;
		mesh->Flags = rffPreCustom;
		mesh->VertexType = _vertexType;
		mesh->VertexBuffer = _vertexBuffer;
		mesh->IndexBuffer = _indexBuffer;
		mesh->Drawers.push(BatchIndexedDrawer(D3DPT_TRIANGLELIST, 0, _verts, 0, _indices / 3));

		// Dodaj fragment
		frame.addFrag(mesh);

		// Zapisz informacje o s�oneczku
		frame.Sun = Sun;
		frame.SunDir = SunDir;
		frame.SunParam = vec4(1, 1, 1, 1);
		frame.SunColor = SunColor;
		frame.SunPRT = Engine::Config->PRT > 0;
		
		// Wygeneruj wsp�czynniki dla PRT
		if(frame.SunPRT)
			SphericalHarmonics::directionalLight(SunDir, SunColor.W * 1.5f, frame.SunPCA);
		else
			frame.Ambient = SunColor * 0.25f;
	}
};
