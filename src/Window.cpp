//------------------------------------//
//
// Window.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-1
//
//------------------------------------//

#include "StdAfx.hpp"

#define ClassName "ayuine2"

namespace ayuine
{
	//------------------------------------//
	// Window: Functions

	static LRESULT CALLBACK windowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
		switch(uMsg) {
			case WM_SIZE:
				break;

			case WM_CLOSE:
				DestroyWindow(hWnd);
				break;
		}
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}

	//------------------------------------//
	// Window: Constructor

	Window::Window() {
		WNDCLASS wc;
		DWORD style = 0, styleEx = 0;

		// Window Fields
		_hWnd = nullptr;

		// Zniszcz stare okno
		if(_hWnd) {
			DestroyWindow(_hWnd);
			_hWnd = nullptr;
		}

		// Odrejestruj star� klas�
		if(GetClassInfo(GetModuleHandle(nullptr), ClassName, &wc)) {
			UnregisterClass(ClassName, GetModuleHandle(nullptr));
		}

		// Wype�nij informacje o klasie
		wc.style = CS_OWNDC;
		wc.lpfnWndProc = windowProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = GetModuleHandle(nullptr);
		wc.hIcon = nullptr;
		wc.hCursor = nullptr;
		wc.hbrBackground = nullptr;
		wc.lpszMenuName = nullptr;
		wc.lpszClassName = ClassName;

		// Zarejestruj now� klas�
		RegisterClass(&wc);

		// Utw�rz okno
		Assert(_hWnd = CreateWindowEx(WS_EX_TOOLWINDOW | WS_EX_APPWINDOW | WS_EX_WINDOWEDGE, ClassName, ClassName, WS_VISIBLE | /*WS_OVERLAPPEDWINDOW |*/ WS_CLIPSIBLINGS | WS_CLIPCHILDREN | /*WS_OVERLAPPED |*/ WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, 0, 0, 128, 128, nullptr, nullptr, GetModuleHandle(nullptr), nullptr));
	}

	//------------------------------------//
	// Window: Destructor

	Window::~Window() {
		// Zniszcz stare okno
		if(_hWnd) {
			DestroyWindow(_hWnd);
			_hWnd = nullptr;
		}

		// Odrejestruj star� klas�
		UnregisterClass(ClassName, GetModuleHandle(nullptr));
	}

	//------------------------------------//
	// Window: Methods

	bool Window::check() {
		return IsWindow(_hWnd);
	}

	void Window::title(const string &name) {
		// Ustaw tytu� okna
		SetWindowText(_hWnd, name.c_str());
	}

	bool Window::update() {
		MSG msg;

		// Pobierz wszystkie komunikaty
		while(PeekMessage(&msg, _hWnd, 0, 0, PM_REMOVE)) {
			// Przet�umacz i przetw�rz
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		return check();
	}

	bool Window::focused() {
		// Sprawd� czy okno jest widoczne
		return GetForegroundWindow() == _hWnd;
	}
};
