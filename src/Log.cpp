//------------------------------------//
//
// Log.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// LogMessage: Constructor

	LogMessage::LogMessage(const string &message, LogMessageType lmt, const string &callStack) {
		Type = lmt;
		Time = Engine::Time->current();
		Message = message;
		CallStack = callStack;

		// Wypisz na standardowe wyj�cie
		string m = va("%s\n", entry().c_str());

		fputs(m.c_str(), stderr);
//#ifdef _DEBUG
		OutputDebugStringA(m.c_str());
//#endif
	}

	//------------------------------------//
	// LogMessage: Destructor

	string LogMessage::time() const {
		// Przekszta�� czas na ci�g znak�w
		return va("%.2f", Time);
	}

	string LogMessage::type() const {
		switch(Type) {
			case lmtInfo: return "info";
			case lmtDebug: return "debug";
			case lmtWarning: return "warning";
			case lmtError: return "error";
			case lmtCritical: return "critical";
			default: return "unknown";
		}
	}
	
	string LogMessage::entry() const {
		return va("%.2f (%s): %s", Time, type().c_str(), Message.c_str());
	}
		
	//------------------------------------//
	// Log: Constructor

	Log::Log() {
	}

	//------------------------------------//
	// Log: Destructor

	Log::~Log() {
	}

	//------------------------------------//
	// Log: Methods

	void Log::print(const c8 *format, ...) {
		dGuard {
			_messages.push(LogMessage(fa(&format).c_str(), lmtInfo));
		}
		dUnguard;
	}

	void Log::debug(const c8 *format, ...) {
		dGuard {
			_messages.push(LogMessage(fa(&format).c_str(), lmtDebug));
		}
		dUnguard;
	}

	void Log::error(const c8 *format, ...) {
		dGuard {
			_messages.push(LogMessage(fa(&format).c_str(), lmtError));
		}
		dUnguard;
	}

	void Log::warning(const c8 *format, ...) {
		dGuard {
			_messages.push(LogMessage(fa(&format).c_str(), lmtWarning));
		}
		dUnguard;
	}

	void Log::dumpLog(const string &name) {
		Guard {
			// Sprawd� argumenty
			Assert(name.size());

			// Spr�buj otworzy� plik do zapisu
			SmartPtr<File> handle(Engine::VFS->saveFile(va("%s.log", name.c_str())));

			// Sprawd� czy otworzono plik
			if(handle != nullptr) {
				// Wypisz wszystkie wpisy do logu
				ArrayEach(LogMessage, i, _messages) {
					// Dodaj wiadomo�� do pliku
					handle->write(i->entry().c_str(), true);
				}
			}
		}
		Unguard;
	}

	void Log::clearLog() {
		Guard {
			// Wyczy�� logi
			_messages.clear();
		}
		Unguard;
	}
};
