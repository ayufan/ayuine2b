//------------------------------------//
//
// StdAfx.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-9
//
//------------------------------------//

#include "StdAfx.hpp"

//------------------------------------//
// OpenAL

#pragma comment(lib, "../common/openal/lib/openal32.lib")
#pragma comment(lib, "../common/openal/lib/alut.lib")

//------------------------------------//
// OGG Vorbis

#pragma comment(lib, "../common/vorbis/lib/ogg.lib")
#pragma comment(lib, "../common/vorbis/lib/vorbis.lib")
#pragma comment(lib, "../common/vorbis/lib/vorbisenc.lib")
#pragma comment(lib, "../common/vorbis/lib/vorbisfile.lib")

//------------------------------------//
// Newton

#pragma comment(lib, "../common/newton/lib/newton.lib")

//------------------------------------//
// DirectX9

#pragma comment(lib, "../common/dx9/lib/d3d9.lib")
#ifdef _DEBUG
#pragma comment(lib, "../common/dx9/lib/d3dx9d.lib")
#else
#pragma comment(lib, "../common/dx9/lib/d3dx9.lib")
#endif
#pragma comment(lib, "../common/dx9/lib/dinput8.lib")
#pragma comment(lib, "../common/dx9/lib/dxerr.lib")
#pragma comment(lib, "../common/dx9/lib/dxguid.lib")
