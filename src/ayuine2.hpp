//------------------------------------//
//
// ayuine2.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-9
//
//------------------------------------//

#pragma once

#ifndef AYUAPI
#ifdef AYUDLL2
#define AYUAPI			__declspec(dllexport)
#define _NEWTON_USE_LIB
#else		// AYUDLL2
#define AYUAPI			__declspec(dllimport)
#endif	// AYUDLL2
#define VAYUAPI			AYUAPI virtual
#define EAYUAPI			AYUAPI extern
#endif	// AYUAPI

#define _CRT_SECURE_NO_DEPRECATE

//------------------------------------//
// Base

#include <windows.h>
#include "../common/dx9/include/d3dx9.h"
#include "../common/dx9/include/dxerr.h"
#include "../common/dx9/include/dinput.h"
#include "../common/newton/include/newton.h"

//------------------------------------//
// Engine

#include "Core.hpp"
#include "Input.hpp"
#include "Window.hpp"
#include "Graphics.hpp"
#include "Material.hpp"
#include "Physics.hpp"
#include "Mesh.hpp"
#include "Camera.hpp"
#include "Render.hpp"
#include "Sound.hpp"
#include "Actor.hpp"
#include "Stats.hpp"
#include "Engine.hpp"
