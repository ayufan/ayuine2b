//------------------------------------//
//
// Joint.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-1
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// Joint class

	class Joint : public Object
	{
		ObjectType(Joint, Object, 0);

		// Fields
	protected:
		NewtonJoint*		_joint;
	public:
		bool						Collide;
		f32							Stiffness;
		f32							ForceLimit;

		// Events
	public:
		Event<Joint*>		OnApply;
		Event<Joint*>		OnUpdate;
		Event<Joint*>		OnDestroy;

		// Constructor
	protected:
		AYUAPI Joint();

		// Destructor
	public:
		AYUAPI virtual ~Joint();

		// Properties
	public:
		AYUAPI bool destroyed() const;

		// Methods
	public:
		AYUAPI virtual void apply();
	};

	//------------------------------------//
	// BallJoint class

	class BallJoint : public Joint
	{
		ObjectType(BallJoint, Joint, 0);

		// Fields
	public:
		vec3		Axis;
		f32			ConeLimit;
		f32			TwistLimit;

		// Joint Update Fields
	public:
		vec3		Force;
		vec3		Angle;
		vec3		Omega;

		// Constructor
	public:
		AYUAPI BallJoint(const vec3 &origin, RigidBody* child, RigidBody* parent = nullptr);

		// Methods
	public:
		AYUAPI virtual void apply();
	};

	//------------------------------------//
	// HingeJoint class

	class HingeJoint : public Joint
	{
		ObjectType(HingeJoint, Joint, 0);

		// Fields
	public:

		// Hinge Update Fields
	public:
		vec3	Force;
		f32		Angle;
		f32		Omega;

		// Constructor
	public:
		AYUAPI HingeJoint(const vec3 &origin, const vec3 &axis, RigidBody* child, RigidBody* parent = nullptr);

		// Methods
	public:
	};

	//------------------------------------//
	// SliderJoint class

	class SliderJoint : public Joint
	{
		ObjectType(SliderJoint, Joint, 0);

		// Fields
	public:

		// Slider Update Fields
	public:
		vec3	Force;
		f32		Origin;
		f32		Velocity;

		// Constructor
	public:
		AYUAPI SliderJoint(const vec3 &origin, const vec3 &axis, RigidBody* child, RigidBody* parent = nullptr);

		// Methods
	public:
	};

	//------------------------------------//
	// UpJoint class

	class UpJoint : public Joint
	{
		// Fields
	public:
		vec3		Axis;

		// Up Update Fields
	public:
		
		// Constructor
	public:
		AYUAPI UpJoint(const vec3 &axis, RigidBody* child);

		// Methods
	public:
		AYUAPI virtual void apply();
	};
};
