//------------------------------------//
//
// IndexBuffer.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-9
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// IndexFormat enumeration

	enum IndexFormat
	{
		if16 = 2,
		if32 = 4
	};

	//------------------------------------//
	// IndexBuffer class

	class IndexBuffer : public DeviceObject
	{
		ObjectType(IndexBuffer, DeviceObject, 0);

		// Fields
	private:
		LPDIRECT3DINDEXBUFFER9			_ib;
		size												_count;
		IndexFormat									_format;
		bool												_dynamic;

		// Constructor
	public:
		AYUAPI IndexBuffer(size count, IndexFormat format, bool dynamic = false);

		// Destructor
	public:
		AYUAPI virtual ~IndexBuffer();

		// Properties:
	public:
		AYUAPI IndexFormat format() const;
		AYUAPI size count() const;
		AYUAPI bool dynamic() const;

		// Methods
	public:
		AYUAPI void *lock(size count, size offset = 0, LockFlags flags = lfNone);
		AYUAPI void unlock();
		AYUAPI size update(const u16 *data, size count, size offset = 0, bool nooverwrite = false);
		AYUAPI size update(const u32 *data, size count, size offset = 0, bool nooverwrite = false);
		AYUAPI void bind();
		AYUAPI void unbind();

		// DeviceObject Methods
	public:
		AYUAPI virtual void deviceLost(bool theEnd);
		AYUAPI virtual void deviceReset(bool theBegin);
	};
};
