//------------------------------------//
//
// Texture.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Texture: Static Fields

	string Texture::_extensions[] = {"%s.bmp", "%s.jpg", "%s.jpeg", "%s.tga", "%s.png", "%s.dds", "%s.ppm", "%s.dib", "%s.hdr", "%s.pfm", "%s"};

	//------------------------------------//
	// Texture: Collection

	template<> Collection<Texture>::ObjectMap Collection<Texture>::_objects;

	//------------------------------------//
	// Texture: Constructor

	Texture::Texture() {
		dGuard {
			memset(&_info, 0, sizeof(_info));
			_texture = nullptr;
			Address = taWrap;
			Filter = tfMipmaps;
		}
		dUnguard;
	}

	//------------------------------------//
	// Texture: Destructor

	Texture::~Texture() {
		Guard {
			if(_texture) {
				_texture->Release();
				_texture = nullptr;
			}
		}
		Unguard;
	}

	//------------------------------------//
	// Texture: Properties

	const D3DXIMAGE_INFO &Texture::info() const {
		dGuard {
			return _info;
		}
		dUnguard;
	}

	//------------------------------------//
	// Texture: Methods

	void Texture::load(const string &name) {
		Engine::Log->print("Texture: Loading %s...", name.c_str());

		// Zniszcz star� tekstur�
		if(_texture) {
			_texture->Release();
			_texture = nullptr;
		}

		// Zaznacz, �e tekstura jest nieza�adowana
		_loaded = false;

		for(u32 i = 0; i < CountOf(_extensions); i++) {
			try {
				// Zczytaj dane z pliku
				string data = Engine::VFS->file(va(_extensions[i].c_str(), name.c_str()), ftTexture);

				// Wczytaj informacje o teksturze
				dxe(D3DXGetImageInfoFromFileInMemory(data.c_str(), data.size(), &_info));

				// Je�li mamy urz�dzenie to wtedy wczytujemy teksturk� :)
				if(Engine::Device->d3ddev()) {
					u32 width, height, depth;
					D3DFORMAT format;

					// Oblicz wielko�� nowych obrazk�w
					width = max(makePower2(_info.Width) >> Engine::Config->TextureQuality, Engine::Config->TextureMinimalSize);
					height = max(makePower2(_info.Height) >> Engine::Config->TextureQuality, Engine::Config->TextureMinimalSize);
					depth = max(makePower2(_info.Depth) >> Engine::Config->TextureQuality, Engine::Config->TextureMinimalSize);

					// Sprawd� format
					if(Engine::Config->TextureCompression) {
						switch(_info.Format) {
							case D3DFMT_DXT1:
							case D3DFMT_DXT2:
							case D3DFMT_DXT3:
							case D3DFMT_DXT4:
							case D3DFMT_DXT5:
								format = _info.Format;
								break;

							default:
								format = D3DFMT_DXT5;
						}
					}
					else {
						format = _info.Format;
					}

					// Za�aduj tekstur�
					switch(_info.ResourceType) {
						case D3DRTYPE_TEXTURE:
							dxe(D3DXCreateTextureFromFileInMemoryEx(Engine::Device->d3ddev(), data.c_str(), data.size(), width, height, D3DX_DEFAULT, 0, format, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, nullptr, nullptr, reinterpret_cast<LPDIRECT3DTEXTURE9*>(&_texture)));
							break;

						case D3DRTYPE_CUBETEXTURE:
							dxe(D3DXCreateCubeTextureFromFileInMemoryEx(Engine::Device->d3ddev(), data.c_str(), data.size(), width, D3DX_DEFAULT, 0, format, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, nullptr, nullptr, reinterpret_cast<LPDIRECT3DCUBETEXTURE9*>(&_texture)));
							break;

						case D3DRTYPE_VOLUMETEXTURE:
							dxe(D3DXCreateVolumeTextureFromFileInMemoryEx(Engine::Device->d3ddev(), data.c_str(), data.size(), width, height, depth, D3DX_DEFAULT, 0, format, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, nullptr, nullptr, reinterpret_cast<LPDIRECT3DVOLUMETEXTURE9*>(&_texture)));
							break;
					}
				}

				// Za�adowano
				_loaded = true;
				return;
			}
			catch(Exception &e) {
			}
		}

		// Wy�wietl komunikat
		Engine::Log->warning("Texture: Couldn't load %s !", name.c_str());

		// Zapisz domy�ln� tekstur�
		_texture = Engine::Device->_defaultTexture;
		_info = Engine::Device->_defaultTextureInfo;

		// Zwi�ksz licznik referencji
		if(_texture) {
			_texture->AddRef();
			_loaded = true;
		}
	}

	void Texture::unload() {
		// Zniszcz star� tekstur�
		if(_texture) {
			_texture->Release();
			_texture = nullptr;
		}

		// Zaznacz, �e tekstura jest nieza�adowana
		_loaded = false;
	}

	void Texture::bind(u32 sampler) {
		if(!this)
			return;

		// Podepnij tekstur�
		Engine::Device->_filter[sampler][1] = Filter;
		Engine::Device->_address[sampler][1] = Address;
		Engine::Device->_textures[sampler][1] = _texture;
	}

	void Texture::unbind(u32 sampler) {
		if(!this)
			return;

		// Odepnij tekstur�
		Engine::Device->_textures[sampler][1] = nullptr;
	}

	//------------------------------------//
	// Texture: DeviceObject Methods

	void Texture::deviceReset(bool theBegin) {
		// Nic nie r�b ;]
	}

	void Texture::deviceLost(bool theEnd) {
		// Zniszcz tekstur�
		if(theEnd) {
			if(_texture) {
				_texture->Release();
				_texture = nullptr;
			}
		}
	}

	//------------------------------------//
	// TextureTarget: Constructor

	TextureTarget::TextureTarget(u32 width, u32 height, D3DFORMAT format) {
		_old = nullptr;
		_set = false;

		// Zapisz parametry
		_info.Width = width;
		_info.Height = height;
		_info.Depth = 1;
		_info.Format = format;
		_info.ImageFileFormat = D3DXIFF_DDS;
		_info.ResourceType = D3DRTYPE_TEXTURE;
		_info.MipLevels = 1;

		// Zresetuj urz�dzenie
		deviceReset(true);
	}

	//------------------------------------//
	// TextureTarget: Properties

	bool TextureTarget::depth() const {
		switch(_info.Format) {
			case D3DFMT_D16_LOCKABLE:
			case D3DFMT_D32:
			case D3DFMT_D15S1:
			case D3DFMT_D24S8:
			case D3DFMT_D24X8:
			case D3DFMT_D24X4S4:
			case D3DFMT_D16:
			case D3DFMT_D32F_LOCKABLE:
			case D3DFMT_D24FS8:
				return true;

			default:
				return false;
		}
	}

	LPDIRECT3DSURFACE9 TextureTarget::surface() {
		LPDIRECT3DSURFACE9 d3dSurface;

		// Pobierz powierzchni�
		dxe(LPDIRECT3DTEXTURE9(_texture)->GetSurfaceLevel(0, &d3dSurface));
		return d3dSurface;
	}

	//------------------------------------//
	// TextureTarget: DeviceObject Methods

	void TextureTarget::deviceReset(bool theBegin) {
		// Utw�rz tekstur�
		dxe(Engine::Device->d3ddev()->CreateTexture(_info.Width, _info.Height, 1, depth() ? D3DUSAGE_DEPTHSTENCIL : D3DUSAGE_RENDERTARGET, _info.Format, D3DPOOL_DEFAULT, (LPDIRECT3DTEXTURE9*)&_texture, nullptr));
	}

	void TextureTarget::deviceLost(bool theEnd) {
		// Wywo�aj rodzica
		Texture::deviceLost(true);
	}

	//------------------------------------//
	// TextureTarget: Methods

	void TextureTarget::begin(u32 sampler) {
		// Sprawd� stan
		Assert("invoke end first" && !_set);

		// Pobierz powierzchni�
		LPDIRECT3DSURFACE9 d3dSurface = surface();

		try {
			// Ustaw cel renderowania i pobierz wcze�niejsz� powierzchni�
			if(depth()) {
				dxe(Engine::Device->d3ddev()->GetDepthStencilSurface(&_old));
				dxe(Engine::Device->d3ddev()->SetDepthStencilSurface(d3dSurface));
			}
			else {
				dxe(Engine::Device->d3ddev()->GetRenderTarget(sampler, &_old));
				dxe(Engine::Device->d3ddev()->SetRenderTarget(sampler, d3dSurface));
			}
			_set = true;
		}
		catch(...) {
			// Zwolnij powierzchni�
			if(d3dSurface) {
				d3dSurface->Release();
			}
			throw;
		}

		// Zwolnij powierzchni�
		if(d3dSurface) {
			d3dSurface->Release();
		}
	}

	void TextureTarget::end(u32 sampler) {
		// Sprawd� stan
		Assert("invoke begin first" && _set);

		// Ustaw stary cel wy�wietlania
		if(depth()) {
			dxe(Engine::Device->d3ddev()->SetDepthStencilSurface(_old));
		}
		else {
			dxe(Engine::Device->d3ddev()->SetRenderTarget(sampler, _old));
		}

		// Zwolnij powierzchni�
		if(_old) {
			_old->Release();
			_old = nullptr;
		}
		_set = false;
	}

	void TextureTarget::copy(TextureTarget *from) {
		// Pobierz powierzchnie
		LPDIRECT3DSURFACE9 fromSurface;
		LPDIRECT3DSURFACE9 toSurface;

		// Sprawd� czy mamy pobra� domy�lny cel, czy tekstur�
		if(from) 
			fromSurface = from->surface();
		else
			dxe(Engine::Device->d3ddev()->GetRenderTarget(0, &fromSurface));

		try {
			toSurface = surface();
		}
		catch(...) {
			fromSurface->Release();
			throw;
		}

		try {
			dxe(Engine::Device->d3ddev()->StretchRect(fromSurface, nullptr, toSurface, nullptr, D3DTEXF_POINT));
		}
		catch(...) {
			fromSurface->Release();
			toSurface->Release();
			throw;
		}
	}

	//------------------------------------//
	// CubeTarget: Constructor

	CubeTarget::CubeTarget(u32 edge, D3DFORMAT format) {
		_old = nullptr;
		_set = false;

		// Zapisz parametry
		_info.Width = edge;
		_info.Height = edge;
		_info.Depth = 6;
		_info.Format = format;
		_info.ImageFileFormat = D3DXIFF_DDS;
		_info.ResourceType = D3DRTYPE_CUBETEXTURE;
		_info.MipLevels = 1;
		
		// Utw�rz tekstur�
		deviceReset(true);
	}

	//------------------------------------//
	// CubeTarget: Properties

	bool CubeTarget::depth() const {
		switch(_info.Format) {
			case D3DFMT_D16_LOCKABLE:
			case D3DFMT_D32:
			case D3DFMT_D15S1:
			case D3DFMT_D24S8:
			case D3DFMT_D24X8:
			case D3DFMT_D24X4S4:
			case D3DFMT_D16:
			case D3DFMT_D32F_LOCKABLE:
			case D3DFMT_D24FS8:
				return true;

			default:
				return false;
		}
	}

	LPDIRECT3DSURFACE9 CubeTarget::surface(CubeMapFace face) {
		LPDIRECT3DSURFACE9 d3dSurface;

		// Pobierz powierzchni�
		dxe(LPDIRECT3DCUBETEXTURE9(_texture)->GetCubeMapSurface((D3DCUBEMAP_FACES)face, 0, &d3dSurface));
		return d3dSurface;
	}

	//------------------------------------//
	// CubeTarget: DeviceObject Methods

	void CubeTarget::deviceReset(bool theBegin) {
		// Utw�rz tekstur�
		dxe(Engine::Device->d3ddev()->CreateCubeTexture(_info.Width, 1, depth() ? D3DUSAGE_DEPTHSTENCIL : D3DUSAGE_RENDERTARGET, _info.Format, D3DPOOL_DEFAULT, (LPDIRECT3DCUBETEXTURE9*)&_texture, nullptr));
	}

	void CubeTarget::deviceLost(bool theEnd) {
		// Wywo�aj rodzica
		Texture::deviceLost(true);
	}

	//------------------------------------//
	// CubeTarget: Methods

	void CubeTarget::begin(u32 sampler, CubeMapFace face) {
		// Sprawd� stan
		Assert("invoke end first" && !_set);

		// Pobierz powierzchni�
		LPDIRECT3DSURFACE9 d3dSurface = surface(face);

		try {
			// Ustaw cel renderowania i pobierz wcze�niejsz� powierzchni�
			if(depth()) {
				dxe(Engine::Device->d3ddev()->GetDepthStencilSurface(&_old));
				dxe(Engine::Device->d3ddev()->SetDepthStencilSurface(d3dSurface));
			}
			else {
				dxe(Engine::Device->d3ddev()->GetRenderTarget(sampler, &_old));
				dxe(Engine::Device->d3ddev()->SetRenderTarget(sampler, d3dSurface));
			}
			_set = true;
		}
		catch(...) {
			// Zwolnij powierzchni�
			if(d3dSurface) {
				d3dSurface->Release();
			}
			throw;
		}

		// Zwolnij powierzchni�
		if(d3dSurface) {
			d3dSurface->Release();
		}
	}

	void CubeTarget::end(u32 sampler) {
		// Sprawd� stan
		Assert("invoke begin first" && _set);

		// Ustaw stary cel wy�wietlania
		if(depth()) {
			dxe(Engine::Device->d3ddev()->SetDepthStencilSurface(_old));
		}
		else {
			dxe(Engine::Device->d3ddev()->SetRenderTarget(sampler, _old));
		}

		// Zwolnij powierzchni�
		if(_old) {
			_old->Release();
			_old = nullptr;
		}
		_set = false;
	}
};
