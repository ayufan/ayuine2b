//------------------------------------//
//
// Actor.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Actor: Constructor

	Actor::Actor() {
		// Fields
		Transform = mat4Identity;
		InvTransform = mat4Identity;
		Local = true;
		Destroyable = false;
		Childs = nullptr;
		Parent = nullptr;
		Next = this;
		NextChanged = nullptr;
		NextUpdate = nullptr;
		NextPrepare = nullptr;
		NextInvalidate = nullptr;
		NextVisible = nullptr;

		// Sector Fields
		Visible = u32Max;

		// Bsp Fields
		_frame = 0;
		Changed = true;

		// Flags
		Flags = afUpdate | afTransform;
	}

	//------------------------------------//
	// Actor: Destructor

	Actor::~Actor() {
		// Odepnij aktora z listy
		if(Parent) {
			unlink();
		}

		// Zniszcz wszystkich aktor�w
		if(Flags & afChilds) {
			// Pobierz list� aktor�w
			Actor *next = Childs;
			Actor *curr;

			// Usu� list� aktor�w
			Childs = nullptr;

			// Usu� wszystkich usuwalnych aktor�w
			while(curr = next) {
				// Odepnij aktora
				curr->Parent = nullptr;

				// Pobierz nast�pnego
				next = curr->Next;

				// Czy jest usuwalny?
				if(curr->Destroyable) {
					delete curr;
				}
			}
		}
	}

	//------------------------------------//
	// Actor: Methods

	void Actor::invalidate(const bbox &box, Actor *owner) {
		// Uaktualnij dzieci
		if(Flags & afChilds) {
			for(Actor *curr = Childs; curr; curr = curr->Next) {
				if(curr->Flags & afInvalidate) {
					curr->invalidate(box, owner);
				}
			}
		}
	}

	void Actor::preupdate(f32 dt) {
		mat4 temp;

		// Oblicz transformacje
		mat4::translation(Transform, Origin);
		mat4::rotation(temp, Angles);
		mat4::multiply(Transform, Transform, temp);

		// Dodaj przekszta�cenie rodzica
		if(Local && Parent) {
			mat4::multiply(Transform, Parent->Transform, Transform);
		}

		// Ustaw transformacj� cia�a
		if(Body) {
			Body->Transform = Transform;
			Body->apply();
		}

		// Uaktualnij dzieci
		if(Flags & afChilds) {
			for(Actor *curr = Childs; curr; curr = curr->Next) {
				if(curr->Flags & afUpdate) {
					curr->preupdate(dt);
				}
			}
		}
	}

	void Actor::postupdate(f32 dt) {
		// Sprawd� cia�o fizyczne
		if(Body && Body->State) {
			// Pobierz transformacje obiektu
			Transform = Body->Transform;
			InvTransform = Body->InvTransform;

			// Oblicz pozycj� w �wiecie
			WorldOrigin = Transform.origin();
			WorldAngles = Transform.angles();

			// Oblicz pozycj� w stosunku do rodzica
			if(Parent && Local) {
				mat4 localTransform;

				// Oblicz transformacj� lokaln�
				mat4::multiply(localTransform, Transform, Parent->InvTransform);

				// Oblicz pozycj� w stosunku do obiektu
				Origin = localTransform.origin();
				Angles = localTransform.angles();
			}
			else {
				// Przepisz pozycj�
				Origin = WorldOrigin;
				Angles = WorldAngles;
			}
		}

		f32 diffOrigin = (_oldOrigin - Origin).length();
		f32 diffAngles = (_oldAngles - Angles).length();

		// Sprawd� o ile zmieni�y si� parametry
		if(diffOrigin < 0.01f) {
			Origin = _oldOrigin;
		}
		else {
			_oldOrigin = Origin;
			Changed |= cfArea;
		}

		if(diffAngles < 0.01f) {
			Angles = _oldAngles;
		}
		else {
			_oldAngles = Angles;
			Changed |= cfArea;
		}

		// Sprawd� zmian� obiektu
		if(Changed & cfArea) {
			_oldBox = Box;
		}

		// Uaktualnij dzieci
		if(Flags & afChilds) {
			for(Actor *curr = Childs; curr; curr = curr->Next) {
				if(curr->Flags & afUpdate) {
					curr->postupdate(dt);
				}
			}
		}
	}

	void Actor::prepare(RenderFrame &frame) {
		// Uaktualnij dzieci
		if(Flags & afChilds) {
			for(Actor *curr = Childs; curr; curr = curr->Next) {
				if(curr->Flags & afPrepare) {
					curr->prepare(frame);
				}
			}
		}
	}

	//------------------------------------//
	// Actor: Hierarchy methods

	bool Actor::link(Actor *parent) {
		// Odepnij aktora
		unlink();

		// Sprawd� czy mamy co podpina�
		if(!parent)
			return false;

		// I czy mo�emy podpi�c do tej ga��zi
		Assert(parent->Flags & afChilds);

		// Ustaw rodzica
		Parent = parent;

		// Podepnij aktora
		Next = Parent->Childs;
		Parent->Childs = this;
		return true;
	}

	bool Actor::unlink() {
		// Czy jest ju� podpi�ty?
		if(Parent) {
			for(Actor **curr = &Parent->Childs; *curr; curr = &(*curr)->Next) {
				if(*curr == this) {
					*curr = Next;
					Next = nullptr;
					Parent = nullptr;
					return true;
				}
			}
			Assert(!Parent);
		}
		return false;
	}
};
