//------------------------------------//
//
// Texture.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-9
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// Texture class

	class Texture : public DeviceObject, public Collection<Texture>
	{
		ObjectType(Texture, DeviceObject, 0);

		// Static Fields
	private:
		static string	_extensions[];

		// Fields
	protected: 
		LPDIRECT3DBASETEXTURE9					_texture;
		D3DXIMAGE_INFO									_info;
	public:
		TextureAddress									Address;
		TextureFilter										Filter;

		// Constructor
	public:
		AYUAPI Texture();

		// Destructor
	public:
		AYUAPI virtual ~Texture();

		// Properties
	public:
		AYUAPI const D3DXIMAGE_INFO &info() const;

		// Methods
	public:
		AYUAPI void load(const string &name);
		AYUAPI void unload();
		AYUAPI void bind(u32 sampler);
		AYUAPI void unbind(u32 sampler);

		// DeviceObject Methods
	public:
		AYUAPI virtual void deviceReset(bool theBegin);
		AYUAPI virtual void deviceLost(bool theEnd);

		// Friends
		friend class Device;
	};

	//------------------------------------//
	// TextureTarget class

	class TextureTarget : public Texture
	{
		ObjectType(TextureTarget, Texture, 0);

		// Fields
	private:
		LPDIRECT3DSURFACE9			_old;
		bool										_set;
	public:
		TextureTarget*					Next;

		// Constructor
	public:
		AYUAPI TextureTarget(u32 width, u32 height, D3DFORMAT format = D3DFMT_A8R8G8B8);

		// Properties
	public:
		AYUAPI bool depth() const;
		AYUAPI LPDIRECT3DSURFACE9 surface();

		// Methods
	public:
		AYUAPI void begin(u32 sampler);
		AYUAPI void end(u32 sampler);
		AYUAPI void copy(TextureTarget *from);

		// DeviceObject Methods
	public:
		AYUAPI virtual void deviceReset(bool theBegin);
		AYUAPI virtual void deviceLost(bool theEnd);
	};

	//------------------------------------//
	// CubeTarget class

	class CubeTarget : public Texture
	{
		ObjectType(CubeTarget, Texture, 0);

		// Fields
	private:
		LPDIRECT3DSURFACE9			_old;
		bool										_set;

		// Constructor
	public:
		AYUAPI CubeTarget(u32 edge, D3DFORMAT format = D3DFMT_A8R8G8B8);

		// Properties
	public:
		AYUAPI bool depth() const;
		AYUAPI LPDIRECT3DSURFACE9 surface(CubeMapFace face);

		// Methods
	public:
		AYUAPI void begin(u32 sampler, CubeMapFace face);
		AYUAPI void end(u32 sampler);

		// DeviceObject Methods
	public:
		AYUAPI virtual void deviceReset(bool theBegin);
		AYUAPI virtual void deviceLost(bool theEnd);
	};
};
