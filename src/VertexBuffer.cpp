//------------------------------------//
//
// VertexBuffer.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// VertexBuffer: Constructor

	VertexBuffer::VertexBuffer(size count, size stride, bool dynamic) {
		dGuard {
			_vb = nullptr;
			_dynamic = dynamic;
			_count = count;
			_stride = stride;

			// Zresetuj
			deviceReset(true);
		}
		dUnguard;
	}

	//------------------------------------//
	// VertexBuffer: Destructor
	
	VertexBuffer::~VertexBuffer() {
		dGuard {
			// Zniszcz
			deviceLost(true);
		}
		dUnguard;
	}

	//------------------------------------//
	// VertexBuffer: Properties

	size VertexBuffer::count() const {
		return _count;
	}

	size VertexBuffer::stride() const {
		return _stride;
	}

	bool VertexBuffer::dynamic() const {
		return _dynamic;
	}

	//------------------------------------//
	// VertexBuffer: DeviceObject Methods

	void VertexBuffer::deviceLost(bool theEnd) {
		// Zniszcz bufor wierzchołków
		if(_vb && (theEnd || _dynamic)) {
			_vb->Release();
			_vb = nullptr;
		}
	}

	void VertexBuffer::deviceReset(bool theBegin) {
		// Utwórz bufor wierzchołków
		if(Engine::Device->d3ddev() && (theBegin || _dynamic)) {
			dxe(Engine::Device->d3ddev()->CreateVertexBuffer((UINT)(_count * _stride), _dynamic ? D3DUSAGE_DYNAMIC : 0, 0, _dynamic ? D3DPOOL_DEFAULT : D3DPOOL_MANAGED, &_vb, nullptr));
		}
	}

	//------------------------------------//
	// VertexBuffer: Methods

	void* VertexBuffer::lock(size count, size offset, LockFlags flags) {
		void *dest;
		DWORD d3dFlags;

		// Wyznacz flagi blokowania
		switch(flags) {
			case lfReadOnly:
				d3dFlags = D3DLOCK_READONLY;
				break;

			case lfNoOverwrite:
				d3dFlags = _dynamic ? D3DLOCK_NOOVERWRITE : 0;
				break;

			default:
				d3dFlags = _dynamic ? D3DLOCK_DISCARD : 0;
				break;
		}

		// Zablokuj bufor
		dxe(_vb->Lock((UINT)(offset * _stride), (UINT)(count * _stride), &dest, d3dFlags));
		return dest;
	}

	void VertexBuffer::unlock() {
		// Odblokuj bufor
		dxe(_vb->Unlock());
	}

	size VertexBuffer::update(const void *data, size count, size offset, bool nooverwrite) {
		void *dest;

		// Sprawdz dane wejsciowe
		Assert(data);

		// Zablokuj bufor
		Assert(dest = lock(count, offset, nooverwrite ? lfNoOverwrite : lfNone));
		
			// Skopiuj dane
		memcpy(dest, data, (size_t)(count * _stride));

		// Odblokuj bufor
		unlock();
		return count;
	}

	void VertexBuffer::bind(u32 stream, u32 offset, u32 stride) {
		if(!this)
			return;

		// Podepnij strumień wierzchołków
		Engine::Device->_vertexBuffer[stream][1] = _vb;
		Engine::Device->_vertexOffset[stream][1] = offset;
		Engine::Device->_vertexStride[stream][1] = stride ? stride : _stride;
	}

	void VertexBuffer::unbind(u32 stream) {
		if(!this)
			return;

		// Odepnij strumień wierzchołków
		Engine::Device->_vertexBuffer[stream][1] = nullptr;
		Engine::Device->_vertexOffset[stream][1] = 0;
		Engine::Device->_vertexStride[stream][1] = 0;
	}
};
