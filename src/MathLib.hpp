//------------------------------------//
//
// MathLib.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-02-08
//
//------------------------------------//

#pragma once

/// DOXYS_OFF
//------------------------------------//
// includes

#include <cmath>
#include <cfloat>

//------------------------------------//
// ayuine namespace

namespace ayuine
{
	// Values
	const f64 Pi = 3.14159265358979323846;
	const f64 Sqrt2 = 1.41421356237309504880;
	const f64 MathErr = 0.0001f;

	// Enums
	enum CubeMapFace
	{
		cmfPositiveX,
		cmfNegativeX,
		cmfPositiveY,
		cmfNegativeY,
		cmfPositiveZ,
		cmfNegativeZ,
	};

	enum Side
	{
		sOn,
		sFront,
		sBack,
		sSplit
	};

	enum Axis
	{
		aX,
		aY,
		aZ,
		aNonAxial
	};

	//------------------------------------//

#undef min
#undef max

	template<typename ValueType>
	static ValueType deg2rad(const ValueType degree) {
		return degree * (f32)Pi / 180;
	}
	template<typename ValueType>
	static ValueType rad2deg(const ValueType radians) {
		return radians * 180 / (f32)Pi;
	}
	template<typename ValueType>
	static ValueType min(const ValueType a, const ValueType b) {
		return a < b ? a : b;
	}
	template<typename ValueType>
	static ValueType max(const ValueType a, const ValueType b) {
		return a > b ? a : b;
	}
	template<typename ValueType>
	static ValueType abs(const ValueType v) {
		return v >= 0 ? v : -v;
	}
	template<typename ValueType>
	static ValueType abs(const ValueType v1, const ValueType v2) {
		ValueType v = v1 - v2;
		return v >= 0 ? v : -v;
	}
	template<typename ValueType>
	static ValueType clamp(const ValueType v, const ValueType low = 0, const ValueType high = 1) {
		return v > low ? (v < high ? v : high) : low;
	}
	template<typename ValueType>
	static ValueType sin(const ValueType angle) {
		return (ValueType)::sin(deg2rad(angle));
	}
	template<typename ValueType>
	static ValueType arcSin(const ValueType x) {
		return rad2deg((ValueType)::asin(x));
	}
	template<typename ValueType>
	static ValueType cos(const ValueType angle) {
		return (ValueType)::cos(deg2rad(angle));
	}
	template<typename ValueType>
	static ValueType arcCos(const ValueType x) {
		return rad2deg((ValueType)::acos(x));
	}
	template<typename ValueType>
	static void sincos(const ValueType angle, ValueType &sinValue, ValueType &cosValue) {
		sinValue = (ValueType)::sin(deg2rad(angle));
		cosValue = (ValueType)::cos(deg2rad(angle));
	}
	template<typename ValueType>
	static ValueType tan(const ValueType angle) {
		return (ValueType)::tan(deg2rad(angle));
	}
	template<typename ValueType>
	static ValueType arcTan(const ValueType x) {
		return rad2deg((ValueType)atan(x));
	}
	template<typename ValueType>
	static ValueType arcTan(const ValueType y, const ValueType x) {
		return rad2deg((ValueType)atan2(y, x));
	}
	template<typename ValueType>
	static ValueType floor(const ValueType value) {
		return (ValueType)::floor(value);
	}
	template<typename ValueType>
	static ValueType floor(const ValueType value, const ValueType e) {
		return (ValueType)::floor(value / e) * e;
	}
	template<typename ValueType>
	static ValueType ceil(const ValueType value) {
		return (ValueType)::ceil(value);
	}
	template<typename ValueType>
	static ValueType round(const ValueType value) {
		return (ValueType)::floor(value + 0.5);
	}
	template<typename ValueType>
	static ValueType round(const ValueType value, const ValueType e) {
		return (ValueType)::floor(value / e + 0.5) * e;
	}
	template<typename ValueType>
	static ValueType ceil(const ValueType value, const ValueType e) {
		return (ValueType)::ceil(value / e) * e;
	}
	template<typename ValueType>
	static ValueType trunc(const ValueType value) {
		return value - (s32)(value);
	}
	template<typename ValueType>
	static ValueType pow(const ValueType x, const ValueType y) {
		return (ValueType)::pow(x, y);
	}
	template<typename ValueType>
	static ValueType sq(const ValueType x) {
		return x * x;
	}
	template<typename ValueType>
	static ValueType sqrt(const ValueType x) {
		return (ValueType)::sqrt(x);
	}
	/*
	template<typename ValueType>
	static rect frustumRect(const ValueType fovy, const ValueType aspect, const ValueType _near)
	{
		f32 ymax = _near * (f32)tan(fovy / 2.0f);
		f32 ymin = -ymax;
		f32 xmin = ymin * aspect;
		f32 xmax = ymax * aspect;

		return rect(xmin, ymin, xmax - xmin, ymax - ymin);
	}
	*/

	static bool isPower2(u32 x) {
		return !(x & (x - 1));
	}

	static u32 nextLargest2(u32 x) {
		x |= (x >> 1);
		x |= (x >> 2);
		x |= (x >> 4);
		x |= (x >> 8);
		x |= (x >> 16);
		return x + 1;
	}

	static u32 leastBit(u32 x) {
		return x & ((~x) + 1);
	}

	static u32 mostBit(u32 x) {
		x |= (x >> 1);
		x |= (x >> 2);
		x |= (x >> 4);
		x |= (x >> 8);
		x |= (x >> 16);
		return x & ~(x >> 1);
	}

	static u32 makePower2(u32 x) {
		u32 xx = mostBit(x);
		return x > xx ? (xx << 1) : xx;
	}

	static u32 bitsCount(u32 x) {
		x -= ((x >> 1) & 0x55555555);
		x = (((x >> 2) & 0x33333333) + (x & 0x33333333));
		x = (((x >> 4) + x) & 0x0f0f0f0f);
		x += (x >> 8);
		x += (x >> 16);
		return(x & 0x0000003f);
	}

	static u32 leadingZeroCount(u32 x) {
		x |= (x >> 1);
		x |= (x >> 2);
		x |= (x >> 4);
		x |= (x >> 8);
		x |= (x >> 16);
		return sizeof(x) * 4 - bitsCount(x);
	}

	//------------------------------------//

	class vec2
	{
		// Fields
	public:
		f32		X, Y;

		// Constructors
	public:
		vec2()										{X = 0; Y = 0;}
		vec2(f32 v[])						{X = v[0]; Y = v[1];}
		vec2(f32 _x, f32 _y)	{X = _x; Y = _y;}
		vec2(f32 _y)						{X = _y;	Y = _y;}

		// Operators
	public:
		vec2 &operator += (const vec2 &v) {	X += v.X;	Y += v.Y;	return *this;	}
		vec2 &operator += (const f32 v)	{	X += v;		Y += v;		return *this;	}
		vec2 &operator -= (const vec2 &v) {	X -= v.X;	Y -= v.Y;	return *this;	}
		vec2 &operator -= (const f32 v)	{	X -= v;		Y -= v;		return *this;	}
		vec2 &operator *= (const vec2 &v)	{	X *= v.X;	Y *= v.Y;	return *this; }
		vec2 &operator *= (const f32 v)	{	X *= v;		Y *= v;		return *this; }
		vec2 &operator /= (const vec2 &v) {	X /= v.X;	Y /= v.Y;	return *this;	}
		vec2 &operator /= (const f32 v)	{	X /= v;		Y /= v;		return *this;	}

		// Indexer
	public:
		f32 &operator [](size_t index) {
			dAssert("Out of range" && index < 2);
			return (&X)[index];
		}
		const f32 &operator [](size_t index) const {
			dAssert("Out of range" && index < 2);
			return (&X)[index];
		}

		// Properties
	public:
		f32 lengthSq() const									{	return X * X + Y * Y;	}
		f32 length() const										{	return sqrt(X * X + Y * Y);	}
		bool empty() const											{ return X == 0.0f && Y == 0.0f; }

		// Methods
	public:
		vec2 normalize(f32 &l) const {
			if((l = length()) < MathErr)
				return vec2();

			f32 invLength = 1.0f / l;
			return vec2(X * invLength, Y * invLength);
		}
		vec2 normalize() const {
			f32 temp;
			return normalize(temp);
		}

		// Functions
	public:
		static vec2 lerp(const vec2 &v1, const vec2 &v2, f32 amount = 0.5f) {
			return vec2(
				v1.X * (1.0f - amount) + v2.X * amount,
				v1.Y * (1.0f - amount) + v2.Y * amount);
		}
		static vec2 minimize(const vec2 &v1, const vec2 &v2) {
			return vec2(min(v1.X, v2.X), min(v1.Y, v2.Y));
		}
		static vec2 maximize(const vec2 &v1, const vec2 &v2) {
			return vec2(max(v1.X, v2.X), max(v1.Y, v2.Y));
		}
	};

	//------------------------------------//

	static vec2		operator + (const vec2 &a, const vec2 &b) {	return vec2(a.X + b.X, a.Y + b.Y);	}
	static vec2		operator + (const f32 a, const vec2 &b) {	return vec2(a + b.X, a + b.Y);			}
	static vec2		operator + (const vec2 &a, const f32 b) {	return vec2(a.X + b, a.Y + b);			}
	static vec2		operator - (const f32 a, const vec2 &b) {	return vec2(a - b.X, a - b.Y);			}
	static vec2		operator - (const vec2 &a, const vec2 &b) {	return vec2(a.X - b.X, a.Y - b.Y);	}
	static vec2		operator - (const vec2 &a, const f32 b) {	return vec2(a.X - b, a.Y - b);			}
	static vec2		operator * (const vec2 &a, const vec2 &b) {	return vec2(a.X * b.X, a.Y * b.Y);	}
	static vec2		operator * (const vec2 &a, const f32 b) {	return vec2(a.X * b, a.Y * b);			}
	static vec2		operator * (const f32 a, const vec2 &b) {	return vec2(a * b.X, a * b.Y);			}
	static vec2		operator / (const vec2 &a, const vec2 &b) {	return vec2(a.X / b.X, a.Y / b.Y);	}
	static vec2		operator / (const vec2 &a, const f32 b) {	return vec2(a.X / b, a.Y / b);			}
	static vec2		operator / (const f32 a, const vec2 &b) {	return vec2(a / b.X, a / b.Y);			}
	static vec2		operator % (const vec2 &a, const vec2 &b) {	return vec2(a.Y - b.X, a.X - b.Y);	}
	static f32	operator ^ (const vec2 &a, const vec2 &b)	{	return a.X * b.X + a.Y * b.Y;				}
	static vec2		operator + (const vec2 &v) { return vec2(+v.X, +v.Y);	}
	static vec2		operator - (const vec2 &v) { return vec2(-v.X, -v.Y);	}
	static bool		operator == (const vec2 &a, const vec2 &b){ return abs(a.X, b.X) < MathErr && abs(a.Y, b.Y) < MathErr;		}
	static bool		operator != (const vec2 &a, const vec2 &b){ return a.X != b.X || a.Y != b.Y;		}
	static bool		operator < (const vec2 &a, const vec2 &b){ return a.X - b.X < -MathErr && a.Y - b.Y < -MathErr; }

	//------------------------------------//

	class vec3
	{
		// Fields
	public:
		f32		X, Y, Z;

		// Constructors
	public:
		vec3() { X = 0; Y = 0; Z = 0; }
		vec3(f32 v[]) { X = v[0];	Y = v[1];	Z = v[2];	}
		vec3(f32 _x, f32 _y, f32 _z) { X = _x;	Y = _y;	Z = _z; }
		vec3(f32 _z) { X = _z;	Y = _z;	Z = _z;	}
		vec3(const vec2 &v, f32 _z){	X = v.X; Y = v.Y; Z = _z; }

			// Operators
	public:
		vec3 &operator += (const vec3 &v) { X += v.X;	Y += v.Y; Z += v.Z; return *this; }
		vec3 &operator += (const f32 v)	{ X += v; Y += v; Z += v; return *this; }
		vec3 &operator -= (const vec3 &v) { X -= v.X; Y -= v.Y; Z -= v.Z;	return *this;	}
		vec3 &operator -= (const f32 v)	{	X -= v;	Y -= v; Z -= v;	return *this; }
		vec3 &operator *= (const vec3 &v)	{	X *= v.X;	Y *= v.Y; Z *= v.Z;	return *this;	}
		vec3 &operator *= (const f32 v)	{	X *= v;	Y *= v; Z *= v;	return *this;	}
		vec3 &operator /= (const vec3 &v) {	X /= v.X;	Y /= v.Y; Z /= v.Z;	return *this;	}
		vec3 &operator /= (const f32 v)	{	X /= v;	Y /= v; Z /= v;	return *this;	}

		// Indexer
	public:
		f32 &operator [](size_t index)	{
			dAssert("Out of range" && index < 3);
			return (&X)[index];
		}
		const f32 &operator [](size_t index) const	{
			dAssert("Out of range" && index < 3);
			return (&X)[index];
		}

		// Properties
	public:
		f32 lengthSq() const {	return X * X + Y * Y + Z * Z;	}
		f32 length() const {	return sqrt(X * X + Y * Y + Z * Z); }
		bool empty() const { return X == 0.0f && Y == 0.0f && Z == 0.0f; }

		// Methods
	public:
		vec3 normalize(f32 &l) const {
			if((l = length()) < MathErr)
				return vec3();

			f32 invLength = 1.0f / l;
			return vec3(X * invLength, Y * invLength, Z * invLength);
		}
		vec3 normalize() const {
			f32 temp;
			return normalize(temp);
		}
		vec3 rotateX(f32 angle) const {
			vec3 value;
			f32 s, c;

			sincos(angle, s, c);

			value.X = X;
			value.Y = Y * c - Z * s;
			value.Z = Y * s + Z * c;

			return value;
		}
		vec3 rotateY(f32 angle) const {
			vec3 value;
			f32 s, c;

			sincos(angle, s, c);

			value.X = Z * c - X * s;
			value.Y = Y;
			value.Z = Z * s + X * c;

			return value;
		}
		vec3 rotateZ(f32 angle) const {
			vec3 value;
			f32 s, c;

			sincos(angle, s, c);

			value.X = X * c - Y * s;
			value.Y = X * s + Y * c;
			value.Z = Z;

			return value;
		}
		vec3 orthogonalize(const vec3 &v) const;
		void normalVectors(vec3 &right) const;
		void normalVectors(vec3 &right, vec3 &up) const;
		void axis(vec3 &xv, vec3 &yv) const;
		vec3 reflect(const vec3 &normal) const;
		vec3 round(f32 ep) const {
			vec3 v;

			v.X = ayuine::round(X, ep);
			v.Y = ayuine::round(Y, ep);
			v.Z = ayuine::round(Z, ep);

			return v;
		}
		// Functions
	public:
		static vec3 polar(f32 yaw, f32 pitch) {
			vec3 v;

			v.X = cos(yaw) * cos(pitch);
			v.Y = sin(yaw) * cos(pitch);
			v.Z = sin(pitch);

			return v;
		}
		static vec3 lerp(const vec3 &v1, const vec3 &v2, f32 amount = 0.5f) {
			return vec3(
				v1.X * (1.0f - amount) + v2.X * amount,
				v1.Y * (1.0f - amount) + v2.Y * amount,
				v1.Z * (1.0f - amount) + v2.Z * amount);
		}
		static vec3 minimize(const vec3 &v1, const vec3 &v2) {
			return vec3(min(v1.X, v2.X), min(v1.Y, v2.Y), min(v1.Z, v2.Z));
		}
		static vec3 maximize(const vec3 &v1, const vec3 &v2) {
			return vec3(max(v1.X, v2.X), max(v1.Y, v2.Y), max(v1.Z, v2.Z));
		}
		static vec3 cubeVector(CubeMapFace face, f32 s, f32 t)
		{
			f32 sc = s * 2.0f - 1.0f;
			f32 tc = t * 2.0f - 1.0f;

			switch (face)
			{
				case cmfPositiveX:
					return vec3(1.0f, -tc, -sc);

				case cmfNegativeX:
					return vec3(-1.0f, -tc, sc);

				case cmfPositiveY:
					return vec3(sc, 1.0f, tc);

				case cmfNegativeY:
					return vec3(sc, -1.0f, -tc);

				case cmfPositiveZ:
					return vec3(sc, -tc, 1.0f);

				case cmfNegativeZ:
					return vec3(-sc, -tc, -1.0f);

				default:
					Assert("Invalid CubeMapFace" && false);
			}
		}
	};

	//------------------------------------//

	static vec3 operator + (const vec3 &a, const vec3 &b) { return vec3(a.X + b.X, a.Y + b.Y, a.Z + b.Z); }
	static vec3 operator + (const f32 a, const vec3 &b) {	return vec3(a + b.X, a + b.Y, a + b.Z); }
	static vec3 operator + (const vec3 &a, const f32 b) { return vec3(a.X + b, a.Y + b, a.Z + b);	}
	static vec3 operator - (const f32 a, const vec3 &b) {	return vec3(a - b.X, a - b.Y, a - b.Z); }
	static vec3 operator - (const vec3 &a, const vec3 &b) {	return vec3(a.X - b.X, a.Y - b.Y, a.Z - b.Z); }
	static vec3 operator - (const vec3 &a, const f32 b) {	return vec3(a.X - b, a.Y - b, a.Z - b); }
	static vec3 operator * (const vec3 &a, const vec3 &b) {	return vec3(a.X * b.X, a.Y * b.Y, a.Z * b.Z); }
	static vec3 operator * (const vec3 &a, const f32 b) {	return vec3(a.X * b, a.Y * b, a.Z * b); }
	static vec3 operator * (const f32 a, const vec3 &b) {	return vec3(a * b.X, a * b.Y, a * b.Z); }
	static vec3 operator / (const vec3 &a, const vec3 &b) { return vec3(a.X / b.X, a.Y / b.Y, a.Z / b.Z); }
	static vec3 operator / (const vec3 &a, const f32 b) {	return vec3(a.X / b, a.Y / b, a.Z / b); }
	static vec3 operator / (const f32 a, const vec3 &b) {	return vec3(a / b.X, a / b.Y, a / b.Z); }
	static vec3 operator % (const vec3 &a, const vec3 &b) {	return vec3(a.Y * b.Z - b.Y * a.Z, a.Z * b.X - b.Z * a.X, a.X * b.Y - b.X * a.Y); }
	static f32 operator ^ (const vec3 &a, const vec3 &b) { return a.X * b.X + a.Y * b.Y + a.Z * b.Z; }
	static vec3 operator + (const vec3 &v) { return vec3(+v.X, +v.Y, +v.Z); }
	static vec3 operator - (const vec3 &v) { return vec3(-v.X, -v.Y, -v.Z); }
	static bool operator == (const vec3 &a, const vec3 &b) { return abs(a.X, b.X) < MathErr && abs(a.Y, b.Y) < MathErr && abs(a.Z, b.Z) < MathErr; }
	static bool operator != (const vec3 &a, const vec3 &b) { return a.X != b.X || a.Y != b.Y || a.Z != b.Z; }
	static bool operator < (const vec3 &a, const vec3 &b){ return a.X - b.X < -MathErr && a.Y - b.Y < -MathErr && a.Z - b.Z < -MathErr; }

	//------------------------------------//

	inline vec3 vec3::orthogonalize(const vec3 &v) const {
		return (*this) - v * (v ^ (*this));
	}

	inline void vec3::normalVectors(vec3 &right) const {
		right = vec3(Z, -X, Y).orthogonalize(*this);
	}

	inline void vec3::normalVectors(vec3 &right, vec3 &up) const {
		right = vec3(Z, -X, Y).orthogonalize(*this);
		up = right % *this;
	}

	inline void vec3::axis(vec3 &xv, vec3 &yv) const {
		static vec3 baseAxis[][3] = {
			{vec3(0,0,1), vec3(1,0,0),vec3(0,-1,0)},		// Floor
			{vec3(0,0,-1), vec3(1,0,0),vec3(0,-1,0)},	// Ceiling
			{vec3(1,0,0), vec3(0,1,0),vec3(0,0,-1)},		// West wall
			{vec3(-1,0,0), vec3(0,1,0),vec3(0,0,-1)},	// East wall
			{vec3(0,1,0), vec3(1,0,0),vec3(0,0,-1)},		// South wall
			{vec3(0,-1,0), vec3(1,0,0),vec3(0,0,-1)}		// North wall
		};

		f32 best = 0;
		s32 axis = 0;

		for (s32 i = 0; i < CountOf(baseAxis); i++)
		{
			f32 dot = *this ^ baseAxis[i][0];
			if (dot > best)
			{
				best = dot;
				axis = i;
			}
		}

		xv = baseAxis[axis][1];
		yv = baseAxis[axis][2];
	}

	inline vec3 vec3::reflect(const vec3 &normal) const {
		return *this - (2 * (*this ^ normal)) * normal;
	}

	//------------------------------------//

	class vec4
	{
		// Fields
	public:
		f32		X, Y, Z, W;

		// Constructors
	public:
		vec4() { X = 0; Y = 0; Z = 0; W = 0; }
		vec4(f32 v[])	{ X = v[0]; Y = v[1]; Z = v[2]; W = v[3]; }
		vec4(f32 _x, f32 _y, f32 _z, f32 _w) { X = _x; Y = _y; Z = _z; W = _w; }
		vec4(f32 _w) { X = _w; Y = _w; Z = _w; W = _w; }
		vec4(const vec3 &a, const vec3 &b, const vec3 &c) { normal() = (b - a) % (c - a); W = -(normal() ^ a); }
		vec4(const vec3 &n, const vec3 &origin) { normal() = n; W = -(normal() ^ origin); }
		vec4(const vec2 &v, f32 _z, f32 _w) { X = v.X; Y = v.Y; Z = _z; W = _w; }
		vec4(const vec3 &v, f32 _w) { X = v.X; Y = v.Y; Z = v.Z; W = _w; }
		vec4(const vec4 &plane, const vec3 &a, const vec3 &b) {
			normal() = ((a - b) % plane.normal()).normalize();
			W = -(normal() ^ a);
		}

		// Operators
	public:
		vec4 &operator += (const vec4 &v) { X += v.X; Y += v.Y; Z += v.Z; W += v.W; return *this; }
		vec4 &operator += (const f32 v)	{ X += v; Y += v; Z += v; W += v;	return *this; }
		vec4 &operator -= (const vec4 &v) { X -= v.X;	Y -= v.Y; Z -= v.Z; W -= v.W;	return *this; }
		vec4 &operator -= (const f32 v)	{ X -= v;	Y -= v; Z -= v; W -= v;	return *this; }
		vec4 &operator *= (const vec4 &v)	{	X *= v.X;	Y *= v.Y; Z *= v.Z; W *= v.W;	return *this;	}
		vec4 &operator *= (const f32 v)	{	X *= v;	Y *= v; Z *= v; W *= v;	return *this; }
		vec4 &operator /= (const vec4 &v) {	X /= v.X;	Y /= v.Y; Z /= v.Z; W /= v.W;	return *this; }
		vec4 &operator /= (const f32 v)	{	X /= v;	Y /= v; Z /= v; W /= v;	return *this; }

		// Indexer
	public:
		f32 &operator [](size_t index)	{
			dAssert("Out of range" && index < 4);
			return (&X)[index];
		}
		const f32 &operator [](size_t index) const	{
			dAssert("Out of range" && index < 4);
			return (&X)[index];
		}

		// Properties
	public:
		vec3 &normal() { return *(vec3*)&X; }
		const vec3 &normal() const { return *(const vec3*)&X; }
		f32 lengthSq() const { return X * X + Y * Y + Z * Z + W * W; }
		f32 length() const { return sqrt(X * X + Y * Y + Z * Z + W * W); }
		bool empty() const { return X == 0.0f && Y == 0.0f && Z == 0.0f && W == 0.0f; }

		// Methods
	public:
		vec4 normalize(f32 &l) const {
			if((l = length()) < MathErr)
				return vec4();

			f32 invLength = 1.0f / l;
			return vec4(X * invLength, Y * invLength, Z * invLength, W * invLength);
		}
		vec4 normalize() const {
			f32 temp;
			return normalize(temp);
		}
		vec4 normalizeNormal(f32 &l) const {
			if((l = normal().length()) < MathErr)
				return vec4();

			f32 invLength = 1.0f / l;
			return vec4(X * invLength, Y * invLength, Z * invLength, W * invLength);
		}
		vec4 normalizeNormal() const {
			f32 temp;
			return normalizeNormal(temp);
		}
		Side test(const vec3 &v, f32 *distance = nullptr) const {
			f32 d = (normal() ^ v) + W;

			if(distance != nullptr)
				*distance = d;

			if(d < -MathErr)	return sBack;
			if(d >  MathErr)	return sFront;
			return sOn;
		}
		Side test(const vec3 *v, u32 count) const {
			u32 result = 0;

			for(u32 i = 0; i < count && result != sSplit; i++, v++) 
				result |= test(*v);
			return (Side)result;
		}
		AYUAPI Side test(const class bsphere &sphere) const;
		AYUAPI Side test(const class bbox &box) const;
		vec3 reflect(const vec3 &v) const {
			return normal().reflect(v);
		}
		vec4 prependicular(const vec3 &a, const vec3 &b) {
			return vec4(((a - b) % normal()).normalize(), a);
		}
		/*
		public f32? Collide(RayObject ray, bool doubleSided)
		{
			f32 a = ray.Direction ^ Normal;
			if(a == 0 || a > 0 && !doubleSided)
				return null;

			return -(this ^ ray.Origin) / a;
		}*/

		// Functions
	public:
		static vec4 lerp(const vec4 &v1, const vec4 &v2, f32 amount = 0.5f) {
			return vec4(
				v1.X * (1.0f - amount) + v2.X * amount,
				v1.Y * (1.0f - amount) + v2.Y * amount,
				v1.Z * (1.0f - amount) + v2.Z * amount,
				v1.W * (1.0f - amount) + v2.W * amount);
		}
		static vec4 minimize(const vec4 &v1, const vec4 &v2) {
			return vec4(min(v1.X, v2.X), min(v1.Y, v2.Y), min(v1.Z, v2.Z), min(v1.W, v2.W));
		}
		static vec4 maximize(const vec4 &v1, const vec4 &v2) {
			return vec4(max(v1.X, v2.X), max(v1.Y, v2.Y), max(v1.Z, v2.Z), max(v1.W, v2.W));
		}
		static u32 compress(const vec4 &value) {
			u32 sum = 0;

			sum |= clamp<u32>(u32((value.X * 0.5f + 0.5f) * 255.0f), 0, 0xFF) << 16;
			sum |= clamp<u32>(u32((value.Y * 0.5f + 0.5f) * 255.0f), 0, 0xFF) <<  8;
			sum |= clamp<u32>(u32((value.Z * 0.5f + 0.5f) * 255.0f), 0, 0xFF) <<  0;
			sum |= clamp<u32>(u32((value.W * 0.5f + 0.5f) * 255.0f), 0, 0xFF) << 24;

			return sum;
		}
		static vec4 uncompress(u32 value) {
			f32 x, y, z, w;

			x = f32(((value >> 16) & 0xFF) * 2 - 0xFF) / 255.0f;
			y = f32(((value >>  8) & 0xFF) * 2 - 0xFF) / 255.0f;
			z = f32(((value >>  0) & 0xFF) * 2 - 0xFF) / 255.0f;
			w = f32(((value >> 24) & 0xFF) * 2 - 0xFF) / 255.0f;

			return vec4(x, y, z, w);
		}
	};

	//------------------------------------//

	static vec4 operator + (const vec4 &a, const vec4 &b) { return vec4(a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.W + b.W); }
	static vec4 operator + (const f32 a, const vec4 &b) { return vec4(a + b.X, a + b.Y, a + b.Z, a + b.W); }
	static vec4 operator + (const vec4 &a, const f32 b) { return vec4(a.X + b, a.Y + b, a.Z + b, a.W + b); }
	static vec4 operator - (const f32 a, const vec4 &b) { return vec4(a - b.X, a - b.Y, a - b.Z, a - b.W); }
	static vec4 operator - (const vec4 &a, const vec4 &b) { return vec4(a.X - b.X, a.Y - b.Y, a.Z - b.Z, a.W - b.W); }
	static vec4 operator - (const vec4 &a, const f32 b) { return vec4(a.X - b, a.Y - b, a.Z - b, a.W - b); }
	static vec4 operator * (const vec4 &a, const vec4 &b) { return vec4(a.X * b.X, a.Y * b.Y, a.Z * b.Z, a.W * b.W); }
	static vec4 operator * (const vec4 &a, const f32 b) { return vec4(a.X * b, a.Y * b, a.Z * b, a.W * b); }
	static vec4 operator * (const f32 a, const vec4 &b) { return vec4(a * b.X, a * b.Y, a * b.Z, a * b.W); }
	static vec4 operator / (const vec4 &a, const vec4 &b) { return vec4(a.X / b.X, a.Y / b.Y, a.Z / b.Z, a.W / b.W); }
	static vec4 operator / (const vec4 &a, const f32 b) { return vec4(a.X / b, a.Y / b, a.Z / b, a.W / b); }
	static vec4 operator / (const f32 a, const vec4 &b) { return vec4(a / b.X, a / b.Y, a / b.Z, a / b.W); }
	static f32 operator ^ (const vec4 &a, const vec4 &b) { return a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W; }
	static f32 operator ^ (const vec4 &a, const vec3 &b) { return a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * 1.0f; }
	static f32 operator % (const vec4 &a, const vec3 &b) { return a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * 0.0f; }
	static vec4 operator + (const vec4 &v) { return vec4(+v.X, +v.Y, +v.Z, +v.W); }
	static vec4 operator - (const vec4 &v) { return vec4(-v.X, -v.Y, -v.Z, -v.W); }
	static bool operator == (const vec4 &a, const vec4 &b) { return abs(a.X, b.X) < MathErr && abs(a.Y, b.Y) < MathErr && abs(a.Z, b.Z) < MathErr && abs(a.W, b.W) < MathErr; }
	static bool operator != (const vec4 &a, const vec4 &b) { return a.X != b.X || a.Y != b.Y || a.Z != b.Z || a.W != b.W; }
	static bool operator < (const vec4 &a, const vec4 &b){ return a.X - b.X < -MathErr && a.Y - b.Y < -MathErr && a.Z - b.Z < -MathErr && a.W - b.W < -MathErr; }

	//------------------------------------//

	class rect
	{
		// Fields
	public:
		f32 Left, Right, Top, Bottom;

		// Constructor
	public:
		rect() {
			Left = Top = FLT_MAX;
			Right = Bottom = -FLT_MAX;
		}
		rect(f32 width, f32 height) {
			Left = Top = 0;
			Right = width;
			Bottom = height;
		}
		rect(f32 left, f32 top, f32 width, f32 height) {
			Left = left;
			Right = left + width;
			Top = top;
			Bottom = top + height;
		}
		rect(const vec2 &min, const vec2 &max) {
			Left = min.X;
			Top = min.Y;
			Right = max.X;
			Bottom = max.Y;
		}

		// Properties
	public:
		f32 width() const {
			return Right - Left;
		}
		f32 height() const {
			return Bottom - Top;
		}
		bool empty() const {
			return Right <= Left || Bottom <= Top;
		}

		// Methods
	public:
		rect intersect(const rect &r) const {
			rect o;
			
			o.Left = max(Left, r.Left);
			o.Top = max(Top, r.Top);
			o.Right = min(Right, r.Right);
			o.Bottom = min(Bottom, r.Bottom);
			return o;
		}
		rect merge(const rect &r) const {
			rect o;
			
			o.Left = min(Left, r.Left);
			o.Top = min(Top, r.Top);
			o.Right = max(Right, r.Right);
			o.Bottom = max(Bottom, r.Bottom);
			return o;
		}
	};

	//------------------------------------//

	static bool operator == (const rect &a, const rect &b) {
		return a.Left == b.Left && a.Right == b.Right && a.Top == b.Top && a.Bottom == b.Bottom;
	}
	static bool operator != (const rect &a, const rect &b) {
		return a.Left != b.Left || a.Right != b.Right || a.Top != b.Top || a.Bottom != b.Bottom;
	}

	//------------------------------------//

	class angles : public vec3
	{
		// Properties
	public:
		f32 &yaw() { return X; }
		f32 &pitch() { return Y; }
		f32 &roll() { return Z;}
		const f32 &yaw() const { return X; }
		const f32 &pitch() const { return Y; }
		const f32 &roll() const { return Z;}

		// Constructor
	public:
		angles() {
		}
		angles(f32 yaw, f32 pitch, f32 roll) : vec3(yaw, pitch, roll) {
		}
		angles(const vec3 &v) : vec3(v) {
		}
		AYUAPI angles(const vec3 &dir, f32 r);
		AYUAPI angles(const vec3 &dir, const vec3 &up);

		// Properties
	public:
		AYUAPI vec3 at() const;
		AYUAPI vec3 right() const;
		AYUAPI vec3 up() const;
		AYUAPI vec3 rotate(const vec3 &in) const;
		AYUAPI vec3 rotate(const vec3 &in, const vec3 &around) const;
	};

	//------------------------------------//

	class mat4
	{
		// Fields
	public:
		union {
			struct {
				f32 m11, m12, m13, m14;
				f32 m21, m22, m23, m24;
				f32 m31, m32, m33, m34;
				f32 m41, m42, m43, m44;

				//f32 m11, m21, m31, m41;
				//f32 m12, m22, m32, m42;
				//f32 m13, m23, m33, m43;
				//f32 m14, m24, m34, m44;
			};
			f32 m[4][4];
		};

		// Constructor
	public:
		mat4() {
			m11 = 0; m21 = 0; m31 = 0; m41 = 0;
			m12 = 0; m22 = 0; m32 = 0; m42 = 0;
			m13 = 0; m23 = 0; m33 = 0; m43 = 0;
			m14 = 0; m24 = 0; m34 = 0; m44 = 0;
		}
		AYUAPI mat4(	
			f32 a11, f32 a21, f32 a31, f32 a41,
			f32 a12, f32 a22, f32 a32, f32 a42,
			f32 a13, f32 a23, f32 a33, f32 a43,
			f32 a14, f32 a24, f32 a34, f32 a44);
		AYUAPI mat4(	
			f32 a11, f32 a21, f32 a31,
			f32 a12, f32 a22, f32 a32,
			f32 a13, f32 a23, f32 a33);

		// Operators
	public:
		vec4 &operator [] (size_t index) {
			dAssert("Out of range" && index >= 5);
			return (vec4&)(&m11)[index * 4];
		}
		const vec4 &operator [] (size_t index) const {
			dAssert("Out of range" && index >= 5);
			return (const vec4&)(&m11)[index * 4];
		}

		// Properties
	public:
		AYUAPI vec4 column(u32 index) const;
		AYUAPI vec4 row(u32 index) const;
		AYUAPI void column(u32 index, const vec4 &value);
		AYUAPI void row(u32 index, const vec4 &value);

		AYUAPI vec3 origin() const;
		AYUAPI vec3 originInv() const;
		AYUAPI angles angles() const;
		AYUAPI vec3 axis(u32 i) const;
		AYUAPI vec3 scale() const;
		AYUAPI void scale(const vec3 &scale, bool origin = true);

		bool empty() const {
			for(s32 i = 0; i < 16; i++) {
				if((&m11)[i] != 0.0f)
					return false;
			}
			return true;
		}
		mat4 inner() const {
			return mat4(
				m11, m21, m31, 0,
				m12, m22, m32, 0,
				m13, m23, m33, 0,
				0,   0,   0,   1);
		}

		// Methods
	public:
		AYUAPI vec4 transform(const vec4 &i) const;
		AYUAPI vec4 transform(const vec3 &i) const;
		AYUAPI vec3 transformCoord(const vec3 &i) const;
		AYUAPI vec3 transformNormal(const vec3 &i) const;
		AYUAPI vec4 transformPlane(const vec4 &i) const;

		// Array Methods
	public:
		void transform(vec4 *o, const vec4 *i, size_t count) const {
			while(count-- > 0)
				*o++ = transform(*i++);
		}
		void transform(vec4 *o, const vec3 *i, size_t count) const {
			while(count-- > 0)
				*o++ = transform(*i++);
		}
		void transformCoord(vec3 *o, const vec3 *i, size_t count) const {
			while(count-- > 0)
				*o++ = transformCoord(*i++);
		}
		void transformNormal(vec3 *o, const vec3 *i, size_t count) const {
			while(count-- > 0)
				*o++ = transformNormal(*i++);
		}
		void transformPlane(vec4 *o, const vec4 *i, size_t count) const {
			while(count-- > 0)
				*o++ = transformPlane(*i++);
		}

		// Functions
	public:
		AYUAPI static void lookAt(mat4 &m, const vec3 &eye, const vec3 &at, const vec3 &up);
		AYUAPI static void frustum(mat4 &m, const rect &bounds, f32 _near, f32 _far);
		AYUAPI static void perspective(mat4 &m, f32 fovy, f32 aspect, f32 _near, f32 _far);
		AYUAPI static void clip(mat4 &m, const vec4 &plane);
		AYUAPI static void ortho(mat4 &m, const rect &bounds, f32 _near, f32 _far);
		AYUAPI static void reflection(mat4 &m, const vec4 &plane);
		AYUAPI static void multiply(mat4 &m, const mat4 &a, const mat4 &b);
		AYUAPI static void firstPersonCamera(mat4 &m, const vec3 &o, const ayuine::angles &a, f32 z = 1.0f);
		AYUAPI static void modelViewCamera(mat4 &m, const vec3 &o, const ayuine::angles &a, f32 z = 1.0f);
		AYUAPI static void fromCols(mat4 &m, const vec4 &col1, const vec4 &col2, const vec4 &col3, const vec4 &col4);
		AYUAPI static void fromRows(mat4 &m, const vec4 &row1, const vec4 &row2, const vec4 &row3, const vec4 &row4);
		AYUAPI static void scaling(mat4 &m, const vec3 &scale);
		AYUAPI static void translation(mat4 &m, const vec3 &value);
		AYUAPI static void rotation(mat4 &m, const ayuine::angles &a);
		AYUAPI static void rotation(mat4 &m, const class quat &q);
		AYUAPI static void invert(mat4 &m, const mat4 &i);
		AYUAPI static void transpose(mat4 &m, const mat4 &i);
		AYUAPI static void transpose(mat4 &m);
	};

	//------------------------------------//

	static bool operator == (const mat4 &a, const mat4 &b) {
		for(u32 i = 0; i < 4; i++)
			for(u32 j = 0; j < 4; j++)
				if(abs(a.m[i][j] - b.m[i][j]) > MathErr)
					return false;
		return true;
	}

	static bool operator != (const mat4 &a, const mat4 &b) {
		for(u32 i = 0; i < 4; i++)
			for(u32 j = 0; j < 4; j++)
				if(abs(a.m[i][j] - b.m[i][j]) > MathErr)
					return true;
		return false;
	}

	//------------------------------------//

	class quat
	{
		// Fields
	public:
		f32 W, X, Y, Z;

		// Constructor
	public:
		quat() {
			W = X = Y = Z = 0;
		}
		quat(f32 w, f32 x, f32 y, f32 z) {
			W = w;
			X = x;
			Y = y;
			Z = z;
		}
		AYUAPI quat(f32 x, f32 y, f32 z);
		AYUAPI quat(const vec3 &axis, f32 angle);
		AYUAPI quat(const mat4 &m);
		AYUAPI quat(const vec3 &at, const vec3 &up);
		AYUAPI quat(const angles &a);

		// Properties
	public:
		bool empty() const {
			return W == 0.0f && X == 0.0f && Y == 0.0f && Z == 0.0f;
		}
		vec3 axis() const {
			return ((vec3&)X).normalize();
		}
		f32 angle() const {
			return arcCos(W) * 2.0f;
		}
		f32 length() const {
			return ((vec4&)W).length();
		}
		f32 lengthSq() const {
			return ((vec4&)W).lengthSq();
		}

		// Methods
	public:
		AYUAPI quat normalize() const;
		AYUAPI quat slerp(const quat &a, const quat &b, f32 time = 0.5f);
	};

	//------------------------------------//

	static quat operator + (const quat &a, const quat &b) {
		return quat(	a.W * b.W - a.X * b.X - a.Y * b.Y - a.Z * b.Z,
									b.Y * a.Z - b.Z * a.Y + a.X * b.W + b.X * a.W,
									b.Z * a.X - b.X * a.Z + a.Y * b.W + b.Y * a.W,
									b.X * a.Y - b.Y * a.X + a.Z * b.W + b.Z * a.W);
	}
	static quat operator * (const quat &a, const quat &b) {
		return quat(	a.W * b.W - a.X * b.X - a.Y * b.Y - a.Z * b.Z,
									a.W * b.X + a.X * b.W + a.Y * b.Z - a.Z * b.Y,
									a.W * b.Y + a.Y * b.W + a.Z * b.X - a.X * b.Z,
									a.W * b.Z + a.Z * b.W + a.X * b.Y - a.Y * b.X);
	}
	static f32 operator ^ (const quat &a, const quat &b) { return a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W; }
	static quat operator + (const quat &q) { return q; }
	static quat operator - (const quat &q) { return quat(-q.W, -q.X, -q.Y, -q.Z); }
	static bool operator == (const quat &a, const quat &b) { return a.X == b.X && a.Y == b.Y && a.Z == b.Z && a.W == b.W; }
	static bool operator != (const quat &a, const quat &b) { return a.X != b.X || a.Y != b.Y || a.Z != b.Z || a.W != b.W; }

	//------------------------------------//

	class ray
	{
		// Fields
	public:
		vec3 Origin, At;

		// Constructor
	public:
		ray() {
		}
		ray(const vec3 &origin, const vec3 &at) {
			assign(origin, at);
		}

		// Methods
	public:
		AYUAPI void assign(const vec3 &origin, const vec3 &at);
		AYUAPI ray transform(const mat4 &m) const;
		AYUAPI vec3 hit(f32 time) const;
	};

	//------------------------------------//

	static bool operator == (const ray &a, const ray &b) {
		return a.Origin == b.Origin && a.At == b.At;
	}
	static bool operator != (const ray &a, const ray &b) {
		return a.Origin != b.Origin || a.At != b.At;
	}

	//------------------------------------//

	class bsphere
	{
		// Fields
	public:
		vec3		Center;
		f32			Radius;

		// Properties
	public:
		bool empty() const {
			return Radius < MathErr;
		}
		vec3 mins() const {
			return Center - vec3(Radius);
		}
		vec3 maxs() const {
			return Center + vec3(Radius);
		}

		// Constructor
	public:
		bsphere() {
		}
		bsphere(const vec3 &center, f32 radius) {
			assign(center, radius);
		}
		bsphere(const vec3 *v, u32 count, u32 stride = sizeof(vec3)) {
			assign(v, count, stride);
		}

		// Methods
	public:
		AYUAPI void assign(const vec3 &center, f32 radius);
		AYUAPI void assign(const vec3 *v, u32 count, u32 stride = sizeof(vec3));
		AYUAPI void assign(const Array<vec3> &verts);
		AYUAPI bsphere transform(const mat4 &m) const;
		AYUAPI bsphere scale(const vec3 &scale) const;
		AYUAPI bool test(const vec3 &v) const;
		AYUAPI bool test(const bsphere &s) const;
		AYUAPI f32 collide(const ray &ray) const;
	};

	//------------------------------------//

	static bool operator == (const bsphere &a, const bsphere &b) {
		return a.Center == b.Center && a.Radius == b.Radius;
	}
	static bool operator != (const bsphere &a, const bsphere &b) {
		return a.Center != b.Center || a.Radius != b.Radius;
	}

	//------------------------------------//

	class bbox
	{
		// Static Fields
	public:
		AYUAPI static const u32 Quads[6][4];
		AYUAPI static const u32 Edges[12][2];

		// Fields
	public:
		vec3		Mins, Maxs;

		// Constructor
	public:
		bbox() {
		}
		bbox(const vec3 &mins, const vec3 &maxs) {
			assign(mins, maxs);
		}
		bbox(const vec3 &origin, f32 radius) {
			assign(origin - radius, origin + radius);
		}
		bbox(const vec3 *v, u32 count, u32 stride = sizeof(vec3)) {
			assign(v, count, stride);
		}

		// Properties
	public:
		bool empty() const {
			return Maxs.X < Mins.X || Maxs.Y < Mins.Y || Maxs.Z < Mins.Z;
		}
		vec3 center() const {
			return (Mins + Maxs) * 0.5f;
		}
		f32 radius() const {
			return (Maxs - Mins).length() * 0.5f;
		}

		// Methods
	public:
		AYUAPI void assign(const vec3 &mins, const vec3 &maxs);
		AYUAPI void assign(const vec3 *v, u32 count, u32 stride = sizeof(vec3));
		AYUAPI void assign(const Array<vec3> &verts);
		AYUAPI void add(const vec3 &v);
		AYUAPI void add(const vec3 *v, size_t count);
		AYUAPI void clear();
		AYUAPI bbox intersect(const bbox &box) const;
		AYUAPI bbox merge(const bbox &box) const;
		AYUAPI void vertexes(vec3 vertexes[8]) const;
		AYUAPI void planes(vec4 planes[6]) const;
		AYUAPI bbox transform(const mat4 &m) const;
		AYUAPI bool test(const vec3 &v) const;
		AYUAPI bool test(const bbox &box) const;
		AYUAPI bool test(const bsphere &sphere) const;
		AYUAPI f32 collide(const ray &ray) const;
	};

	//------------------------------------//

	static bool operator == (const bbox &a, const bbox &b) {
		return a.Mins == b.Mins && a.Maxs == b.Maxs;
	}
	static bool operator != (const bbox &a, const bbox &b) {
		return a.Mins != b.Mins || a.Maxs != b.Maxs;
	}

	//------------------------------------//

	class tangentVectors
	{
		// Methods
	public:
		static void calculateVectors(const vec3 &v1, const vec3 &v2, const vec3 &v3, const vec2 &st1, const vec2 &st2, const vec2 &st3, vec3 vectors[3]) {
			vec3 p = v2 - v1;
			vec3 q = v3 - v1;
			vec2 s = st2 - st1;
			vec2 t = st3 - st1;

			// Oblicz wektory
			vectors[0] = (p * t.Y - q * t.X).normalize();
			vectors[1] = (p * s.X - q * s.Y).normalize();
			vectors[2] = (p % q).normalize();
		}
		static vec3 calculateTangent(const vec3 &normal, const vec3 vectors[3]) {
			return vectors[0].orthogonalize(normal).normalize();
		}
		static vec3 calculateBinormal(const vec3 &normal, const vec3 vectors[3]) {
			return vectors[1].orthogonalize(normal).normalize();
		}
		static vec4 calculateTangentFlag(const vec3 &normal, const vec3 vectors[3]) {
			vec3 newTangent, newBinormal;
			f32 result;

			// Oblicz wektory
			newTangent = calculateTangent(normal, vectors);
			newBinormal = calculateBinormal(normal, vectors);

			// Oblicz flag� binormala
			result = newBinormal ^ (normal % newTangent);
			return vec4(newTangent, (result >= 0.0f ? 1.0f : -1.0f));
		}
	};

	template<typename Index, typename Vertex>
	static Array<Index> optimize(Array<Vertex> &vertices) {
		// Sprawd� dane
		if(vertices.empty())
			return Array<Index>();

		// Indeksy
		Array<Index> order(vertices.size());
		Array<Index> imap(vertices.size());
		Array<Index> vmap(vertices.size());
		static void* optimizeVertexTable = nullptr;
		Vertex prev, curr;
		u32 index = 0;

		// Zapisz tablic� wierzcho�k�w
		optimizeVertexTable = &vertices.at(0);

		// Wype�nij tablic� indeks�w rosn�cymi numerami
		for(u32 i = 0; i < order.size(); i++)
			order[i] = i;

		// Funkcja por�wnuj�ca
		struct Comparer
		{
			static inline int compare(const void *a, const void *b) {
				return ayuine::compare(((Vertex*)optimizeVertexTable)[*(Index*)a], ((Vertex*)optimizeVertexTable)[*(Index*)b]);
			}
		};

		// Sortuj tablic�
		//std::sort(order.begin(), order.end(), IndicesSorter<Vertex>(vertices));
		qsort(&order.at(0), order.size(), sizeof(Index), Comparer::compare);

		// Pobierz pierwszy wierzcho�ek
		prev = vertices.at(order[0]);
		u32 count = 1;
		vmap[imap[0] = order[0]] = 1;

		// Zapisz powtarzaj�ce si� indeksy
		for(u32 i = 1; i < order.size(); i++) {
			curr = vertices.at(order[i]);

			// Popraw indeksy
			if(prev == curr) {
				// Zapisz indeks
				imap[i] = order[i-1];
			}
			else {
				prev = curr;
				count++;
				// Zapisz indeks
				vmap[imap[i] = order[i]] = 1;
			}
		}

		// Znajdz pierwszy wierzcho�ek do usuni�cia
		for(index = 0; index < vertices.size() && vmap[index] > 0; index++) {
			vmap[index] = index;
		}

		// Przepisz wierzcho�ki
		for(u32 i = index; i < vertices.size(); i++) {
			if(vmap[i] == 0)
				continue;

			// Przepisz wierzcho�ek
			vertices[vmap[i] = index++] = vertices[i];
		}

		// Przeskaluj list� wierzcho�k�w
		vertices.resize(index);

		// Uaktualnij map� wierzcho�k�w
		for(u32 i = 0; i < imap.size(); i++) {
			vmap[order[i]] = vmap[imap[i]];
		}

		// Uaktualnij indeksy
		for(u32 i = 0; i < order.size(); i++) {
			imap[order[i]] = vmap[order[i]];
		}
		return Array<Index>(imap, imap.size());
	}

	//------------------------------------//

	enum FrustumPlanes
	{
		fpLeft,
		fpRight,
		fpTop,
		fpBottom,
		fpBack,
		fpFront,
		fpCount
	};

	//------------------------------------//

	class AYUAPI frustum
	{
		// Fields
	private:
		mat4			_object;
		mat4			_invObject;
		vec4			_planes[fpCount];

		// Constructor
	public:
		frustum();
		frustum(const mat4 &transform);
		frustum(const mat4 &projection, const mat4 &view);

		// Methods
	public:
		bool test(const vec3 &point) const;
		bool test(const vec4 &plane) const;
		bool test(const vec3 *verts, u32 count, u32 stride = sizeof(vec3)) const;
		bool test(const Array<vec3> &verts) const;
		bool test(const bbox &box) const;
		bool test(const bsphere &sphere) const;

		// Methods
	public:
		vec3 screen(const vec3 &point) const;
		vec3 world(const vec3 &point) const;
		rect screenRect(const bbox &box) const;
	};

	//------------------------------------//
	// Typedefs

	typedef Array<vec3> polyVerts;
	typedef Array<vec4> polyEdges;

	//------------------------------------//
	// Poly class

	class poly
	{
		// Functions
	public:
		AYUAPI static vec4 plane(const polyVerts &poly);
		AYUAPI static polyVerts build(const polyVerts &points);
		AYUAPI static polyVerts portal(const vec4 &plane, float size = 65536);
		AYUAPI static Side split(const polyVerts &poly, const vec4 &plane, polyVerts *front = nullptr, polyVerts *back = nullptr);
		AYUAPI static polyVerts add(u32 count = 0, ...);
		AYUAPI static polyVerts merge(u32 count = 0, ...);
		AYUAPI static void removeCollinear(polyVerts &poly);
		AYUAPI static f32 area(const polyVerts &poly);
		AYUAPI static bool isTiny(const polyVerts &poly, float size);
		AYUAPI static bool isHuge(const polyVerts &poly, float size);
		AYUAPI static bool isPlanar(const polyVerts &poly);
		AYUAPI static bool isConvex(const polyVerts &poly);
		AYUAPI static polyEdges edges(const polyVerts &poly);
		AYUAPI static Side test(const polyVerts &verts, const vec4 &plane);
		AYUAPI static bool test(const polyVerts &verts, const vec3 &point);
		AYUAPI static bool test(const polyVerts &a, const polyVerts &b);
	};

	//------------------------------------//
	// SphericalHarmonics class
	
	class SphericalHarmonics
	{
		// Classes
	public:
		struct Sample 
		{
			vec3	Normal;
			vec2	Sphere;
			f64		Coeffs[16];
		};

		// Static Fields
	private:
		static Array<Sample> _samples;

		// Functions
	public:
		AYUAPI static vec3 fromSphere(const vec2 &v);
		AYUAPI static vec2 toSphere(const vec3 &v);
		AYUAPI static void computeSamples(u32 quality);
		AYUAPI static void directionalLight(const vec3 &dir, f32 intensity, vec4 out[4]);
		AYUAPI static void sphericalLight(const vec3 &origin, f32 radius, f32 intensity, vec4 out[4]);
		AYUAPI static void coneLight(const vec3 &origin, const vec3 &dir, f32 radius, f32 intensity, vec4 out[4]);
		AYUAPI static void clearSkyLight(const vec2 &sun, f32 zenithIntensity, vec4 out[4]);
		AYUAPI static void overcastLight(f32 zenithIntensity, vec4 out[4]);
	};

	//------------------------------------//

	static const vec2 vec2Zero = vec2(0.0f, 0.0f);

	//------------------------------------//

	static const vec3 vec3Zero = vec3(0.0f, 0.0f, 0.0f);

	//------------------------------------//

	static const vec4 vec4Zero = vec4(0.0f, 0.0f, 0.0f, 0.0f);
	static const vec4 vec4Identity = vec4(0.0f, 0.0f, 0.0f, 1.0f);

	//------------------------------------//

	static const angles angleZero = angles(0.0f, 0.0f, 0.0f);

	//------------------------------------//

	static const mat4 mat4Zero = mat4(	0, 0, 0, 0,
																			0, 0, 0, 0,
																			0, 0, 0, 0,
																			0, 0, 0, 0);

	static const mat4 mat4Identity = mat4(	1, 0, 0, 0,
																					0, 1, 0, 0,
																					0, 0, 1, 0,
																					0, 0, 0, 1);

	//------------------------------------//

	static const quat quatZero = quat(0, 0, 0, 0);
	static const quat quatIdentity = quat(0, 0, 0, 1);

	//------------------------------------//

	static const bbox bboxEmpty = bbox(vec3(+FLT_MAX, +FLT_MAX, +FLT_MAX), vec3(-FLT_MAX, -FLT_MAX, -FLT_MAX));

	//------------------------------------//

	static s32 compare(const u32 &a, const u32 &b) {
		return a - b;
	}

	static s32 compare(const f32 &a, const f32 &b) {
		f32 r = a - b;
		return r < -MathErr ? -1 : r > MathErr ? 1 : 0;
	}

	static s32 compare(const vec2 &a, const vec2 &b) {
		s32 r;

		if(r = compare(a.X, b.X)) 
			return r;
		if(r = compare(a.Y, b.Y))
			return r;
		return 0;
	}

	static s32 compare(const vec3 &a, const vec3 &b) {
		s32 r;

		if(r = compare(a.X, b.X)) 
			return r;
		if(r = compare(a.Y, b.Y))
			return r;
		if(r = compare(a.Z, b.Z))
			return r;
		return 0;
	}

	static s32 compare(const vec4 &a, const vec4 &b) {
		s32 r;

		if(r = compare(a.X, b.X)) 
			return r;
		if(r = compare(a.Y, b.Y))
			return r;
		if(r = compare(a.Z, b.Z))
			return r;
		if(r = compare(a.W, b.W))
			return r;
		return 0;
	}
};
