//------------------------------------//
//
// RenderFrame.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-14
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// RenderFrame: Constructor

	RenderFrame::RenderFrame() {
		_fragments = nullptr;
		_lights = nullptr;
	}

	//------------------------------------//
	// RenderFrame: Destructor

	RenderFrame::~RenderFrame() {
		// Zwolnij wszystkie fragmenty
		for(RenderFragment *curr = _fragments, *next; curr; curr = next) {
			next = curr->Next;
			delete curr;
		}

		// Zwolnij wszystkie �wiat�a
		for(RenderLight *curr = _lights, *next; curr; curr = next) {
			next = curr->Next;
			delete curr;
		}
	}

	//------------------------------------//
	// RenderFrame: Methods

	void RenderFrame::addFrag(RenderFragment *frag) {
		// Dodaj fragment
		frag->Next = _fragments;
		_fragments = frag;
	}

	void RenderFrame::addLight(RenderLight *light) {
		// Dodaj �wiat�o
		light->Next = _lights;
		_lights = light;
	}
};
