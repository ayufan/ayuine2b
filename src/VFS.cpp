//------------------------------------//
//
// VFS.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// VFS: Constructor

	VFS::VFS() {
	}

	//------------------------------------//
	// VFS: Destructor

	VFS::~VFS() {
	}

	//------------------------------------//
	// VFS: Methods

	File* VFS::loadFile(const string &name, FileType ft) {
		// Otw�rz plik
		return new FileWin(va(fileType(ft).c_str(), name.c_str()), true);
	}

	File* VFS::saveFile(const string &name, FileType ft) {
		// Otw�rz plik
		return new FileWin(va(fileType(ft).c_str(), name.c_str()), false);
	}

	string VFS::file(const string &name, FileType ft) {
		// Otw�rz plik
		SmartPtr<File> file(loadFile(name, ft));

		// Wczytaj ca�a zawarto�� pliku
		return file->read();
	}

	void VFS::file(const string &name, const string &context, FileType ft) {
		// Otw�rz plik
		SmartPtr<File> file(saveFile(name, ft));

		// Zapisz zawarto�� do pliku
		Assert(file->write(&context[0], context.size()) == context.size());
	}

	bool VFS::exists(const string &name, FileType ft) {
		// Sprawd�, czy plik istnieje
		try {
			// Otw�rz plik
			SmartPtr<File> file(loadFile(name, ft));

			// Sprawd� wska�nik na plik
			return file != nullptr;
		}
		catch(...) {
		}
		return false;
	}

	u32 VFS::find(Array<string> &files, const string &pattern, FileType ft) {
		// TODO: Znajd� wszystkie pliki...
		return 0;
	}

	string VFS::fileType(FileType ft) {
		// Utw�rz tablic�
		if(_fileTypes.empty())
			_fileTypes.resize(ftMax);

		// Sprawd�, czy podano poprawny deskryptor
		Assert(0 <= ft && ft < _fileTypes.size());

		// Sprawd�, czy mamy wpis w tabelce
		if(!_fileTypes.at(ft).empty())
			return _fileTypes.at(ft).c_str();

		// Pobierz jeden z domy�lnych wpis�w
		switch(ft) {
			case ftMesh:					return "meshes/%s";
			case ftTexture:				return "textures/%s";
			case ftMaterial:			return "materials/%s";
			case ftShader:				return "shaders/%s";
			case ftFont:					return "fonts/%s";
			case ftSound:					return "sounds/%s";
			case ftMusic:					return "music/%s";
			case ftWorld:					return "worlds/%s";
			case ftNone:
			default:							return "%s";
		}
	}

	void VFS::fileType(FileType ft, const string &name) {
		// Utw�rz tablic�
		if(_fileTypes.empty())
			_fileTypes.resize(ftMax);

		// Sprawd�, czy podano poprawny deskryptor
		Assert(0 <= ft && ft < _fileTypes.size());

		// Zapisz warto��
		_fileTypes.at(ft) = name;
	}
};
