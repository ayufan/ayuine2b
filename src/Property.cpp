//------------------------------------//
//
// Property.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-1
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Property: Constructor


	//------------------------------------//
	// Property: Functions

	u32 Property::count(Property *properties) {
		u32 count = 0;

		// Oblicz ilo�� w�a�ciwo�ci
		while(properties++->end())
			count++;
		return count;
	}

	////------------------------------------//
	//// Object: Protected methods

	//u32 Object::idOf_() {
	//	return id();
	//}

	//bool Object::isKindOf_(u32 hash) {
	//	return hash == id();
	//}

	//u32 Object::properties_() const {
	//	return 0;
	//}

	//const Property *Object::property_(u32 index) const {
	//	throw(std::invalid_argument("property not found"));
	//}

	////------------------------------------//
	//// Object: Private methods

	//u32 Object::idOf() {
	//	return this != nullptr ? idOf_() : 0;
	//}

	//bool Object::isKindOf(u32 hash) {
	//	return this != nullptr && isKindOf_(hash);
	//}

	//u32 Object::properties() const {
	//	return this != nullptr ? properties_() : 0;
	//}

	//const Property *Object::property(u32 index) const {
	//	return this != nullptr ? property_(index) : nullptr;
	//}

	////------------------------------------//
	//// Object: Destructor

	//Object::~Object() {

	//}
};
