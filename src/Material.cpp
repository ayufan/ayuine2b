//------------------------------------//
//
// Material.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// BlendModeEnum

	static EnumValue BlendModeEnum[] = {
		{"None", bmNone},
		{"Blend", bmBlend},
		{"Additive", bmAdditive},
		{"Decal", bmDecal},
		{"Overlay", bmOverlay},
		{"", 0}
	};

	//------------------------------------//
	// Material: Static Fields

	Property Material::Properties[] = {
		// Fields
		Property(OffsetOf(Material, Param),								ptVec4,			"Param"),

		// Physics Fields
		Property(OffsetOf(Material, _collidable),					ptBoolean,	"Collidable"),
		Property(OffsetOf(Material, _staticFriction),			ptValue,		"StaticFriction"),
		Property(OffsetOf(Material, _dynamicFriction),		ptValue,		"DynamicFriction"),
		Property(OffsetOf(Material, _elasticity),					ptValue,		"Elasticity"),
		Property(OffsetOf(Material, _softness),						ptValue,		"Softness"),

		// Render Fields
		Property(OffsetOf(Material, _textures[0]),				ptResource, "Texture0"),
		Property(OffsetOf(Material, _textures[1]),				ptResource, "Texture1"),
		Property(OffsetOf(Material, _textures[2]),				ptResource, "Texture2"),
		Property(OffsetOf(Material, _textures[3]),				ptResource, "Texture3"),
		Property(OffsetOf(Material, _renderable),					ptBoolean,	"Renderable"),
		Property(OffsetOf(Material, _reflective),					ptBoolean,	"Reflective"),
		Property(OffsetOf(Material, _alphaTest),					ptBoolean,	"AlphaTest"),
		Property(OffsetOf(Material, _cull),								ptBoolean,	"Cull"),
		Property(OffsetOf(Material, _blend),					BlendModeEnum,	"Blend"),

		// Shadow Fields
		Property(OffsetOf(Material, _castShadows),				ptBoolean,	"CastShadows"),
		Property(OffsetOf(Material, _receiveShadows),			ptBoolean,	"ReceiveShadows"),

		// Shader Fields
		Property(OffsetOf(Material, _ambientShader),			ptString,		"AmbientShader"),
		Property(OffsetOf(Material, _lightShader),				ptString,		"LightShader"),
		Property(OffsetOf(Material, _refractionShader),		ptString,		"RefractionShader"),
		Property(OffsetOf(Material, _customShader),				ptString,		"CustomShader"),
		Property()
	};

	//------------------------------------//
	// Material: Collections

	template<> Collection<Material>::ObjectMap Collection<Material>::_objects;

	//------------------------------------//
	// Material: Constructor

	Material::Material() {
		// Physics Fields
		_collidable = true;
		_staticFriction = 1.0f;
		_dynamicFriction = 1.0f;
		_elasticity = 1.0f;
		_softness = 1.0f;

		// Render Fields
		_renderable = true;
		_reflective = false;
		_alphaTest = false;
		_cull = true;
		_blend = bmNone;

		// Shadow Fields
		_castShadows = true;
		_receiveShadows = true;

		// Shader Fields
		_ambientShader = "@ambient_default.fx";
		_lightShader = "@light_default.fx";
	}

	//------------------------------------//
	// Material: Methods

	void Material::load(const string &name) {
		Engine::Log->print("Material: Loading %s...", name.c_str());
		
		// Spr�buj wczyta� materia�
		try {
			// Wczytaj materia�
			Reader reader(Engine::VFS->file(va("%s.mtl", name.c_str()), ftMaterial).c_str());

			// Wczytaj w�a�ciwo�ci
			reader.readProperties(Properties, this);
		}
		catch(Exception &e) {
			// Wypisz informacje o wyj�tku
			Engine::Log->warning("Material: Can't load %s", name.c_str());

			// Domy�lny materia�
			_textures[0] = name;
			_textures[1] = name + "_bump";
		}

		// Skompiluj shadery
		compile();

		// Za�aduj tekstury
		for(u32 i = 0; i < CountOf(_textures); i++)
			_textures[i].cache();

		// Oznacz jako wczytany
		_loaded = true;
	}

	void Material::compile() {
		// Skompiluj Custom Shaders
		if(_customShader.size()) {
			// Program dla wierzcho�k�w
			_customVertexShader.reset	(VertexShader::createFromCode(_customShader, "VertexShader", rtCustom));

			// Program dla pikseli
			_customPixelShader.reset	(PixelShader::createFromCode(_customShader, "PixelShader", rtCustom));
		}
		else {
			// Skompiluj Ambient Shaders
			if(_ambientShader.size()) {
				// Programy dla wierzcho�k�w
				_ambientVertexShader.reset						(VertexShader::createFromCode(_ambientShader, "VertexShader", rtAmbient));
				_ambientVertexShaderSkinned.reset			(VertexShader::createFromCode(_ambientShader, "VertexShader", rtAmbient | sfSkinning));

				// Programy dla pikseli
				_ambientPixelShader.reset							(PixelShader::createFromCode(_ambientShader, "PixelShader", rtAmbient));
				_ambientPixelShaderSun.reset					(PixelShader::createFromCode(_ambientShader, "PixelShader", rtAmbient | sfSun));
				_ambientPixelShaderSunPRT.reset				(PixelShader::createFromCode(_ambientShader, "PixelShader", rtAmbient | sfSun | sfPRT));
				_ambientPixelShaderSunShadows.reset		(PixelShader::createFromCode(_ambientShader, "PixelShader", rtAmbient | sfSun | sfShadows));
				_ambientPixelShaderSunShadowsPRT.reset(PixelShader::createFromCode(_ambientShader, "PixelShader", rtAmbient | sfSun | sfShadows | sfPRT));
			}

			// Skompiluj Light Shaders
			if(_lightShader.size()) {
				// Programy dla wierzcho�k�w
				_lightVertexShader.reset						(VertexShader::createFromCode(_lightShader, "VertexShader", rtLight));
				_lightVertexShaderSkinned.reset			(VertexShader::createFromCode(_lightShader, "VertexShader", rtLight | sfSkinning));

				// Programy dla pikseli
				_lightPixelShader.reset							(PixelShader::createFromCode(_lightShader, "PixelShader", rtLight));
				_lightPixelShaderPRT.reset					(PixelShader::createFromCode(_lightShader, "PixelShader", rtLight | sfPRT));
				_lightPixelShaderShadows.reset			(PixelShader::createFromCode(_lightShader, "PixelShader", rtLight | sfShadows));
				_lightPixelShaderShadowsPRT.reset		(PixelShader::createFromCode(_lightShader, "PixelShader", rtLight | sfShadows | sfPRT));
			}

			// Skompiluj Refractive Shaders
			if(_refractionShader.size()) {
				// Programy dla wierzcho�k�w
				_refractionVertexShader.reset				(VertexShader::createFromCode(_refractionShader, "VertexShader", rtRefraction));
				_refractionVertexShaderSkinned.reset(VertexShader::createFromCode(_refractionShader, "VertexShader", rtRefraction | sfSkinning));

				// Program dla pikseli
				_refractionPixelShader.reset				(PixelShader::createFromCode(_refractionShader, "PixelShader", rtRefraction));
			}
		}
	}

	ShaderPair Material::shader(class RenderState &state) {
		ShaderPair pair;

		// Zresetuj stan shader�w
		pair.first = nullptr;
		pair.second = nullptr;

		// Sprawd� czy mamy do czynienia z customowymi shaderkami
		if(_customShader.size()) {
			// Sprawd� typ renderingu
			if(state.Type == rtAmbient || state.Type == rtCustom) {
				// Sprawd� cieniowanie wierzcho�k�w
				pair.first = _customVertexShader;
				
				// Sprawd� cieniowanie pikseli
				pair.second = _customPixelShader;
			}
		}
		// A mo�e jednak normalne :D
		else {
			// Pobierz odpowiednie shadery
			switch(state.Type) {
				case rtAmbient:
				case rtCustom:
					// Sprawd� cieniowanie wierzcho�k�w
					if(state.Skinning)
						pair.first = _ambientVertexShaderSkinned;
					else
						pair.first = _ambientVertexShader;

					// Sprawd� cieniowanie pikseli
					if(state.Sun)
						if(state.SunPRT && !state.NoPRT)
							if(state.SunShadow)
								pair.second = _ambientPixelShaderSunShadowsPRT;
							else
								pair.second = _ambientPixelShaderSunPRT;
						else
							if(state.SunShadow)
								pair.second = _ambientPixelShaderSunShadows;
							else
								pair.second = _ambientPixelShaderSun;
					else
						pair.second = _ambientPixelShader;
					break;

				case rtLight:
					// TODO: Obsluga swiatel kierunkowych

					// Sprawd� cieniowanie wierzcho�k�w
					if(state.Skinning)
						pair.first = _lightVertexShaderSkinned;
					else
						pair.first = _lightVertexShader;

					// Sprawd� cieniowanie pikseli
					if(state.LightPRT && !state.NoPRT)
						if(state.LightShadow)
							pair.second = _lightPixelShaderShadowsPRT;
						else
							pair.second = _lightPixelShaderPRT;
					else
						if(state.LightShadow)
							pair.second = _lightPixelShaderShadows;
						else
							pair.second = _lightPixelShaderShadowsPRT;
					break;

				case rtRefraction:
					// Sprawd� cieniowanie wierzcho�k�w
					if(state.Skinning)
						pair.first = _refractionVertexShaderSkinned;
					else
						pair.first = _refractionVertexShader;

					// Sprawd� cieniowanie pikseli
					pair.second = _refractionPixelShader;
					break;
			}
		}
		return pair;
	}

	bool Material::bind(RenderState &state) {
		// Pobierz konfiguracje shader�w
		ShaderPair pair = shader(state);

		// Sprawd� czy mamy par� shader�w
		if(!pair.first || !pair.second)
			return false;

		// Ustaw parametry materia�u
		Engine::Device->vertexConst(vscParam, &Param.X);
		Engine::Device->pixelConst(pscParam, &Param.X);

		// Ustaw shadery
		pair.first->bind();
		pair.second->bind();

		// Ustaw tekstury
		for(u32 i = 0; i < CountOf(_textures); i++) {
			if(pair.second->sampler(i).empty())
				continue;
			_textures[i]->bind(i);
		}

		// Ustaw AlphaTest
		Engine::Device->alphaTest(_alphaTest);

		// Ustaw Culling
		Engine::Device->cullMode(_cull);

		// TODO: Ustaw blendowanie
		if(state.Type == rtAmbient || state.Type == rtCustom)
			Engine::Device->blendMode(_blend);

		return true;
	}

	void Material::unbind(RenderState &state) {
		// Pobierz konfiguracje shader�w
		ShaderPair pair = shader(state);

		// Sprawd� czy mamy par� shader�w
		if(!pair.first || !pair.second)
			return;

		// Ustaw shadery
		pair.first->unbind();
		pair.second->unbind();

		// Ustaw tekstury
		for(u32 i = 0; i < CountOf(_textures); i++) {
			if(pair.second->sampler(i).empty())
				continue;
			_textures[i]->unbind(i);
		}

		// Ustaw AlphaTest
		Engine::Device->alphaTest(false);

		// Ustaw Culling
		Engine::Device->cullMode(true);

		// TODO: Ustaw blendowanie
		if(state.Type == rtAmbient || state.Type == rtCustom)
			Engine::Device->blendMode(bmNone);
	}

	void Material::unload() {
		// Usu� wszystkie odwo�ania do tekstur
		for(u32 i = 0; i < CountOf(_textures); i++)
			_textures[i].reset();

		// Usu� dynamiczne dane
		deviceLost(true);

		// Oznacz jako niewczytany
		_loaded = false;
	}

	//------------------------------------//
	// Material: DeviceObject Methods

	void Material::deviceReset(bool theBegin) {
		// Skompiluj shadery
		if(theBegin) {
			compile();
		}
	}

	void Material::deviceLost(bool theEnd) {
		// Zniszcz shadery
		if(theEnd) {
			// Zniszcz Ambient Shaders
			_ambientVertexShader.reset();
			_ambientVertexShaderSkinned.reset();
			_ambientPixelShader.reset();
			_ambientPixelShaderSun.reset();
			_ambientPixelShaderSunPRT.reset();
			_ambientPixelShaderSunShadows.reset();
			_ambientPixelShaderSunShadowsPRT.reset();

			// Zniszcz Light Shaders
			_lightVertexShader.reset();
			_lightVertexShaderSkinned.reset();
			_lightPixelShader.reset();
			_lightPixelShaderPRT.reset();
			_lightPixelShaderShadows.reset();
			_lightPixelShaderShadowsPRT.reset();

			// Zniszcz Refractive Shaders
			_refractionVertexShader.reset();
			_refractionVertexShaderSkinned.reset();
			_refractionPixelShader.reset();

			// Zniszcz Custom Shaders
			_customVertexShader.reset();
			_customPixelShader.reset();
		}
	}
};
