//------------------------------------//
//
// Shader.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-9
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// ShaderVersion enumeration

	enum ShaderVersion
	{
		svVertexAuto,
		svVertex11,
		svVertex20,
		svVertex30,

		svPixelAuto,
		svPixel11,
		svPixel14,
		svPixel20,
		svPixel30,

		svTextureAuto,
		svTexture10
	};

	//------------------------------------//
	// ShaderFlags enumeration

	enum ShaderFlags // : RenderType
	{
		sfNone							= 0,
		sfRenderTypeMask		= 3,
		sfSun								= 8,
		sfPRT								= 16,
		sfShadows						= 32,
		sfSkinning					= 64,
	};

	//------------------------------------//
	// ShaderCode type

	typedef Array<DWORD> ShaderCode;

	//------------------------------------//
	// Shader class

	class ShaderCompiler
	{
		ValueType(ShaderCompiler, 0);

		// Methods
	public:
		AYUAPI static ShaderCode compileFromCode(const string &shaderCode, const string &shaderEntry, ShaderVersion sv, u32 flags = sfNone);
		AYUAPI static ShaderCode compileFromFile(const string &shaderFile, const string &shaderEntry, ShaderVersion sv, u32 flags = sfNone);
	};

	//------------------------------------//
	// PixelShader class

	class PixelShader : public DeviceObject
	{
		ObjectType(PixelShader, DeviceObject, 0);

		// Fields
	private:
		ShaderCode								_code;
		LPDIRECT3DPIXELSHADER9		_shader;
		string										_samplers[16];

		// Constructor
	public:
		AYUAPI PixelShader(const ShaderCode &code);

		// Destructor
	public:
		AYUAPI ~PixelShader();

		// Properties
	public:
		AYUAPI const string &sampler(u32 i) const;

		// Methods
	public:
		AYUAPI void bind();
		AYUAPI void unbind();

		// Functions
	public:
		AYUAPI static PixelShader *createFromCode(const string &shaderCode, const string &shaderEntry, u32 flags = sfNone);
		AYUAPI static PixelShader *createFromFile(const string &shaderFile, const string &shaderEntry, u32 flags = sfNone);
	};

	//------------------------------------//
	// VertexShader class

	class VertexShader : public DeviceObject
	{
		ObjectType(VertexShader, DeviceObject, 0);

		// Fields
	public:
		ShaderCode								_code;
		LPDIRECT3DVERTEXSHADER9		_shader;

		// Constructor
	public:
		AYUAPI VertexShader(const ShaderCode &code);

		// Destructor
	public:
		AYUAPI ~VertexShader();

		// Methods
	public:
		AYUAPI void bind();
		AYUAPI void unbind();

		// Functions
	public:
		AYUAPI static VertexShader *createFromCode(const string &shaderCode, const string &shaderEntry, u32 flags = sfNone);
		AYUAPI static VertexShader *createFromFile(const string &shaderFile, const string &shaderEntry, u32 flags = sfNone);
	};
};
