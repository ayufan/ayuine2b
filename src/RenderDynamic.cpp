//------------------------------------//
//
// RenderDynamic.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-18
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// RenderDynamic: Constructor

	RenderDynamic::RenderDynamic() {
		VertexType = nullptr;
		Vertices = nullptr;
		VertexStride = 0;
		VertexCount = 0;
		Indices = nullptr;
		IndexCount = 0;
	}

	//------------------------------------//
	// RenderDynamic: Methods

	void RenderDynamic::render(RenderState &state) {
		// Ustaw lustro
		if(Mirror) {
			// Je�li ju� jest wy�wietlane lustro to nie wy�wietlamy luster
			if(state.Level)
				return;
			state.Reflection = Mirror->Texture;
			state.bindReflection();
		}

		// Ustaw ramk�
		state.Object = Transform;
		state.ObjectPRT = PRT;

		// Podepnij obiekt
		state.bindObject();

		// Ustaw bufory
		VertexType->bind();
		Engine::Draw->bindVertices(0, Vertices, VertexStride, VertexCount);
		Engine::Draw->bindIndices(Indices, IndexCount);

		// Wy�wietl mesha
		for(u32 i = 0; i < Drawers.size(); i++)
			Drawers.at(i).draw();

		// Wy��cz bufory
		Engine::Draw->unbindIndices();
		Engine::Draw->unbindVertices(0);

		// Odepnij obiekt
		state.unbindObject();

		// Odepnij lustro
		if(Mirror) {
			state.unbindReflection();
		}
	}
};
