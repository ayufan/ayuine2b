//------------------------------------//
//
// Core.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#pragma once

//------------------------------------//
// disable warnings

#ifdef _MSC_VER
#pragma warning(disable : 4251 4275)
#endif

/// DOXYS_OFF
//------------------------------------//
// c base includes

#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cstring>
#include <cctype>
#include <cmath>
#include <cfloat>
#include <cassert>
#include <ctime>

//------------------------------------//
// stl base includes

#include <string>
#include <map>
#include <algorithm>

namespace ayuine
{
	//------------------------------------//
	// Define

	#define OffsetOf(Type,Of)										((u32)&((Type*)nullptr)->Of)
	#define CountOf(x)													(sizeof(x)/sizeof((x)[0]))
	//#define TypeOf(x)														x::id()

	//------------------------------------//
	// Base Types

	// bool
	typedef char								c8;
	typedef signed char					s8;
	typedef signed short				s16;
	typedef signed int					s32;
	typedef signed long long		s64;
	typedef unsigned char				u8;
	typedef unsigned short			u16;
	typedef unsigned int				u32;
	typedef unsigned long long	u64;
	typedef float								f32;
	typedef double							f64;
	using	namespace	std;
#ifdef _W64
	typedef u64	size;
#else
	typedef	u32	size;
#endif

	//------------------------------------//
	// Exception defines

#define newExceptionTrace() ExceptionTrace(__FUNCTION__, __FILE__, __LINE__)
#define Throw(e)						Exception::ThrowException(e, newExceptionTrace())
#define Guard								try {
#define Unguard							} catch(Exception &e) { e.CallStack.push(newExceptionTrace()); throw; } catch(...) { Throw(UnhandledException()); }
#define Assert(x)						if(!(x)) { Throw(AssertException(#x)); }

#ifdef _DEBUG
	#define dGuard			Guard
	#define dUnguard		Unguard
	#define dAssert(x)	Assert(x)
#else
	#define dGuard
	#define dUnguard
	#define dAssert(x)	(x)
#endif

	//------------------------------------//
	// Consts

	const u32 nullptr = 0;

	//------------------------------------//
	// Limits

	const u32 u32Max = UINT_MAX;
	const u32 u32Min = 0;
	const s32 s32Max = INT_MAX;
	const s32 s32Min = INT_MIN;

	//------------------------------------//
	// Functions

	static string fa(const c8 **format, ...) {
		c8 temp[4000];
		va_list args;

		// Pobierz argumenty i utw�rz tekst
		va_start(args, *format);
#ifdef _MSC_VER
		vsprintf_s(temp, sizeof(temp), *format, args);
#else
		vsnprintf(temp, sizeof(temp), *format, args);
#endif
		va_end(args);

		// Zwr�� utworzony tekst
		return temp;
	}

	static string va(const c8 *format, ...) {
		return fa(&format);
	}

	static u32 hash(const string &text) {
		u32 h = (u32)text.length();
		for(string::const_iterator i = text.begin(); i != text.end(); i++)
			h ^= (h << 2) + (h >> 5) + *i;
		return h;
	}

	static u32 hashi(const string &text) {
		u32 h = (u32)text.length();
		for(string::const_iterator i = text.begin(); i != text.end(); i++)
			h ^= (h << 2) + (h >> 5) + *i;
		return h;
	}

	template<typename Type>
	static Type &offset(void *data, u32 count, u32 size = sizeof(Type)) {
		return *(Type*)((u8*)data + count * size);
	}
	
	template<typename Type>
	static Type &next(Type *&data, u32 stride, u32 index = 1) {
		return *(Type*)((u8*&)data += index * stride);
	}

	//------------------------------------//
	// Memory functions

	AYUAPI void *alloc(size_t size);
	AYUAPI void free(void *data);
};

//------------------------------------//
// Operators

static void *operator new (size_t size) {
	return ayuine::alloc(size);
}

static void *operator new[] (size_t size) {
	return ayuine::alloc(size);
}

static void operator delete (void *data) {
	ayuine::free(data);
}

static void operator delete[] (void *data) {
	ayuine::free(data);
}

#undef min
#undef max

//------------------------------------//
// Includes

#include "TypeInfo.hpp"
#include "Allocator.hpp"
#include "Array.hpp"
#include "List.hpp"
#include "SmartPtr.hpp"
#include "Delegate.hpp"
#include "Object.hpp"
#include "Exception.hpp"
#include "MathLib.hpp"
#include "File.hpp"
#include "Collection.hpp"
#include "Property.hpp"
#include "Reader.hpp"
#include "Config.hpp"
#include "Log.hpp"
#include "Stats.hpp"
#include "VFS.hpp"
#include "Time.hpp"