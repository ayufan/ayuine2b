//------------------------------------//
//
// Config.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class Config
	{
		ValueType(Config, 0);

		// Fields
	public:
		u32										Width, Height;
		bool									Windowed;
		f32										SceneIntensity, BlurIntensity, HighlightIntensity;
		u8										Shadows;
		u8										PRT;
		bool									PostProcess;
		u32										Sleep;
		u32										TextureQuality;
		bool									TextureCompression;
		u32										TextureMinimalSize;
		f32										UpdateFrameTime;
		bool									UseOcclusion;

		// Constructor
	public:
		Config();

		// Destructor
	public:
		~Config();
	};
};
