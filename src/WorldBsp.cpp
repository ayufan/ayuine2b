//------------------------------------//
//
// WorldBsp.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-11
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// BspBox structure

	struct BspBox
	{
		u32						Zone;
		bbox					Box;
		u32						Used;

		BspBox(u32 zone, const bbox &box, u32 used = 0) {
			Zone = zone;
			Box = box;
			Used = used;
		}
	};

	//------------------------------------//
	// World: Visibility Methods

	u32 World::findNode(const vec3 &origin) {
		u32 i = _root;

		// Sprawdzaj iteracyjnie kazd� ga���
		while(i < _nodes.size()) {
			BspNode &node = _nodes.at(i);

			// Sprawd�, czy dotarli�my do li�cia
			if(node.leaf())
				break;

			// Pobierz nast�pn� ga���
			i = node.Childs[(origin[node.Axis] - node.Split) < 0];
		}
		return i;
	}

	void World::findZones(Actor *actor) {
		u32 stack[4096];
		u32 stackSize = 0;

		// Dodaj korze� do stosu
		stack[stackSize++] = _root;

		// Sprawdzaj do puki nie znajdziemy ga��zi
		while(stackSize) {
			u32 i = stack[--stackSize];

			while(i < u32Max) {
				BspNode &node = _nodes.at(i);

				// Sprawd� ga���
				if(node.leaf()) {
					for(u32 j = 0; j < node.Zones.size(); j++) {
						BspZone &zone = _zones.at(node.Zones.at(j));

						// Sprawd� czy strefa jest ju� dodana
						if(zone.Frame == _frame)
							continue;
						zone.Frame = _frame;

						// Dodaj stref�
						actor->_zones.push(node.Zones.at(j));
					}
					break;
				}
				else {
					if(node.Split < actor->Box.Mins[node.Axis])
						i = node.Childs[0];
					else if(node.Split > actor->Box.Maxs[node.Axis])
						i = node.Childs[1];
					else {
						Assert(stackSize < CountOf(stack));
						stack[stackSize++] = node.Childs[0];
						i = node.Childs[1];
					}
				}
			}
		}
	}

	void World::buildBspTree() {
		Array<BspBox> boxes;

		// Dodaj wszystkie strefy
		for(u32 i = 0; i < _zones.size(); i++) {
			boxes.push(BspBox(i, _zones.at(i).Box, 0));
		}

		// Wyczy�� poprzednie drzewo
		_nodes.clear();

		// Utw�rz nowe drzewo
		_root = buildBspTree_r(boxes);
	}

	u32 World::buildBspTree_r(Array<BspBox> &boxes) {
		u32						bestResult = 0;
		u32						axes[3] = {0, 0, 0};
		Axis					axis;
		f32						split;

		// Oblicz ilo�� osi
		ArrayEach(BspBox, i, boxes) {
			for(u32 j = 0; j < 6; j++) {
				if(i->Used & (1 << j))
					continue;
				axes[j/2]++;
			}
		}

		// Wyznacz najlepsz� o�
		if(axes[0] < axes[1])
			if(axes[1] < axes[2])
				axis = aZ;
			else
				axis = aY;
		else
			if(axes[0] < axes[2])
				axis = aZ;
			else
				axis = aX;

		// Li��
		if(!axes[axis]) {
			// Sprawd� ilo�� sektor�w
			if(boxes.empty())
				return u32Max;

			BspNode node;

			// Dodaj strefy
			ArrayEach(BspBox, i, boxes) {
				node.Zones.push(i->Zone);
			}

			// Posortuj list� stref
			std::sort(node.Zones.begin(), node.Zones.end());

			// Usu� powtarzaj�ce si�
			node.Zones.erase(std::unique(node.Zones.begin(), node.Zones.end()), node.Zones.end());

			// Dodaj ga��� do drzewa
			return _nodes.push(node);
		}

		// Pobierz pierwsz� p�aszczyzn� ci�cia
		ArrayEach(BspBox, i, boxes) {
			// Minimalna
			if((i->Used & (1 << ((axis << 1) + 0))) == 0) {
				split = i->Box.Mins[axis];
				goto found;
			}
		
			// Maksymalna
			if((i->Used & (1 << ((axis << 1) + 1))) == 0) {
				split = i->Box.Maxs[axis];
				goto found;
			}
		}
		Assert("No plane left & there should be !" && false);

found:;

		// Kolekcje pocietych box�w
		Array<BspBox> front, back;

		// Tniemy
		ArrayEach(BspBox, i, boxes) {
			// Na
			if(abs(i->Box.Mins[axis] - split) < MathErr)
				i->Used |= 1 << ((axis << 1) + 0);
			if(abs(i->Box.Maxs[axis] - split) < MathErr)
				i->Used |= 1 << ((axis << 1) + 1);

			// Prz�d
			if(split < i->Box.Mins[axis] + MathErr) {
				front.push(*i);
				continue;
			}
		
			// Ty�
			if(i->Box.Maxs[axis] - MathErr < split) {
				back.push(*i);
				continue;
			}

			// Przecina?
			bbox frontBox, backBox;

			// Zapisz oryginalne boxy
			frontBox = backBox = i->Box;

			// Przetnij je
			frontBox.Mins[axis] = split;
			backBox.Maxs[axis] = split;

			// Dodaj
			front.push(BspBox(i->Zone, frontBox, i->Used | (1 << ((axis << 1) + 0))));
			back.push(BspBox(i->Zone, backBox, i->Used | (1 << ((axis << 1) + 1))));
		}

		BspNode node;

		// Wyczy�� poprzednie boxy
		boxes.clear();

		// Zapisz informacje o ga��zi
		node.Axis = axis;
		node.Split = split;

		// Utw�rz dzieci
		node.Childs[0] = buildBspTree_r(front);
		node.Childs[1] = buildBspTree_r(back);

		// Dodaj ga���
		return _nodes.push(node);
	}

	void World::ensureVisbility_r(u32 id, const rect &clip, const frustum &f, Array<u32> &seen, BspZone* &zones) {
		BspZone &zone = _zones.at(id);

		// Zapisz kolejk� przechodzenia stref
		if(zone.Frame != _frame) {
			// Ustaw nowe parametry widoczno�ci
			zone.Visible = seen.size();
			zone.VisibleRect = clip;
			zone.Frame = _frame;

			// Dodaj stref� do listy widocznych
			zone.Next = zones;
			zones = &zone;
		}
		else if(zone.Visible > seen.size()) {
			// Zapisz nowe parametry widoczno�ci dla strefy, "bo jest bli�ej"
			zone.Visible = seen.size();
			zone.VisibleRect = clip;
		}
		else if(zone.Visible == seen.size()) {
			// Po��cz poprzednie parametry z nowymi, "bo s� tak samo daleko"
			zone.VisibleRect = zone.VisibleRect.merge(clip);
		}

		// Dodaj stref� do odwiedzonych
		seen.push(id);

		// Sprawd� wszystkie portale nale��ce do sektora
		ArrayEach(BspPortal, i, zone.Portals) {
			// Odwiedzili�my ju� ten sektor?
			if(seen.find(i->OtherZone) != u32Max)
				continue;

			// Przytnij portal do frustuma
			rect newClip = clip.intersect(f.screenRect(i->Box));

			// Jest widoczny?
			if(newClip.empty())
				continue;

			// Ruszamy dalej
			ensureVisbility_r(i->OtherZone, newClip, f, seen, zones);
		}
		
		// Zdejmij sektor z odwiedzonych
		seen.pop();
	}

	World::BspZone* World::ensureVisbility(const vec3 &origin, const frustum &frustum, const rect &clip) {
		Array<u32> seen;
		BspZone* zones = nullptr;

		// Wyznacz li�� w kt�rym si� znajdujemy
		u32 id = findNode(origin);

		// Sprawd� rezultat szukania
		if(id < u32Max) {
			BspNode &node = _nodes.at(id);

			// Wyznacz widoczno�� dla wszystkich stref nachodz�cych na dan� ga���
			for(u32 i = 0; i < node.Zones.size(); i++) {
				ensureVisbility_r(node.Zones.at(i), clip, frustum, seen, zones);
			}
		}
		else {
			// Je�li opu�cili�my scen�, to poka� wszystko
			for(u32 i = 0; i < _zones.size(); i++) {
				BspZone &zone = _zones.at(i);

				zone.Frame = _frame;
				zone.Visible = 0;
				zone.VisibleRect = clip;
				
				// Dodaj stref� do listy widocznych stref
				zone.Next = zones;
				zones = &zone;
			}
		}
		return zones;
	}
};
