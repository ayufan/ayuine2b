//------------------------------------//
//
// RenderFragment.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-14
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// RenderFragment: Constructor

	RenderFragment::RenderFragment() {
		// Fields
		Transform = mat4Identity;
		Material = nullptr;
		PRT = nullptr;
		Mirror = nullptr;
		Next = nullptr;
		NextChain = nullptr;
		Distance = 0;
		Flags = rffNone;
	}

	//------------------------------------//
	// RenderFragment: Destructor

	RenderFragment::~RenderFragment() {
	}

	//------------------------------------//
	// RenderFragment: Methods

	void RenderFragment::renderMaterial(RenderState &state) {
		// W��cz materia�
		if(Material->bind(state)) {
			// Wy�wietl fragment
			render(state);

			// Wy��cz materia�
			Material->unbind(state);
		}
	}
};
