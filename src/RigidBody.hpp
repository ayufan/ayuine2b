//------------------------------------//
//
// RigidBody.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-21
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// RigidBody class

	class RigidBody : public Object
	{
		ObjectType(RigidBody, Object, 0);

		// Fields
	private:
		NewtonBody*							_body;
		NewtonRagDollBone*			_bone;
		Collider*								_collider;

	public:
		bool					Movable;
		bool					State;
		bool					Gravity;
		bool					Fluids;
		mat4					Transform;
		mat4					InvTransform;
		vec3					Velocity, Omega;

		// Constructor
	public:
		AYUAPI RigidBody(Collider* collider);

		// Destructor
	public:
		AYUAPI virtual ~RigidBody();

		// Methods
	public:
		AYUAPI void collider(Collider* collider);
		AYUAPI void mass(f32 mass, const vec3 &inertia, const vec3 &center = vec3(0, 0, 0));
		AYUAPI void impulse(const vec3 &p, const vec3 &i);
		AYUAPI void apply();
		AYUAPI bool cast(const vec3 &start, const vec3 &end, Contact *contact = nullptr);
		AYUAPI bbox bounds();

		// Friends
		friend class Joint;
		friend class BallJoint;
		friend class HingeJoint;
		friend class SliderJoint;
		friend class UpJoint;
	};
};
