//------------------------------------//
//
// Window.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-9
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class Window
	{
		ValueType(Window, 0);

		// Fields
	private:
		HWND _hWnd;

		// Constructor
	public:
		Window();

		// Destructor
	public:
		~Window();

		// Properties
	public:
		HWND handle() {
			return _hWnd;
		}

		// Methods
	public:
		AYUAPI bool check();
		AYUAPI void title(const string &name);
		AYUAPI bool update();
		AYUAPI bool focused();
	};
};