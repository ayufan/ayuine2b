//------------------------------------//
//
// Reader.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-25
//
//------------------------------------//

#pragma once

namespace ayuine
{
	enum QuoteFlags
	{
		qfQuote = 1,
		qfSection = 2,
		qfString = 4,
		qfAll = (qfQuote | qfSection | qfString)
	};

	class AYUAPI Reader
	{
		ValueType(Reader, 0);

		// Fields
	private:
		string								_data;
		string::iterator			_at;

		// Constructor
	public:
		Reader(const string &data);
		Reader(const Reader &reader);

		// Destructor
	public:
		~Reader();

		// Helper Methods
	private:
		bool skip(bool crossline);
	public:
		u32							at();
		u32							atline();
		void						at(u32 at);

		// Methods
	public:
		u32							line(string &out);
		string					command(bool crossline = true);
		string					quote(bool crossline = false, u32 flags = qfAll);
		bool						boolean(bool crossline = false);
		s32							integer(bool crossline = false);
		f32							value(bool crossline = false);
		bool						match(const string &pattern, bool whitespace = true, bool crossline = true);
		vec2						vector2(bool crossline = false);
		vec3						vector3(bool crossline = false);
		vec4						vector4(bool crossline = false);
		mat4						matrix4(bool crossline = false);
		u32							enumeration(const EnumValue *enums, bool crossline = false);

		// Expect Methods
	public:
		void						expectBeginOfSection();
		void						expectEndOfSection();
		void						expectEndOfCommand();

		// Move Methods
	public:
		void						moveToStartOfSection(bool in = true);
		void						moveToEndOfSection(bool in = true);
		void						moveToStartOfCommand();
		void						moveToEndOfCommand();

		// Query Methods
	public:
		bool						beginOfSection(bool omit = true);
		bool						endOfSection(bool omit = true);
		bool						endOfLine(bool omit = true);
		bool						endOfFile();

		// Read Methods
	public:
		bool						readProperty(const Property &property, void *data);
		void						readProperties(Property* properties, void *data, bool section = false);
	};
};
