//------------------------------------//
//
// List.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-2
//
//------------------------------------//

#pragma once

namespace ayuine
{
	template<typename Type>
	class List
	{
		ValueType(List<Type>, 0);

		// Fields
	public:
		Type*					Object;
		List<Type>*		Next;
		List<Type>*		Prev;

		// Constructor
	public:
		List(Type *object) {
			// Ustaw domy�ln� list�
			Object = object;
			Next = this;
			Prev = this;
		}

		// Destructor
	public:
		~List() {
			// Odepnij list�
			unlink();
		}

		// Properties
	public:
		bool empty() const {
			return Next == Prev;
		}
		u32 count() const {
			u32 index = 0;
			for(List<Type>* curr = Next; curr != this; curr = curr->Next) {
				index++;
			}
			return index;
		}

		// Methods
	public:
		void linkFront(List<Type> &list) {
			// Podepnij list� z przodu
			Next = list.Next;
			Prev = &list;

			// Uaktualnij wska�niki
			Next->Prev = this;
			Prev->Next = this;
		}
		void linkBack(List<Type> &list) {
			// Podepnij list� z ty�u
			Prev = list.Prev;
			Next = &list;

			// Uaktualnij wska�niki
			Next->Prev = this;
			Prev->Next = this;
		}
		void unlink() {
			Assert(Next);
			Assert(Prev);

			// Odepnij list�
			Next->Prev = Prev;
			Prev->Next = Next;

			// Ustaw warto�ci domy�lne
			Next = this;
			Prev = this;
		}
	};
};
