//------------------------------------//
//
// Engine.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Engine: Static Fields

	u32							Engine::Flags				= efNone;
	Input*					Engine::Input				= nullptr;
	World*					Engine::World				= nullptr;
	Sound*					Engine::Sound				= nullptr;
	Window*					Engine::Window			= nullptr;
	Device*					Engine::Device			= nullptr;
	Draw*						Engine::Draw				= nullptr;
	Render*					Engine::Render			= nullptr;
	Stats*		Engine::Stats = nullptr;
	VFS*						Engine::VFS					= nullptr;
	Log*						Engine::Log					= nullptr;
	Time*						Engine::Time				= nullptr;
	Camera*					Engine::Camera			= nullptr;
	Config*					Engine::Config			= nullptr;

	//------------------------------------//
	// Engine Methods

	void Engine::init(u32 flags) {
		// Zapisz parametry uruchomienia
		Flags = flags;

		// Log
		if((flags & efLog) == efLog) {
			Log = new ayuine::Log();
		}

		// Log
		if((flags & efConfig) == efConfig) {
			Config = new ayuine::Config();
		}

		// VFS
		if((flags & efVFS) == efVFS) {
			VFS = new ayuine::VFS();
		}

		// Time
		if((flags & efTime) == efTime) {
			Time = new ayuine::Time();
		}

		// Stats
		if((flags & efStats) == efStats) {
			Stats = new ayuine::Stats();
		}

		// Input
		if((flags & efInput) == efInput) {
			Input = new ayuine::Input();
		}

		// World
		if((flags & efWorld) == efWorld) {
			World = new ayuine::World();
		}

		// Sound
		if((flags & efSound) == efSound) {
			Sound = new ayuine::Sound();
		}

		// Window
		if((flags & efWindow) == efWindow) {
			Window = new ayuine::Window();
		}

		// Device
		if((flags & efDevice) == efDevice) {
			Device = new ayuine::Device();
		}

		// Draw
		if((flags & efDraw) == efDraw) {
			Draw = new ayuine::Draw();
		}

		// Render
		if((flags & efRender) == efRender) {
			Render = new ayuine::Render();
		}
	}

	bool Engine::done() {
		return Window && !Window->check();
	}

	bool Engine::update() {
		// Uaktualnij czas
		Time->update();

		// Zresetuj statystyki
		if(Stats) {
			Stats->reset();
		}

		// Uaktualnij okno
		if(Window) {
			Window->update();

			// Czy okno jest aktywne?
			if(!Window->focused()) {
				return false;
			}
		}

		// Uaktualnij d�wi�k
		if(Sound) {
			Sound->update();
		}

		// Uaktualnij wej�cie
		if(Input) {
			Input->update();
		}

		// Uaktualnij �wiat
		if(World) {
			World->update(Time->delta());
		}

		// Uaktualnij kamer�
		if(Camera) {
			Camera->update(Time->delta());
		}
		return true;
	}

	void Engine::render() {
		if(!Device || !Device->swap(nullptr))
			return;

		// Rozpocznij wy�wietlanie
		dxe(Device->d3ddev()->BeginScene());

		// Wy�wietl widok z kamery
		if(Camera) {
			if(Input && Input->keyState(DIK_LSHIFT, false)) 
				renderView(Camera, rmWire);
			else if(Input && Input->keyState(DIK_LCONTROL, false))
				renderView(Camera, rmSolid);
			else
				renderView(Camera, rmShaded);
		}

		// Poka� statystyki
		if(Stats) {
			Stats->show(Font::get("Verdana"));
		}

		// Zako�cz wy�wietlanie
		dxe(Device->d3ddev()->EndScene());
	}
	
	void Engine::run() {
		// Zresetuj licznik czasu
		Time->reset();

		// G��wna p�tla
		while(!done()) {
			if(update()) {
				render();
				Sleep(Config->Sleep);
			}
			else {
				Sleep(10);
			}
		}
	}

	void Engine::release() {
		// Render
		if(Render) {
			delete Render;
			Render = nullptr;
		}

		// Draw
		if(Draw) {
			delete Draw;
			Draw = nullptr;
		}

		// Device
		if(Device) {
			delete Device;
			Device = nullptr;
		}

		// Window
		if(Window) {
			delete Window;
			Window = nullptr;
		}

		// World
		if(World) {
			delete World;
			World = nullptr;
		}

		// Sound 
		if(Sound) {
			delete Sound;
			Sound = nullptr;
		}

		// Input 
		if(Input) {
			delete Input;
			Input = nullptr;
		}

		// Stats 
		if(Stats) {
			delete Stats;
			Stats = nullptr;
		}

		// Camera
		Camera = nullptr;

		// Config
		if(Config) {
			delete Config;
			Config = nullptr;
		}

		// Log
		if(Log) {
			delete Log;
			Log = nullptr;
		}

		// VFS
		if(VFS) {
			delete VFS;
			VFS = nullptr;
		}

		// Time
		if(Time) {
			delete Time;
			Time = nullptr;
		}

		// Zniszcz wszystkie za�adowane obiekty
		Font::clear();
		Texture::clear();
		Material::clear();
		Mesh::clear();
		SoundBuffer::clear();
	}
};