//------------------------------------//
//
// AMesh.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class AMesh : public Actor
	{
		ObjectType(AMesh, Actor, 0);

		// Fields
	public:
		Mesh*						Mesh;
		u32							Object;

		// Constructor
	public:
		AYUAPI AMesh();

		// Destructor
	public:
		AYUAPI virtual ~AMesh();

		// Methods
	public:
		AYUAPI virtual void preupdate(f32 dt);
		AYUAPI virtual void postupdate(f32 dt);
		AYUAPI virtual void prepare(RenderFrame &frame);
		AYUAPI void rigidBody(f32 massScalar);
	private:
		AYUAPI void prepare(RenderFrame &frame, MeshObject &object);
	};
};
