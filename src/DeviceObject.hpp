//------------------------------------//
//
// DeviceObject.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-14
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// DeviceObject class

	class DeviceObject : public Object
	{
		ObjectType(DeviceObject, Object, 0);
		
		// Fields
	private:
		List<DeviceObject>		_itor;

		// Constructor
	protected:
		AYUAPI DeviceObject();

		// Destructor
	public:
		AYUAPI virtual ~DeviceObject();

		// Methods
	public:
		AYUAPI virtual u32 type();
		AYUAPI virtual void deviceReset(bool theBegin);
		AYUAPI virtual void deviceLost(bool theEnd);

		// Friends
		friend class Device;
	};
};
