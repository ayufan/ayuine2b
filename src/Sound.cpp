//------------------------------------//
//
// SoundBuffer.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"
#include "../common/openal/include/al.h"
#include "../common/openal/include/alut.h"
#include "../common/openal/include/alc.h"
#include "../common/vorbis/include/codec.h"
#include "../common/vorbis/include/vorbisenc.h"
#include "../common/vorbis/include/vorbisfile.h"

namespace ayuine
{
	class OpenALException : public Exception
	{
		// Fields
	public:
		u32		ErrCode;

		// Constructor
	public:
		AYUAPI OpenALException(u32 errCode) : ErrCode(errCode) {
		}
	};

	//------------------------------------//
	// SoundBuffer: Error Handling

	static void ale_(const ExceptionTrace &trace) {
	#ifdef _DEBUG
		u32 err;
		if((err = alGetError()) != AL_NO_ERROR)
			Exception::ThrowException(OpenALException(err), trace);
	#endif
	}
	#define ale(x)	((x),ale_(newExceptionTrace()))

	//------------------------------------//
	// SoundBuffer: Consts

	const int MusicBufferSize = 32 * 4096;
	const int MusicBufferCount = 4;

	//------------------------------------//
	// SoundBuffer: Collections

	template<> Collection<SoundBuffer>::ObjectMap Collection<SoundBuffer>::_objects;

	//------------------------------------//
	// ov: Callbacks

	struct ovCallbacksImpl : ov_callbacks
	{
		// Constructor
	public:
		ovCallbacksImpl() {
			// Ustaw funkcj�
			reinterpret_cast<void*&>(read_func) = (void*)(read);
			reinterpret_cast<void*&>(seek_func) = (void*)(seek);
			reinterpret_cast<void*&>(close_func) = (void*)(close);
			reinterpret_cast<void*&>(tell_func) = (void*)(tell);
		}

		// Functions
	private:
		static size_t read(void *ptr, size_t s, size_t nmemb, File *file) {
			// Odczytaj dane z pliku
			return (size_t)file->read(ptr, s * nmemb) / s;
		}
		static s32 seek(File *file, ogg_int64_t offset, s32 whence) {
			// Ustaw pozycj� w pliku
			switch(whence) {
				case SEEK_CUR:
					file->at((size_t)((ogg_int64_t)file->at() + offset));
					break;

				case SEEK_SET:
					file->at((size_t)offset);
					break;

				case SEEK_END:
					file->at(file->length());
					break;
			}
			return 0;
		}
		static s32 close(File *file) {
			// Plik zamkniemy r�cznie
			return 0;
		}
		static long tell(File *file) {
			// Pobierz pozycj� w pliku
			return (long)file->at();
		}
	};

	//------------------------------------//
	// SoundSource: Constructor

	SoundSource::SoundSource(SoundBuffer *sound) {
		Guard {
			// Sprawd� dane
			Assert(sound);

			// Utw�rz nowe �r�d�o
			ale(alGenSources(1, &_source));

			// Ustaw bufor
			ale(alSourcei(_source, AL_BUFFER, sound->_buffer));

			// Zapisz dane
			_sound = sound;

			// Dodaj do listy
			_sound->_sourceList.push(this);
		}
		Unguard;
	}

	//------------------------------------//
	// SoundSource: Destructor

	SoundSource::~SoundSource() {
		Guard {
			// Zniszcz �r�d�o
			ale(alDeleteSources(1, &_source));

			// Usu� z listy
			_sound->_sourceList.eraseFind(this, 1);
		}
		Unguard;
	}

	//------------------------------------//
	// SoundSource: Base methods

	void SoundSource::play() {
		dGuard {
			// W��cz �r�d�o
			ale(alSourcePlay(_source));
		}
		dUnguard;
	}

	void SoundSource::pause() {
		dGuard {
			// Zatrzymaj �r�d�o
			ale(alSourcePause(_source));
		}
		dUnguard;
	}

	void SoundSource::stop() {
		dGuard {
			// Wy��cz �r�d�o
			ale(alSourceStop(_source));
		}
		dUnguard;
	}

	void SoundSource::rewind() {
		dGuard {
			// Przewi� �r�d�o
			ale(alSourceRewind(_source));
		}
		dUnguard;
	}

	bool SoundSource::stopped() {
		dGuard {
			s32 value;

			// Pobierz stan �r�d�a
			ale(alGetSourcei(_source, AL_SOURCE_STATE, &value));
			return value == AL_STOPPED || value == AL_INITIAL;
		}
		dUnguard;
	}

	bool SoundSource::paused() {
		dGuard {
			s32 value;

			// Pobierz stan �r�d�a
			ale(alGetSourcei(_source, AL_SOURCE_STATE, &value));
			return value == AL_PAUSED;
		}
		dUnguard;
	}

	f32 SoundSource::length() {
		dGuard {
			// D�ugo�� �r�d�a
			return _sound->length();
		}
		dUnguard;
	}

	//------------------------------------//
	// SoundSource: Source methods

	f32 SoundSource::pitch() {
		dGuard {
			f32 value;
			ale(alGetSourcef(_source, AL_PITCH, &value));
			return value;
		}
		dUnguard;
	}

	f32 SoundSource::gain() {
		dGuard {
			f32 value;
			ale(alGetSourcef(_source, AL_GAIN, &value));
			return value;
		}
		dUnguard;
	}

	f32 SoundSource::gainMin() {
		dGuard {
			f32 value;
			ale(alGetSourcef(_source, AL_MIN_GAIN, &value));
			return value;
		}
		dUnguard;
	}


	f32 SoundSource::gainMax() {
		dGuard {
			f32 value;
			ale(alGetSourcef(_source, AL_MAX_GAIN, &value));
			return value;
		}
		dUnguard;
	}


	f32 SoundSource::distance() {
		dGuard {
			f32 value;
			ale(alGetSourcef(_source, AL_MAX_DISTANCE, &value));
			return value;
		}
		dUnguard;
	}


	f32 SoundSource::coneOuterGain() {
		dGuard {
			f32 value;
			ale(alGetSourcef(_source, AL_CONE_OUTER_GAIN, &value));
			return value;
		}
		dUnguard;
	}


	f32 SoundSource::coneInnerAngle() {
		dGuard {
			f32 value;
			ale(alGetSourcef(_source, AL_CONE_INNER_ANGLE, &value));
			return value;
		}
		dUnguard;
	}


	f32 SoundSource::coneOuterAngle() {
		dGuard {
			f32 value;
			ale(alGetSourcef(_source, AL_CONE_OUTER_ANGLE, &value));
			return value;
		}
		dUnguard;
	}


	const vec3 &SoundSource::origin() {
		dGuard {
			ale(alGetSourcefv(_source, AL_POSITION, &_origin.X));
			return _origin;
		}
		dUnguard;
	}

	const vec3 &SoundSource::velocity() {
		dGuard {
			ale(alGetSourcefv(_source, AL_VELOCITY, &_velocity.X));
			return _velocity;
		}
		dUnguard;
	}

	const vec3 &SoundSource::direction() {
		dGuard {
			ale(alGetSourcefv(_source, AL_DIRECTION, &_direction.X));
			return _direction;
		}
		dUnguard;
	}

	bool SoundSource::looping() {
		dGuard {
			s32 value;
			ale(alGetSourcei(_source, AL_LOOPING, &value));
			return value == AL_TRUE;
		}
		dUnguard;
	}

	bool SoundSource::relative() {
		dGuard {
			s32 value;
			ale(alGetSourcei(_source, AL_SOURCE_RELATIVE, &value));
			return value == AL_TRUE;
		}
		dUnguard;
	}

	void SoundSource::pitch(f32 value) {
		dGuard {
			ale(alSourcef(_source, AL_PITCH, value));
		}
		dUnguard;
	}

	void SoundSource::gain(f32 value) {
		dGuard {
			ale(alSourcef(_source, AL_GAIN, value));
		}
		dUnguard;
	}

	void SoundSource::gainMin(f32 value) {
		dGuard {
			ale(alSourcef(_source, AL_MIN_GAIN, value));
		}
		dUnguard;
	}

	void SoundSource::gainMax(f32 value) {
		dGuard {
			ale(alSourcef(_source, AL_MAX_GAIN, value));
		}
		dUnguard;
	}

	void SoundSource::distance(f32 value) {
		dGuard {
			ale(alSourcef(_source, AL_MAX_DISTANCE, value));
		}
		dUnguard;
	}

	void SoundSource::coneOuterGain(f32 value) {
		dGuard {
			ale(alSourcef(_source, AL_CONE_OUTER_GAIN, value));
		}
		dUnguard;
	}

	void SoundSource::coneInnerAngle(f32 value) {
		dGuard {
			ale(alSourcef(_source, AL_CONE_INNER_ANGLE, value));
		}
		dUnguard;
	}

	void SoundSource::coneOuterAngle(f32 value) {
		dGuard {
			ale(alSourcef(_source, AL_CONE_OUTER_ANGLE, value));
		}
		dUnguard;
	}

	void SoundSource::origin(const vec3 &value) {
		dGuard {
			ale(alSourcefv(_source, AL_POSITION, &value.X));
		}
		dUnguard;
	}

	void SoundSource::velocity(const vec3 &value) {
		dGuard {
			ale(alSourcefv(_source, AL_VELOCITY, &value.X));
		}
		dUnguard;
	}

	void SoundSource::direction(const vec3 &value) {
		dGuard {
			ale(alSourcefv(_source, AL_DIRECTION, &value.X));
		}
		dUnguard;
	}

	void SoundSource::looping(bool value) {
		dGuard {
			ale(alSourcei(_source, AL_LOOPING, value ? AL_TRUE : AL_FALSE));
		}
		dUnguard;
	}

	void SoundSource::relative(bool value) {
		dGuard {
			ale(alSourcei(_source, AL_SOURCE_RELATIVE, value ? AL_TRUE : AL_FALSE));
		}
		dUnguard;
	}

	//------------------------------------//
	// SoundBuffer: Constructor

	SoundBuffer::SoundBuffer() {
		Guard {
			// Utw�rz bufor
			alGenBuffers(1, &_buffer);
		}
		Unguard;
	}

	//------------------------------------//
	// SoundBuffer: Destructor

	SoundBuffer::~SoundBuffer() {
		Guard {
			// Znisz bufor
			alDeleteBuffers(1, &_buffer);
		}
		Unguard;
	}

	//------------------------------------//
	// SoundBuffer: Methods

	void SoundBuffer::load(const string &soundFileName) {
		// Sprawd� nazw� pliku
		Assert(soundFileName.size());

		// Wczytaj .wav
		if(Engine::VFS->exists(va("%s.wav", soundFileName.c_str()), ftSound)) {
			SmartPtr<File> soundFile(Engine::VFS->loadFile(va("%s.wav", soundFileName.c_str()), ftSound));

			// Wczytaj d�wi�k
			load(soundFile);
		}

		// Wczytaj .ogg
		if(Engine::VFS->exists(va("%s.ogg", soundFileName.c_str()), ftSound)) {
			SmartPtr<File> soundFile(Engine::VFS->loadFile(va("%s.ogg", soundFileName.c_str()), ftSound));

			// Wczytaj d�wi�k
			load(soundFile);
		}

		// Inny plik
		if(Engine::VFS->exists(soundFileName, ftSound)) {
			SmartPtr<File> soundFile(Engine::VFS->loadFile(soundFileName, ftSound));

			// Wczytaj d�wi�k
			load(soundFile);
		}
	}

	void SoundBuffer::load(File *soundFile) {
		Guard {
			ALenum		format;
			ALsizei		freq;

			// Sprawd� czy plik zosta� otworzony poprawnie
			Assert(soundFile != nullptr);

			// Spr�buj za�adowa� ogg'a
			try {
				OggVorbis_File		oggFile;
				vorbis_info*			oggInfo;

				// Przesu� pozycj� w pliku na sam pocz�tek
				soundFile->at(0);

				// Otw�rz plik
				if(ov_open_callbacks(soundFile, &oggFile, nullptr, 0, ovCallbacksImpl()) == 0) {
					Array<c8> data;

					// Pobierz informacje o pliku
					oggInfo = ov_info(&oggFile, -1);

					// Ustaw dane o typie d�wi�ku
					format = oggInfo->channels == 1 ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16;
					freq = oggInfo->rate;

					// Wczytaj dane z pliku
					while(true) {
						c8 temp[4096];
						long bytes;
						s32 bitStream;

						// Zdekoduj plik
						bytes = ov_read(&oggFile, temp, sizeof(temp), 0, 2, 1, &bitStream);

						// Dodaj dane do bufora
						data.push(temp, bytes);

						// Wczytano jeszcze co�?
						if(bytes <= 0)
							break;
					}

					// Zamknij plik ogg
					ov_clear(&oggFile);

					// Zapisz wczytane dane
					ale(alBufferData(_buffer, format, data, data.size(), freq));

					// Ustaw flage ze za�adowano
					_loaded = true;
					return;
				}
			}
			catch(Exception &e) {
				// Wypisz b��d
				Engine::Log->error("OpenAL::OGG: %s", e.Message);
				return;
			}

			// Spr�buj za�adowa� wav'a
			try {
				ALvoid*		data;
				ALsizei		size;
				ALboolean loop;

				// Przesu� pozycj� w pliku na sam pocz�tek
				soundFile->at(0);

				// Odczytaj zawarto�� ca�ego pliku do pami�ci
				ale(alutLoadWAVFile(reinterpret_cast<const ALbyte*>(soundFile->read().c_str()), &format, &data, &size, &freq, &loop));

				// Zapisz wczytane dane
				ale(alBufferData(_buffer, format, data, size, freq));

				// Zako�cz �adowanie pliku
				ale(alutUnloadWAV(format, data, size, freq));

				// Ustaw flage ze za�adowano
				_loaded = true;
				return;
			}
			catch(Exception &e) {
				// Wypisz b��d
				Engine::Log->error("OpenAL::WAV: %s", e.Message);
				return;
			}
		}
		Unguard;
	}

	void SoundBuffer::unload() {
		// TODO
		// Trzeba za�adowa� jeszcze raz
		_loaded = false;
	}

	u32 SoundBuffer::frequency() {
		dGuard {
			s32 value;

			// Pobierz i zwr�� cz�stotliwo�� d�wi�ku
			ale(alGetBufferi(_buffer, AL_FREQUENCY, &value));
			return value;
		}
		dUnguard;
	}

	u32 SoundBuffer::bits() {
		dGuard {
			s32 value;

			// Pobierz i zwr�� ilo�� bit�w
			ale(alGetBufferi(_buffer, AL_BITS, &value));
			return value;
		}
		dUnguard;
	}

	u32 SoundBuffer::channels() {
		dGuard {
			s32 value;

			// Pobierz i zwr�� ilo�� kana��w
			ale(alGetBufferi(_buffer, AL_CHANNELS, &value));
			return value;
		}
		dUnguard;
	}

	u32 SoundBuffer::size() {
		dGuard {
			s32 value;

			// Pobierz i zwr�� rozmiar pr�bki
			ale(alGetBufferi(_buffer, AL_SIZE, &value));
			return value;
		}
		dUnguard;
	}

	f32 SoundBuffer::length() {
		dGuard {
			// Oblicz d�ugo�� �cie�ki
			return f32(size() * 8) / f32(frequency() * bits() * channels());
		}
		dUnguard;
	}

	//------------------------------------//
	// Sound: Constructor

	Sound::Sound() {
		// Sound Fields
		_soundGain = 1.0f;
	
		// Music Fields
		_music = 0;
		_musicFile = nullptr;
		_musicInfo = nullptr;
		_musicOggFile = nullptr;
		_musicLoop = true;

		Guard {
			Engine::Log->print("OpenAL: Initializing...");

			// Otw�rz urz�dzenie
			ALCdevice *device = alcOpenDevice("DirectSound3D");
			Assert("Can't create device" && device);

			// Utw�rz kontekst
			ALCcontext *context = alcCreateContext(device, nullptr);
			if(!context) {
				// Zamknij urz�dzenie
				alcCloseDevice(device);

				// Rzu� wyj�tek
				Assert("Can't create context" && context);
			}

			// Ustaw kontekst
			alcMakeContextCurrent(context);

			// Wy�wietl informacje o obs�udze d�wi�ku
			Engine::Log->print("OpenAL: Vender = %s", alGetString(AL_VENDOR));
			Engine::Log->print("OpenAL: Version = %s", alGetString(AL_VERSION));
			Engine::Log->print("OpenAL: Renderer = %s", alGetString(AL_RENDERER));
			Engine::Log->print("OpenAL: Extensions = %s", alGetString(AL_EXTENSIONS));

			// Wygeneruj �r�d�o muzyczne
			ale(alGenSources(1, &_music));

			// Ustaw parametry �r�d�a
			ale(alSource3f(_music, AL_POSITION,        0.0, 0.0, 0.0));
			ale(alSource3f(_music, AL_VELOCITY,        0.0, 0.0, 0.0));
			ale(alSource3f(_music, AL_DIRECTION,       0.0, 0.0, 0.0));
			ale(alSourcef	(_music, AL_ROLLOFF_FACTOR,  0.0          ));
			ale(alSourcei	(_music, AL_SOURCE_RELATIVE, AL_TRUE      ));
			ale(alSourcei	(_music, AL_LOOPING,				 AL_FALSE     ));

			// Utw�rz miejsce na plik
			_musicOggFile = new OggVorbis_File();
		}
		Unguard;
	}

	//------------------------------------//
	// Sound: Destructor

	Sound::~Sound() {
		Guard {
			Engine::Log->print("OpenAL: Releasing...");

			// Zatrzymaj odtwarzanie
			ale(alSourceStop(_music));

			// Zamknij plik ogg
			if(_musicFile) {
				// Wyczy�� dane
				_musicInfo = nullptr;

				// Zamknij ogg
				ov_clear(_musicOggFile);

				// Zamknij plik
				delete _musicFile;

				// TODO: Usu� bufory
				//if(_musicBuffers.size()) {
				//	ale(alDeleteBuffers(_musicBuffers.size(), &*_musicBuffers.begin()));
				//	_musicBuffers.clear();
				//}
			}

			// Zniszcz miejsce na plik
			delete _musicOggFile;
			_musicOggFile = nullptr;

			// Usu� �r�d�o
			ale(alDeleteSources(1, &_music));

			// Usu� wszystkie odtwarzane d�wi�ki
			for(u32 i = 0; i < _sounds.size(); i++) {
				delete _sounds.at(i);
			}
			_sounds.clear();

			// Pobierz aktywny kontekst
			ALCcontext *context = alcGetCurrentContext();

			if(context) {
				// Pobierz urz�dzenie dla kontekstu
				ALCdevice *device = alcGetContextsDevice(context);

				// Wy��cz kontekst
				alcMakeContextCurrent(nullptr);

				// Zwolnij kontekst
				alcDestroyContext(context);

				if(device) {
					// Zwolnij urz�dzenie
					alcCloseDevice(device);
				}
			}
		}
		Unguard;
	}

	//------------------------------------//
	// Sound: Music Methods

	bool Sound::musicUpdate(u32 buffer) {
		c8		  data[MusicBufferSize];
		size_t	size = 0;
		s32			result, section;

		// Wczytuj dop�ki nie wype�nimy ca�ego bufora
		while(size < sizeof(data)) {
			// Wczytaj dane do podr�cznego bufora
			result = ov_read(_musicOggFile, data + size, sizeof(data) - size, 0, 2, 1, &section);

			// Sprawd� ile danych wczytano
			if(result > 0)
				size += result;
			else {
				if(result < 0) {
					// TODO : Dok�adna informacja o b��dzie zwracana przez modu� do wczytywania ogg
					return false;
				}
				else {
					// Sprawd�, czy mamy przewin�c bufor do pocz�tku
					if(_musicLoop)
						ov_raw_seek(_musicOggFile, 0);
					else
						break;
				}
			}
		}

		// Je�li nie wczytano �adnych danych zwr�� b�ad
		if(size == 0)
			return false;

		// Uaktualnij dane w buforze
		ale(alBufferData(buffer, _musicInfo->channels == 1 ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16, data, size, _musicInfo->rate));
		return true;
	}

	void Sound::musicUpdate() {
		Guard {
			ALint left = 0;
			ALuint buffer = 0;

			// Sprawd�, czy plik muzyczny jest za�adowany
			if(_musicFile == nullptr)
				return;

			// Pobierz ilo�� przetworzonych bufor�w
			ale(alGetSourcei(_music, AL_BUFFERS_PROCESSED, &left));

			// Za�aduj nowe dane do bufor�w
			while(left-- > 0) {
				bool result;

				// Pobierz numer bufora
				ale(alSourceUnqueueBuffers(_music, 1, &buffer));

				// Zapisz nowe dane do bufora
				musicUpdate(buffer);

				// Dodaj bufor ponownie do listy
				ale(alSourceQueueBuffers(_music, 1, &buffer));
			}
		}
		Unguard;
	}

	void Sound::musicPlay(const string &name, bool loop) {
		Guard {
			if(name.length()) {
				// Zamknij plik ogg
				if(_musicFile) {
					// Wyczy�� dane
					_musicInfo = nullptr;

					// Zamknij ogg
					ov_clear(_musicOggFile);

					// Zamknij plik
					delete _musicFile;

					// Zatrzymaj odtwarzanie
					alSourceStop(_music);

					// Usu� bufory
					if(_musicBuffers.size()) {
						ale(alDeleteBuffers(_musicBuffers.size(), &_musicBuffers[0]));
						_musicBuffers.clear();
					}
				}

				SmartPtr<File> musicFile(Engine::VFS->loadFile(va("%s.ogg", name.c_str()), ftMusic));

				// Za�aduj nowy plik
				if(ov_open_callbacks(musicFile, _musicOggFile, nullptr, 0, ovCallbacksImpl()) != 0)
					Assert("Invalid ogg file" && false);

				// Pobierz parametry �cie�ki
				_musicInfo = ov_info(_musicOggFile, -1);
				_musicFile = musicFile.release();

				// Zarezerwuj miejsce na bufory
				_musicBuffers.resize(MusicBufferCount);

				// Utw�rz bufory
				ale(alGenBuffers(_musicBuffers.size(), &_musicBuffers[0]));

				// Prze�aduj do nich dane
				for(u32 i = 0; i < _musicBuffers.size(); i++)
					if(!musicUpdate(_musicBuffers[i]))
						break;

				// Skojarz je z �r�d�em
				ale(alSourceQueueBuffers(_music, _musicBuffers.size(), &_musicBuffers[0]));
			}

			// Ustaw flag� powtarzania d�wi�ku
			_musicLoop = loop;

			// W��cz odtwarzanie �r�d�a
			ale(alSourcePlay(_music));
		}
		Unguard;
	}

	void Sound::musicPause() {
		Guard {
			// Zapazuuj odgrywanie d�wi�ku
			ale(alSourcePause(_music));
		}
		Unguard;
	}

	void Sound::musicStop() {
		Guard {
			// Zatrzymaj odgrywanie d�wi�ku
			ale(alSourceStop(_music));
		}
		Unguard;
	}

	bool Sound::musicPaused() {
		Guard {
			ALint	sourceState;

			// Sprawd� stan �r�d�a
			ale(alGetSourcei(_music, AL_SOURCE_STATE, &sourceState));
			return sourceState == AL_PAUSED;
		}
		Unguard;
	}

	bool Sound::musicStopped() {
		Guard {
			ALint	sourceState;

			// Sprawd� stan �r�d�a
			ale(alGetSourcei(_music, AL_SOURCE_STATE, &sourceState));
			return sourceState == AL_STOPPED || sourceState == AL_INITIAL;
		}
		Unguard;
	}

	f32 Sound::musicGain() {
		Guard {
			f32 gain;

			// Pobierz g�osno��
			ale(alGetSourcef(_music, AL_GAIN, &gain));
			return gain;
		}
		Unguard;
	}

	void Sound::musicGain(f32 g) {
		Guard {
			// Ustaw g�osno��
			ale(alSourcef(_music, AL_GAIN, g));
		}
		Unguard;
	}

	//------------------------------------//
	// Sound: Methods

	void Sound::update() {
		Guard {
			vec3 origin, velocity, direction;

			// Pobierz parametry s�uchacza
			// TODO : Obs�uga wektora pr�dko�ci dla s�uchacza (czyli zastosowanie efektu dopplera)
			origin = vec3();//TODO: Engine::Current->modelview().origin();
			direction = vec3();//TODO: Engine::Current->modelview().at();
			velocity = vec3(0, 0, 0);

			// Ustaw parametry s�uchacza
			//ale(alListenerfv(AL_POSITION, &origin.X));
			//ale(alListenerfv(AL_VELOCITY, &velocity.X));
			//ale(alListenerfv(AL_DIRECTION, &direction.X));

			// Uaktualnij muzyk�
			musicUpdate();

			// Usu� d�wi�ki, kt�re przesta�y by� odgrywane
			for(u32 index = _sounds.size(); index-- > 0; ) {
				// Sprawd�, czy �r�d�o jest zatrzymane (zako�czony?)
				if(_sounds.at(index)->stopped()) {
					// Zniszcz �r�d�o
					delete _sounds.at(index);

					// Usu� �r�d�o z listy
					_sounds.erase(index);
				}
			}
		}
		Unguard;
	}

	void Sound::play(const string &name, bool loop, bool stopPrev) {
		Guard {
			if(stopPrev) {
				// Przerwij odtwarzanie poprzednich d�wi�k�w
				for(u32 i = 0; i < _sounds.size(); i++)
					delete _sounds.at(i);
				_sounds.reset();
			}

			// Za�aduj nowy d�wi�k
			if(name.length()) {
				SoundSource *source;

				// Utw�rz nowe �r�d�o d�wi�ku
				source = new SoundSource(SoundBuffer::get(name));

				// Ustaw �r�d�o d�wi�ku
				source->gain(_soundGain);
				source->relative(true);
				source->looping(loop);

				// Dodaj �r�d�o d�wi�ku do listy �r�de�
				_sounds.push(source);
			}
		}
		Unguard;
	}

	f32 Sound::gain() {
		dGuard {
			// Pobierz g�osno�� nowych �r�de� d�wi�ku
			return _soundGain;
		}
		dUnguard;
	}

	void Sound::gain(f32 g) {
		dGuard {
			// Ustaw g�osno�� nowych �r�de� d�wi�ku
			_soundGain = g;
		}
		dUnguard;
	}
};
