//------------------------------------//
//
// VertexType.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// VertexType: Constructor

	VertexType::VertexType(VertexDeclArray elements) : _decl(nullptr) {
		// Skopiuj opis wierzcho�ka
		memcpy(_elements, elements, sizeof(VertexDeclElement) * (D3DXGetDeclLength(elements) + 1));

		// Oblicz d�ugo�� opisu wierzcho�ka
		_stride = D3DXGetDeclVertexSize(_elements, 0);

		// Utw�rz obiekt
		deviceReset();
	}

	VertexType::VertexType(u32 format) : _decl(nullptr) {
		// Pobierz opis wierzcho�ka
		dxe(D3DXDeclaratorFromFVF(format, _elements));

		// Oblicz d�ugo�� opisu wierzcho�ka
		_stride = D3DXGetDeclVertexSize(_elements, 0);

		// Utw�rz obiekt
		deviceReset();
	}

	//------------------------------------//
	// VertexType: Destructor

	VertexType::~VertexType() {
		// Zniszcz obiekt
		deviceLost(true);
	}

	//------------------------------------//
	// VertexType: Properties

	u32	VertexType::stride() {
		return _stride;
	}

	VertexDeclArray VertexType::elements() {
		return &_elements[0];
	}

	//------------------------------------//
	// VertexType: Methods

	u32 VertexType::type() {
		return 0;
	}

	void VertexType::deviceReset() {
		// Utw�rz opis
		if(Engine::Device->d3ddev()) {
			dxe(Engine::Device->d3ddev()->CreateVertexDeclaration(_elements, &_decl));
		}
	}

	void VertexType::deviceLost(bool theEnd) {
		// Zniszcz opis wierzcho�ka
		if(_decl) {
			_decl->Release();
			_decl = nullptr;
		}
	}
	
	void VertexType::bind() {
		if(!this)
			return;

		// Podepnij opis wierzcho�k�w
		Engine::Device->_decl[1] = _decl;
	}
};
