//------------------------------------//
//
// MeshUni.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-13
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	struct UniImporter
	{
		enum {
			MESH_MAGIC = ('m' | 'i' << 8 | '0' << 16 | '4' << 24),
		};

		struct Vertex : public MeshVertex {
			vec3 Vectors[3];

			void unserialize(Serializer &s) {
				s >> Origin >> Vectors[2] >> Coords;
			}
		};

		struct Surface {
			char name[128];								// surface name
			vector<Vertex> vertex;
			int num_triangles;
			vector<int> indices;
			bbox box;
			bsphere sphere;

			void unserialize(Serializer &s) {
				s.read(name, sizeof(name));
				s >> vertex;
				s >> num_triangles;
				if(num_triangles) {
					indices.resize(num_triangles * 3);
					s.read(&indices[0], sizeof(int) * num_triangles * 3);
				}
				if(vertex.size()) {
					box = bbox(&vertex[0].Origin, vertex.size(), sizeof(Vertex));
					sphere = bsphere(&vertex[0].Origin, vertex.size(), sizeof(Vertex));
				}
			}
		};
		
		vector<Surface> surfaces;

		void load(const string &name) {
			Serializer s(Engine::Current->loadFile(va("%s.mesh", name.c_str()), ftMesh));
			int magic;

			// Pobierz nag��wek
			s >> magic;

			// Sprawd� nag��wek
			if(magic != MESH_MAGIC)
				throw(std::runtime_error("MeshU: Invalid mesh format !"));

			// Wczytaj powierzchnie
			s >> surfaces;
		}
	};

	void Mesh::loadMeshU(const string &name) {
		UniImporter importer;

		// Wczytaj mesha
		importer.load(name);

		// Dodaj wszystkie powierzchnie
		foreach(vector<UniImporter::Surface>, i, importer.surfaces) {
			MeshObject object;
			MeshSurface surface;
			vec3 avgNormal;
					
			// Ustaw parametry obiektu
			object.Name = i->name;
			object.PRT = va("%s/prt_%s", name.c_str(), i->name);
			object.VertexStart = Vertices.size();
			object.IndexStart = Indices.size();
			object.Box = i->box;
			object.Sphere = i->sphere;

			// Ustaw pocz�tkowe parametry powierzchnii
			surface.Material = va("%s/%s", name.c_str(), i->name);
			surface.IndexStart = Indices.size();
			surface.Box = i->box;
			surface.Sphere = i->sphere;

			// Zapisz wektor styczne dla ka�dego wierzcho�ka ka�dego tr�jk�ta
			for(u32 j = 0; j < i->num_triangles; j++) {
				// Zamie� indeksy
				std::swap(i->indices.at(j * 3 + 0), i->indices.at(j * 3 + 2));

				// Pobierz wierzcho�ki
				UniImporter::Vertex v[3] = {
					i->vertex.at(i->indices.at(j * 3 + 0)), 
					i->vertex.at(i->indices.at(j * 3 + 1)),
					i->vertex.at(i->indices.at(j * 3 + 2))};
				vec3 vectors[3];

				// Oblicz wektory styczne
				tangentVectors::calculateVectors(
					v[0].Origin, v[1].Origin, v[2].Origin,
					v[0].Coords[0], v[1].Coords[0], v[2].Coords[0],
					vectors);

				// Zapisz wektory styczne
				for(u32 k = 0; k < 3; k++) {
					v[k].Vectors[0] += vectors[0];
					v[k].Vectors[1] += vectors[1];
				}
				avgNormal += vectors[2];
			}

			// Doko�cz obliczenia wierzcho�k�w
			foreach(vector<UniImporter::Vertex>, j, i->vertex) {
				// Znormalizuj wektory styczne
				j->Vectors[0] = j->Vectors[0].normalize();
				j->Vectors[1] = j->Vectors[1].normalize();

				// Oblicz wektory styczne
				j->Normal = vec4::compress(vec4(j->Vectors[2], 1.0f));
				j->Tangent = vec4::compress(tangentVectors::calculateTangentFlag(j->Vectors[2], j->Vectors));
			}

			// Loguj
			Engine::Current->print("MeshU: %s::%s has %i indices and %i vertices", name.c_str(), i->name, i->indices.size(), i->vertex.size());

			// Dodaj wierzcho�ki i indeksy
			Vertices.insert(Vertices.end(), i->vertex.begin(), i->vertex.end());
			Indices.insert(Indices.end(), i->indices.begin(), i->indices.end());

			// Znormalizuj wektor
			surface.Plane = vec4(/*avgNormal.normalize()*/vec3(0, 0, 1), i->sphere.Center);

			// Ustaw ko�cowe parametry powierzchnii i obiektu
			surface.IndexCount = i->indices.size();
			object.IndexCount = i->indices.size();
			object.VertexCount = i->vertex.size();

			// Dodaj powierzchni� do obiektu
			object.Surfaces.push_back(surface);
			
			// Dodaj obiekt do mesha
			Objects.push_back(object);
		}
		_loaded = true;
	}
};
