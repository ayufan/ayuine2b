//------------------------------------//
//
// Shader.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// iShaderInclude structure

	struct iShaderInclude : public ID3DXInclude
	{
		STDMETHOD(Open)(THIS_ D3DXINCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes) {
			// Wczytaj plik
			string data = Engine::VFS->file(pFileName, ftShader);

			// Sprawd� co wczytano
			if(data.empty())
				return S_FALSE;

			// Zaalokuj dane
			*ppData = memcpy(new u8[data.size()], data.c_str(), data.size());
			*pBytes = data.size();
			return S_OK;
		}
		STDMETHOD(Close)(THIS_ LPCVOID pData) {
			// Usu� dane
			delete [] (u8*)pData;
			return S_OK;
		}
	};

	//------------------------------------//
	// ShaderCompiler: Functions

	ShaderCode ShaderCompiler::compileFromCode(const string &shaderCode, const string &shaderEntry, ShaderVersion sv, u32 flags) {
		// Sprawd� kod shadera
		Assert(shaderCode.size());

		// Sprawd� czy jest to jaki� #include :)
		if(shaderCode[0] == '@')
			return compileFromFile(shaderCode.substr(1), shaderEntry, sv, flags);

		D3DXMACRO macros[10];
		u32 macroCount = 0;
		string shaderType, shaderVersion;
		string shadowVersion;

		// Wyznacz wersje shadera
		switch(sv) {
			case svVertexAuto:
				shaderType = "_VERTEX";
				shaderVersion = D3DXGetVertexShaderProfile(Engine::Device->d3ddev());
				break;

			case svVertex11:				
				shaderType = "_VERTEX";
				shaderVersion = "vs_1_1";
				break;

			case svVertex20:				
				shaderType = "_VERTEX";
				shaderVersion = "vs_2_0";
				break;

			case svVertex30:
				shaderType = "_VERTEX";
				shaderVersion = "vs_3_0";
				break;

			case svPixelAuto:
				shaderType = "_PIXEL";
				shaderVersion = D3DXGetPixelShaderProfile(Engine::Device->d3ddev());
				break;

			case svPixel11:
				shaderType = "_PIXEL";
				shaderVersion = "ps_1_1";
				break;

			case svPixel14:
				shaderType = "_PIXEL";
				shaderVersion = "ps_1_4";
				break;

			case svPixel20:
				shaderType = "_PIXEL";
				shaderVersion = "ps_2_0";
				break;

			case svPixel30:
				shaderType = "_PIXEL";
				shaderVersion = "ps_3_0";
				break;

			case svTextureAuto:
			case svTexture10:
				shaderType = "_TEXTURE";
				shaderVersion = "tx_1_0";
				break;

			default:
				Assert("Invalid ShaderVersion" && false);
		}

		// Wyczy�� wszystkie makra
		memset(macros, 0, sizeof(macros));

		// Dodaj typ shadera
		macros[macroCount].Name = shaderType.c_str();
		macros[macroCount++].Definition = shaderVersion.c_str();

		// Dodaj makra
		if(flags) {
			// Dodaj informacje o skinningu
			if(flags & sfSkinning) {
				macros[macroCount].Name = "SKINNING";
				macros[macroCount++].Definition = "1";
			}

			// Dodaj informacje o PRT
			if(flags & sfPRT) {
				macros[macroCount].Name = "PRT";
				macros[macroCount++].Definition = "1";
			}

			// Dodaj informacje o S�o�cu
			if(flags & sfSun) {
				macros[macroCount].Name = "SUN";
				macros[macroCount++].Definition = "1";
			}

			// Dodaj wersji cienii
			if(flags & sfShadows && Engine::Config->Shadows) {
				shadowVersion = va("%i", u32(Engine::Config->Shadows));
				macros[macroCount].Name = "SHADOWS";
				macros[macroCount++].Definition = shadowVersion.c_str();
			}

			// Dodaj informacje o typie shadera
			switch(flags & sfRenderTypeMask) {
				case rtAmbient:
					macros[macroCount].Name = "AMBIENT";
					macros[macroCount++].Definition = "1";
					break;

				case rtLight:
					macros[macroCount].Name = "LIGHT";
					macros[macroCount++].Definition = "1";
					break;

				case rtRefraction:
					macros[macroCount].Name = "REFRACTION";
					macros[macroCount++].Definition = "1";
					break;

				case rtCustom:
					macros[macroCount].Name = "CUSTOM";
					macros[macroCount++].Definition = "1";
					break;
			}
		}

		SmartPtr<ID3DXBuffer> shaderText, shaderError;
		//LPD3DXBUFFER shaderText, shaderError;

		// Przekompiluj shadera
		if(FAILED(D3DXPreprocessShader(shaderCode.c_str(), shaderCode.length(), macros, &iShaderInclude(), &shaderText.Value, &shaderError.Value))) {
			string errorMessage = (c8*)shaderError->GetBufferPointer();

			// Zniszcz bufory
			//if(shaderError)
			//	shaderError->Release();

			// Wypisz b��d
			MessageBox(nullptr, errorMessage.c_str(), "Shader preprocess error", MB_OK);
			exit(1);
		}

		// Wyznacz nazw� shadera w cacheu
		string shaderCache = va("cache/0%08X_%s.csh", hash((c8*)shaderText->GetBufferPointer()), shaderVersion.c_str());

		// Spr�buj wczyta� shadera
		try {
			// Wczytaj shadera z pliku
			string shaderFunction = Engine::VFS->file(shaderCache); 

			// Zwr�� shadera
			return ShaderCode((DWORD*)&shaderFunction[0], shaderFunction.size() / 4);
		}
		catch(Exception &e) {
			Engine::Log->debug("ShaderCompiler::compileFromCode: Can't find shader %s", shaderCache.c_str());
		}

		//LPD3DXBUFFER shaderFunction;
		SmartPtr<ID3DXBuffer> shaderFunction;

		// Skompiluj shadera
		if(FAILED(D3DXCompileShader((LPCSTR)shaderText->GetBufferPointer(), shaderText->GetBufferSize(), macros, &iShaderInclude(), shaderEntry.c_str(), shaderVersion.c_str(), 0, &shaderFunction.Value, &shaderError.Value, nullptr))) {
			string errorMessage = (c8*)shaderError->GetBufferPointer();

			// Zniszcz bufory
			//if(shaderError)
			//	shaderError->Release();
			//if(shaderText)
			//	shaderText->Release();

			// Wypisz b��d
			MessageBox(nullptr, errorMessage.c_str(), "Shader compile error", MB_OK);
			exit(1);
		}

		// Zapisz shadera do cache'u
		try {
			SmartPtr<File> shaderCacheFile(Engine::VFS->saveFile(shaderCache));

			// Zapisz shadera
			shaderCacheFile->write(shaderFunction->GetBufferPointer(), shaderFunction->GetBufferSize());
		}
		catch(Exception &e) {
			Engine::Log->debug("ShaderCompiler::compileFromCode: Can't save shader %s", shaderCache.c_str());
		}
		
		// Zapisz shadera
		ShaderCode code((DWORD*)shaderFunction->GetBufferPointer(), shaderFunction->GetBufferSize() / 4);

		// Zniszcz wszystkie bufory
		//if(shaderText)
		//	shaderText->Release();
		//if(shaderFunction)
		//	shaderFunction->Release();
		return code;
	}

	ShaderCode ShaderCompiler::compileFromFile(const string &shaderFile, const string &shaderEntry, ShaderVersion sv, u32 flags) {
		return compileFromCode(Engine::VFS->file(shaderFile, ftShader), shaderEntry, sv, flags);
	}

	//------------------------------------//
	// PixelShader: Constructor

	PixelShader::PixelShader(const ShaderCode &code) {
		LPCSTR samplers[16];
		UINT samplerCount;

		// Zapisz kod shadera
		_code = code;

		// Utw�rz shadera
		dxe(Engine::Device->d3ddev()->CreatePixelShader(&_code.at(0), &_shader));

		// Pobierz samplery
		dxe(D3DXGetShaderSamplers(&_code.at(0), samplers, &samplerCount));

		// Przepisz dane
		for(u32 i = 0; i < samplerCount; i++) {
			if(!samplers[i])
				continue;
			_samplers[i] = samplers[i];
		}
	}

	//------------------------------------//
	// PixelShader: Destructor

	PixelShader::~PixelShader() {
		if(_shader) {
			_shader->Release();
			_shader = nullptr;
		}
	}

	//------------------------------------//
	// PixelShader: Properties

	const string &PixelShader::sampler(u32 i) const {
		return _samplers[i];
	}

	//------------------------------------//
	// PixelShader: Methods

	void PixelShader::bind() {
		if(!this)
			return;

		// Ustaw pixel shader
		Engine::Device->_pixelShader[1] = _shader;
	}

	void PixelShader::unbind() {
		if(!this)
			return;

		// Ustaw pixel shader
		Engine::Device->_pixelShader[1] = nullptr;
	}

	//------------------------------------//
	// PixelShader: Functions

	PixelShader *PixelShader::createFromCode(const string &shaderCode, const string &shaderEntry, u32 flags) {
		return new PixelShader(ShaderCompiler::compileFromCode(shaderCode, shaderEntry, svPixelAuto, flags));
	}

	PixelShader *PixelShader::createFromFile(const string &shaderFile, const string &shaderEntry, u32 flags) {
		return new PixelShader(ShaderCompiler::compileFromFile(shaderFile, shaderEntry, svPixelAuto, flags));
	}
	
	//------------------------------------//
	// VertexShader: Constructor

	VertexShader::VertexShader(const ShaderCode &code) {
		// Zapisz kod shadera
		_code = code;

		// Utw�rz shadera
		dxe(Engine::Device->d3ddev()->CreateVertexShader(&_code.at(0), &_shader));
	}

	//------------------------------------//
	// VertexShader: Destructor

	VertexShader::~VertexShader() {
		if(_shader) {
			_shader->Release();
			_shader = nullptr;
		}
	}

	//------------------------------------//
	// VertexShader: Methods

	void VertexShader::bind() {
		if(!this)
			return;

		// Ustaw vertex shader
		Engine::Device->_vertexShader[1] = _shader;
	}

	void VertexShader::unbind() {
		if(!this)
			return;

		// Ustaw vertex shader
		Engine::Device->_vertexShader[1] = nullptr;
	}

	//------------------------------------//
	// VertexShader: Functions

	VertexShader *VertexShader::createFromCode(const string &shaderCode, const string &shaderEntry, u32 flags) {
		return new VertexShader(ShaderCompiler::compileFromCode(shaderCode, shaderEntry, svVertexAuto, flags));
	}

	VertexShader *VertexShader::createFromFile(const string &shaderFile, const string &shaderEntry, u32 flags) {
		return new VertexShader(ShaderCompiler::compileFromFile(shaderFile, shaderEntry, svVertexAuto, flags));
	}
};
