//------------------------------------//
//
// TypeInfo.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-2
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// TypeInfoFlags enumeration

	enum TypeInfoFlags
	{
		tifNone,
		tifPrimitive
	};

	//------------------------------------//
	// TypeInfo class

	class TypeInfo
	{
		// Fields
	public:
		string					Name;
		u32							Hash;
		u32							Flags;
		const TypeInfo*	Super;

		// Constructors
	public:
		TypeInfo() {
			Hash = 0;
			Flags = 0;
			Super = nullptr;
		}
		TypeInfo(const string &name, u32 flags = tifNone) {
			Name = name;
			Hash = hash(name);
			Flags = flags;
			Super = nullptr;
		}
		TypeInfo(const TypeInfo &super, const string &name, u32 flags = tifNone) {
			Name = name;
			Hash = hash(name);
			Flags = flags;
			Super = &super;
		}

		// Operators
	public:
		bool operator == (const TypeInfo &other) const {
			return Hash == other.Hash;
		}
		bool operator != (const TypeInfo &other) const {
			return Hash != other.Hash;
		}
	};

	//------------------------------------//
	// BaseTypeOf class

	class BaseTypeOf
	{
		// Fields
	public:
		const TypeInfo*	Type;

		// Constructor
	public:
		BaseTypeOf() {
			Type = nullptr;
		}

		// Operators
	public:
		operator const TypeInfo* () const {
			return Type;
		}
		const TypeInfo* operator -> () const {
			return Type;
		}
	};

	//------------------------------------//
	// TypeOf class

	template<typename _Type = class Object>
	class TypeOf : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			Type = &_Type::typeInfo();
		}
		TypeOf(const _Type* object) {
			Type = object ? &object->typeInfoOf() : nullptr;
		}

		// Functions
	public:
		static _Type* cast(const class Object* object) {
			// Sprawd� adres obiektu
			if(!object)
				return nullptr;

			// Pobierz typy
			const TypeInfo *objectType = &object->typeInfoOf();
			const TypeInfo *queryType = &_Type::typeInfo();

			// Sprawd� czy to jest nasz typ
			while(objectType && objectType != queryType)
				objectType = objectType->Super;

			// Sprawd� czy znale�li�my nasz typ
			if(objectType)
				return (_Type*)object;
			return nullptr;
		}
	};

	//------------------------------------//
	// TypeOf Specialzations

	template<>
	class TypeOf<c8> : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			static TypeInfo type("c8", tifPrimitive);
			Type = &type;
		}

		// Limits
	public:
		static c8 e() {
			return 0;
		}
		static c8 min() {
			return (c8)0x00;
		}
		static c8 max() {
			return (c8)0xFF;
		}
	};

	template<>
	class TypeOf<s8> : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			static TypeInfo type("s8", tifPrimitive);
			Type = &type;
		}

		// Limits
	public:
		static s8 e() {
			return 0;
		}
		static s8 min() {
			return -0x7f;
		}
		static s8 max() {
			return 0x7f;
		}
	};

	template<>
	class TypeOf<s16> : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			static TypeInfo type("s16", tifPrimitive);
			Type = &type;
		}

		// Limits
	public:
		static s16 e() {
			return 0;
		}
		static s16 min() {
			return SHRT_MIN;
		}
		static s16 max() {
			return SHRT_MAX;
		}
	};

	template<>
	class TypeOf<s32> : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			static TypeInfo type("s32", tifPrimitive);
			Type = &type;
		}

		// Limits
	public:
		static s32 e() {
			return 0;
		}
		static s32 min() {
			return LONG_MIN;
		}
		static s32 max() {
			return LONG_MAX;
		}
	};

	template<>
	class TypeOf<s64> : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			static TypeInfo type("s64", tifPrimitive);
			Type = &type;
		}

		// Limits
	public:
		static s64 e() {
			return 0;
		}
		static s64 min() {
			return LLONG_MIN;
		}
		static s64 max() {
			return LLONG_MAX;
		}
	};

	template<>
	class TypeOf<u8> : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			static TypeInfo type("u8", tifPrimitive);
			Type = &type;
		}

		// Limits
	public:
		static u8 e() {
			return 0;
		}
		static u8 min() {
			return 0;
		}
		static u8 max() {
			return 0xFF;
		}
	};

	template<>
	class TypeOf<u16> : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			static TypeInfo type("u16", tifPrimitive);
			Type = &type;
		}

		// Limits
	public:
		static u16 e() {
			return 0;
		}
		static u16 min() {
			return 0;
		}
		static u16 max() {
			return USHRT_MAX;
		}
	};

	template<>
	class TypeOf<u32> : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			static TypeInfo type("u32", tifPrimitive);
			Type = &type;
		}

		// Limits
	public:
		static u32 e() {
			return 0;
		}
		static u32 min() {
			return 0;
		}
		static u32 max() {
			return ULONG_MAX;
		}
	};

	template<>
	class TypeOf<u64> : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			static TypeInfo type("u64", tifPrimitive);
			Type = &type;
		}

		// Limits
	public:
		static u64 e() {
			return 0;
		}
		static u64 min() {
			return 0;
		}
		static u64 max() {
			return ULLONG_MAX;
		}
	};

	template<>
	class TypeOf<f32> : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			static TypeInfo type("f32", tifPrimitive);
			Type = &type;
		}

		// Limits
	public:
		static f64 e() {
			return 0.001f;
		}
		static f64 min() {
			return -FLT_MAX;
		}
		static f64 max() {
			return FLT_MAX;
		}
	};

	template<>
	class TypeOf<f64> : public BaseTypeOf
	{
		// Constructor
	public:
		TypeOf() {
			static TypeInfo type("f64", tifPrimitive);
			Type = &type;
		}

		// Limits
	public:
		static f64 e() {
			return 0.000001f;
		}
		static f64 min() {
			return -DBL_MAX;
		}
		static f64 max() {
			return DBL_MAX;
		}
	};

	//------------------------------------//
	// Defines

#define ValueType(Value, Flags)		\
	public: \
	static const TypeInfo &typeInfo() { \
		static TypeInfo type(#Value, Flags); \
		return type; \
	} \
	private:

#define BaseObjectType(Class, Flags) \
	public: \
	static const TypeInfo &typeInfo() { \
		static TypeInfo type(#Class, Flags); \
		return type; \
	} \
	virtual const TypeInfo &typeInfoOf() const { \
		return typeInfo(); \
	} \
	private:

#define ObjectType(Class, BaseClass, Flags)		\
	public: \
	typedef BaseClass SuperClass; \
	static const TypeInfo &typeInfo() { \
		static TypeInfo type(SuperClass::typeInfo(), #Class, Flags); \
		return type; \
	} \
	virtual const TypeInfo &typeInfoOf() const { \
		return typeInfo(); \
	} \
	private:
};
