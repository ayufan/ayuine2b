//------------------------------------//
//
// String.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-2
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class StringEntry
	{
		// Fields
	public:
		u32						Used;
		u32						Size;
		StringEntry*	Next;

		// Properties
	public:

	};

	class StringManager
	{
		// Static Fields
	public:
		static StringEntry*		Used;
		static StringEntry*		Free[6];
	};

	//------------------------------------//
	// String class

	class String 
	{
		ValueType(String, 0);

		// Fields
	public:

	};
};
