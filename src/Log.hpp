//------------------------------------//
//
// Log.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// LogMessageType enumeration

	enum LogMessageType
	{
		lmtInfo,
		lmtDebug,
		lmtWarning,
		lmtError,
		lmtCritical
	};

	//------------------------------------//
	// LogMessage class

	class LogMessage
	{
		ValueType(LogMessage, 0);

		// Fields
	public:
		LogMessageType	Type;
		f32							Time;
		string					Message;
		string					CallStack;

		// Constructor
	public:
		AYUAPI LogMessage(const string &message, LogMessageType lmt = lmtInfo, const string &callStack = "");

		// Properties
	public:
		AYUAPI string time() const;
		AYUAPI string type() const;
		AYUAPI string entry() const;
	};

	//------------------------------------//
	// Log class

	class Log
	{
		// Fields
	private:
		Array<LogMessage>				_messages;

		// Constructor
	public:
		Log();

		// Destructor
	public:
		~Log();

		// Methods
	public:
		AYUAPI void print(string::const_pointer, ...);
		AYUAPI void debug(string::const_pointer, ...);
		AYUAPI void error(string::const_pointer, ...);
		AYUAPI void warning(string::const_pointer, ...);
		AYUAPI void dumpLog(const string &file);
		AYUAPI void clearLog();
	};
};
