//------------------------------------//
//
// Object.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-1
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class Object
	{
		BaseObjectType(Object, 0);

		// Events
	public:
		Event<Object*>	OnDispose;

		// Constructor
	public:
		AYUAPI Object();
	private:
		AYUAPI Object(const Object &other);

		// Destructor
	public:
		AYUAPI virtual ~Object();
	};
};
