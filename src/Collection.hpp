//------------------------------------//
//
// Collection.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-9
//
//------------------------------------//

#pragma once

namespace ayuine
{
	template<typename Type>
	class Collection
	{
		ValueType(Collection<Type>, 0);

		// Typedefs
	private:
		typedef std::map<u32, Type*>		ObjectMap;

		// Fields
	private:
		string _name;
		u32	_hash;
		bool _cache;
	protected:
		bool _loaded;

		// Static Fields
	private:
		AYUAPI static ObjectMap _objects;

		// Constructor
	protected:
		Collection() {
			_hash = 0;
			_loaded = false;
		}

		// Destructor
	protected:
		~Collection() {
			if(_hash) {
				// Usun obiekt z mapy
				_objects.erase(_hash);
			}
		}

		// Properties
	public:
		string::const_pointer name() const {
			return this ? _name.c_str() : nullptr;
		}
		u32 hash() const {
			return this ? _hash : nullptr;
		}

		// Functions
	public:
		static void check(Type* object) {
			if(!object->_loaded)
				object->load(object->_name);
		}
		static void cache() {
			// Prze�aduj wszystkie obiekty
			for(typename ObjectMap::iterator i = _objects.begin(); i != _objects.end(); i++) {
				// Za�aduj obiekt
				if(i->second->_cache)
					check(i->second);
			}
		}
		static void unload() {
			// Zniszcz za�adowane obiekty
			for(typename ObjectMap::iterator i = _objects.begin(); i != _objects.end(); i++) {
				if(i->second->_loaded)
					i->second->unload();
			}
		}
		static void clear() {
			// Zniszcz za�adowane obiekty
			for(typename ObjectMap::iterator i = _objects.begin(); i != _objects.end(); ) {
				delete (i++)->second;
			}
		}
		static void reset() {
			// Oznacz wszystkie jako nie cache'owane
			for(typename ObjectMap::iterator i = _objects.begin(); i != _objects.end(); i++) {
				i->second->_cache = false;
			}
		}
		static u32 key(const string &name) {
			return ayuine::hash(name);
		}
		static Type* find(u32 hash) {
			typename ObjectMap::iterator i;

			// Znajdz obiekt
			i = _objects.find(hash);

			// Zwroc wskaznik na obiekt
			if(i != _objects.end())
				return i->second;
			return nullptr;
		}
		static Type* find(const string &name) {
			return find(key(name));
		}
		static Type* cache(const string &name) {
			Type* object;
			u32 hash = key(name);

			// Znajdz obiekt
			if(object = find(hash)) {
				// Oznacz jako cache'owany
				object->_cache = true;
			}
			else {
				// Wczytaj nowy obiekt lub utworz domyslny
				object = new Type();

				// Zapisz dane
				object->_name = name;
				object->_hash = hash;
				object->_cache = true;

				// Dodaj obiekt do mapy 
				_objects[hash] = object;
			}

			// Dodaj obiekt do mapy
			return object;
		}
		static Type* get(const string &name) {
			Type* object;

			// Pobierz obiekt
			object = cache(name);
			assert(object);
			check(object);
			return object;
		}
	};

	template<typename Type>
	class Resource
	{
		ValueType(Resource<Type>, 0);

		// Fields
	private:
		string			_name;
		Type*				_value;

		// Constructor
	public:
		Resource() {
			_value = nullptr;
		}
		Resource(const string &name) {
			_value = nullptr;
			_name = name;
		}
		Resource(string::const_pointer name) {
			_value = nullptr;
			_name = name ? name : string();
		}
		Resource(Type *value) {
			_value = value;
			_name = value ? value->name() : "";
		}

		// Operators
	public:
		operator Type* () {
			if(_name.empty())
				return nullptr;
			if(!_value)
				_value = Collection<Type>::get(_name);
			else
				Collection<Type>::check(_value);
			return _value;
		}
		Type* operator -> () {
			return (Type*)*this;
		}

		// Methods
	public:
		void load() {
			if(_name.empty())
				return;
			_value = Collection<Type>::get(_name);
		}
		void cache() {
			if(_name.empty())
				return;
			_value = Collection<Type>::cache(_name);
		}
		void reset() {
			_value = nullptr;
		}

		// Serializer
	public:
		friend File& operator << (File &s, Resource<Type> &r) {
			if(s.loading()) {
				// Wczytaj obiekt
				s << r._name;
			}
			else {
				// Zapisz obiekt
				s << r._name;
				r._value = nullptr;
			}
			return s;
		}
	};
};
