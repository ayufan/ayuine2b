//------------------------------------//
//
// World.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// World: Functions

	static void *WorldAllocMemory(s32 size) {
		return malloc(size);
	}

	static void WorldFreeMemory(void *ptr, s32 size) {
		free(ptr);
	}

	//------------------------------------//
	// World: Constructor

	World::World() {
		bbox worldBounds(vec3(-wcSize), vec3(wcSize));

		// State Fields
		Flags = afUpdate | afZones | afPrepare | afChilds;
		Gravity = vec3(0.0f, 0.0f, -0.981f);
		Time = 0;

		// Precompiled Fields
		_root = u32Max;
		_frame = 0;

		// Wyzeruj dane
		_worldMesh = nullptr;
		_leftTime = 0;

		// Utw�rz fizyk�
		_worldPhysics = NewtonCreate(WorldAllocMemory, WorldFreeMemory);

		// Ustaw fizyk�
		NewtonSetWorldSize(_worldPhysics, &worldBounds.Mins.X, &worldBounds.Maxs.X);
		NewtonSetSolverModel(_worldPhysics, 1);
		NewtonSetFrictionModel(_worldPhysics, 1);
		NewtonMaterialSetDefaultCollidable(_worldPhysics, NewtonMaterialGetDefaultGroupID(_worldPhysics), NewtonMaterialGetDefaultGroupID(_worldPhysics), true);
		NewtonMaterialSetDefaultFriction(_worldPhysics, NewtonMaterialGetDefaultGroupID(_worldPhysics), NewtonMaterialGetDefaultGroupID(_worldPhysics), 0.95f, 0.9f);
		NewtonMaterialSetDefaultElasticity(_worldPhysics, NewtonMaterialGetDefaultGroupID(_worldPhysics), NewtonMaterialGetDefaultGroupID(_worldPhysics), 0.1f);
		NewtonMaterialSetDefaultSoftness(_worldPhysics, NewtonMaterialGetDefaultGroupID(_worldPhysics), NewtonMaterialGetDefaultGroupID(_worldPhysics), 0.05f);
	}

	//------------------------------------//
	// World: Destructor

	World::~World() {
		// Wyczy�� �wiat
		unload();

		// Zniszcz fizyk�
		if(_worldPhysics) {
			NewtonDestroyAllBodies(_worldPhysics);
			NewtonDestroy(_worldPhysics);
			_worldPhysics = nullptr;
		}
	}

	//------------------------------------//
	// World: Actor Methods

	void World::invalidate(const bbox &box) {
		// TODO?
	}

	void World::update(f32 dt) {
		// Dodaj czas
		_leftTime += dt;
		
		// Ustaw czas trwania ramki
		if(Engine::Config->UpdateFrameTime > MathErr) {
			dt = Engine::Config->UpdateFrameTime;
		}

		// Uruchom ramk�
		while(_leftTime >= dt) {
			Actor *changedList = nullptr;

			// Wyzeruj listy
			NextChanged = nullptr;
			NextUpdate = nullptr;
			NextPrepare = nullptr;
			NextInvalidate = nullptr;
			NextVisible = nullptr;

			// Dodaj obiekty aktor�w (specjalna wersja dla obiekt�w �wiata)
			for(u32 i = 0; i < _worldObjects.size(); i++) {
				Actor* actor = _worldObjects.at(i);
				
				// Dodaj aktora do listy przygotowywanych
				if(actor->Flags & afPrepare) {
					actor->NextPrepare = NextPrepare;
					NextPrepare = actor;
				}

				// Dodaj aktora do listy widocznych
				if(~actor->Flags & afZones) {
					actor->NextVisible = NextVisible;
					NextVisible = actor;
				}
			}

			// Posortuj aktor�w wed�ug w�a�ciwo�ci
			for(Actor *actor = Childs; actor; actor = actor->Next) {
				// Dodaj aktora do listy aktualnianych
				if(actor->Flags & afUpdate) {
					// Dodaj aktora do listy uaktualnialnych
					actor->NextUpdate = NextUpdate;
					NextUpdate = actor;
				}

				// Dodaj aktora do listy przygotowywanych
				if(actor->Flags & afPrepare) {
					actor->NextPrepare = NextPrepare;
					NextPrepare = actor;
				}

				// Dodaj aktora do listy od�wie�anych
				if(actor->Flags & afInvalidate) {
					actor->NextInvalidate = NextInvalidate;
					NextInvalidate = actor;
				}

				// Dodaj aktora do listy widocznych
				if(~actor->Flags & afZones) {
					actor->NextVisible = NextVisible;
					NextVisible = actor;
				}
			}

			// Uaktualnij aktor�w
			for(Actor* actor = NextUpdate; actor; actor = actor->NextUpdate) {
				actor->preupdate(dt);
			}

			// Ustaw �wiat
			if(Body) {
				Body->apply();
			}

			// Uaktualnij fizyk�
			NewtonUpdate(_worldPhysics, dt);

			// Zako�cz uaktualnianie aktor�w
			for(Actor* actor = NextUpdate; actor; actor = actor->NextUpdate) {
				actor->postupdate(dt);

				// Dodaj aktora do listy zmienionych
				if(actor->Changed) {
					actor->NextChanged = NextChanged;
					NextChanged = actor;
				}
			}

			// Wyznacz nowe strefy dla zmienionych aktor�w
			for(Actor* actor = NextChanged; actor; actor = actor->NextChanged) {
				// Uaktualnij aktor�w kt�rzy obs�uguj� strefy
				if(~actor->Flags & afZones)
					continue;

				// Ale kt�rzy zmienili obszar
				if(~actor->Changed & cfArea)
					continue;

				// Zwi�ksz klatk�
				_frame++;

				// Zapisz stare strefy
				actor->_oldZones = actor->_zones;
				actor->_zones.clear();

				// Wyznacz nowe strefy
				findZones(actor);

				// Wyznacz usuni�te strefy
				ArrayEach(u32, j, actor->_oldZones) {
					BspZone &zone = _zones.at(*j);

					// Sprawd� stref�
					if(zone.Frame == _frame) {
						// Oznacz jako dodan�
						zone.Frame = 0;
					}
					else {
						// Znajd� i usu� aktora
						zone.Actors.eraseFind(actor, 1);
					}
				}

				// Dodaj aktora do stref
				ArrayEach(u32, j, actor->_zones) {
					BspZone &zone = _zones.at(*j);

					// Sprawd� stref� 
					if(zone.Frame == _frame) {
						// Dodaj aktora do strefy
						zone.Actors.push(actor);
					}
				}
			}

			// Uaktualnij obszary aktor�w kt�rzy si� zmienili
			for(Actor* actor = NextChanged; actor; actor = actor->NextChanged) {
				// Sprawd� czy obiekt obs�uguje uaktualnianie innych obiekt�w
				if(~actor->Flags & afInvalidated)
					goto done;

				// Zwi�ksz klatk�
				_frame++;

				// Sprawd� czy mamy uaktualni� stary obszar
				if(actor->Changed & cfArea) {
					ArrayEach(u32, j, actor->_oldZones) {
						BspZone &zone = _zones.at(*j);

						// Cz�� wsp�lna
						bbox box = actor->_oldBox.intersect(zone.Box);
			
						// Jest co� z tej cz�sci wsp�lnej?
						if(box.empty())
							continue;

						// Uaktualnij wszystkich aktor�w znajduj�cych si� w strefie
						ArrayEach(Actor*, k, zone.Actors) {
							Actor *source = *k;

							// A taki test, to nie powinno si� zda�y�
							if(actor == source)
								continue;

							// Sprawd� czy obiekt obs�uguje uaktualnianie
							if(~source->Flags & afInvalidate)
								continue;

							// Uaktualnij aktora
							source->invalidate(box, actor);
						}
					}
				}

				// Uaktualnijmy teraz nowy obszar
				ArrayEach(u32, j, actor->_zones) {
					BspZone &zone = _zones.at(*j);

					// Cz�� wsp�lna
					bbox box = actor->Box.intersect(zone.Box);
		
					// Jest co� z tej cz�sci wsp�lnej?
					if(box.empty())
						continue;

					// Uaktualnij wszystkich aktor�w znajduj�cych si� w strefie
					ArrayEach(Actor*, k, zone.Actors) {
						Actor *source = *k;

						// A czy nie staramy si� uaktualni� naszego aktora :)
						if(actor == source)
							continue;

						// Sprawd� czy obiekt obs�uguje uaktualnianie
						if(~source->Flags & afInvalidate)
							continue;

						// Uaktualnij aktora
						source->invalidate(box, actor);
					}
				}

done:
				// Oznacz takiego aktora jako zaktualizowanego
				actor->Changed = 0;
			}

			// Ca�kuj czas :)
			Time += dt;

			// Zminiejsz pozosta�y czas do przetworzenia
			_leftTime -= dt;
		}
	}

	bool ActorVisibilitySorter(Actor *a, Actor *b) {
		return a->Visible < b->Visible;
	}

	void World::prepare(RenderFrame &frame) {
		Array<Actor*> actors;

		// Zwi�ksz klatk�
		_frame++;

		// Sprawd� widoczno��
		BspZone *zones = ensureVisbility(frame.CameraOrigin, frame.CameraFrustum, frame.Clip);

		// Ustaw parametry �wiata
		frame.Ambient = Ambient;
		frame.Time = Time;

		// Przejrzyj wszystkie widoczne strefy
		for(BspZone *zone = zones; zone; zone = zone->Next) {
			// Przygotuj wszystkich aktor�w
			ArrayEach(Actor*, j, zone->Actors) {
				Actor* actor = *j;
				rect actorRect;

				// Interesuj� nas tylko aktorzy, kt�rych mo�emy wy�wietli�
				if(~actor->Flags & afPrepare)
					continue;
				
				// Wyznacz widoczny obszar aktora przyci�ty do sektora i ekranu
				actorRect = frame.CameraFrustum.screenRect(actor->Box).intersect(frame.Clip).intersect(zone->VisibleRect);

				// Sprawd� czy jest widoczny w frustumie
				if(actorRect.empty())
					continue;

				// Zapisz parametry strefy
				if(actor->_frame != _frame) {
					// Dodaj nowego aktora do widocznych
					actor->Visible = zone->Visible;
					actor->VisibleRect = actorRect;
					actor->_frame = _frame;
					actors.push(actor);
				}
				else if(actor->Visible > zone->Visible) {
					// Aktor jest ju� widoczny, ale s� lepsze parametry jego widoczno�ci
					actor->Visible = zone->Visible;
					actor->VisibleRect = actorRect;
				}
				else if(actor->Visible == zone->Visible) {
					// Po��cz parametry widoczno�ci ze starymi
					actor->VisibleRect = actor->VisibleRect.merge(actorRect);
				}
			}
		}

		// Posortuj aktor�w
		std::sort(actors.begin(), actors.end(), ActorVisibilitySorter);

		// Przygotuj aktor�w w kolejno�ci wy�wietlania
		ArrayEach(Actor*, i, actors) {
			// Przygotuj aktora
			(*i)->prepare(frame);
		}

		// Przygotuj aktor�w zawsze widocznych
		for(Actor* actor = NextVisible; actor; actor = actor->NextVisible) {
			// Interesuj� nas tylko aktorzy, kt�rych mo�emy wy�wietli�
			if(~actor->Flags & afPrepare)
				continue;

			// Przygotuj aktora
			actor->prepare(frame);
		}
	}

	//------------------------------------//
	// World: Methods

	void World::unload() {
		// Zniszcz obiekty
		ArrayEach(AMesh*, i, _worldObjects) {
			delete *i;
		}
		_worldObjects.clear();

		// Zniszcz informacje o �wiecie
		_nodes.clear();
		_zones.clear();
		_root = u32Max;
		_frame = 0;
		_worldMesh = nullptr;

		// Usu� cia�o fizyczne �wiata
		Body.reset();
	}

	void World::load(const string &name) {
		Array<bbox> portals;

		// Usu� poprzednio za�adowany �wiat
		unload();

		// Za�aduj mesha
		if(_worldMesh = Mesh::get(name)) {
			// Zarezerwuj miejsce na obiekty
			_worldObjects.reserve(_worldMesh->Objects.size());

			// Wczytaj obiekty do planszy
			for(u32 i = 0; i < _worldMesh->Objects.size(); i++) {
				MeshObject& meshObject = _worldMesh->Objects.at(i);

				// Sprawd� typ obiektu
				if(strstr(meshObject.Name.c_str(), "sector") == meshObject.Name.c_str()) {
					BspZone zone;

					// Ustaw stref�
					zone.Box = meshObject.Box;

					// Dodaj stref�
					_zones.push(zone);
				}
				else if(strstr(meshObject.Name.c_str(), "portal") == meshObject.Name.c_str()) {
					// Dodaj portal
					portals.push(meshObject.Box);
				}

				// Utw�rz aktora
				AMesh* meshActor = new AMesh();

				// Ustaw aktora
				meshActor->Mesh = _worldMesh;
				meshActor->Object = i;

				// Dodaj aktora
				_worldObjects.push(meshActor);
			}

			// Brak stref? Utw�rz jedn� wielk� na ca�y �wiat
			if(_zones.empty() && _worldMesh->Vertices.size()) {
				BspZone zone;

				// Ustaw stref�
				zone.Box = bbox(&_worldMesh->Vertices[0].Origin, _worldMesh->Vertices.size(), sizeof(MeshVertex));

				// Dodaj stref�
				_zones.push(zone);
			}

			// Utw�rz i ustaw cia�o fizyczne 
			Body.reset(new RigidBody(_worldMesh->Collider));
		}

		// Sprawd� ka�dy portal
		ArrayEach(bbox, i, portals) {
			// Sprawd� z ka�dym sektorem
			ArrayEach(BspZone, j, _zones) {
				// Czy si� pokrywaj�?
				if(!i->test(j->Box))
					continue;

				// Dodaj do ka�dego sektora z kt�rym si� ��czy
				for(Array<BspZone>::Iterator k = j+1; k != _zones.end(); k++) {
					// Czy si� pokrywaj�?
					if(!i->test(k->Box))
						continue;

					// Dodaj portal do stref					
					k->Portals.push(BspPortal(*i, j - _zones.begin()));
					j->Portals.push(BspPortal(*i, k - _zones.begin()));
				}
			}
		}

		// Zbuduj drzewo
		buildBspTree();

		// Dodaj strefy dla sta�ych aktor�w
		for(u32 i = 0; i < _worldObjects.size(); i++) {
			Actor* actor = _worldObjects.at(i);

			// Dodaj ramk�
			_frame++;

			// Pierwsza ramka
			if(actor->Flags & afUpdate) {
				actor->preupdate(0);
				actor->postupdate(0);
			}

			// Wyszukaj strefy dla sta�ych aktor�w
			findZones(actor);

			// Dodaj aktora do stref
			ArrayEach(u32, j, actor->_zones) {
				_zones.at(*j).Actors.push(actor);
			}

			// Odznacz zaznaczenie
			actor->Changed = cfNone;
		}
	}
};
