//------------------------------------//
//
// AEmitter.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-18
//
//------------------------------------//

#pragma once

namespace ayuine
{
	enum EmitterType
	{
		etPoint,
		etBox,
		etSphere
	};

	class ParticleVertex 
	{
		ValueType(ParticleVertex, 0);

		// Static Fields
	public:
		AYUAPI static VertexDeclElement Decl[];

		// Fields
	public:
		vec3				Origin;
		vec4				Coords;
		u32					Color;

		// Constructor
	public:
		ParticleVertex() {
		}
	};

	class AEmitter : public Actor
	{
		class Particle
		{
			ValueType(Particle, 0);

			// Fields
		public:
			vec3				Origin;
			angles			Rotation;
			vec3				Velocity;
			angles			Angular;
			f32					LifeTime;
			f32					Radius;
			vec4				Color;
			Particle*		Next;

			// Constructor
		public:
			Particle() {
				LifeTime = 0;
				Radius = 0;
				Next = nullptr;
			}
		};

		ObjectType(AEmitter, Actor, 0);

		// Render Fields
	private:
		SmartPtr<VertexType>					_vertexType;
		Array<ParticleVertex>					_vertices;
		Array<u16>										_indices;

		// Particle Fields
	private:
		Array<Particle>								_particles;
		Particle*											_active;
		Particle*											_free;
		bool													_needsToUpdate;
		f32														_last, _time;

		// Configuration Fields
	public:
		EmitterType										Type;
		vec3													Size;
		bool													Enabled;
		bool													World;
		f32														Mass;
		f32														LifeTime;
		f32														Radius, RadiusDerivative;
		vec4													StartColor, EndColor;
		vec3													Force;
		f32														StartVelocity;
		f32														MaxVelocity, MaxAngular;
		Resource<ayuine::Material>		Material;

		// Constructor
	public:
		AYUAPI AEmitter(u32 particles);

		// Destructor
	public:
		AYUAPI virtual ~AEmitter();

		// Methods
	public:
		AYUAPI virtual void preupdate(f32 dt);
		AYUAPI virtual void postupdate(f32 dt);
		AYUAPI virtual void prepare(RenderFrame &frame);
	};
};
