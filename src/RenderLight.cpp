//------------------------------------//
//
// RenderLight.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-14
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// RenderLight: Constructor
	
	RenderLight::RenderLight() {
		Range = 0;
		PRT = false;
		ShadowMap = ProjectionMap = nullptr;
		Next = nullptr;
		Transform = mat4Identity;
		Visible = false;
	}

	//------------------------------------//
	// RenderLight: Methods

	bool RenderLight::test(RenderFragment *fragment) {
		return fragment->Box.test(Box);
	}
};
