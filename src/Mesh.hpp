//------------------------------------//
//
// Mesh.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	enum MeshColliderType
	{
		mctNone,
		mctSphere,
		mctBox,
		mctConvex,
		mctStatic
	};

	class MeshVertex 
	{
		ValueType(MeshVertex, 0);

		// Static Fields
	public:
		static	VertexDeclElement	Decl[];

		// Fields
	public:
		vec3	Origin;
		vec2	Coords[2];
		u32		Normal;
		u32		Tangent;

		// Operators
	public:
		bool operator < (const MeshVertex &mv) const {
			return Origin < mv.Origin && Coords[0] < mv.Coords[0] && Coords[1] < mv.Coords[1] && 
				Normal < mv.Normal && Tangent < mv.Tangent;
		}
		bool operator == (const MeshVertex &mv) const {
			return Origin == mv.Origin && Coords[0] == mv.Coords[0] && Coords[1] == mv.Coords[1] &&  
				Normal == mv.Normal && Tangent == mv.Tangent;
		}

		// Serializer
	public:
		friend File& operator << (File &s, MeshVertex &v) {
			if(s.loading()) {
				s.read(&v, sizeof(MeshVertex));
			}
			else {
				s.write(&v, sizeof(MeshVertex));
			}
			return s;
		}
	};

	static s32 compare(const MeshVertex &a, const MeshVertex &b) {
		s32 r;

		if(r = compare(a.Origin, b.Origin))
			return r;
		if(r = compare((vec4&)a.Coords[0], (vec4&)b.Coords[0]))
			return r;
		if(r = compare(a.Normal, b.Normal))
			return r;
		if(r = compare(a.Tangent, b.Tangent))
			return r;
		return 0;
	}

	class MeshSurface
	{
		ValueType(MeshSurface, 0);

		// Fields
	public:
		Resource<Material>		Material;
		bbox									Box;
		bsphere								Sphere;
		vec4									Plane;
		u32										IndexStart;
		u32										IndexCount;

		// Constructor
	public:
		AYUAPI MeshSurface();

		// Serializer
	public:
		friend File& operator << (File &s, MeshSurface &m) {
			return s << m.Material << m.Box << m.Sphere << m.Plane << m.IndexStart << m.IndexCount;
		}
	};

	class MeshObject
	{
		ValueType(MeshObject, 0);

		// Fields
	public:
		string								Name;
		Array<MeshSurface>		Surfaces;
		bbox									Box;
		bsphere								Sphere;

		// Render Fields
	public:
		u32										VertexStart;
		u32										VertexCount;
		u32										IndexStart;
		u32										IndexCount;
		u32										Frame;
		Resource<Texture>			PRT;

		// Physics Fields
	public:
		SmartPtr<Collider>		Collider;
		f32										Mass;

		// Constructor
	public:
		AYUAPI MeshObject();
		AYUAPI MeshObject(const MeshObject &oldObjectConst);

		// Operators
	public:
		MeshObject &operator = (const MeshObject &oldObjectConst) {
			this->MeshObject::~MeshObject();
			this->MeshObject::MeshObject(oldObjectConst);
			return *this;
		}

		// Serializer
	public:
		friend File& operator << (File &s, MeshObject &o) {
			return s << o.Name << o.Surfaces << o.Box << o.Sphere << o.VertexStart << o.VertexCount << o.IndexStart << o.IndexCount << o.PRT;
		}
	};

	class Mesh : public DeviceObject, public Collection<Mesh>
	{
		ObjectType(Mesh, DeviceObject, 0);

		// Static Fields
	private:
		static Property Properties[];

		// Fields
	public:
		Array<MeshVertex>						Vertices;
		Array<u16>									Indices;
		Array<MeshObject>						Objects;
		SmartPtr<VertexBuffer>			VertexBuffer;
		SmartPtr<IndexBuffer>				IndexBuffer;
		SmartPtr<VertexType>				VertexType;

		// Physics Fields
	public:
		MeshColliderType						ColliderType;
		bool												ColliderObject;
		SmartPtr<Collider>					Collider;
		f32													Density;
		f32													Mass;

		// Render Fields
	public:
		bool												PRT;

		// Constructor
	public:
		AYUAPI Mesh();

		// Destructor
	public:
		AYUAPI virtual ~Mesh();

		// Methods
	public:
		AYUAPI void load(const string &name);
		AYUAPI void loadMesh(const string &name);
		AYUAPI void loadMeshU(const string &name);
		AYUAPI void loadObj(const string &name);
		AYUAPI void save(const string &name);
		AYUAPI void unload();

		// DeviceObject Methods
	public:
		AYUAPI virtual void deviceReset(bool theBegin);
		AYUAPI virtual void deviceLost(bool theEnd);

		// Physics Methods
	public:
		AYUAPI void physicsReset();
		AYUAPI void physicsLost();

		// Serializer
	public:
		friend File& operator << (File &s, Mesh &m) {
			u32 magic, version;

			// Wczytaj magica i numer wersji :)
			if(s.loading()) {
				// Pobierz z pliku
				s << magic << version;

				// Sprawd� dane
				Assert(magic == ayuine::hash("Mesh") && version == 1);
			}
			// Zapisz magica i numer wersji ;]
			else {
				// Ustaw magica i numer wersji
				magic = ayuine::hash("Mesh");
				version = 1;

				// Wrzu� do pliku
				s << magic << version;
			}
			
			// Reszta danych
			return s << m.Vertices << m.Indices << m.Objects;
		}
	};
};
