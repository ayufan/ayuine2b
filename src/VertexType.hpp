//------------------------------------//
//
// VertexType.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-9
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// Typedefs

	typedef D3DVERTEXELEMENT9			VertexDeclElement;
	typedef VertexDeclElement*		VertexDeclArray;

	//------------------------------------//
	// VertexType class

	class VertexType : public DeviceObject
	{
		ObjectType(VertexType, DeviceObject, 0);

		// Fields
	private:
		VertexDeclElement								_elements[MAX_FVF_DECL_SIZE];
		LPDIRECT3DVERTEXDECLARATION9		_decl;
		u32															_stride;

		// Constructor
	public:
		AYUAPI VertexType(VertexDeclArray elements);
		AYUAPI VertexType(u32 format);

		// Destructor
	public:
		AYUAPI virtual ~VertexType();

		// Properties
	public:
		AYUAPI u32	stride();
		AYUAPI VertexDeclArray elements();

		// DeviceObject Methods
	public:
		AYUAPI virtual u32 type();
		AYUAPI virtual void deviceReset();
		AYUAPI virtual void deviceLost(bool theEnd);

		// Methods
	public:
		AYUAPI void bind();
	};
};
