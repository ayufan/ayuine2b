//------------------------------------//
//
// Joint.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-1
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Joint: Constructor

	Joint::Joint() {
		// Ustaw domy�lne warto�ci
		_joint = nullptr;
		Collide = false;
		Stiffness = 0.9f;
		ForceLimit = 0.0f;
	}

	//------------------------------------//
	// Joint: Destructor

	Joint::~Joint() {
		// Zniszcz jointa :)
		if(_joint) {
			if(Engine::World) {
				NewtonDestroyJoint(Engine::World->_worldPhysics, _joint);
			}
			_joint = nullptr;
		}
	}

	//------------------------------------//
	// Joint: Properties

	bool Joint::destroyed() const {
		return _joint == nullptr;
	}

	//------------------------------------//
	// Joint: Methods

	void Joint::apply() {
		// Wywo�aj zdarzenie
		OnApply(this);

		// Ustaw parametry jointa
		NewtonJointSetCollisionState(_joint, Collide);
		NewtonJointSetStiffness(_joint, Stiffness);
	}

	//------------------------------------//
	// BallJoint: Functions

	static void BallJointCallback(const NewtonJoint* ball) {
		BallJoint* joint = (BallJoint*)NewtonJointGetUserData(ball);
		Assert(joint);

		// Pobierz parametry jointa
		NewtonBallGetJointForce(ball, &joint->Force.X);
		NewtonBallGetJointOmega(ball, &joint->Omega.X);
		NewtonBallGetJointAngle(ball, &joint->Angle.X);

		// Wywo�aj zdarzenie
		joint->OnUpdate(joint);

		// Sprawd� si�� i ew. zniszcz jointa
		if(joint->ForceLimit > MathErr && 
			joint->ForceLimit < joint->Force.lengthSq()) {
			// Wywo�aj zdarzenie
			joint->OnDestroy(joint);

			// Zniszcz jointa
			joint->~BallJoint();			
		}		
	}

	//------------------------------------//
	// BallJoint: Constructor

	BallJoint::BallJoint(const vec3 &origin, RigidBody* child, RigidBody* parent) {
		Assert(child);

		// Utw�rz jointa
		_joint = NewtonConstraintCreateBall(Engine::World->_worldPhysics, &origin.X, child->_body, parent ? parent->_body : nullptr);

		// Skonfiguruj jointa
		NewtonJointSetUserData(_joint, this);
		NewtonBallSetUserCallback(_joint, BallJointCallback);

		// Ustaw domy�lne warto�ci
		ConeLimit = 0;
		TwistLimit = 0;
	}

	//------------------------------------//
	// BallJoint: Methods

	void BallJoint::apply() {
		Joint::apply();

		// Ustaw BallJoint
		NewtonBallSetConeLimits(_joint, &Axis.X, deg2rad(ConeLimit), deg2rad(TwistLimit));
	}

	//------------------------------------//
	// HingeJoint: Functions

	static unsigned HingeJointCallback(const NewtonJoint* hinge, NewtonHingeSliderUpdateDesc* desc) {
		// TODO: Taka nie sko�czona wersja
		HingeJoint* joint = (HingeJoint*)NewtonJointGetUserData(hinge);
		Assert(joint);

		// Pobierz parametry jointa
		NewtonHingeGetJointForce(hinge, &joint->Force.X);
		joint->Omega = NewtonHingeGetJointOmega(hinge);
		joint->Angle = NewtonHingeGetJointAngle(hinge);

		// Wywo�aj zdarzenie
		joint->OnUpdate(joint);

		// Sprawd� si�� i ew. zniszcz jointa
		if(joint->ForceLimit > MathErr && 
			joint->ForceLimit < joint->Force.lengthSq()) {
			// Wywo�aj zdarzenie
			joint->OnDestroy(joint);

			// Zniszcz jointa
			joint->~HingeJoint();			
		}
		return 0;
	}

	//------------------------------------//
	// HingeJoint: Constructor

	HingeJoint::HingeJoint(const vec3 &origin, const vec3 &axis, RigidBody* child, RigidBody* parent) {
		Assert(child);

		// Utw�rz jointa
		_joint = NewtonConstraintCreateHinge(Engine::World->_worldPhysics, &origin.X, &axis.X, child->_body, parent ? parent->_body : nullptr);

		// Skonfiguruj jointa
		NewtonJointSetUserData(_joint, this);
		NewtonHingeSetUserCallback(_joint, HingeJointCallback);
	}

	//------------------------------------//
	// SliderJoint: Functions

	static unsigned SliderJointCallback(const NewtonJoint* slider, NewtonHingeSliderUpdateDesc* desc) {
		// TODO: Taka nie sko�czona wersja
		SliderJoint* joint = (SliderJoint*)NewtonJointGetUserData(slider);
		Assert(joint);

		// Pobierz parametry jointa
		NewtonSliderGetJointForce(slider, &joint->Force.X);
		joint->Origin =	NewtonSliderGetJointPosit(slider);
		joint->Velocity = NewtonSliderGetJointVeloc(slider);

		// Wywo�aj zdarzenie
		joint->OnUpdate(joint);

		// Sprawd� si�� i ew. zniszcz jointa
		if(joint->ForceLimit > MathErr && 
			joint->ForceLimit < joint->Force.lengthSq()) {
			// Wywo�aj zdarzenie
			joint->OnDestroy(joint);

			// Zniszcz jointa
			joint->~SliderJoint();			
		}
		return 0;
	}

	//------------------------------------//
	// SliderJoint: Constructor

	SliderJoint::SliderJoint(const vec3 &origin, const vec3 &axis, RigidBody* child, RigidBody* parent) {
		Assert(child);

		// Utw�rz jointa
		_joint = NewtonConstraintCreateSlider(Engine::World->_worldPhysics, &origin.X, &axis.X, child->_body, parent ? parent->_body : nullptr);

		// Skonfiguruj jointa
		NewtonJointSetUserData(_joint, this);
		NewtonSliderSetUserCallback(_joint, SliderJointCallback);
	}

	//------------------------------------//
	// UpJoint: Constructor

	UpJoint::UpJoint(const vec3 &axis, RigidBody* child) {
		Assert(child);
		
		// Zapisz parametry
		Axis = axis;

		// Utw�rz jointa
		_joint = NewtonConstraintCreateUpVector(Engine::World->_worldPhysics, &axis.X, child->_body);

		// Skonfiguruj jointa
		NewtonJointSetUserData(_joint, this);
	}

	//------------------------------------//
	// UpJoint: Methods

	void UpJoint::apply() {
		Joint::apply();

		// Ustaw wektor
		NewtonUpVectorSetPin(_joint, &Axis.X);
	}
};
