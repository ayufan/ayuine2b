//------------------------------------//
//
// Render.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Functions

	static u32 renderOpaqueSorter(RenderFragment *a, RenderFragment *b) {
		return a->Distance < b->Distance;
	}

	//------------------------------------//
	// Render: Constructor

	Render::Render() {
		_sceneTargets = nullptr;

		// Utw�rz cele wy�wietlania
		_sceneColor.reset(new TextureTarget(Engine::Device->width(), Engine::Device->height(), D3DFMT_A8R8G8B8));
		//_sceneCopy.reset(new TextureTarget(Engine::Device->width(), Engine::Device->height(), D3DFMT_A8R8G8B8));
		_sceneBlur[0].reset(new TextureTarget(Engine::Device->width() / 4, Engine::Device->height() / 4, D3DFMT_A16B16G16R16));
		_sceneBlur[1].reset(new TextureTarget(Engine::Device->width() / 4, Engine::Device->height() / 4, D3DFMT_A16B16G16R16));
	}

	//------------------------------------//
	// Render: Destructor

	Render::~Render() {
		// Zwolnij cele wy�wietlania
		_sceneColor.reset();
		//_sceneCopy.reset();
		_sceneBlur[0].reset();
		_sceneBlur[1].reset();

		// Zwolnij wszystkie cele
		for(TextureTarget *target = _sceneTargets, *next; target; target = next) {
			next = target->Next;
			delete target;
		}
		_sceneTargets = nullptr;
	}

	//------------------------------------//
	// Render: Targets Methods

	TextureTarget *Render::allocSceneTarget() {
		TextureTarget *target;

		if(_sceneTargets) {
			// Pobierz woln� powierzchni�
			target = _sceneTargets;
			_sceneTargets = _sceneTargets->Next;
		}
		else {
			// Utw�rz now� powierzchni�
			target = new TextureTarget(512, 512, D3DFMT_A8R8G8B8);

			// Informacja
			Engine::Log->debug("Render: Allocating new scene target !");
		}
		return target;
	}

	void Render::freeSceneTarget(TextureTarget *target) {
		// Zwolnij powierzchni�
		target->Next = _sceneTargets;
		_sceneTargets = target;
	}

	//------------------------------------//
	// Render: Methods

	void Render::frameCustom(RenderFrame &frame) {
		// Sprawd� materia�
		Assert(frame.CustomMaterial);

		// Ustaw urz�dzenie
		Engine::Device->viewport(rect(frame.Width, frame.Height));
		Engine::Device->clear(true, true);
		Engine::Device->depthMode(true, true);

		// Ustaw ramk�
		frame.Ambient = vec4(1, 1, 1, 1);

		// Podepnij ramk� i kamer� i t�o
		frame.bindFrame();
		frame.bindCamera();
		frame.bindAmbient();

		// W��cz materia�
		if(frame.CustomMaterial->bind(frame)) {
			// Wy�wietl wszystkie fragmenty
			for(RenderFragment *fragment = frame._fragments; fragment; fragment = fragment->Next)
				fragment->render(frame);

			// Wy��cz materia�
			frame.CustomMaterial->unbind(frame);
		}


		// Odepnij ramk� i kamer�
		frame.unbindAmbient();
		frame.unbindCamera();
		frame.unbindFrame();
	}

	void Render::frameWire(RenderFrame &frame) {
		// Ustaw materia�
		frame.CustomMaterial = Material::get("common/Wireframe");

		// Ustaw urz�dzenie
		Engine::Device->fillMode(false);

		// Wy�wietl klatk�
		frameCustom(frame);

		// Ustaw wype�nianie
		Engine::Device->fillMode(true);
	}

	void Render::frameDepth(RenderFrame &frame) {
		// Ustaw materia�
		frame.CustomMaterial = Material::get("common/SceneDepth");

		// Wy�wietl klatk�
		frameCustom(frame);
	}

	void Render::frameSolid(RenderFrame &frame) {
		// Ustaw urz�dzenie
		Engine::Device->viewport(rect(frame.Width, frame.Height));
		Engine::Device->clear(true, true);
		Engine::Device->depthMode(true, true);

		// Ustaw ramk�
		frame.Ambient = vec4(1, 1, 1, 1);

		// Podepnij ramk� i kamer� i t�o
		frame.bindFrame();
		frame.bindCamera();
		frame.bindAmbient();

		// Wy�wietl wszystkie fragmenty
		for(RenderFragment *fragment = frame._fragments; fragment; fragment = fragment->Next)
			fragment->renderMaterial(frame);

		// Podepnij ramk� i kamer� i t�o
		frame.unbindAmbient();
		frame.unbindCamera();
		frame.unbindFrame();
	}

	void Render::frameShaded(RenderFrame &frame) {
		Array<RenderFragment*>	opaques;
		Material*								materials = nullptr;
		RenderFragment*					customs[2] = {nullptr, nullptr};
		RenderMirror*						mirrors = nullptr;
		bool										refractive = false;

		// Zresetuj stan wszystkich fragment�w
		for(RenderFragment *fragment = frame._fragments; fragment; fragment = fragment->Next) {
			// Brak materia�u? To darujmy sobie to
			if(!fragment->Material)
				continue;

			// Wyczy�� stan materia�u 
			fragment->Material->Chain[frame.Level] = nullptr;
			fragment->Material->ChainPRT[frame.Level] = nullptr;
		}

		// Posortuj wszystkie fragmenty wed�ug materia�u
		for(RenderFragment *fragment = frame._fragments; fragment; fragment = fragment->Next) {
			// Fragment specjalny
			if(fragment->Flags & rffCustom) {
				if(~~fragment->Flags & rffPreCustom) {
					fragment->NextChain = customs[0];
					customs[0] = fragment;
				}
				else if(~~fragment->Flags & rffPostCustom) {
					fragment->NextChain = customs[1];
					customs[1] = fragment;
				}
				continue;
			}

			// Brak materia�u? To darujmy sobie to
			if(!fragment->Material)
				continue;

			// Fragment u�ywa "refrakcji"
			if(fragment->Material->refractive()) {
				refractive = true;
			}

			// Fragment u�ywa "odbicia"
			if(fragment->Material->reflective() && !frame.NoReflection) {
				RenderMirror *mirror;

				// Znajd� lustro
				for(mirror = mirrors; mirror; mirror = mirror->Next) {
					if(mirror->Plane == fragment->Plane)
						break;
				}

				// Utw�rz nowe lustro
				if(!mirror) {
					// Zapisz parametry lustra
					mirror = new RenderMirror();
					mirror->Plane = fragment->Plane;
					mirror->Next = mirrors;
					mirrors = mirror;
				}

				// Uaktualnij parametry lustra
				mirror->Box = mirror->Box.merge(fragment->Box);
				fragment->Mirror = mirror;
			}

			// Fragment przezroczysty
			if(fragment->Material->opaque()) {
				// Oblicz odleg�o�� fragmentu od obserwatora
				fragment->Distance = (fragment->Box.center() - frame.CameraOrigin).lengthSq();

				// Dodaj fragment
				opaques.push(fragment);
				continue;
			}

			// Sprawd� czy materia� zosta� ju� dodany, je�li nie to dodaj
			if(!fragment->Material->Chain[frame.Level] && 
				!fragment->Material->ChainPRT[frame.Level]) {
				fragment->Material->Next[frame.Level] = materials;
				materials = fragment->Material;
			}

			// Dodaj fragment do materia�u
			if(fragment->PRT) {
				fragment->NextChain = fragment->Material->ChainPRT[frame.Level];
				fragment->Material->ChainPRT[frame.Level] = fragment;
			}
			else {
				fragment->NextChain = fragment->Material->Chain[frame.Level];
				fragment->Material->Chain[frame.Level] = fragment;
			}
		}

		// Posortuj przezroczyste fragmenty
		std::sort(opaques.begin(), opaques.end(), renderOpaqueSorter);

		// Wy�wietl lustra :D, ale tylko dla 1 poziomu :)
		if(!frame.Level) {
			for(RenderMirror *mirror = mirrors; mirror; mirror = mirror->Next) {
				// Oblicz widoczny obszar lustra
				rect mirrorClip = frame.Clip.merge(frame.CameraFrustum.screenRect(mirror->Box));

				// Jest widoczny?
				if(mirrorClip.empty())
					continue;

				// Zaalokuj lustro
				mirror->Texture = allocSceneTarget();
				
				// Mamy?
				if(!mirror->Texture)
					continue;

				// Utw�rz ramk�
				RenderFrame mirrorFrame;
				mat4 mirrorTemp;

				// Skopiuj stare dane do ramki
				(RenderState&)mirrorFrame = (RenderState&)frame;

				// Oblicz macierz dla lustra
				mat4::reflection(mirrorTemp, mirror->Plane);

				// Uaktualnij dane dla ramki
				mat4::multiply(mirrorFrame.CameraView, mirrorFrame.CameraView, mirrorTemp);
				// TODO
				//mat4::clip(mirrorFrame.CameraProjection, mirrorFrame.CameraView.transformPlane(mirror->Plane));

				// Utw�rz frustum
				mirrorFrame.CameraFrustum = frustum(mirrorFrame.CameraProjection, mirrorFrame.CameraView);

				// Zwi�ksz poziom
				mirrorFrame.Level++;

				// Wy��cz nie potrzebne efekty
				mirrorFrame.NoPostProcess = true;

				// Uaktualnij obszar przycinania
				mirrorFrame.Clip = mirrorClip;
				mirrorFrame.Width = mirror->Texture->info().Width;
				mirrorFrame.Height = mirror->Texture->info().Height;

				// Wyznacz widoczny fragmenty
				Engine::World->prepare(mirrorFrame);

				// Rozpocznij wy�wietlanie lustra
				mirror->Texture->begin(0);

				// Wy�wietl ramk�
				this->frame(mirrorFrame);

				// Zako�cz wy�wietlanie lustra
				mirror->Texture->end(0);
			}
		}

		// Ustaw urz�dzenie
		if(!frame.NoPostProcess) {
			// Zacznij wy�wietlanie do sceny
			_sceneColor->begin(0);
			Engine::Device->viewport(rect(_sceneColor->info().Width, _sceneColor->info().Height));
		}
		else {
			Engine::Device->viewport(rect(frame.Width, frame.Height));
		}
		Engine::Device->clear(true, true);

		// Podepnij ramk� i kamer�
		frame.bindFrame();
		frame.bindCamera();

		////////////////////////////////////
		// T�O
		////////////////////////////////////

		if(!frame.NoAmbient) {
			// Podepnij wy�wietlanie t�a
			frame.bindAmbient();

			// PRE-CUSTOMY
			if(!frame.NoPreCustom) {
				Engine::Device->depthMode(false, false);
				
				// Wy�wietl fragmenty specjalne
				for(RenderFragment *fragment = customs[0]; fragment; fragment = fragment->NextChain) {
					// Wy��cz PRT
					frame.NoPRT = false;

					// Wy�wietl fragment
					fragment->renderMaterial(frame);
				}
			}

			Engine::Device->depthMode(true, true);

			// FRAGMENTY NIE PRZEZROCZYSTE
			// Wy�wietl fragmenty wed�ug materia��w
			for(Material *material = materials; material; material = material->Next[frame.Level]) {
				// W��cz PRT
				frame.NoPRT = false;

				// Podepnij materia� PRT
				if(material->ChainPRT[frame.Level] && material->bind(frame)) {
					// Wy�wietl fragmenty przypisane do materia�u
					for(RenderFragment *fragment = material->ChainPRT[frame.Level]; fragment; fragment = fragment->NextChain) 
						fragment->render(frame);

					// Odepnij materia�
					material->unbind(frame);
				}

				// Wy��cz PRT
				frame.NoPRT = true;

				// Podepnij materia� bez PRT
				if(material->Chain[frame.Level] && material->bind(frame)) {
					// Wy�wietl fragmenty przypisane do materia�u
					for(RenderFragment *fragment = material->Chain[frame.Level]; fragment; fragment = fragment->NextChain) 
						fragment->render(frame);

					// Odepnij materia�
					material->unbind(frame);
				}
			}

			// Wy��cz wy�wietlanie t�a
			frame.unbindAmbient();
		}

		////////////////////////////////////
		// O�WIETLENIE
		////////////////////////////////////

		if(!frame.NoLighting) {
			// Wyznacz widoczno�� dla ka�dego �wiat�a
			for(RenderLight* light = frame._lights; light; light = light->Next) {
				// Wyznacz obszar widoczno�ci �wiat�a
				light->VisibleRect = frame.CameraFrustum.screenRect(light->Box).intersect(rect(vec2(-1, -1), vec2(1, 1)));

				// Czy �wiat�o jest widoczne?
				if(light->VisibleRect.empty()) {
					light->Visible = false;
					continue;
				}

				// Sprawd� odleg�o�� kamery od sfery otaczaj�cej �wiat�o oraz wy�wietlaj�c taka sfer�
				if(Engine::Config->UseOcclusion && (light->Origin - frame.CameraOrigin).length() > light->Range) {
					u32 result;

					// Ustaw urz�dzenie
					Engine::Device->depthMode(true, false);
					Engine::Device->colorWrite(false, false);
					Engine::Device->cullMode(false);
					Engine::Device->scissor(&light->VisibleRect);

					// Rozpocznij okre�lanie widoczno�ci
					Engine::Device->beginOcclusion();

					// Wy�wietl sfer�
					Engine::Draw->drawSphere(light->Origin, light->Range);

					// Zako�cz okre�lanie widoczno�ci
					Engine::Device->endOcclusion();

					// Sprawd� widoczno��
					while(!Engine::Device->flushOcclusion(result));

					// Przywr�� stan urz�dzenia
					Engine::Device->scissor(&frame.Clip);
					Engine::Device->cullMode(true);
					Engine::Device->colorWrite(true, true);
					Engine::Device->depthMode(true, true);

					// Zapisz wynik
					light->Visible = result > 0;
				}
				else {
					light->Visible = true;
				}
			}

			// Wy�wietl ka�de �wiat�o
			for(RenderLight* light = frame._lights; light; light = light->Next) {
				// Czy �wiat�o jest widoczne?
				if(!light->Visible)
					continue;

				// Ustaw parametry �wiat�a
				frame.LightOrigin = light->Origin;
				frame.LightRange = light->Range;
				frame.LightColor = light->Color;
				frame.LightTransform = light->Transform;
				frame.LightSpot = false;
				frame.LightShadow = light->ShadowMap;
				frame.LightTexture = light->ProjectionMap;
				frame.LightParam = light->Param;
				frame.LightClip = light->VisibleRect;
				frame.LightPRT = light->PRT;
				frame.LightPCA[0] = light->PCA[0];
				frame.LightPCA[1] = light->PCA[1];
				frame.LightPCA[2] = light->PCA[2];
				frame.LightPCA[3] = light->PCA[3];

				// Ustaw �wiat�o
				frame.bindLight();

				// FRAGMENTY NIE PRZEZROCZYSTE
				// Wy�wietl fragmenty wed�ug materia��w
				for(Material *material = materials; material; material = material->Next[frame.Level]) {
					// W��cz PRT
					frame.NoPRT = false;

					// Wy�wietl fragmenty z PRT
					if(material->ChainPRT[frame.Level] && material->bind(frame)) {
						// Wy�wietl fragmenty przypisane do materia�u
						for(RenderFragment *fragment = material->ChainPRT[frame.Level]; fragment; fragment = fragment->NextChain) {
							// Sprawd� fragment, czy znajduje si� w zasi�gu �wiat�a
							if(!light->test(fragment))
								continue;

							// Wy�wietl fragment
							fragment->render(frame);
						}

						// Odepnij materia�
						material->unbind(frame);
					}

					// Wy��cz PRT
					frame.NoPRT = true;

					// Wy�wietl fragmenty bez PRT
					if(material->Chain[frame.Level] && material->bind(frame)) {
						// Wy�wietl fragmenty przypisane do materia�u
						for(RenderFragment *fragment = material->Chain[frame.Level]; fragment; fragment = fragment->NextChain) {
							// Sprawd� fragment, czy znajduje si� w zasi�gu �wiat�a
							if(!light->test(fragment))
								continue;

							// Wy�wietl fragment
							fragment->render(frame);
						}

						// Odepnij materia�
						material->unbind(frame);
					}
				}

				// FRAGMENTY PRZEZROCZYSTE
				// Wy��cz zapisywanie g��boko�ci
				Engine::Device->depthMode(true, false);

				// Wy�wietl fragmenty wed�ug materia��w
				for(u32 i = 0; i < opaques.size(); i++) {
					// W��cz/Wy��cz PRT
					frame.NoPRT = opaques.at(i)->PRT == nullptr;

					// Wy�wietl fragment
					opaques.at(i)->renderMaterial(frame);
				}

				// W��cz zapisywanie g��boko�ci
				Engine::Device->depthMode(true, true);

				// Wy��cz �wiat�o
				frame.unbindLight();
			}
		}

		// Ogranicz widoczny obszar �wiata
		Engine::Device->scissor(&frame.Clip);

		////////////////////////////////////
		// T�O - PRZEZROCZYSTE
		////////////////////////////////////

		if(!frame.NoAmbient && opaques.size()) {
			// Podepnij wy�wietlanie t�a
			frame.bindAmbient();

			// FRAGMENTY PRZEZROCZYSTE
			// Wy��cz zapisywanie g��boko�ci
			Engine::Device->depthMode(true, false);

			// Wy�wietl fragmenty wed�ug materia��w
			for(u32 i = 0; i < opaques.size(); i++) {
				// W��cz/Wy��cz PRT
				frame.NoPRT = opaques.at(i)->PRT == nullptr;

				// Wy�wietl fragment
				opaques.at(i)->renderMaterial(frame);
			}

			// W��cz zapisywanie g��boko�ci
			Engine::Device->depthMode(true, true);

			// Wy��cz wy�wietlanie t�a
			frame.unbindAmbient();
		}

		////////////////////////////////////
		// POST-CUSTOMY
		////////////////////////////////////

		if(!frame.NoPostCustom) {
			Engine::Device->depthMode(true, false);

			// Ustaw spos�b wy�wietlania
			frame.Type = rtCustom;

			// Wy��cz PRT
			frame.NoPRT = false;
			
			// Wy�wietl fragmenty specjalne
			for(RenderFragment *fragment = customs[1]; fragment; fragment = fragment->NextChain)
				fragment->renderMaterial(frame);
		}

		////////////////////////////////////
		// REFRAKCJA
		////////////////////////////////////

		// Je�li mamy refrakcj� to wy�wietl j� w drugim pasie
		if(!frame.NoRefraction && refractive) {
			TextureTarget *target;

			// Pobierz wolny cel wy�wietlania sceny
			if(target = allocSceneTarget()) {
				// Skopiuj scen�
				target->copy(nullptr);

				// Ustaw ramk�
				frame.Refraction = target;
				frame.bindRefraction();

				// FRAGMENTY NIE PRZEZROCZYSTE
				// Wy�wietl fragmenty wed�ug materia��w
				for(Material *material = materials; material; material = material->Next[frame.Level]) {
					// Sprawd� materia�
					if(!material->refractive())
						continue;

					// Wy��cz PRT
					frame.NoPRT = false;

					// Podepnij materia�
					if(material->bind(frame)) {
						// Wy�wietl fragmenty przypisane do materia�u i oznaczone jako PRT
						for(RenderFragment *fragment = material->ChainPRT[frame.Level]; fragment; fragment = fragment->NextChain) 
							fragment->render(frame);

						// Wy�wietl fragmenty przypisane do materia�u i oznaczone bez PRT
						for(RenderFragment *fragment = material->Chain[frame.Level]; fragment; fragment = fragment->NextChain) 
							fragment->render(frame);

						// Odepnij materia�
						material->unbind(frame);
					}
				}

				// FRAGMENTY PRZEZROCZYSTE
				// Wy��cz zapisywanie g��boko�ci
				Engine::Device->depthMode(true, false);

				// Wy�wietl fragmenty wed�ug materia��w
				for(u32 i = 0; i < opaques.size(); i++) {
					if(!opaques.at(i)->Material->refractive())
						continue;
					opaques.at(i)->renderMaterial(frame);
				}

				// W��cz zapisywanie g��boko�ci
				Engine::Device->depthMode(true, true);

				// Odepnij refrakcj�
				frame.unbindRefraction();

				// Zwolnij cel
				freeSceneTarget(target);
			}
		}

		// Zwolnij lustra
		for(RenderMirror *mirror = mirrors, *next; mirror; mirror = next) {
			next = mirror->Next;

			// Zwolnij tekstur� przypisan� do lustra
			if(mirror->Texture)
				freeSceneTarget(mirror->Texture);
			delete mirror;
		}

		// Wy��cz kamer�
		frame.unbindCamera();

		// Zako�cz scen�
		if(!frame.NoPostProcess) {
			_sceneColor->end(0);

			// Wy�wietl postprocess
			framePostProcess(frame);
		}

		// Wy��cz ramk�
		frame.unbindFrame();
	}

	void Render::framePostProcess(RenderFrame &frame) {
		Material *material;

		// Ustaw urz�dzenie
		Engine::Device->depthMode(false, false);
		frame.Type = rtCustom;

		// Zmniejsz rozdzielczo�� sceny
		if(material = Material::get("common/BloomDownsample")) {
			// Ustaw wyrenderowan� scen�
			_sceneColor->bind(stScene);

			// Ustaw obszar wy�wietlania
			Engine::Device->viewport(rect(_sceneBlur[0]->info().Width, _sceneBlur[0]->info().Height));

			// Wy�wietl
			_sceneBlur[0]->begin(0);
			Engine::Draw->drawScreenQuad(material, &frame);
			_sceneBlur[0]->end(0);

			// Wy��cz wyrenderowan� scen�
			_sceneColor->unbind(stScene);
		}

		// Blurujemy poziomo
		if(material = Material::get("common/BloomBlur")) {
			vec4 taps[15];
			vec2 pixel(1.0f / (frame.Width / 2), 1.0f / (frame.Height / 2));

			// Ustaw zmiejszon� scen�
			_sceneBlur[0]->bind(stBlur);

			// Wyznacz wsp�czynniki
			for(s32 i = 0; i < 15; i++) {
				s32 delta = i - 7;
				f32 rho = 3.0f;
				f32 g = expf(delta * delta / (-2.0f * rho * rho)) / sqrtf(2.0f * (f32)Pi * rho * rho);
				taps[i] = vec4(delta * pixel.X + 0.5f * pixel.X, 0.5f * pixel.Y, g, 0.0f);
			}

			// Ustaw urz�dzenie
			Engine::Device->shaderConst(vscNone, pscTaps, &taps->X, 15);
			Engine::Device->viewport(rect(_sceneBlur[1]->info().Width, _sceneBlur[1]->info().Height));

			// Wy�wietl
			_sceneBlur[1]->begin(0);
			Engine::Draw->drawScreenQuad(material, &frame);
			_sceneBlur[1]->end(0);
		}

		// Blurujemy pionowo
		if(material = Material::get("common/BloomBlur")) {
			vec4 taps[15];
			vec2 pixel(1.0f / (frame.Width / 2), 1.0f / (frame.Height / 2));

			// Ustaw zmiejszon� scen�
			_sceneBlur[1]->bind(stBlur);

			// Wyznacz wsp�czynniki
			for(s32 i = 0; i < 15; i++) {
				s32 delta = i - 7;
				f32 rho = 3.0f;
				f32 g = expf(delta * delta / (-2.0f * rho * rho)) / sqrtf(2.0f * (f32)Pi * rho * rho);
				taps[i] = vec4(0.5f * pixel.X, delta * pixel.Y + 0.5f * pixel.Y, g, 0.0f);
			}

			// Ustaw urz�dzenie
			Engine::Device->shaderConst(vscNone, pscTaps, &taps->X, 15);
			Engine::Device->viewport(rect(_sceneBlur[0]->info().Width, _sceneBlur[0]->info().Height));

			// Wy�wietl
			_sceneBlur[0]->begin(0);
			Engine::Draw->drawScreenQuad(material, &frame);
			_sceneBlur[0]->end(0);
		}

		// ��czymy scene
		if(material = Material::get("common/SceneBlend")) {
			// Ustaw urz�dzenie
			Engine::Device->colorConst(vec4(Engine::Config->SceneIntensity, Engine::Config->BlurIntensity, Engine::Config->HighlightIntensity, 1.0f));
			Engine::Device->viewport(rect(frame.Width, frame.Height));

			// Ustaw tekstury
			_sceneColor->bind(stScene);
			_sceneBlur[0]->bind(stBlur);

			// Wy�wietl scen�
			Engine::Draw->drawScreenQuad(material, &frame);

			// Zresetuj tekstury
			_sceneColor->unbind(stScene);
			_sceneBlur[0]->unbind(stBlur);
		}
	}

	void Render::frame(RenderFrame &frame) {	
		// Wy�wietl scene w zale�no�ci od trybu
		switch(frame.Mode) {
			case rmWire:
				frameWire(frame);
				break;

			case rmSolid:
				frameSolid(frame);
				break;

			case rmShaded:
				frameShaded(frame);
				break;

			case rmDepth:
				frameDepth(frame);
				break;

			case rmCustom:
				frameCustom(frame);
				break;
		}
	}

	void Engine::renderView(ayuine::Camera* camera, RenderMode mode, u32 flags, class Viewport* viewport) {
		RenderFrame frame;

		// Sprawd� parametry
		Assert(camera);

		// Ustaw kamer�
		frame.camera(camera);

		// Ustaw parametry wy�wietlania
		frame.Mode = mode;
		frame.Flags = flags;

		// Ustaw wielko�� ekranu
		if(viewport) {
			// TODO
		}
		else {
			frame.Width = Device->width();
			frame.Height = Device->height();
		}

		// Ustaw parametry wy�wietlania
		frame.NoPostProcess = !Config->PostProcess;
		frame.NoReflection = false;
		frame.NoRefraction = false;

		// Pobierz scen�
		World->prepare(frame);

		// Wy�wietl klatk�
		Render->frame(frame);
	}
};
