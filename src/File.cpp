//------------------------------------//
//
// File.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-29
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// File: Constructor

	File::File() {
		_length = 0;
		_at = 0;
		_size = CountOf(_buffer);
		_loading = false;
	}

	//------------------------------------//
	// File: Destructor

	File::~File() {

	}

	//------------------------------------//
	// File: Name methods

	const string &File::name() const {
		return _name;
	}

	//------------------------------------//
	// File: Base methods

	u32 File::at() const {
		return _at;
	}

	u32 File::length() const {
		return _length;
	}

	bool File::loading() const {
		return _loading;
	}

	bool File::eof() const {
		return _at >= _length;
	}

	//------------------------------------//
	// File: Methods

	u32 File::read(void *buffer, u32 count) {
		// Sprawd� czy mo�emy wczytywa�
		Assert(_loading);

		u8 *ubuffer = (u8*)buffer;
		
		// Wczytuj dopuki jest gdzie :)
		while(count > 0) {
			// Bufor tymczasowy jest pusty?
			if(_size == CountOf(_buffer)) {
				// Wczytaj dane
				if(!flush())
					break;
			}

			// Pobierz dane z bufora tymczasowego
			if(_size < CountOf(_buffer)) {
				u32 size = __min(CountOf(_buffer) - _size, count);

				// Skopiuj dane
				memcpy(ubuffer, _buffer + _size, size);

				// Przesu� bufor
				ubuffer += size;

				// Zmniejsz ilo�� danych mo�liwych do wczytania
				_size += size;

				// Zmniejsz ilo�� wczytanych danych
				count -= size;
			}
		}
		return ubuffer - (u8*)buffer;
	}

	string File::read(u32 count) {
		// Sprawd� czy mo�emy wczytywa�
		string data;
		c8 temp[CountOf(_buffer)];
		u32 tempSize;

		// Wczytuj stringa paczkami
		while(tempSize = read(temp, __min(sizeof(temp), count))) {
			// Dodaj dane do stringa
			data.insert(data.end(), temp, temp + tempSize);

			// Zmniejsz ilo�� wczytanych danych
			count -= tempSize;
		}
		return data;
	}

	u32 File::write(const void *buffer, u32 count) {
		// Sprawd� czy mo�emy zapisywa�
		Assert(!_loading);

		const u8 *ubuffer = (const u8*)buffer;

		while(count > 0) {
			// Dodaj dane do bufora tymczasowego
			if(_size < CountOf(_buffer)) {
				u32 size = __min(CountOf(_buffer) - _size, count);

				// Skopiuj dane
				memcpy(_buffer + _size, ubuffer, size);

				// Przesu� bufor
				ubuffer += size;

				// Zwi�ksz ilo�� zapisanych danych
				_size += size;

				// Zmniejsz ilo�� danych do zapisania
				count -= size;
			}

			// Przepe�nienie bufora?
			if(_size == CountOf(_buffer)) {
				// Wyczy�� bufor
				if(!flush()) {
					Assert("Can't write data" && false);
				}

				// A taka ma�a kontrolka ;)
				Assert(_size != CountOf(_buffer));
			}
		}

		// Zwr�� ilo�� zapisanych danych
		return ubuffer - (const u8*)buffer;
	}

	u32 File::write(const string &data) {
		// Zapisz ci�g znak�w
		return write(&data[0], data.size());
	}

	//------------------------------------//
	// File: Serialize operators

	File &File::operator << (bool &value) {
		u8 boolean;

		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&boolean, 1) == 1);

			// Przekonwertuj warto��
			value = boolean != 0;
		}
		else {
			// Przekonwertuj warto��
			boolean = value ? 1 : 0;

			// Zapisz warto��
			Assert(write(&boolean, 1) == 1);
		}
		return *this;
	}

	File &File::operator << (c8 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, 1) == 1);
		}
		else {
			// Zapisz warto��
			Assert(write(&value, 1) == 1);
		}
		return *this;
	}

	File &File::operator << (s8 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, 1) == 1);
		}
		else {
			// Zapisz warto��
			Assert(write(&value, 1) == 1);
		}
		return *this;
	}

	File &File::operator << (s16 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, 2) == 2);
		}
		else {
			// Zapisz warto��
			Assert(write(&value, 2) == 2);
		}
		return *this;
	}

	File &File::operator << (s32 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, 4) == 4);
		}
		else {
			// Zapisz warto��
			Assert(read(&value, 4) == 4);
		}
		return *this;
	}

	File &File::operator << (s64 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, 8) == 8);
		}
		else {
			// Zapisz warto��
			Assert(write(&value, 8) == 8);
		}
		return *this;
	}

	File &File::operator << (u8 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, 1) == 1);
		}
		else {
			// Zapisz warto��
			Assert(write(&value, 1) == 1);
		}
		return *this;
	}

	File &File::operator << (u16 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, 2) == 2);
		}
		else {
			// Zapisz warto��
			Assert(write(&value, 2) == 2);
		}
		return *this;
	}

	File &File::operator << (u32 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, 4) == 4);
		}
		else {
			// Zapisz warto��
			Assert(write(&value, 4) == 4);
		}
		return *this;
	}

	File &File::operator << (u64 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, 8) == 8);
		}
		else {
			// Zapisz warto��
			Assert(write(&value, 1) == 1);
		}
		return *this;
	}

	File &File::operator << (f32 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, 4) == 4);
		}
		else {
			// Zapisz warto��
			Assert(write(&value, 4) == 4);
		}
		return *this;
	}

	File &File::operator << (f64 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, 8) == 8);
		}
		else {
			// Zapisz warto��
			Assert(write(&value, 8) == 8);
		}
		return *this;
	}

	File &File::operator << (vec2 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, sizeof(vec2)) == sizeof(vec2));
		}
		else {
			// Zapisz warto��
			Assert(write(&value, sizeof(vec2)) == sizeof(vec2));
		}
		return *this;
	}

	File &File::operator << (vec3 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, sizeof(vec3)) == sizeof(vec3));
		}
		else {
			// Zapisz warto��
			Assert(write(&value, sizeof(vec3)) == sizeof(vec3));
		}
		return *this;
	}

	File &File::operator << (vec4 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, sizeof(vec4)) == sizeof(vec4));
		}
		else {
			// Zapisz warto��
			Assert(write(&value, sizeof(vec4)) == sizeof(vec4));
		}
		return *this;
	}

	File &File::operator << (mat4 &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, sizeof(mat4)) == sizeof(mat4));
		}
		else {
			// Zapisz warto��
			Assert(write(&value, sizeof(mat4)) == sizeof(mat4));
		}
		return *this;
	}

	File &File::operator << (angles &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, sizeof(angles)) == sizeof(angles));
		}
		else {
			// Zapisz warto��
			Assert(write(&value, sizeof(angles)) == sizeof(angles));
		}
		return *this;
	}

	File &File::operator << (bbox &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, sizeof(bbox)) == sizeof(bbox));
		}
		else {
			// Zapisz warto��
			Assert(write(&value, sizeof(bbox)) == sizeof(bbox));
		}
		return *this;
	}

	File &File::operator << (bsphere &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, sizeof(bsphere)) == sizeof(bsphere));
		}
		else {
			// Zapisz warto��
			Assert(write(&value, sizeof(bsphere)) == sizeof(bsphere));
		}
		return *this;
	}

	File &File::operator << (quat &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, sizeof(quat)) == sizeof(quat));
		}
		else {
			// Zapisz warto��
			Assert(write(&value, sizeof(quat)) == sizeof(quat));
		}
		return *this;
	}

	File &File::operator << (ray &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, sizeof(ray)) == sizeof(ray));
		}
		else {
			// Zapisz warto��
			Assert(write(&value, sizeof(ray)) == sizeof(ray));
		}
		return *this;
	}

	File &File::operator << (rect &value) {
		// Wczytaj/Zapisz warto��
		if(_loading) {
			// Wczytaj warto��
			Assert(read(&value, sizeof(rect)) == sizeof(rect));
		}
		else {
			// Zapisz warto��
			Assert(write(&value, sizeof(rect)) == sizeof(rect));
		}
		return *this;
	}

	//------------------------------------//
	// FileWin: Constructor

	FileWin::FileWin(const string &name, bool loading) {
		// Otw�rz plik
		_handle = CreateFile(name.c_str(), loading ? GENERIC_READ : GENERIC_WRITE, loading ? FILE_SHARE_READ : 0, nullptr, loading ? OPEN_EXISTING : CREATE_NEW, 0, nullptr);

		// Sprawd� uchwyt
		Assert("Can't open file" && _handle != INVALID_HANDLE_VALUE);

		// Zapisz parametry
		_name = name;
		_loading = loading;
		_size = loading ? CountOf(_buffer) : 0;
		_length = GetFileSize(_handle, nullptr);
	}

	//------------------------------------//
	// FileWin: Destructor

	FileWin::~FileWin() {
		// Zapisz reszki :)
		if(!_loading && _size > 0) {
			flush();
		}

		// Zniszcz plik
		CloseHandle(_handle);
	}

	//------------------------------------//
	// FileWin: Base methods

	void FileWin::at(size pos) {
		if(_loading) {

		}
		else {
			
		}
	}

	//------------------------------------//
	// FileWin: Methods

	bool FileWin::flush() {
		DWORD result;

		if(_loading) {
			u32 temp[CountOf(_buffer)];

			// Wczytaj dane
			if(ReadFile(_handle, temp, _size, &result, nullptr) && result) {
				u32 rsize = CountOf(_buffer) - _size;
				u32 rresult = CountOf(_buffer) - result;

				// Przenie� stare dane
				memmove(_buffer + _size - result, _buffer + _size, rsize);

				// Skopiuj nowe dane
				memcpy(_buffer + rresult, temp, result);

				// Ustaw rozmiar bufora
				_size -= result;
			}
			return _size != CountOf(_buffer);
		}
		else {
			// Zapisz dane znajduj�ce si� w tymczasowym buforze
			if(WriteFile(_handle, _buffer, _size, &result, nullptr) && result) {
				// Przenie� nie zapisane dane na sam pocz�tek
				if(_size != result) {
					memmove(_buffer, _buffer + result, _size -= result);
				}
				// Wszystko zapisane :)
				else {
					_size = 0;
				}
			}
			return _size == 0;
		}
	}
};
