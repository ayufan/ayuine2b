//------------------------------------//
//
// Collider.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-21
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// Contact class

	class Contact
	{
		ValueType(Contact, 0);

		// Fields
	public:
		vec3	Origin;
		vec3	Normal;
		f32		Time;
		u32		Id;

		// Constructor
	public:
		Contact() {
			Time = 0;
			Id = 0;
		}
	};

	//------------------------------------//
	// Collider class

	class Collider : public Object
	{
		ObjectType(Collider, Object, 0);

		// Fields
	protected:
		NewtonCollision*			_collider;

		// Precalculated Values
	public:
		vec3		Center;
		vec3		Inertia;
		f32			Volume;

		// Constructor
	protected:
		AYUAPI Collider();

		// Destructor
	public:
		AYUAPI virtual ~Collider();

		// Methods
	public:
		bool cast(const vec3 &start, const vec3 &end, Contact *contact = nullptr); 
		bbox bounds(const mat4 &transform = mat4Identity);

		// Friends
		friend class RigidBody;
	};

	//------------------------------------//
	// BoxCollider class

	class BoxCollider : public Collider
	{
		ObjectType(BoxCollider, Collider, 0);

		// Constructor
	public:
		AYUAPI BoxCollider(const vec3 &size, const mat4 &transform = mat4Identity);
		AYUAPI BoxCollider(const bbox &box);
	};

	//------------------------------------//
	// SphereCollider class

	class SphereCollider : public Collider
	{
		ObjectType(SphereCollider, Collider, 0);

		// Constructor
	public:
		AYUAPI SphereCollider(f32 radius, const mat4 &transform = mat4Identity);
		AYUAPI SphereCollider(const bsphere &sphere);
	};

	//------------------------------------//
	// ConvexCollider class

	class ConvexCollider : public Collider
	{
		ObjectType(ConvexCollider, Collider, 0);

		// Constructor
	public:
		AYUAPI ConvexCollider(const vec3 *verts, u32 count, u32 stride = sizeof(vec3), const mat4 &transform = mat4Identity);
	};

	//------------------------------------//
	// StaticCollider class

	class StaticCollider : public Collider
	{
		ObjectType(StaticCollider, Collider, 0);

		// Constructor
	public:
		AYUAPI StaticCollider();

		// Methods
	public:
		AYUAPI void begin();
		AYUAPI void addFace(u32 id, const vec3 *verts, u32 count, u32 stride = sizeof(vec3));
		AYUAPI void end(bool optimize = true);

		// Serializer
	private:
		AYUAPI void seralize(File &s);
	public:
		friend File& operator << (File &s, StaticCollider &c) {
			c.seralize(s);
			return s;
		}
	};

	//------------------------------------//
	// CompoundCollider class

	class CompoundCollider : public Collider
	{
		ObjectType(CompoundCollider, Collider, 0);

		// Constructor
	public:
		AYUAPI CompoundCollider(const Array<Collider*> &colliders);
	};

	//------------------------------------//
	// NullCollider class

	class NullCollider : public Collider
	{
		ObjectType(NullCollider, Collider, 0);

		// Constructor
	public:
		AYUAPI NullCollider();
	};
};
