//------------------------------------//
//
// Stats.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-13
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class Stats
	{
		ValueType(Stats, 0);

		// Fields
	public:
		u32		Draws;
		u32		Primitives;
		u32		Textures;
		u32		Buffers;
		u32		States;
		u32		Consts;
		u32		Shaders;
		u32		Clips;

		// Constructor
	public:
		Stats();

		// Methods
	public:
		AYUAPI void reset();
		AYUAPI void show(class Font *font);
	};
};
