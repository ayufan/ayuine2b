//------------------------------------//
//
// Render.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// RenderMode enumeration

	enum RenderMode
	{
		rmWire,
		rmSolid,
		rmShaded,
		rmDepth,
		rmCustom
	};

	//------------------------------------//
	// RenderFlags enumeration

	enum RenderFlags
	{
		rfNone						= 0,
		rfShowGrid				= 1,
		rfShowPortals			= 2,
		rfShowSelected		= 4
	};

	//------------------------------------//
	// RenderFragmentFlags enumeration

	enum RenderFragmentFlags
	{
		rffNone						= 0,
		rffCustom					= 1,
		rffPreCustom			= 3,
		rffPostCustom			= 5,
		rffParam					= 8,
		rffColor					= 16,
	};

	//------------------------------------//
	// RenderType enumeration

	enum RenderType
	{
		rtCustom					= 0,
		rtAmbient					= 1,
		rtLight						= 2,
		rtRefraction			= 3
	};

	//------------------------------------//
	// RenderMirror class

	class RenderMirror
	{
		ValueType(RenderMirror, 0);

		// Fields
	public:
		vec4						Plane;
		bbox						Box;
		TextureTarget*	Texture;
		RenderMirror*		Next;
		
		// Constructor
	public:
		RenderMirror() {
			Texture = nullptr;
			Next = nullptr;
		}
	};

	//------------------------------------//
	// RenderFragment class

	class RenderFragment
	{
		ValueType(RenderFragment, 0);

		// Fields
	public:
		mat4										Transform;
		bbox										Box;
		bsphere									Sphere;
		ayuine::Material*				Material;
		Texture*								PRT;
		vec4										Plane;
		RenderFragment*					Next;
		RenderFragment*					NextChain;
		RenderMirror*						Mirror;
		u32											Flags;

		// Render Fields
	public:
		f32											Distance;

		// Constructor
	protected:
		AYUAPI RenderFragment();

		// Destructor
	public:
		AYUAPI virtual ~RenderFragment();

		// Methods
	public:
		AYUAPI virtual void render(RenderState &state) = 0;
		AYUAPI virtual void renderMaterial(RenderState &state);
	};

	//------------------------------------//
	// RenderDynamic class

	class RenderDynamic : public RenderFragment
	{
		ObjectType(RenderDynamic, RenderFragment, 0);

		// Fields
	public:
		VertexType*									VertexType;
		void*												Vertices;
		u32													VertexStride;
		u32													VertexCount;
		u16*												Indices;
		u32													IndexCount;
		Array<BatchIndexedDrawer>		Drawers;

		// Constructor
	public:
		AYUAPI RenderDynamic();

		// Methods
	public:
		AYUAPI virtual void render(RenderState &state);
	};

	//------------------------------------//
	// RenderMesh class

	class RenderMesh : public RenderFragment
	{
		ObjectType(RenderMesh, RenderFragment, 0);

		// Fields
	public:
		ayuine::VertexType*					VertexType;
		ayuine::VertexBuffer*				VertexBuffer;
		ayuine::IndexBuffer*				IndexBuffer;
		u32													Offset;
		Array<BatchIndexedDrawer>		Drawers;

		// Constructor
	public:
		AYUAPI RenderMesh();

		// Methods
	public:
		AYUAPI virtual void render(RenderState &state);
	};

	//------------------------------------//
	// RenderLight class

	class RenderLight 
	{
		ValueType(RenderLight, 0);

		// Fields
	public:
		bbox						Box;
		bsphere					Sphere;
		vec3						Origin;
		f32							Range;
		vec4						Color;
		vec4						Param;
		bool						PRT;
		vec4						PCA[4];
		Texture*				ShadowMap;
		Texture*				ProjectionMap;
		RenderLight*		Next;
		mat4						Transform;

		// Render Fields
	public:
		bool						Visible;
		rect						VisibleRect;

		// Constructor
	public:
		AYUAPI RenderLight();

		// Methods
	public:
		AYUAPI virtual bool test(RenderFragment *fragment);
	};

	//------------------------------------//
	// RenderState class

	class RenderState 
	{
		ValueType(RenderState, 0);

		// Fields
	public:
		RenderMode	Mode;
		u32					Flags;
		RenderType	Type;
		u32					Width, Height;
		f32					Time;
		u32					Level;
		rect				Clip;

		// Global Fields
	public:
		Material*		CustomMaterial;
		bool				NoPreCustom, NoPostCustom;
		bool				NoMaterial;
		bool				NoAmbient;
		bool				NoLighting;
		bool				NoRefraction;
		bool				NoReflection;
		bool				NoPostProcess;
		bool				NoPRT;

		// Ambient Fields
	public:
		vec4				Ambient;
		bool				Sun;
		vec3				SunDir;
		vec4				SunColor;
		vec4				SunParam;
		Texture*		SunShadow;
		bool				SunPRT;
		vec4				SunPCA[4];

		// Light Fields
	public:
		bool				LightSpot;
		vec3				LightOrigin;
		f32					LightRange;
		vec3				LightDir;
		vec4				LightColor;
		vec4				LightParam;
		mat4				LightTransform;
		rect				LightClip;
		Texture*		LightShadow;
		Texture*		LightTexture;
		bool				LightPRT;
		vec4				LightPCA[4];

		// Skinning Fields
	public:
		bool				Skinning;
		mat4				SkinningBones[16];

		// Camera Fields
	public:
		vec3				CameraOrigin;
		vec3				CameraAt, CameraUp, CameraRight;
		mat4				CameraProjection, CameraView;
		frustum			CameraFrustum;
		f32					CameraDistance;

		// Object Fields
	public:
		mat4				Object;
		Texture*		ObjectPRT;

		// Reflective/Refractive Fields
	public:
		Texture*		Reflection;
		Texture*		Refraction;

		// Custom Fields
	public:
		rect				CustomClip;

		// Constructor
	public:
		AYUAPI RenderState();

		// Methods
	public:
		AYUAPI void camera(Camera *camera);
		AYUAPI void bindFrame();
		AYUAPI void bindCamera();
		AYUAPI void bindColor();
		AYUAPI void bindSkinning();
		AYUAPI void bindLight();
		AYUAPI void bindAmbient();
		AYUAPI void bindReflection();
		AYUAPI void bindRefraction();
		AYUAPI void bindObject();
		AYUAPI void unbindObject();
		AYUAPI void unbindReflection();
		AYUAPI void unbindRefraction();
		AYUAPI void unbindAmbient();
		AYUAPI void unbindLight();
		AYUAPI void unbindSkinning();
		AYUAPI void unbindCamera();
		AYUAPI void unbindFrame();
	};

	//------------------------------------//
	// RenderFrame class

	class RenderFrame : public RenderState
	{
		ObjectType(RenderFrame, RenderState, 0);

		// Private Fields
	private:
		RenderFragment*		_fragments;
		RenderLight*			_lights;

		// Constructor
	public:
		AYUAPI RenderFrame();

		// Destructor
	public:
		AYUAPI ~RenderFrame();

		// Methods
	public:
		AYUAPI void addFrag(RenderFragment *frag);
		AYUAPI void addLight(RenderLight *light);

		// Friends
		friend class Render;
	};

	//------------------------------------//
	// Render class

	class Render
	{
		ValueType(Render, 0);

		// Fields
	private:
		SmartPtr<TextureTarget>	_sceneColor;
		SmartPtr<TextureTarget>	_sceneBlur[2];
		TextureTarget*					_sceneTargets;

		// Constructor
	public:
		Render();

		// Destructor
	public:
		~Render();

		// Targets Methods
	private:
		TextureTarget *allocSceneTarget();
		void freeSceneTarget(TextureTarget *target);

		// Methods
	private:
		void frameCustom(RenderFrame &frame);
		void frameWire(RenderFrame &frame);
		void frameSolid(RenderFrame &frame);
		void frameShaded(RenderFrame &frame);
		void frameDepth(RenderFrame &frame);
		void framePostProcess(RenderFrame &frame);
	public:
		AYUAPI void frame(RenderFrame &frame);
	};
};
