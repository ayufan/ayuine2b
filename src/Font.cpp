//------------------------------------//
//
// Font.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-13
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Collections

	template<>
	Collection<Font>::ObjectMap Collection<Font>::_objects;

	//------------------------------------//
	// Font: Constructor

	Font::Font() {
		// Wyzeruj dane
		_font = nullptr;
		memset(&_desc, 0, sizeof(_desc));
		memset(&_metrics, 0, sizeof(_metrics));
		memset(&_widths, 0, sizeof(_widths));
	}

	//------------------------------------//
	// Font: Destructor

	Font::~Font() {
		// Zwolnij czczionk�
		deviceLost(true);
	}

	//------------------------------------//
	// Font: DeviceObject Methods
	
	void Font::deviceReset(bool theBegin) {
		if(theBegin) {
			dxe(D3DXCreateFontIndirect(Engine::Device->d3ddev(), &_desc, &_font));

			// Pobierz szeroko�� liter
			GetCharWidth32(_font->GetDC(), 0, CountOf(_widths) - 1, _widths);

			// Pobierz parametry czcionki
			dxe(_font->GetTextMetrics(&_metrics));
		}
		else {
			dxe(_font->OnResetDevice());
		}
	}

	void Font::deviceLost(bool theEnd) {
		if(theEnd) {
			if(_font) {
				_font->Release();
				_font = nullptr;
			}
		}
		else {
			dxe(_font->OnLostDevice());
		}
	}

	//------------------------------------//
	// Font: Methods

	void Font::load(const string &name) {
		// Okre�l parametry czcionki
		memset(&_desc, 0, sizeof(_desc));
		_desc.Height = 12;
		_desc.Width = 6;
		strcpy_s(_desc.FaceName, sizeof(_desc.FaceName), name.c_str());

		// Utw�rz czcionk�
		deviceReset(true);
		_loaded = true;
	}

	void Font::unload() {
		// Zniszcz czcionk�
		deviceLost(true);
		_loaded = false;
	}

	vec2 Font::measure(const string &text) {
		f32 width = 0;
		const c8 *p = text.c_str();

		// Oblicz szeroko�� tekstu
		while(*p) {
			width += (*p < CountOf(_widths)) ? _widths[*p] : _metrics.tmAveCharWidth;
			p++;
		}
		return vec2(width, _metrics.tmHeight);
	}

	rect Font::measure(const vec2 &pos, const string &text, u32 format) {
		vec2 size;
		vec2 rpos;

		// Pobierz wielko�� tekstu
		size = measure(text);

		// Jest co�?
		if(size.empty())
			return rect(pos, pos);

		// Oblicz po�o�enie poziome
		switch(format & (DT_LEFT | DT_CENTER | DT_RIGHT)) {
			case DT_LEFT:			rpos.X = pos.X;											break;
			case DT_CENTER:		rpos.X = pos.X - size.X / 2.0f;			break;
			case DT_RIGHT:		rpos.X = pos.X - size.X;						break;
		}

		// Oblicz po�o�enie pionowe
		switch(format & (DT_TOP | DT_VCENTER | DT_BOTTOM)) {
			case DT_TOP:			rpos.Y = pos.Y;											break;
			case DT_VCENTER:	rpos.Y = pos.Y - size.Y / 2.0f;			break;
			case DT_BOTTOM:		rpos.Y = pos.Y - size.Y;						break;
		}

		// Zwr�� obszar wy�wietlania tekstu
		return rect(rpos.X, rpos.Y, size.X, size.Y);
	}

	void Font::draw(const vec2 &pos, const string &text, u32 format, const vec3 &color) {
		RECT drawRect;
		rect textRect;

		// Oblicz wielko�� tekstu
		textRect = measure(pos, text, format);

		// Przekszta�� wielko�� tekstu
		drawRect.left		= (LONG)textRect.Left;
		drawRect.right	= (LONG)textRect.Right;
		drawRect.top		= (LONG)textRect.Top;
		drawRect.bottom = (LONG)textRect.Bottom;

		// Wy�wietl tekst
		dxe(_font->DrawText(nullptr, text.c_str(), text.length(), &drawRect, format & (DT_EXPANDTABS | DT_RTLREADING | DT_NOCLIP | DT_SINGLELINE | DT_WORDBREAK), D3DCOLOR_COLORVALUE(color.X, color.Y, color.Z, 1.0f)));
	}
};
