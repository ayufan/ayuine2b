//------------------------------------//
//
// Draw.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-9
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// BatchDrawer class

	class BatchDrawer
	{
		ValueType(BatchDrawer, 0);

		// Fields
	public:
		D3DPRIMITIVETYPE Type;
		u32 StartVertex;
		u32 PrimitiveCount;

		// Constructor
	public:
		AYUAPI BatchDrawer();
		AYUAPI BatchDrawer(D3DPRIMITIVETYPE type, u32 startVertex, u32 primitiveCount);

		// Methods
	public:
		AYUAPI void draw();
	};

	//------------------------------------//
	// BatchIndexedDrawer class

	class BatchIndexedDrawer
	{
		ValueType(BatchIndexedDrawer, 0);

		// Fields
	public:
		D3DPRIMITIVETYPE Type;
		s32 BaseIndex;
		u32 MinIndex;
		u32 NumVertices;
		u32 StartIndex;
		u32 PrimitiveCount;

		// Constructor
	public:
		AYUAPI BatchIndexedDrawer();
		AYUAPI BatchIndexedDrawer(D3DPRIMITIVETYPE type, u32 minIndex, u32 numVertices, u32 startIndex, u32 primitiveCount, s32 baseIndex = 0);

		// Methods
	public:
		AYUAPI void draw();
	};

	//------------------------------------//
	// Draw class

	class Draw
	{
		ValueType(Draw, 0);

		// Fields
	private:
		SmartPtr<VertexType>			_formatXyz;

		// Static Buffers Fields
	private:
		SmartPtr<VertexBuffer>		_vertexBuffer;
		SmartPtr<IndexBuffer>			_indexBuffer;
		BatchIndexedDrawer				_sphereBatch, _boxBatch;

		// Dynamic Buffers Fields
	private:
		SmartPtr<VertexBuffer>		_dynamicVertexBuffer;
		u32												_usedVertexBytes;
		SmartPtr<IndexBuffer>			_dynamicIndexBuffer;
		u32												_usedIndexBytes;

		// Constructor
	public:
		Draw();

		// Destructor
	public:
		~Draw();

		// Methods
	public:
		AYUAPI void draw(D3DPRIMITIVETYPE type, u32 startVertex, u32 primitiveCount);
		AYUAPI void draw(D3DPRIMITIVETYPE type, u32 minIndex, u32 numVertices, u32 startIndex, u32 primitiveCount, s32 baseIndex = 0);
		AYUAPI void drawSphere(const vec3 &origin, f32 radius, class Material *material = nullptr, class RenderState *state = nullptr);
		AYUAPI void drawScreenQuad(class Material *material = nullptr, class RenderState *state = nullptr);

		// Dynamic Methods
	public:
		AYUAPI void bindVertices(u32 stream, const void *vertices, u32 stride, u32 count);
		AYUAPI void bindIndices(const u16 *indices, u32 count);
		AYUAPI void unbindIndices();
		AYUAPI void unbindVertices(u32 stream);
	};
};
