//------------------------------------//
//
// AMesh.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-1
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// AMesh: Constructor

	AMesh::AMesh() {
		// Ustaw dane o aktorze
		Mesh = nullptr;
		Object = u32Max;

		// Flags
		Flags = afCastShadows | afReceiveShadows | afInvalidated | afUpdate | afZones | afArea | afPrepare | afTransform;
	}

	//------------------------------------//
	// AMesh: Destructor

	AMesh::~AMesh() {
	}

	//------------------------------------//
	// AMesh: Methods

	void AMesh::preupdate(f32 dt) {
		// Wywo�aj g��wn� metod�
		Actor::preupdate(dt);	
	}
	
	void AMesh::postupdate(f32 dt) {
		// Wywo�aj g��wn� metod�
		Actor::postupdate(dt);

		// Uaktualnij ka�dy obiekt mesha
		if(Changed & cfArea) {
			if(Mesh->Objects.size() <= Object) {
				// Zresetuj boxa
				Box = bbox();

				// Oblicz ka�dego boxa
				ArrayEach(MeshObject, i, Mesh->Objects) {
					Box = Box.merge(i->Box.transform(Transform));
				}
			}
			// Uaktualnij konkretny obiekt mesha
			else {
				Box = Mesh->Objects.at(Object).Box.transform(Transform);
			}
		}
	}

	void AMesh::prepare(RenderFrame &frame, MeshObject &object) {
		// A mamy co wy�wietla�?
		if(!Mesh)
			return;

		// Czy obiekt rzuca cie�?
		if(frame.Mode == rmDepth) {
			if(~Flags & afCastShadows)
				return;
		}

		// Specjalny wersja dla siatki i bufora g��boko�ci
		if(frame.Mode == rmDepth || frame.Mode == rmWire) {
			RenderMesh *renderMesh = new RenderMesh();

			// Wype�nij obiekt
			renderMesh->Transform = Transform;
			renderMesh->Box = Box;
			renderMesh->Sphere = bsphere(Box.center(), Box.radius());
			renderMesh->VertexType = Mesh->VertexType;
			renderMesh->VertexBuffer = Mesh->VertexBuffer;
			renderMesh->IndexBuffer = Mesh->IndexBuffer;
			renderMesh->Drawers.push(BatchIndexedDrawer(D3DPT_TRIANGLELIST, 0, object.VertexCount, object.IndexStart, object.IndexCount / 3, object.VertexStart));

			// Dodaj fragment
			frame.addFrag(renderMesh);
		}
		else {
			// Wy�wietl wszystkie powierzchnie
			ArrayEach(MeshSurface, i, object.Surfaces) {
				// Sprawd� materia�
				if(!i->Material)
					continue;
				if(!i->Material->renderable())
					continue;

				RenderMesh *renderMesh = new RenderMesh();

				// Wype�nij obiekt
				renderMesh->Transform = Transform;
				renderMesh->Box = Box;
				renderMesh->Sphere = bsphere(Box.center(), Box.radius());
				renderMesh->Material = i->Material;
				renderMesh->PRT = Mesh->PRT ? (Texture*)object.PRT : nullptr;
				renderMesh->Plane = i->Plane;
				renderMesh->VertexType = Mesh->VertexType;
				renderMesh->VertexBuffer = Mesh->VertexBuffer;
				renderMesh->IndexBuffer = Mesh->IndexBuffer;
				renderMesh->Drawers.push(BatchIndexedDrawer(D3DPT_TRIANGLELIST, 0, object.VertexCount, i->IndexStart, i->IndexCount / 3, object.VertexStart));
				
				// Dodaj fragment
				frame.addFrag(renderMesh);
			}
		}
	}

	void AMesh::prepare(RenderFrame &frame) {
		if(!Mesh)
			return;

		// Przygotuj ka�dy obiekt mesha
		if(Mesh->Objects.size() <= Object) {
			ArrayEach(MeshObject, i, Mesh->Objects) {
				prepare(frame, *i);
			}
		}
		// Przygotuj konkretny obiekt mesha
		else {
			prepare(frame, Mesh->Objects.at(Object));
		}
	}

	void AMesh::rigidBody(f32 massScalar) {
		Collider *collider = nullptr;
		f32 mass = 0.0f;

		// Komunikat z b��dem
		Assert(Mesh != nullptr);

		// Pobierz collidera
		collider = Mesh->Objects.size() <= Object ? Mesh->Collider : Mesh->Objects.at(Object).Collider;

		// Sprawd� collidera
		Assert(collider != nullptr);

		// Pobierz mas�
		mass = (Mesh->Objects.size() <= Object ? Mesh->Mass : Mesh->Objects.at(Object).Mass) * (massScalar > 0.0f ? massScalar : 1.0f);

		// Utw�rz cia�o fizyczn�
		Body.reset(new RigidBody(collider));

		// Ustaw cia�o fizyczne
		Body->mass(mass, collider->Inertia, collider->Center);
		Body->Transform = Transform;
	}
};
