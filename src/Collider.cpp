//------------------------------------//
//
// Collider.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-21
//
//------------------------------------//

#include "StdAfx.hpp"
#include "Collider.hpp"

namespace ayuine
{
	//------------------------------------//
	// Collider: Constructor

	Collider::Collider() {
		_collider = nullptr;
	}

	//------------------------------------//
	// Collider: Destructor

	Collider::~Collider() {
		// Zniszcz collidera
		if(_collider) {
			if(Engine::World) {
				NewtonReleaseCollision(Engine::World->_worldPhysics, _collider);
			}
			_collider = nullptr;
		}
	}

	//------------------------------------//
	// Collider: Methods

	bool Collider::cast(const vec3 &start, const vec3 &end, Contact *contact) {
		vec3 normal;
		f32 time;
		u32 id;

		// Wyznacz punkt kolizji kolidera z odcinkiem
		time = NewtonCollisionRayCast(_collider, &start.X, &end.X, &normal.X, (int*)&id);

		// Sprawd� rezultat
		if(time > 1.0f)
			return false;

		// Zapisz wynik
		if(contact) {
			contact->Origin = vec3::lerp(start, end, time);
			contact->Time = time;
			contact->Normal = normal;
			contact->Id = id;
		}
		return true;
	}

	bbox Collider::bounds(const mat4 &transform) {
		bbox box;

		// Oblicz bbox collidera
		NewtonCollisionCalculateAABB(_collider, &transform.m11, &box.Mins.X, &box.Maxs.X);
		return box;
	}

	//------------------------------------//
	// BoxCollider: Constructor

	BoxCollider::BoxCollider(const vec3 &size, const mat4 &transform) {
		// Utw�rz collidera
		_collider = NewtonCreateBox(Engine::World->_worldPhysics, size.X, size.Y, size.Z, &transform.m11);

		// TODO: Oblicz parametry obiektu
		Center = transform.transformCoord(vec3(0, 0, 0));
		Inertia = transform.transformCoord(vec3(0, 0, 1)).normalize();
		Volume = size.X * size.Y * size.Z;
	}

	BoxCollider::BoxCollider(const bbox &box) {
		mat4 temp;
		vec3 size;

		// Wyznacz transformacj�
		mat4::translation(temp, box.center());

		// Oblicz wielko�� boxa
		size = box.Maxs - box.Mins;

		// Wywo�aj konstruktor
		this->BoxCollider::BoxCollider(size, temp);
	}

	//------------------------------------//
	// SphereCollider: Constructor

	SphereCollider::SphereCollider(f32 radius, const mat4 &transform) {
		// Utw�rz collidera
		_collider = NewtonCreateSphere(Engine::World->_worldPhysics, radius, radius, radius, &transform.m11);

		// TODO: Oblicz parametry obiektu
		Center = transform.transformCoord(vec3(0, 0, 0));
		Inertia = transform.transformCoord(vec3(0, 0, 1)).normalize();
		Volume = 4.0f * Pi * radius * radius * radius / 3.0f;
	}

	SphereCollider::SphereCollider(const bsphere &sphere) {
		mat4 temp;

		// Wyznacz transformacj�
		mat4::translation(temp, sphere.Center);

		// Wywo�aj konstruktor
		this->SphereCollider::SphereCollider(sphere.Radius, temp);
	}

	//------------------------------------//
	// ConvexCollider: Constructor

	ConvexCollider::ConvexCollider(const vec3 *verts, u32 count, u32 stride, const mat4 &transform) {
		// Sprawd� dane
		Assert(verts);
		Assert(count);
		Assert(stride);

		// Utw�rz collidera
		_collider = NewtonCreateConvexHull(Engine::World->_worldPhysics, count, (dFloat*)&verts->X, stride, (dFloat*)&transform.m11);

		// Oblicz parametry obiektu
		NewtonConvexCollisionCalculateInertialMatrix(_collider, &Inertia.X, &Center.X);
		Volume = NewtonConvexCollisionCalculateVolume(_collider);
	}

	//------------------------------------//
	// StaticCollider: Constructor

	StaticCollider::StaticCollider() {
		// Utw�rz collidera
		_collider = NewtonCreateTreeCollision(Engine::World->_worldPhysics, nullptr);
	}

	//------------------------------------//
	// StaticCollider: Methods

	void StaticCollider::begin() {
		// Rozpocznij budowanie collidera
		NewtonTreeCollisionBeginBuild(_collider);

		// Oblicz parametry obiektu
		Center = vec3(0, 0, 0);
		Inertia = vec3(0, 0, 0);
		Volume = FLT_MAX;
	}

	void StaticCollider::addFace(u32 id, const vec3 *verts, u32 count, u32 stride) {
		// Sprawd� dane
		Assert(verts != 0);
		Assert(count != 0);
		Assert(stride != 0);
		
		// Dodaj �ciank�
		NewtonTreeCollisionAddFace(_collider, count, &verts->X, stride, id);
	}

	void StaticCollider::end(bool optimize) {
		// Zako�cz budowanie collidera
		NewtonTreeCollisionEndBuild(_collider, optimize);
	}

	//------------------------------------//
	// StaticCollider: Serializer

	static void StaticColliderSerialize(File& s, const void* buffer, size_t size) {
		// Zapisz dane
		s.write(buffer, size);
	}

	static void StaticColliderDeserialize(File& s, void* buffer, size_t size) {
		// Odczytaj dane
		s.read(buffer, size);
	}

	void StaticCollider::seralize(File &s) {
		if(s.loading()) {
			// Zniszcz starego collidera
			if(_collider) {
				NewtonReleaseCollision(Engine::World->_worldPhysics, _collider);
				_collider = nullptr;
			}

			// Odczytaj collidera
			_collider = NewtonCreateTreeCollisionFromSerialization(Engine::World->_worldPhysics, nullptr, (NewtonDeserialize)StaticColliderDeserialize, &s);
		}
		else {
			// Zapisz collidera
			NewtonTreeCollisionSerialize(_collider, (NewtonSerialize)StaticColliderSerialize, &s);
		}
	}

	//------------------------------------//
	// CompoundCollider: Constructor

	CompoundCollider::CompoundCollider(const Array<Collider*> &colliders) {
		Array<NewtonCollision*> collisions(colliders.size());

		// Wyzeruj parametry obiektu
		Center = vec3(0, 0, 0);
		Volume = 0.0f;

		// Pobierz list� wszystkich collider�w
		for(u32 i = 0; i < colliders.size(); i++) {
			// Sprawd� dane
			Assert(colliders.at(i));

			// HACK: Dodaj collidera do listy
			collisions.at(i) = ((CompoundCollider*)colliders.at(i))->_collider;

			// TODO: Dodaj parametry collidera
			Center += colliders.at(i)->Center;
			Inertia += colliders.at(i)->Inertia;
			Volume += colliders.at(i)->Volume;
		}

		// Wyznacz �rodek collidera
		Center /= Volume;
		Inertia /= Volume;

		// Utw�rz collidera
		_collider = NewtonCreateCompoundCollision(Engine::World->_worldPhysics, colliders.size(), &collisions[0]);
	}

	//------------------------------------//
	// NullCollider class

	NullCollider::NullCollider() {
		// Utw�rz collidera
		_collider = NewtonCreateNull(Engine::World->_worldPhysics);

		// Oblicz parametry obiektu
		Volume = 0;
	}
};
