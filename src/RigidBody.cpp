//------------------------------------//
//
// RigidBody.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-21
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// RigidBody: Functions

	static void RigidBodyApplyForceAndTorque(const NewtonBody* body) {
		RigidBody *rb = (RigidBody*)NewtonBodyGetUserData(body);
		f32 mass, ixx, iyy, izz;

		// Sprawd� czy mamy doczynienia z obiektem "RigidBody"
		if(!rb)
			return;

		// Pobierz parametry cia�a
		NewtonBodyGetMassMatrix(body, &mass, &ixx, &iyy, &izz);
		
		// Grawitacja
		if(rb->Gravity) {
			// Oblicz grawitacj�
			vec3 force = Engine::World->Gravity * mass;

			// Dodaj si��
			NewtonBodyAddForce(body, &force.X);
		}

		// Ciecze
		if(rb->Fluids) {
			// TODO
		}
	}

	static void RigidBodyTransform(const NewtonBody* body, const dFloat* matrix) {
		RigidBody *rb = (RigidBody*)NewtonBodyGetUserData(body);

		// Sprawd� czy mamy doczynienia z obiektem "RigidBody"
		if(!rb)
			return;

		// I czy mo�na nim rusza� :P
		if(!rb->Movable)
			return;

		// Zapisz macierz transformacji
		mat4::transpose(rb->Transform, *(mat4*)matrix);

		// Oblicz odwr�con� macierz transformacji
		mat4::invert(rb->InvTransform, rb->Transform);

		// Pobierz pr�dko�� liniow� i k�tow�
		NewtonBodyGetVelocity(body, &rb->Velocity.X);
		NewtonBodyGetOmega(body, &rb->Omega.X);
	}

	//------------------------------------//
	// RigidBody: Constructor

	RigidBody::RigidBody(Collider* collider) {
		vec3 angularDamping(0.2f);

		// Ustaw domy�lne warto�ci
		_collider = collider;
		_bone = nullptr;
		Transform = mat4Identity;
		State = true;
		Gravity = true;
		Fluids = true;
		Movable = true;

		// Utw�rz cia�o fizyczne
		_body = NewtonCreateBody(Engine::World->_worldPhysics, _collider ? _collider->_collider : nullptr);

		// Ustaw wska�nik na RigidBody
		NewtonBodySetUserData(_body, this);

		// Ustaw callbacki
		NewtonBodySetForceAndTorqueCallback(_body, RigidBodyApplyForceAndTorque);
		NewtonBodySetTransformCallback(_body, RigidBodyTransform);

		// Ustaw cia�o fizyczne
		//NewtonBodySetMaterialGroupID(_body, NewtonMaterialGetDefaultGroupID(Engine::World->_worldPhysics));
	}

	//------------------------------------//
	// RigidBody: Destructor

	RigidBody::~RigidBody() {
		// Zniszcz cia�o
		if(_body) {
			if(Engine::World) {
				NewtonDestroyBody(Engine::World->_worldPhysics, _body);
			}
			_body = nullptr;
		}
	}

	//------------------------------------//
	// RigidBody: Methods

	void RigidBody::collider(Collider* collider) {
		// Zapisz collidera
		_collider = collider;

		// Ustaw collidera
		NewtonBodySetCollision(_body, collider ? collider->_collider : nullptr);
	}

	void RigidBody::mass(f32 mass, const vec3 &inertia, const vec3 &center) {
		// Ustaw parametry cia�a
		NewtonBodySetCentreOfMass(_body, &center.X);
		NewtonBodySetMassMatrix(_body, mass, inertia.X, inertia.Y, inertia.Z);
	}

	void RigidBody::impulse(const vec3 &p, const vec3 &i) {
		// Dodaj impuls
		NewtonAddBodyImpulse(_body, &i.X, &p.X);
	}

	void RigidBody::apply() {
		mat4 temp;

		// Transponuj macierz
		mat4::transpose(temp, Transform);

		// Ustaw transformacj� cia�a
		NewtonBodySetMatrix(_body, &temp.m11);

		// Ustaw parametry cia�a
		NewtonBodySetVelocity(_body, &Velocity.X);
		NewtonBodySetOmega(_body, &Omega.X);

		// Ustaw stan cia�a
		if((NewtonBodyGetAutoFreeze(_body) == 0) != State) {
			if(State) {
				NewtonWorldUnfreezeBody(NewtonBodyGetWorld(_body), _body);
			}
			else {
				NewtonWorldFreezeBody(NewtonBodyGetWorld(_body), _body);
			}
		}
	}

	bool RigidBody::cast(const vec3 &start, const vec3 &end, Contact *contact) {
		// Castuj na collidera
		if(_collider && _collider->cast(InvTransform.transformCoord(start), InvTransform.transformCoord(end), contact)) {
			// Przekszta�� dane do przestrzenii �wiata
			if(contact) {
				contact->Origin = Transform.transformCoord(contact->Origin);
				contact->Normal = Transform.transformNormal(contact->Normal).normalize();
			}
			return true;
		}
		return false;
	}

	bbox RigidBody::bounds() {
		bbox box;

		// Oblicz bbox cia�a
		NewtonBodyGetAABB(_body, &box.Mins.X, &box.Maxs.X);
		return box;
	}
};
