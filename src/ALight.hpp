//------------------------------------//
//
// ALight.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-1
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// ALight class

	class ALight : public Actor
	{
		ObjectType(ALight, Actor, 0);

		// Fields
	public:
		vec4											Color;
		f32												Range;
	private:
		SmartPtr<CubeTarget>		_shadowMap;
		u32											_shadowFlags;

		// Constructor
	public:
		AYUAPI ALight();

		// Destructor
	public:
		AYUAPI virtual ~ALight();

		// Methods
	public:
		AYUAPI virtual void invalidate(const bbox &box, Actor *owner);
		AYUAPI virtual void preupdate(f32 dt);
		AYUAPI virtual void postupdate(f32 dt);
		AYUAPI virtual void prepare(RenderFrame &frame);
	};
};
