//------------------------------------//
//
// Input.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Input: Constructor

	Input::Input() {
		_mouseX = _mouseY = _mouseDeltaX = _mouseDeltaY = 0;
		_mouseState = mNone;
		_input = nullptr;
		_inputKeyboard = _inputMouse = nullptr;

		// Wyczy�� stan klawiszy
		memset(_keyState, 0, sizeof(_keyState));
		memset(_keyPrevState, 0, sizeof(_keyPrevState));

		// Wiadomo�� do konsoli
		Engine::Log->print("Input: Initializing...");

		// Utw�rz urz�dzenia do obs�ugi wej�cia
		if(FAILED(DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (LPVOID*)&_input, nullptr))) {
			Engine::Log->error("Input: Can't initialize input");
			return;
		}

		// Utw�rz urz�dzenia do obs�ugi klawiatury
		if(SUCCEEDED(_input->CreateDevice(GUID_SysKeyboard, &_inputKeyboard, nullptr))) {
			// Ustaw format klawiatury
			if(SUCCEEDED(_inputKeyboard->SetDataFormat(&c_dfDIKeyboard))) {
				// Zacznij �ledzenie klawiatury
				if(FAILED(_inputKeyboard->Acquire())) {
					Engine::Log->error("Input: Can't acquire keyboard");
					_inputKeyboard->Release();
					_inputKeyboard = nullptr;
				}
			}
			else {
				Engine::Log->error("Input: Can't set keyboard format");
				_inputKeyboard->Release();
				_inputKeyboard = nullptr;
			}
		}
		else {
			Engine::Log->error("Input: Can't initialize keyboard");
		}

		// Utw�rz urz�dzenia do obs�ugi myszki
		if(SUCCEEDED(_input->CreateDevice(GUID_SysMouse, &_inputMouse, nullptr))) {
			// Ustaw format klawiatury
			if(SUCCEEDED(_inputMouse->SetDataFormat(&c_dfDIMouse2))) {
				// Zacznij �ledzenie klawiatury
				if(FAILED(_inputMouse->Acquire())) {
					Engine::Log->error("Input: Can't acquire mouse");
					_inputKeyboard->Release();
					_inputKeyboard = nullptr;
				}
			}
			else {
				Engine::Log->error("Input: Can't set mouse format");
				_inputKeyboard->Release();
				_inputKeyboard = nullptr;
			}
		}
		else {
			Engine::Log->error("Input: Can't initialize mouse");
		}
	}

	//------------------------------------//
	// Input: Destructor

	Input::~Input() {
		// Wiadomo�� do konsoli
		Engine::Log->print("Input: Releasing...");

		// Zako�cz �ledzenie klawiatury
		if(_inputKeyboard != nullptr) {
			_inputKeyboard->Unacquire();
			_inputKeyboard->Release();
			_inputKeyboard = nullptr;
		}

		// Zako�cz �ledzenie myszki
		if(_inputMouse != nullptr) {
			_inputMouse->Unacquire();
			_inputMouse->Release();
			_inputMouse = nullptr;
		}

		// Usu� urz�dzenie wej�cia
		if(_input != nullptr) {
			_input->Release();
			_input = nullptr;
		}
	}

	//------------------------------------//
	// Input: Methods

	s32 Input::mouseX(bool delta) {
		dGuard {
			// Pobierz przesuni�cie myszki
			return delta ? _mouseDeltaX : _mouseX;
		}
		dUnguard;
	}

	s32 Input::mouseY(bool delta) {
		dGuard {
			// Pobierz przesuni�cie myszki
			return delta ? _mouseDeltaY : _mouseX;
		}
		dUnguard;
	}

	Mouse Input::mouseState(bool delta) {
		// Pobierz stan klawiszy myszki
		return delta ? _mouseState : _mouseState;
	}

	bool Input::keyState(u32 key, bool once) {
		dGuard {
			// Sprawd� argumenty
			Assert(0 <= key && key < CountOf(_keyState));

			// Pobierz stan klawisza
			return (_keyState[key] /*& 0x80*/) != 0 && (once == false || (_keyPrevState[key] /*& 0x80*/) == 0);
		}
		dUnguard;
	}

	void Input::update() {
		// Skopiuj stary stan klawiatury
		memcpy(_keyPrevState, _keyState, sizeof(_keyPrevState));

		// Pobierz stan klawiatury
		if(_inputKeyboard != nullptr) {
			// Pobierz stan klawiatury u�ywaj�c DInput8
			if(FAILED(_inputKeyboard->GetDeviceState(256, _keyState)))
				Engine::Log->error("Input: Can't get keyboard state");
		}
		else {
			// Pobierz stan klawiatury u�ywaj�c WinAPI
			//if(!GetKeyboardState(_keyState))
			//	error("Input: Can't get keyboard state");
		}

		// Pobierz stan myszki
		if(_inputMouse != nullptr) {
			DIMOUSESTATE2	mouseState;

			// Pobierz stan klawiatury u�ywaj�c DInput8
			if(SUCCEEDED(_inputMouse->GetDeviceState(sizeof(mouseState), &mouseState))) {
				// Zapisz nowy stan myszy
				_mouseDeltaX = mouseState.lX;
				_mouseDeltaY = mouseState.lY;
				_mouseState = mNone;

				// Zapisz stan przycisk�w myszy
				for(s32 i = 0; i < CountOf(mouseState.rgbButtons); i++) {
					if((mouseState.rgbButtons[i] & 0x80) != 0) {
						_mouseState = Mouse(_mouseState | (1 << i));
					}
				}

				// Zapisz stan rolki
				_mouseState = Mouse(_mouseState | (mouseState.lZ > 0 ? mUp : (mouseState.lZ < 0 ? mDown : mNone)));
			}
			else {
				Engine::Log->error("Input: Can't get mouse state");
			}
		}

		POINT mouse;

		// Pobierz pozycj� kursora
		if(GetCursorPos(&mouse)) {
			// Zapisz stary stan myszy
			//_mousePrevX = _mouseX;
			//_mousePrevY = _mouseY;
			//_mousePrevState = _mouseState;

			// Zapisz nowy stan myszy
			// TODO : Obs�uga przycisk�w myszy
			_mouseX = mouse.x;
			_mouseY = mouse.y;
		}
		else {
			Engine::Log->error("Input: Can't get mouse state");
		}
	}
};
