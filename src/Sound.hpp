//------------------------------------//
//
// SoundBuffer.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#pragma once

struct vorbis_info;
struct OggVorbis_File;

namespace ayuine
{
	class SoundSource;

	//------------------------------------//
	// Typedefs

	typedef Array<SoundSource*>		SoundSourceArray;

	//------------------------------------//
	// SoundBuffer class

	class SoundBuffer : public Object, public Collection<SoundBuffer>
	{
		ObjectType(SoundBuffer, Object, 0);

		// Fields
	private:
		u32													_buffer;
		SoundSourceArray						_sourceList;

		// Constructor
	public:
		AYUAPI SoundBuffer();

		// Destructor
	public:
		AYUAPI virtual ~SoundBuffer();

		// Methods
	public:
		AYUAPI void load(const string &soundFileName);
		AYUAPI void load(File *soundFile);
		AYUAPI void unload();
		AYUAPI u32 frequency();
		AYUAPI u32 bits();
		AYUAPI u32 channels();
		AYUAPI u32 size();
		AYUAPI f32 length();

		// Friends
		friend class SoundSource;
	};

	//------------------------------------//
	// SoundSource class

	class SoundSource
	{
		ValueType(SoundSource, 0);

		// Fields
	private:
		u32 _source;
		SoundBuffer* _sound;
		vec3 _origin, _direction, _velocity;

		// Constructor
	public:
		AYUAPI SoundSource(SoundBuffer *sound);

		// Destructor
	public:
		AYUAPI virtual ~SoundSource();

		// Base methods
	public:
		AYUAPI void play();
		AYUAPI void pause();
		AYUAPI void stop();
		AYUAPI void rewind();
		AYUAPI bool stopped();
		AYUAPI bool paused();
		AYUAPI f32 length();

		// Source methods
	public:
		AYUAPI f32 pitch();
		AYUAPI f32 gain();
		AYUAPI f32 gainMin();
		AYUAPI f32 gainMax();
		AYUAPI f32 distance();
		AYUAPI f32 coneOuterGain();
		AYUAPI f32 coneInnerAngle();
		AYUAPI f32 coneOuterAngle();
		AYUAPI const vec3& origin();
		AYUAPI const vec3& velocity();
		AYUAPI const vec3& direction();
		AYUAPI bool looping();
		AYUAPI bool relative();
		AYUAPI void pitch(f32 pitch);
		AYUAPI void gain(f32 g);
		AYUAPI void gainMin(f32 gmin);
		AYUAPI void gainMax(f32 gmax);
		AYUAPI void distance(f32 maxDistance);
		AYUAPI void coneOuterGain(f32 gain);
		AYUAPI void coneInnerAngle(f32 angle);
		AYUAPI void coneOuterAngle(f32 angle = 360.0f);
		AYUAPI void origin(const vec3 &pt);
		AYUAPI void velocity(const vec3 &vel);
		AYUAPI void direction(const vec3 &dir);
		AYUAPI void looping(bool loop = false);
		AYUAPI void relative(bool relative = false);
	};

	//------------------------------------//
	// Sound class

	class Sound
	{
		ValueType(Sound, 0);

		// Fields
	private:
		SoundSourceArray			_sounds;
		f32										_soundGain;

		// Music Fields
	private:
		u32										_music;
		Array<u32>						_musicBuffers;
		File*									_musicFile;
		vorbis_info*					_musicInfo;
		OggVorbis_File*				_musicOggFile;
		bool									_musicLoop;

		// Constructor
	public:
		Sound();

		// Destructor
	public:
		~Sound();

		// Methods
	public:
		AYUAPI void update();
		AYUAPI void release();
		AYUAPI void play(const string &name, bool loop = false, bool stopPrev = false);
		AYUAPI f32  gain();
		AYUAPI void gain(f32 g);

		// Music Methods
	public:
		AYUAPI void musicPlay(const string &name, bool loop);
		AYUAPI void musicPause();
		AYUAPI void musicStop();
		AYUAPI bool musicPaused();
		AYUAPI bool musicStopped();
		AYUAPI f32  musicGain();
		AYUAPI void musicGain(f32 g);
	private:
		void musicUpdate();
		bool musicUpdate(u32 buffer);
	};
};
