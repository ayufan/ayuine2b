//------------------------------------//
//
// Core.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-1
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	void *alloc(size_t size) {
		return ::malloc(size);
	}

	void free(void *data) {
		::free(data);
	}
};
