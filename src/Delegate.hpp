//------------------------------------//
//
// Delegate.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-1
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class Object;

	//------------------------------------//
	// Delegate class

	template<typename _Type>
	class Delegate
	{
		ValueType(Delegate<_Type>, 0);

		// Typedefs
	public:
		typedef _Type Type;
		typedef void (Object::*Method)(_Type data);
		typedef void (*Function)(_Type data);

		// Fields
	private:
		Object*			_target;
		Method			_method;

		// Constructor
	public:
		Delegate() {
			_target = nullptr;
			_function = nullptr;
		}
		Delegate(Object* target, Method method) {
			_target = target;
			_method = method;
		}
		Delegate(Function function) {
			_target = nullptr;
			_method = (Method)function;
		}
		
		// Operators
	public:
		void operator () (Type data) {
			// Sprawd� funkcj�
			if(!_method)
				return;

			// Wykonaj delegata
			(_target->*_method)(data);
		}
	};

	//------------------------------------//
	// Event class

	template<typename Type>
	class Event : private Array<ayuine::Delegate<Type> >
	{
		ValueType(Event, 0);

		// Typedef
	public:
		typedef ayuine::Delegate<Type> Delegate;

		// Constructor
	public:
		Event() {
		}
		Event(const Event& event)
			: Array(event) {
		}

		// Properties
	public:
		bool empty() const {
			return Array<Delegate>::empty();
		}

		// Operators
	public:
		void operator () (Type data) {
			// Wykonaj wszystkie delegaty
			for(u32 i = size(); i-- > 0; ) {
				at(i)(data);
			}
		}
		Event<Delegate> &operator += (Delegate &delegate) {
			// Dodaj delegata
			push(delegate);
			return *this;
		}
		Event<Delegate> &operator -= (Delegate &delegate) {
			// Usu� delegata
			eraseFind(delegate, 1);
			return *this;
		}
	};
};
