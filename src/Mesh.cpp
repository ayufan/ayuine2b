//------------------------------------//
//
// Mesh.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Mesh: Collection

	Collection<Mesh>::ObjectMap	Collection<Mesh>::_objects;

	//------------------------------------//
	// MeshCollider: Enumeration

	static EnumValue ColliderTypeEnum[] = {
		{"none",		mctNone},
		{"sphere",	mctSphere},
		{"box",			mctBox},
		{"convex",	mctConvex},
		{"static",	mctStatic},
		{"", 0}
	};

	//------------------------------------//
	// Mesh: Static Fields

	Property Mesh::Properties[] = {
		// Physics Fields
		Property(OffsetOf(Mesh, ColliderType),	ColliderTypeEnum,		"Collider"),
		Property(OffsetOf(Mesh, ColliderObject),ptBoolean,					"ColliderObject"),
		Property(OffsetOf(Mesh, Density),				ptValue,						"Density"),

		// Render Fields
		Property(OffsetOf(Mesh, PRT),						ptBoolean,					"PRT"),
		Property()
	};

	//------------------------------------//
	// MeshVertex: Static Fields

	VertexDeclElement MeshVertex::Decl[] = {
		{0, OffsetOf(MeshVertex, Origin),		D3DDECLTYPE_FLOAT3,		D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, OffsetOf(MeshVertex, Coords[0]),D3DDECLTYPE_FLOAT4,		D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, OffsetOf(MeshVertex, Normal),		D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
		{0, OffsetOf(MeshVertex, Tangent),	D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0},
		D3DDECL_END()
	};

	//------------------------------------//
	// MeshSurface: Constructor

	MeshSurface::MeshSurface() {
		IndexStart = IndexCount = 0;
	}

	//------------------------------------//
	// MeshObject: Constructor

	MeshObject::MeshObject() {
		VertexStart = VertexCount = 0;
		IndexStart = IndexCount = 0;
		Frame = 0;
	}

	MeshObject::MeshObject(const MeshObject &oldObjectConst) {
		MeshObject &oldObject = const_cast<MeshObject&>(oldObjectConst);

		// Skopiuj i usu� stary obiekt
		memcpy(this, &oldObject, sizeof(MeshObject));
		memset(&oldObject, 0, sizeof(MeshObject));
	}

	//------------------------------------//
	// Mesh: Constructor

	Mesh::Mesh() {
		// Physics Fields
		ColliderType = mctNone;
		ColliderObject = false;
		Density = 1.0f;
		Mass = 0;

		// Render Fields
		PRT = false;
	}

	//------------------------------------//
	// Mesh: Destructor

	Mesh::~Mesh() {

	}

	//------------------------------------//
	// Mesh: Methods

	void Mesh::load(const string &name) {
		Engine::Log->print("Mesh: Loading %s...", name.c_str());

		// Zresetuj ustawienia
		ColliderType = mctNone;
		Mass = 10.0f;

		try {
			// Wczytaj mesha
			Reader reader(Engine::VFS->file(va("%s.cfg", name.c_str()), ftMesh).c_str());

			// Wczytaj podstawowe parametry
			reader.readProperties(Properties, this);
		}
		catch(Exception &e) {
		}

		// Spr�buj wczyta� .mesh
		try {
			loadMesh(name);
			goto done;
		}
		catch(Exception &e) {
		}

		// Spr�buj wczyta� .mesh (unigine)
		try {
			loadMeshU(name);
			goto done;
		}
		catch(Exception &e) {
		}

		// Spr�buj wczyta� .obj
		try {
			loadObj(name);
			goto done;
		}
		catch(Exception &e) {
		}

		// Wypisz b��d
		Engine::Log->warning("Mesh: Couldn't load %s !", name.c_str());

		// Wyczy�� bufory
		Vertices.clear();
		Indices.clear();
		Objects.clear();

done:
		// Zainiciuj dane
		deviceReset(true);
		physicsReset();
	}

	void Mesh::loadMesh(const string &name) {
		// Otw�rz plik z meshem
		SmartPtr<File> s(Engine::VFS->loadFile(va("%s.mesh", name.c_str()), ftMesh));

		// Wczytaj mesha
		*s << *this;
		_loaded = true;
	}

	void Mesh::save(const string &name) {
		Engine::Log->print("Mesh: Saving %s...", name.c_str());

		// Odtw�rz plik do zapisu
		SmartPtr<File> s(Engine::VFS->saveFile(va("%s.mesh", name.c_str()), ftMesh));

		// Zapisz mesha
		*s << *this;
	}

	void Mesh::deviceReset(bool theBegin) {
		// Oznacz wszystkie materia�y
		for(u32 i = 0; i < Objects.size(); i++) {
			MeshObject* object = &Objects.at(i);
			
			for(u32 j = 0; j < object->Surfaces.size(); j++) {
				object->Surfaces.at(j).Material.cache();
			}
			if(Engine::Config->PRT && PRT) {
				object->PRT.cache();
			}
		}

		// Utw�rz nowe bufory
		if(theBegin) {
			// Utw�rz opis formatu wierzcho�k�w
			VertexType.reset(new ayuine::VertexType(MeshVertex::Decl));

			// Utw�rz nowy bufor wierzcho�k�w
			if(Vertices.size()) {
				VertexBuffer.reset(new ayuine::VertexBuffer(Vertices.size(), sizeof(MeshVertex)));
				VertexBuffer->update(Vertices, Vertices.size());
			}

			// Utw�rz nowy bufor indeks�w
			if(Indices.size()) {
				IndexBuffer.reset(new ayuine::IndexBuffer(Indices.size(), if16, false));
				IndexBuffer->update(Indices, Indices.size());
			}
		}
	}

	void Mesh::deviceLost(bool theEnd) {
		// Zniszcz bufory
		if(theEnd) {
			// Zniszcz opis formatu wierzcho�k�w
			VertexType.reset();

			// Zniszcz stary bufor wierzcho�k�w
			VertexBuffer.reset();

			// Zniszcz stary bufor indeks�w
			IndexBuffer.reset();
		}
	}

	void Mesh::physicsReset() {
		// Utw�rz odpowiedniego collidera
		if(ColliderType == mctNone) {
			// Ustaw collidera
			Collider.reset(new NullCollider());

			if(ColliderObject) {
				// Zresetuj kolidery dla obiekt�w
				for(u32 i = 0; i < Objects.size(); i++) {
					MeshObject* object = &Objects.at(i);
					
					// Zresetuj collidera
					object->Collider.reset(new NullCollider());

					// Oblicz mas�
					object->Mass = 0;
				}
			}

			// Oblicz mas�
			Mass = 0;
		}
		else if(ColliderType == mctStatic) {
			StaticCollider* collider = new StaticCollider();

			// Spr�buj wczyta� collidera
			try {
				// Otw�z plik z colliderem
				SmartPtr<File> s(Engine::VFS->loadFile(va("%s.cld", name()), ftMesh));

				// Wczytaj collidera
				*s << *collider;
				goto done;
			}
			catch(Exception &e) {
				Engine::Log->warning("Mesh: Collider not found ! Building...");
			}

			// Rozpocznij budowanie collidera
			collider->begin();
			{
				Array<vec3> verts;

				// Dodaj ka�d� obiekt
				for(u32 i = 0; i < Objects.size(); i++) {
					MeshObject* object = &Objects.at(i);
				
					// Sprawd� wszystkie powierzchnie
					for(u32 j = 0; j < object->Surfaces.size(); j++) {
						MeshSurface* surface = &object->Surfaces.at(j);

						// Sprawd� materia�
						if(!surface->Material || !surface->Material->collidable())
							continue;

						// Zmie� rozmiar listy wierzcho�k�w
						verts.resize(surface->IndexCount);

						// Dodaj wierzcho�ki materia�u
						for(u32 k = 0; k < surface->IndexCount; k++) {
							verts.at(k) = Vertices.at(object->VertexStart + Indices.at(surface->IndexStart + surface->IndexCount - 1 - k)).Origin;
						}

						// Dodaj �ciank� do collidera
						if(verts.size()) {
							// Dodaj ka�dy tr�jk�t oddzielnie
							for(u32 k = 2; k < verts.size(); k += 3)
								collider->addFace(surface->Material->hash(), &verts.at(k - 2), 3);
							//collider->addFace(surface->Material->hash(), &verts[0], verts.size());
						}
					}
				}
			}
			// Zako�cz budowanie collidera
			collider->end(false);

			// Zapisz collidera
			try {
				// Otw�rz plik z colliderem
				SmartPtr<File> s(Engine::VFS->saveFile(va("%s.cld", name()), ftMesh));

				// Zapisz collidera
				*s << *collider;
			}
			catch(Exception &e) {
				Engine::Log->warning("Mesh: Can't save collider %s !", name());
			}
done:
			// Ustaw collidera
			Collider.reset(collider);

			// Oblicz mas�
			Mass = FLT_MAX;
		}
		else {
			Array<vec3> verts;

			// Zbuduj sfer�
			if(ColliderObject) {
				Array<ayuine::Collider*> colliders;

				// Wyzeruj mas�
				Mass = 0;

				// Zbuduj collidery dla ka�dego obiektu oddzielnie
				for(u32 i = 0; i < Objects.size(); i++) {
					MeshObject* object = &Objects.at(i);

					// Wyczy�� list� wierzcho�k�w
					verts.reset();

					// Sprawd� wszystkie powierzchnie
					for(u32 j = 0; j < object->Surfaces.size(); j++) {
						MeshSurface* surface = &object->Surfaces.at(j);

						// Sprawd� materia�
						if(!surface->Material || !surface->Material->collidable())
							continue;

						// Dodaj wierzcho�ki materia�u
						for(u32 k = 0; k < surface->IndexCount; k++) {
							verts.push(Vertices.at(object->VertexStart + Indices.at(surface->IndexStart + k)).Origin);
						}
					}

					// Utw�rz collidera
					if(verts.empty()) {
						object->Collider.reset(new NullCollider());

						// Ustaw mas�
						object->Mass = 0;
					}
					else {
						if(ColliderType == mctSphere) {
							bsphere sphere;

							// Oblicz sfer�
							sphere.assign(&verts[0], verts.size());

							// Utw�rz collidera
							object->Collider.reset(new SphereCollider(sphere));
						}
						else if(ColliderType == mctBox) {
							bbox box;

							// Oblicz sfer�
							box.assign(&verts[0], verts.size());

							// Utw�rz collidera
							object->Collider.reset(new BoxCollider(box));
						}
						else if(ColliderType == mctConvex) {
							// Utw�rz collidera
							object->Collider.reset(new ConvexCollider(&verts[0], verts.size()));
						}
						else {
							Assert("Invalid ColliderType" && false);
						}

						// Oblicz mas�
						object->Mass = Density * object->Collider->Volume;

						// Dodaj mas�
						Mass += object->Mass;

						// Dodaj collidera do listy
						colliders.push(object->Collider);
					}
				}

				// Utw�rz collidera z�o�onego z collider�w pojedy�czych obiekt�w
				if(colliders.empty()) {
					Collider.reset(new NullCollider());
				}
				else {
					Collider.reset(new CompoundCollider(colliders));
				}
			}
			else {
				// Zbuduj collidery dla ca�ego mesha
				for(u32 i = 0; i < Objects.size(); i++) {
					MeshObject* object = &Objects.at(i);

					// Wyczy�� list� wierzcho�k�w
					verts.reset();

					// Sprawd� wszystkie powierzchnie
					for(u32 j = 0; j < object->Surfaces.size(); j++) {
						MeshSurface* surface = &object->Surfaces.at(j);

						// Sprawd� materia�
						if(!surface->Material || !surface->Material->collidable())
							continue;

						// Dodaj wierzcho�ki materia�u
						for(u32 k = 0; k < surface->IndexCount; k++) {
							verts.push(Vertices.at(object->VertexStart + Indices.at(surface->IndexStart + k)).Origin);
						}
					}
				}

				// Utw�rz collidera
				if(verts.empty()) {
					Collider.reset(new NullCollider());

					// Ustaw mas�
					Mass = 0;
				}
				else {
					if(ColliderType == mctSphere) {
						bsphere sphere;

						// Oblicz sfer�
						sphere.assign(&verts[0], verts.size());

						// Utw�rz collidera
						Collider.reset(new SphereCollider(sphere));
					}
					else if(ColliderType == mctBox) {
						bbox box;

						// Oblicz sfer�
						box.assign(&verts[0], verts.size());

						// Utw�rz collidera
						Collider.reset(new BoxCollider(box));
					}
					else if(ColliderType == mctConvex) {
						// Utw�rz collidera
						Collider.reset(new ConvexCollider(&verts[0], verts.size()));
					}
					else {
						Assert("Invalid ColliderType" && false);
					}

					// Oblicz mas�
					Mass = Density * Collider->Volume;
				}
			}
		}
	}

	void Mesh::physicsLost() {
		// Zniszcz g��wnego collidera
		Collider.reset();

		// Zniszcz collidera dla ka�dego obiektu
		for(u32 i = 0; i < Objects.size(); i++) {
			MeshObject* object = &Objects.at(i);

			// Zresetuj collidera
			object->Collider.reset();
		}
	}
};
