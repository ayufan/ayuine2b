//------------------------------------//
//
// Material.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// ShaderPair type

	typedef pair<VertexShader*, PixelShader*> ShaderPair;

	//------------------------------------//
	// Material class

	class Material : public DeviceObject, public Collection<Material>
	{
		ObjectType(Material, DeviceObject, 0);

		// Static Fields
	public:
		AYUAPI static Property Properties[];

		// Fields
	public:
		vec4								Param;

		// Physics Fields
	private:
		bool								_collidable;
		f32									_staticFriction, _dynamicFriction;
		f32									_elasticity, _softness;

		// Render Fields
	private:
		Resource<Texture>		_textures[4];
		bool								_renderable;
		bool								_reflective;
		bool								_alphaTest;
		bool								_cull;
		BlendMode						_blend;

		// Shadow Fields
	private:
		bool								_castShadows;
		bool								_receiveShadows;

		// Shader Fields
	private:
		string							_ambientShader;
		string							_lightShader;
		string							_refractionShader;
		string							_customShader;

		// Ambient Shader Fields
	private:
		SmartPtr<VertexShader>			_ambientVertexShader, _ambientVertexShaderSkinned;
		SmartPtr<PixelShader>				_ambientPixelShader, 
																_ambientPixelShaderSun, _ambientPixelShaderSunPRT,
																_ambientPixelShaderSunShadows, _ambientPixelShaderSunShadowsPRT;

		// Light Shader Fields
	private:
		SmartPtr<VertexShader>			_lightVertexShader, _lightVertexShaderSkinned;
		SmartPtr<PixelShader>				_lightPixelShader, _lightPixelShaderPRT,
																_lightPixelShaderShadows, _lightPixelShaderShadowsPRT;

		// Refractive Shader Fields
	private:
		SmartPtr<VertexShader>			_refractionVertexShader, _refractionVertexShaderSkinned;
		SmartPtr<PixelShader>				_refractionPixelShader;

		// Custom Shader Fields
	private:
		SmartPtr<VertexShader>			_customVertexShader;
		SmartPtr<PixelShader>				_customPixelShader;

		// Render Temp Fields
	public:
		class RenderFragment*				Chain[4];
		class RenderFragment*				ChainPRT[4];
		class Material*							Next[4];

		// Constructor
	public:
		AYUAPI Material();

		// Properties
	private:
		ShaderPair shader(class RenderState &state);

		// Physics Properties
	public:
		bool collidable() const {
			return _collidable;
		}
		f32 staticFriction() const {
			return _staticFriction;
		}
		f32 dynamicFriction() const {
			return _dynamicFriction;
		}
		f32 elasticity() const {
			return _elasticity;
		}
		f32 softness() const {
			return _softness;
		}

		// Render Properties
	public:
		bool renderable() const {
			return _renderable;
		}
		bool refractive() const {
			return _refractionShader.size() != 0;
		}
		bool reflective() const {
			return _reflective;
		}
		bool opaque() const {
			return _blend != bmNone;
		}
		bool castShadows() const {
			return _castShadows;
		}
		bool receiveShadows() const {
			return _receiveShadows;
		}

		// Methods
	public:
		AYUAPI void load(const string &name);
		AYUAPI void compile();
		AYUAPI bool bind(class RenderState &state);
		AYUAPI void unbind(class RenderState &state);
		AYUAPI void unload();

		// DeviceObject Methods
	public:
		AYUAPI virtual void deviceReset(bool theBegin);
		AYUAPI virtual void deviceLost(bool theEnd);
	};
};
