//------------------------------------//
//
// Font.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-13
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class Font : public DeviceObject, public Collection<Font>
	{
		ObjectType(Font, DeviceObject, 0);

		// Fields
	private:
		D3DXFONT_DESC			_desc;
		TEXTMETRIC				_metrics;
		s32								_widths[256];
		LPD3DXFONT				_font;

		// Constructor
	public:
		AYUAPI Font();

		// Destructor
	public:
		AYUAPI ~Font();

		// DeviceObject Methods
	public:
		virtual void deviceReset(bool theBegin);
		virtual void deviceLost(bool theEnd);

		// Properties
	public:
		f32 height() const {
			return (f32)_metrics.tmHeight;
		}

		// Methods
	public:
		AYUAPI void load(const string &name);
		AYUAPI void unload();
		AYUAPI vec2 measure(const string &text);
		AYUAPI rect measure(const vec2 &pos, const string &text, u32 format = DT_LEFT | DT_TOP);
		AYUAPI void draw(const vec2 &pos, const string &text, u32 format = DT_LEFT | DT_TOP, const vec3 &color = vec3(1, 1, 1));
	};
};
