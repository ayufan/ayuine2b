//------------------------------------//
//
// RenderState.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-14
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// RenderState: Constructor

	RenderState::RenderState() {
		memset(this, 0, sizeof(RenderState));
		Clip = rect(vec2(-1, -1), vec2(1, 1));
	}

	//------------------------------------//
	// RenderState: Methods

	void RenderState::camera(Camera *camera) {
		CameraOrigin = camera->origin();
		CameraAt = camera->at();
		CameraUp = camera->up();
		CameraRight = camera->right();
		CameraProjection = camera->projection();
		CameraView = camera->view();
		CameraDistance = camera->distance();
		CameraFrustum = camera->frustum();
	}

	void RenderState::bindFrame() {
		// Ustaw kierunek tr�jk�t�w
		Engine::Device->winding(Level & 1);

		// TODO: Ustaw widoczny obszar
		Engine::Device->scissor(&Clip);

		// Ustaw sta�e
		Engine::Device->shaderConst(vscFrame, pscFrame, vec4(1.0f / Width, 1.0f / Height, Time, 1.0f));
	}

	void RenderState::unbindFrame() {
		// Wy��cz widoczny obszar
		Engine::Device->scissor(nullptr);

		// Ustaw kierunek tr�jk�t�w
		Engine::Device->winding(false);
	}

	void RenderState::bindCamera() {
		// Ustaw sta�e kamery dla shader�w
		Engine::Device->shaderConst(vscCameraOrigin, pscNone, vec4(CameraOrigin, 1.0f / CameraDistance));
		Engine::Device->shaderConst(vscCameraAt, pscNone, vec4(CameraAt, 1.0f));
		Engine::Device->shaderConst(vscCameraUp, pscNone, vec4(CameraUp, 1.0f));
		Engine::Device->shaderConst(vscCameraRight, pscNone, vec4(CameraRight, 1.0f));

		// Ustaw transformacje
		Engine::Device->transform(CameraProjection, CameraView);
	}

	void RenderState::unbindCamera() {

	}

	void RenderState::bindSkinning() {
		// Ustaw skinning
		if(Skinning) {
			Engine::Device->vertexConst(vscSkinning, &SkinningBones[0].m11, CountOf(SkinningBones) * 4);
		}
	}

	void RenderState::unbindSkinning() {

	}

	void RenderState::bindAmbient() {
		Type = rtAmbient;

		// Ustaw kolor t�a
		Engine::Device->colorConst(Ambient);

		// Sprawd� czy mamy zdefiniowane s�o�ce
		if(Sun) {
			// Ustaw sta�e s�o�ca dla shader�w
			Engine::Device->shaderConst(vscLightColor, pscLightColor, SunColor);
			Engine::Device->shaderConst(vscLightParam, pscLightParam, SunParam);
			Engine::Device->shaderConst(vscLightTransform, pscNone, &mat4Identity.m11);
			Engine::Device->shaderConst(vscLightOrigin, pscNone, vec4(-SunDir * 4096.0f, 0.0f));
			Engine::Device->shaderConst(vscLightDir, pscNone, vec4(SunDir, 1.0f));
			if(SunPRT) {
				Engine::Device->shaderConst(vscNone, pscPCA, &SunPCA[0].X, CountOf(SunPCA));
			}
		}
	}

	void RenderState::unbindAmbient() {

	}

	void RenderState::bindLight() {
		// Ustaw typ materia��w
		Type = rtLight;

		// Ustaw blendowanie �wiat�a
		Engine::Device->blendMode(bmAdditive);

		// Ustaw sta�e �wiat�a dla shader�w
		Engine::Device->shaderConst(vscLightColor, pscLightColor, LightColor);
		Engine::Device->shaderConst(vscLightParam, pscLightParam, LightParam);
		Engine::Device->shaderConst(vscLightTransform, pscNone, &LightTransform.m11, 4);
		Engine::Device->shaderConst(vscLightOrigin, pscNone, vec4(LightOrigin, 1.0f / LightRange));
		Engine::Device->shaderConst(vscLightDir, pscNone, vec4(LightDir, 1.0f));
		if(LightPRT) {
			Engine::Device->shaderConst(vscNone, pscPCA, &LightPCA[0].X, CountOf(LightPCA));
		}

		// Ustaw przycinanie �wiat�a
		Engine::Device->scissor(&LightClip);

		// Ustaw projekcj� tekstury
		LightTexture->bind(stProjection);

		// Ustaw map� cienii
		LightShadow->bind(stShadow);
	}

	void RenderState::unbindLight() {
		// Ustaw map� cienii
		LightShadow->unbind(stShadow);

		// Ustaw projekcj� tekstury
		LightTexture->unbind(stProjection);

		// Ustaw przycinanie ramki
		Engine::Device->scissor(&Clip);

		// Wy��cz blendowanie �wiat�a
		Engine::Device->blendMode(bmNone);
	}

	void RenderState::bindReflection() {
		// Podepnij odbicie
		Reflection->bind(stReflective);
	}

	void RenderState::unbindReflection() {
		// Odepnij odbicie
		Reflection->unbind(stReflective);
	}

	void RenderState::bindRefraction() {
		Type = rtRefraction;

		// Podepnij refrakcj�
		Refraction->bind(stRefractive);
	}

	void RenderState::unbindRefraction() {
		// Odepnij refrakcj�
		Refraction->unbind(stRefractive);
		Refraction = nullptr;
	}

	void RenderState::bindObject() {
		// Ustaw macierz obiektu
		Engine::Device->object(Object);

		// Ustaw map� PRT
		if((Type == rtAmbient && SunPRT || Type == rtLight && LightPRT) && !NoPRT) {
			ObjectPRT->bind(stPRT);
		}
	}

	void RenderState::unbindObject() {
		// Wy��cz PRT
		if((Type == rtAmbient && SunPRT || Type == rtLight && LightPRT) && !NoPRT) {
			ObjectPRT->unbind(stPRT);
		}
	}
};
