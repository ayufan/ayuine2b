//------------------------------------//
//
// ASkyDome.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-16
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class ASkyDome : public Actor
	{
		ObjectType(ASkyDome, Actor, 0);

		// Fields
	private:
		SmartPtr<VertexType>				_vertexType;
		SmartPtr<VertexBuffer>			_vertexBuffer;
		SmartPtr<IndexBuffer>				_indexBuffer;
		SmartPtr<TextureTarget>			_shadow;
		u32													_verts, _indices;

		// Material Fields
	public:
		Resource<ayuine::Material>	Material;
		bool												Sun;
		vec3												SunDir;
		vec4												SunColor;
		f32													SunArea;

		// Constructor
	public:
		AYUAPI ASkyDome();

		// Methods
	public:
		AYUAPI virtual void preupdate(f32 dt);
		AYUAPI virtual void postupdate(f32 dt);
		AYUAPI virtual void prepare(RenderFrame &frame);
	};
};
