//------------------------------------//
//
// MeshObj.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"
#include <sstream>

namespace ayuine
{
	class ObjImporter
	{
		ValueType(ObjImporter, 0);

		// Classes
	public:
		class Index
		{
			// Fields
		public:
			s32 Vertex, Coords, Normal;
			u32 Method;

			// Constructor
		public:
			Index() {
				Vertex = 0;
				Coords = 0;
				Normal = 0;
				Method = 0;
			}
		};

		class Face
		{
			// Fields
		public:
			Array<Index>			Indices;
			Array<u32>				CompiledIndices;
			u32								Group;

			// Constructor
		public:
			Face() {
				Group = 0;
			}
		};

		// Typedefs
	public:
		typedef Array<Face>							Surface;
		typedef map<string, Surface>		Object;
		typedef map<string, Object>			Context;

		// Fields
	public:
		Array<vec3>							Vertices;
		Array<vec3>							Normals;
		Array<vec3>							Coords;
		Context									Objects;
	
		// Constructor
	public:
		ObjImporter(const string &name) {
			string line, command;
			u32 sg = 0xFFFFFFFF;

			Engine::Log->print("Obj: Importing %s...", name.c_str());

			// Wyczy�� bufory tymczasowe
			Vertices.clear();
			Normals.clear();
			Coords.clear();
			Objects.clear();

			// Pobierz domy�lne dane
			Object *object = &Objects[""];
			Surface *surface = &object[0][""];

			// Wczytaj plik
			stringstream stream(Engine::VFS->file(va("%s.obj", name.c_str()), ftMesh));

			// Czytaj ze strumienia dopuki nie osi�gni�to ko�ca pliku
			while(true) {
				// Spr�buj wczyta� linij�
				std::getline(stream, line);

				// Sprawd� czy koniec wczytywania
				if(!stream)
					break;

				// Pusta linija?
				if(line.empty())
					continue;

				// Komentarz?
				if(line[0] == '#')
					continue;

				// Zamie� ci�g znak�w na strumie�
				stringstream stream2(line);

				// Pobierz nazw�
				stream2 >> command;

				// Sprawd� czy pobrano komend�
				if(!stream2)
					continue;

				// Sprawd� typ komendy
				if(command == "v") {
					vec3 vertex;

					// Pobierz wierzcho�ek
					stream2 >> vertex.X >> vertex.Y >> vertex.Z;

					// Dodaj wierzcho�ek
					Vertices.push(vertex);
				}
				else if(command == "vt") {
					vec3 coord;

					// Pobierz koordynanty tekstury
					stream2 >> coord.X >> coord.Y >> coord.Z;

					// Odwr�c koordynanty
					coord.Y *= -1.0f;

					// Dodaj koordynanty tekstury
					Coords.push(coord);
				}
				else if(command == "vn") {
					vec3 normal;

					// Pobierz wektor normalny
					stream2 >> normal.X >> normal.Y >> normal.Z;

					// Dodaj wektor normalny
					Normals.push(normal);
				}
				else if(command == "g") {
					string groupName;

					// Pobierz nazw� grupy
					stream2 >> groupName;

					// Ustaw aktualn� grup�
					object = &Objects[groupName];
					surface = &object[0][""];
				}
				else if(command == "usemtl") {
					string surfaceName;

					// Pobierz nazw� powierzchnii
					stream2 >> surfaceName;

					// Ustaw aktualn� powierzchni�
					surface = &object[0][surfaceName];
				}
				else if(command == "s") {
					// Pobierz "smoothing object"
					stream2 >> sg;
				}
				else if(command == "f") {
					Face face;
					Index index;
					string indexName;
					bool result = true;

					// Zapisz sg
					face.Group = sg;

					// Pobierz wszystkie indeksy
					while(true) {
						stream2 >> indexName;

						// Sprawd� czy pobrano
						if(!stream2)
							break;

						// Przeanalizuj dane
						if(sscanf(indexName.c_str(), "%i/%i/%i", &index.Vertex, &index.Coords, &index.Normal) != 3) {
							if(sscanf(indexName.c_str(), "%i//%i", &index.Vertex, &index.Normal) != 2) {
								if(sscanf(indexName.c_str(), "%i/%i", &index.Vertex, &index.Coords) != 2) {
									if(sscanf(indexName.c_str(), "%i", &index.Vertex) != 1) {
										result = false;
									}
									else {
										index.Method = 1;
									}
								}
								else {
									index.Method = 2;
								}
							}
							else {
								index.Method = 3;
							}
						}
						else {
							index.Method = 4;
						}

						// Popraw indeks wierzcho�ka
						if(index.Vertex < 0)
							index.Vertex += Vertices.size();
						else
							index.Vertex--;

						// Popraw indeks wektora normalnego
						if(index.Normal < 0)
							index.Normal += Normals.size();
						else
							index.Normal--;

						// Popraw indeks koordynant�w tekstury
						if(index.Coords < 0)
							index.Coords += Coords.size();
						else
							index.Coords--;

						// Sprawd� rezultat analizy
						if(!result)
							break;

						// Dodaj indeks
						face.Indices.push(index);
					}

					// Sprawd� wynik operacji
					if(result && face.Indices.size() >= 3)
						// Dodaj �ciank� do listy
						surface->push(face);
				}
			}
		}
	};

	//------------------------------------//
	// Mesh: Importer

	void Mesh::loadObj(const string &name) {
		// Importuj mesha
		ObjImporter importer(name);

		// Sprawd� typ wczytanych danych
		//if(importer.Normals.empty())
		//	throw(std::runtime_error("Obj: Normal vectors not found !"));
		Assert("No TexCoords" && false);

		// Skompiluj ka�d� grup� oddzielnie
		for(ObjImporter::Context::iterator i = importer.Objects.begin(); i != importer.Objects.end(); i++) {
			Array<MeshVertex> vertices;
			Array<u32> indices;
			MeshObject meshObject;

			// Zapisz nazw� obiektu
			meshObject.Name = i->first;

			// Zapisz indeks pocz�tkowy
			meshObject.IndexStart = Indices.size();

			// Dodaj wierzcho�ki z ka�dej powierzchnii
			for(ObjImporter::Object::iterator	j = i->second.begin(); j != i->second.end(); j++) {
				MeshSurface meshSurface;
				vec3 avgNormal;

				// Ustaw materia� dla powierzchnii
				meshSurface.Material = j->first.c_str();

				// Zapisz pocz�tkowy wierzcho�ek
				u32 vertexStart = vertices.size();

				// Zapisz indeks startowy
				meshSurface.IndexStart = Indices.size() + indices.size();

				Assert("MeshObj: TODO !!" && false);

				//// Dodaj wierzcho�ki z ka�dej �cianki
				//foreach(ObjImporter::Surface, k, j->second) {
				//	// Oblicz wektory
				//	tangentVectors tangent(
				//		importer.Vertices.at(k->Indices.at(0).Vertex), importer.Vertices.at(k->Indices.at(1).Vertex), importer.Vertices.at(k->Indices.at(2).Vertex),
				//		(vec2&)importer.Coords.at(k->Indices.at(0).Coords), (vec2&)importer.Coords.at(k->Indices.at(1).Coords), (vec2&)importer.Coords.at(k->Indices.at(2).Coords));

				//	// Dodaj wektor normalny
				//	avgNormal += tangent.Normal;

				//	// Zapisz pocz�tkowy indeks
				//	u32 startIndex = vertices.size();

				//	// Dodaj wierzcho�ki
				//	foreach(vector<ObjImporter::Index>, l, k->Indices) {
				//		MeshVertex meshVertex;
				//		vec3 normal;

				//		// Wyznacz wektor normalny
				//		if(l->Method == 3 || l->Method == 4)
				//			normal = importer.Normals.at(l->Normal).normalize();
				//		else 
				//			normal = tangent.Normal;

				//		// Oblicz wektory styczne
				//		vec4 t = tangent.calculateTangent(normal);

				//		// Zapisz wierzcho�ek
				//		meshVertex.Origin = importer.Vertices.at(l->Vertex);
				//		if(l->Method == 2 || l->Method == 4)
				//			meshVertex.Coords[0] = (vec2&)importer.Coords.at(l->Coords);
				//		else
				//			meshVertex.Coords[0] = (vec2&)meshVertex.Origin;

				//		// Zapisz wektor normalny i styczny dla wierzcho�ka
				//		meshVertex.Normal = vec4::compress(vec4(normal, 1.0f));
				//		meshVertex.Tangent = vec4::compress(t);

				//		// Dodaj wierzcho�ek do listy
				//		vertices.push_back(meshVertex);
				//	}

				//	// Dodaj indeksy
				//	for(u32 i = 2; i < k->Indices.size(); i++) {
				//		// Oblicz indeksy
				//		u32 in[3] = {startIndex + i, startIndex + i - 1, startIndex};

				//		// Dodaj indeksy
				//		indices.insert(indices.end(), in + 0, in + 3);
				//	}
				//}

				// Zapisz ilo�� indeks�w
				meshSurface.IndexCount = Indices.size() + indices.size() - meshSurface.IndexStart;

				// Dodaj tylko powierzchnie na kt�rej s� wierzcho�ki
				if(!meshSurface.IndexCount)
					continue;

				// Oblicz ilo�� dodanych wierzcho�k�w
				u32 vertexCount = vertices.size() - vertexStart;

				// Zbuduj z tego bbox i bsphere
				if(vertexCount) {
					meshSurface.Box			= bbox		(&vertices[vertexStart].Origin, vertexCount, sizeof(MeshVertex));
					meshSurface.Sphere	= bsphere	(&vertices[vertexStart].Origin, vertexCount, sizeof(MeshVertex));
				}

				// Znormalizuj wektor normalny
				meshSurface.Plane = vec4(avgNormal.normalize(), meshSurface.Sphere.Center);

				// Dodaj powierzchni�
				meshObject.Surfaces.push(meshSurface);
			}

			// Zapisz ilo�� indeks�w
			meshObject.IndexStart = Indices.size();
			meshObject.IndexCount = indices.size();

			// Dodaj tylko obiekt na kt�rym s� powierzchnie i indeksy
			if(meshObject.Surfaces.empty() || !meshObject.IndexCount)
				continue;

			// Zoptymilizuj tablic� wierzcho�k�w
			Array<u32> map = optimize<u32>(vertices);

			// Sprawd� ilo�� wierzcho�k�w
			if(vertices.size() >= (1 << 16)) {
				Engine::Log->warning("Obj: Too many vertices for %s (%s)", meshObject.Name.c_str(), name.c_str());
			}

			// Zarezerwuj miejsce na dodatkowe wierzcho�ki
			Indices.resize(Indices.size() + meshObject.IndexCount);

			// Przekszta�� wierzcho�ki
			for(u32 i = 0; i < meshObject.IndexCount; i++)
				Indices.at(meshObject.IndexStart + i) = map.at(indices.at(i));

			// Pobierz indeks pocz�tkowy
			meshObject.VertexStart = Vertices.size();
			meshObject.VertexCount = vertices.size();

			// Dodaj wierzcho�ki do tablicy g��wnej
			Vertices.push(vertices, vertices.size());

			// Oblicz bbox i bsphere
			if(vertices.size()) {
				meshObject.Box			= bbox		(&vertices[0].Origin, vertices.size(), sizeof(MeshVertex));
				meshObject.Sphere		= bsphere	(&vertices[0].Origin, vertices.size(), sizeof(MeshVertex));
			}

			// Dodaj obiekt do mesha g��wnego
			Objects.push(meshObject);
		}

		// Oznacz jako wczytane
		_loaded = true;

		// Automatycznie zapisz
		save(name);
	}
};
