//------------------------------------//
//
// File.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-9
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class File : public Object
	{
		ObjectType(File, Object, 0);

		// Fields
	protected:
		string	_name;
		u32			_at, _length;
		bool		_loading;
		u8			_buffer[16 * 1024];
		u32			_size;

		// Constructor
	protected:
		AYUAPI File();

		// Destructor
	public:
		VAYUAPI ~File();

		// Name methods
	public:
		AYUAPI const string &name() const;

		// Base methods
	public:
		AYUAPI u32 at() const;
		AYUAPI u32 length() const;
		AYUAPI bool loading() const;
		AYUAPI bool	eof() const;
		VAYUAPI void at(size pos) = 0;

		// Methods
	public:
		VAYUAPI bool flush() = 0;

	public:
		AYUAPI u32 read(void *buffer, u32 count);
		AYUAPI string	read(u32 count = u32Max);

	public:
		AYUAPI u32 write(const void *buffer, u32 count);
		AYUAPI u32 write(const string &data);

		// Serialize operators
	public:
		AYUAPI File &operator << (bool &value);
		AYUAPI File &operator << (c8 &value);
		AYUAPI File &operator << (s8 &value);
		AYUAPI File &operator << (s16 &value);
		AYUAPI File &operator << (s32 &value);
		AYUAPI File &operator << (s64 &value);
		AYUAPI File &operator << (u8 &value);
		AYUAPI File &operator << (u16 &value);
		AYUAPI File &operator << (u32 &value);
		AYUAPI File &operator << (u64 &value);
		AYUAPI File &operator << (f32 &value);
		AYUAPI File &operator << (f64 &value);
		AYUAPI File &operator << (vec2 &value);
		AYUAPI File &operator << (vec3 &value);
		AYUAPI File &operator << (vec4 &value);
		AYUAPI File &operator << (mat4 &value);
		AYUAPI File &operator << (angles &value);
		AYUAPI File &operator << (bbox &value);
		AYUAPI File &operator << (bsphere &value);
		AYUAPI File &operator << (quat &value);
		AYUAPI File &operator << (ray &value);
		AYUAPI File &operator << (rect &value);
		friend File &operator << (File &s, string &o) {
			u32 length;
			
			if(s.loading()) {
				s.read(&length, sizeof(u32));
				o.resize(length);
				s.read(&o[0], length);
			}
			else {
				length = (u32)o.length();
				s.write(&length, sizeof(u32));
				s.write(&o[0], length);
			}
			return s;
		}
	};

	class FileWin : public File
	{
		// Fields
	private:
		HANDLE			_handle;

		// Constructor
	public:
		AYUAPI FileWin(const string &name, bool loading);

		// Destructor
	public:
		AYUAPI virtual ~FileWin();

		// Base methods
	public:
		AYUAPI virtual void at(size pos);

		// Methods
	public:
		AYUAPI virtual bool flush();
	};
};
