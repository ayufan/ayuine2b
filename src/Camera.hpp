//------------------------------------//
//
// Camera.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-1
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class Camera : public Object
	{
		ObjectType(Camera, Object, 0);

		// Static Fields
	public:
		AYUAPI static const mat4 BaseView;

		// Fields
	protected:
		ayuine::frustum _frustum;
		mat4 _projection;
		mat4 _view;
		vec3 _origin;
		vec3 _at;
		vec3 _up;
		vec3 _right;
		f32	_distance;

		// Constructor
	public:
		AYUAPI Camera();

		// Destructor
	public:
		AYUAPI virtual ~Camera();

		// Properties
	public:
		AYUAPI virtual const ayuine::frustum& frustum() const;
		AYUAPI virtual const vec3 &origin() const;
		AYUAPI virtual const vec3 &at() const;
		AYUAPI virtual const vec3 &up() const;
		AYUAPI virtual const vec3 &right() const;
		AYUAPI virtual const mat4 &projection() const;
		AYUAPI virtual const mat4 &view() const;
		AYUAPI virtual f32 distance() const;

		// Methods
	public:
		AYUAPI virtual void update(f32 dtime);
	};

	class FreeCamera : public Camera
	{
		ObjectType(FreeCamera, Camera, 0);

		// Fields
	public:
		vec3							Origin;
		angles						Angles;
	private:
		vec3							_velocity;
		angles						_angular;
		f32								_maxVelocity;
		f32								_maxAngular;
		f32								_velocityAccel;
		f32								_angularAccel;
		f32								_dumping;
		bool							_controlled;

		// Constructor
	public:
		AYUAPI FreeCamera();

		// Destructor
	public:
		AYUAPI virtual ~FreeCamera();

		// Update methods
	public:
		AYUAPI virtual void update(f32 dtime);
	};

	class CubeCamera : public Camera
	{
		ObjectType(CubeCamera, Camera, 0);

		// Fields
	public:
		vec3					Origin;
		CubeMapFace		Face;
		f32						Distance;

		// Constructor
	public:
		AYUAPI CubeCamera();

		// Update methods
	public:
		AYUAPI virtual void update(f32 dtime);
	};
};
