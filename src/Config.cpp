//------------------------------------//
//
// Config.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Config: Constructor

	Config::Config() {
		Width = 800;
		Height = 600;
		Windowed = true;
		SceneIntensity = 1.0f;
		BlurIntensity = 3.0f;//5.0f;
		HighlightIntensity = 0.0f;//0.5f;
		Shadows = 2;
		PRT = 1;
		PostProcess = true;
		Sleep = 0;
		TextureQuality = 0;
		TextureCompression = false;
		TextureMinimalSize = 32;
		UseOcclusion = true;
		UpdateFrameTime = 1.0f / 60.0f;
	}

	//------------------------------------//
	// Config: Destructor

	Config::~Config() {
	}
};
