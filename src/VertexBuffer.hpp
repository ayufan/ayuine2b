//------------------------------------//
//
// VertexBuffer.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-9
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// VertexBuffer class

	class VertexBuffer : public DeviceObject
	{
		ObjectType(VertexBuffer, DeviceObject, 0);

		// Fields
	private:
		LPDIRECT3DVERTEXBUFFER9			_vb;
		size												_count;
		size												_stride;
		bool												_dynamic;

		// Constructor
	public:
		AYUAPI VertexBuffer(size count, size stride = 1, bool dynamic = false);

		// Destructor
	public:
		AYUAPI virtual ~VertexBuffer();

		// Properties:
	public:
		AYUAPI size count() const;
		AYUAPI size stride() const;
		AYUAPI bool dynamic() const;

		// Methods
	public:
		AYUAPI void *lock(size count, size offset = 0, LockFlags flags = lfNone);
		AYUAPI void unlock();
		AYUAPI size update(const void *data, size count, size offset = 0, bool nooverwrite = false);
		AYUAPI void bind(u32 stream, u32 offset, u32 stride = 0);
		AYUAPI void unbind(u32 stream);

		// DeviceObject Methods
	public:
		AYUAPI virtual void deviceLost(bool theEnd);
		AYUAPI virtual void deviceReset(bool theBegin);
	};
};
