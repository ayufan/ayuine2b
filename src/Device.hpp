//------------------------------------//
//
// Device.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// Device class

	class Device
	{
		// Structures
	private:
		template<typename Type>
		class AIU
		{
			// Fields
		public:
			Type		Value;

			// Constructor
		public:
			AIU() {
				Value = nullptr;
			}
			AIU(Type value) {
				if(Value = value) {
					Value->AddRef();
				}
			}
			AIU(const AIU<Type> &value) {
				if(Value = value.Value) {
					Value->AddRef();
				}
			}

			// Destructor
		public:
			~AIU() {
				if(Value) {
					Value->Release();
					Value = nullptr;
				}
			}

			// Operators
		public:
			operator Type () {
				return Value;
			}
			AIU<Type> &operator = (const AIU<Type> &value) {
				if(Value) {
					Value->Release();
				}
				if(Value = value.Value) {
					Value->AddRef();
				}
				return *this;
			}

			// Methods
		public:
			void reset(Type value = nullptr) {
				if(Value) {
					Value->Release();
				}
				if(Value = value) {
					Value->AddRef();
				}
			}
		};

		ValueType(Device, 0);

		// Device Fields
	private:
		List<DeviceObject>		_deviceObjects;
		HWND									_d3dWnd;
		LPDIRECT3D9						_d3d9;
		LPDIRECT3DDEVICE9			_d3dDev;
		D3DCAPS9							_d3dCaps;
		D3DPRESENT_PARAMETERS	_d3dpp;
		D3DVIEWPORT9					_d3dViewport;
		LPDIRECT3DSURFACE9		_renderTarget;
		LPDIRECT3DSURFACE9		_depthTarget;
		LPDIRECT3DQUERY9			_query;
		LPDIRECT3DQUERY9			_occlusion;
		LPDIRECT3DTEXTURE9		_defaultTexture;
		D3DXIMAGE_INFO				_defaultTextureInfo;

		// Cached State Fields
	private:
		mat4															_transform[2];
		mat4															_object[2];
		vec4															_color[2];
		TextureFilter											_filter[16][2];
		TextureAddress										_address[16][2];
		BlendMode													_blendMode[2];
		TextureMode												_textureMode[2];
		bool															_winding[2];
		bool															_depthTest[2], _depthWrite[2], _depthLE[2], _depthBias[2];
		f32																_depthScale[2];
		bool															_cullMode[2];
		bool															_fillMode[2];
		u32																_colorWrite[2];
		bool															_alphaTest[2];
		u32																_alphaValue[2];
		bool															_scissorTest[2];
		RECT															_scissorRect[2];
		AIU<LPDIRECT3DVERTEXDECLARATION9>	_decl[2];
		AIU<LPDIRECT3DVERTEXSHADER9>			_vertexShader[2];
		AIU<LPDIRECT3DPIXELSHADER9>				_pixelShader[2];
		AIU<LPDIRECT3DBASETEXTURE9>				_textures[16][2];
		AIU<LPDIRECT3DINDEXBUFFER9>				_indexBuffer[2];
		AIU<LPDIRECT3DVERTEXBUFFER9>			_vertexBuffer[8][2];
		u32																_vertexOffset[8][2];
		u32																_vertexStride[8][2];
		vec4															_pixelConsts[256];
		vec4															_vertexConsts[256];
		u32																_pixelConstsDirty[2];
		u32																_vertexConstsDirty[2];

		// Device Capabilities
	public:

		// Constructor
	public:
		Device();

		// Destructor
	public:
		~Device();

		// Properties
	public:
		LPDIRECT3DDEVICE9 d3ddev() {
			return _d3dDev;
		}

		// Methods
	public:
		AYUAPI bool check();
		AYUAPI void lost(bool theEnd);
		AYUAPI void reset(bool theBegin);
		AYUAPI bool swap(HWND destWnd);
		AYUAPI f32 width();
		AYUAPI f32 height();
		AYUAPI f32 aspect();

		// State Manager Methods
	public:
		AYUAPI void clear(bool target, bool depth, const vec4 &color = vec4(0, 0, 0, 1), f32 z = 1.0f, const Array<RECT> &rects = Array<RECT>());
		AYUAPI void viewport(const rect &r);
		AYUAPI void scissor(const rect *r);
		AYUAPI void transform(const mat4 &projection, const mat4 &view);
		AYUAPI void transform(const mat4 &transform);
		AYUAPI void object(const mat4 &object);
		AYUAPI void blendMode(BlendMode mode);
		AYUAPI void cullMode(bool cull);
		AYUAPI void depthMode(bool test, bool write, bool le = true);
		AYUAPI void depthBias(bool bias);
		AYUAPI void depthScale(f32 scale);
		AYUAPI void fillMode(bool fill);
		AYUAPI void colorWrite(bool rgb, bool a);
		AYUAPI void alphaTest(bool test, u32 value = 0x7f);
		AYUAPI void winding(bool reverse);
		AYUAPI void colorConst(const vec4 &color);
		AYUAPI void vertexConst(u32 i, const f32 *vectors, u32 count = 1);
		AYUAPI void vertexConst(u32 i, const vec4 &value);
		AYUAPI void pixelConst(u32 i, const f32 *vectors, u32 count = 1);
		AYUAPI void pixelConst(u32 i, const vec4 &value);
		AYUAPI void shaderConst(VertexShaderConst vertex, PixelShaderConst pixel, const f32 *vectors, u32 count = 1);
		AYUAPI void shaderConst(VertexShaderConst vertex, PixelShaderConst pixel, const vec4 &value);
		AYUAPI void resetStates();
		AYUAPI void updateStates();
		AYUAPI void beginOcclusion();
		AYUAPI void endOcclusion();
		AYUAPI bool flushOcclusion(u32 &result);

		// Friends
		friend class DeviceObject;
		friend class VertexType;
		friend class VertexBuffer;
		friend class IndexBuffer;
		friend class VertexShader;
		friend class PixelShader;
		friend class Texture;
		friend class TextureTarget;
		friend class CubeTarget;
	};
};
