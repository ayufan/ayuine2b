//------------------------------------//
//
// IndexBuffer.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// IndexBuffer: Constructor

	IndexBuffer::IndexBuffer(size count, IndexFormat format, bool dynamic) {
		dGuard {
			_ib = nullptr;
			_dynamic = dynamic;
			_count = count;
			_format = format;

			// Zresetuj bufor
			deviceReset(true);
		}
		dUnguard;
	}

	//------------------------------------//
	// IndexBuffer: Destructor
	
	IndexBuffer::~IndexBuffer() {
		dGuard {
			// Zniszcz bufor
			deviceLost(true);
		}
		dUnguard;
	}

	//------------------------------------//
	// IndexBuffer: Properties

	IndexFormat IndexBuffer::format() const {
		return _format;
	}

	size IndexBuffer::count() const {
		return _count;
	}

	bool IndexBuffer::dynamic() const {
		return _dynamic;
	}

	//------------------------------------//
	// IndexBuffer: DeviceObject Methods

	void IndexBuffer::deviceLost(bool theEnd) {
		// Zniszcz bufor wierzcho�k�w
		if(_ib && (theEnd || _dynamic)) {
			_ib->Release();
			_ib = nullptr;
		}
	}

	void IndexBuffer::deviceReset(bool theBegin) {
		// Utw�rz bufor wierzcho�k�w
		if(Engine::Device->d3ddev() && (theBegin || _dynamic)) {
			dxe(Engine::Device->d3ddev()->CreateIndexBuffer((UINT)(_count * _format), _dynamic ? D3DUSAGE_DYNAMIC : 0, _format == if16 ? D3DFMT_INDEX16 : _format == if32 ? D3DFMT_INDEX32 : D3DFMT_UNKNOWN, _dynamic ? D3DPOOL_DEFAULT : D3DPOOL_MANAGED, &_ib, nullptr));
		}
	}

	//------------------------------------//
	// IndexBuffer: Methods

	void* IndexBuffer::lock(size count, size offset, LockFlags flags) {
		void *dest;
		DWORD d3dFlags;

		// Wyznacz flagi blokowania
		switch(flags) {
			case lfReadOnly:
				d3dFlags = D3DLOCK_READONLY;
				break;

			case lfNoOverwrite:
				d3dFlags = _dynamic ? D3DLOCK_NOOVERWRITE : 0;
				break;

			default:
				d3dFlags = _dynamic ? D3DLOCK_DISCARD : 0;
				break;
		}

		// Zablokuj bufor
		dxe(_ib->Lock((UINT)(offset * _format), (UINT)(count * _format), &dest, d3dFlags));
		return dest;
	}

	void IndexBuffer::unlock() {
		// Odblokuj bufor
		dxe(_ib->Unlock());
	}

	size IndexBuffer::update(const u16 *data, size count, size offset, bool nooverwrite) {
		void *dest;

		// Sprawdz dane wejsciowe
		Assert(data);

		// Zablokuj bufor
		Assert(dest = lock(count, offset, nooverwrite ? lfNoOverwrite : lfNone));

		// Skopiuj dane
		if(_format == if16)
			memcpy(dest, data, (size_t)(count * if16));
		else
			for(u32 *destp = (u32*)dest; count > 0; count--)
				*destp++ = *data++;

		// Odblokuj bufor
		unlock();
		return count;
	}

	size IndexBuffer::update(const u32 *data, size count, size offset, bool nooverwrite) {
		void *dest;

		// Sprawdz dane wejsciowe
		Assert(data);

		// Zablokuj bufor
		Assert(dest = lock(count, offset, nooverwrite ? lfNoOverwrite : lfNone));

		// Skopiuj dane
		if(_format == if32)
			memcpy(dest, data, (size_t)(count * if32));
		else
			for(u16 *destp = (u16*)dest; count > 0; count--) {
				dAssert(*data < (1<<16));
				*destp++ = (u16)*data++;
			}

		// Odblokuj bufor
		unlock();
		return count;
	}

	void IndexBuffer::bind() {
		if(!this)
			return;

		// Podepnij strumie� indeks�w
		Engine::Device->_indexBuffer[1] = _ib;
	}

	void IndexBuffer::unbind() {
		if(!this)
			return;

		// Odepnij strumie� indeks�w
		Engine::Device->_indexBuffer[1] = nullptr;
	}

};
