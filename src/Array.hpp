//------------------------------------//
//
// Array.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-1
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// ArrayConsts enumeration

	enum ArrayConsts
	{
		acMinimum = 64,
		acBy = 2
	};

	//------------------------------------//
	// Array class

	template<typename Type>
	class Array
	{
		ValueType(Array<Type>, 0);

		// Typedefs
	public:
		typedef Type	ValueType;
		typedef Type*	Iterator;

		// Fields
	private:
		Type*		_array;
		u32			_size, _capacity;

		// Constructor
	public:
		Array() {
			_array = nullptr;
			_size = _capacity = 0;
		}
		Array(const Array<Type> &copy) {
			if(copy._size) {
				_size = copy._size;
				_capacity = copy._size;
				_array = Allocator<Type>::alloc(_size);
				Allocator<Type>::ctor(_array, copy._array, _size);
			}
			else {
				_array = nullptr;
				_size = _capacity = 0;
			}
		}
		Array(const Type *data, u32 count) {
			if(count) {
				_size = count;
				_capacity = count;
				_array = Allocator<Type>::alloc(_size);
				Allocator<Type>::ctor(_array, data, _size);
			}
			else {
				_array = nullptr;
				_size = _capacity = 0;
			}
		}
		Array(u32 count) {
			if(count) {
				_size = count;
				_capacity = count;
				_array = Allocator<Type>::alloc(_size);
				Allocator<Type>::ctor(_array, _size);
			}
			else {
				_array = nullptr;
				_size = _capacity = 0;
			}
		}

		// Destructor
	public:
		~Array() {
			Allocator<Type>::dtor(_array, _size);
			Allocator<Type>::free(_array);
		}

		// Properties
	public:
		bool empty() const {
			return !_size;
		}
		u32 size() const {
			return _size;
		}
		u32 capacity() const {
			return _capacity;
		}
		Type &at(u32 index) {
			Assert(index < _size);
			return _array[index];
		}
		const Type &at(u32 index) const {
			Assert(index < _size);
			return _array[index];
		}
		Type &front() {
			return at(0);
		}
		const Type &front() const {
			return at(0);
		}
		Type &back() {
			return at(_size - 1);
		}
		const Type &back() const {
			return at(_size - 1);
		}
		Type *begin() {
			return &_array[0];
		}
		Type *end() {
			return &_array[_size];
		}
		const Type *begin() const {
			return &_array[0];
		}
		const Type *end() const {
			return &_array[_size];
		}

		// Operators
	public:
		operator Type *() {
			return _array;
		}
		operator const Type *() const {
			return _array;
		}
		Array<Type>& operator = (const Array<Type> &copy) {
			Allocator<Array<Type> >::dtor(this);
			Allocator<Array<Type> >::ctor(this, &copy);
			return *this;
		}

		// Methods
	public:
		template<typename FindType>
		const u32 find(const FindType &value) const {
			for(u32 i = 0; i < _size; i++)
				if(_array[i] == value)
					return i;
			return (u32)-1;
		}
		void clear() {
			// Wyzeruj tablic�
			Allocator<Type>::dtor(_array, _size);
			Allocator<Type>::free(_array);
			_size = 0;
			_capacity = 0;
		}
		void reset() {
			// Usu� wszystkie elementy
			Allocator<Type>::dtor(_array, _size);
			_size = 0;
		}
		void resize(u32 size, bool shrink_ = false) {
			// Shrink
			if(size <= _size) {
				// Zniszcz nadmiarowe dane
				Allocator<Type>::dtor(&_array[size], _size - size);

				// Zmniejsz rozmiar
				if(shrink_)
					shrink();
			}
			// Enlarge
			else {
				// Zarezerwuj miejsce
				reserve(size);
				
				// Utw�rz dane
				Allocator<Type>::ctor(&_array[_size], size - _size);

				// Ustaw nowy rozmiar
				_size = size;
			}
		}
		void reserve(u32 capacity, bool shrink_ = false) {
			// Mniej ni� jest zarezerwowane ?
			if(capacity <= _capacity && !shrink_) {
				return;
			}

			// Oblicz minimalny rozmiar
			_capacity = __max(capacity, _size);

			// Utw�rz dane
			Type *newArray = Allocator<Type>::alloc(_capacity);

			// Skopiuj dane
			Allocator<Type>::cdtor(newArray, _array, _size);

			// Zniszcz stary bufor
			Allocator<Type>::free(_array);

			// Zapisz nowy bufor
			_array = newArray;

		}
		void shrink() {
			// Zmniejsz dodatkowy obszar do wielko�ci bufora z u�ywanymi elementami
			reserve(_size, true);
		}
		u32 erase(u32 at, u32 count = 1) {
			// Poza obszarem?
			if(at >= _size)
				return 0;

			// Przytnij rozmiar
			count = __min(count, _size - at);

			// Zniszcz wybrane elementy
			Allocator<Type>::dtor(&_array[at], count);

			// Przenie� elementy
			Allocator<Type>::cdtor(&_array[at], &_array[at + count], _size - at - count);

			// Popraw rozmiar tablicy
			_size -= count;
			return count;
		}
		u32 erase(Iterator s, Iterator e) {
			if(s > e) {
				Iterator temp;

				temp = s;
				s = e;
				e = temp;
			}
			return erase(s - begin(), end() - e);
		}
		template<typename FindType>
		u32 eraseFind(const FindType &value, u32 maxCount = u32Max) {
			u32 count = 0;

			for(u32 i = _size; i-- > 0; ) {
				if(_array[i] == value) {
					count++;
					erase(i, 1);
					if(count >= maxCount)
						break;
				}
			}
			return count;
		}
		u32 insert(u32 at, const Type *data, u32 count) {
			u32 left = _capacity - _size;

			// Sprawd� indeks
			at = __min(at, _size);

			// Sprawd� ile mamy miejsca
			if(left >= count) {
				// Przenie� dane na koniec tablicy
				Allocator<Type>::cdtor(&_array[at + count], &_array[at], _size - at);
			}
			else {
				// Oblicz now� wielko�� tablicy
				_capacity = __max(__max(_size * acBy, _size + count + 8), acMinimum);

				// Utw�rz now� tablic�
				Type *newArray = Allocator<Type>::alloc(_capacity);

				// Przepisz dane pocz�tkowe
				Allocator<Type>::cdtor(&newArray[0], &_array[0], at);

				// Przepisz dane ko�cowe
				Allocator<Type>::cdtor(&newArray[at + count], &_array[at], _size - at);

				// Zwolnij star� tablic�
				Allocator<Type>::free(_array);

				// Zapisz now� tablic�
				_array = newArray;
			}

			// Wklej dane do tablicy
			Allocator<Type>::ctor(&_array[at], data, count);

			// Zwi�ksz rozmiar danych
			_size += count;
			return at;
		}
		u32 insert(u32 at, const Type &value) {
			return insert(at, &value, 1);
		}
		u32 push(const Type *data, u32 count) {
			return insert(_size, data, count);
		}
		u32 push(const Type &value) {
			return insert(_size, &value, 1);
		}
		void pop(u32 count = 1) {
			erase(size() - count, count);
		}
		template<typename Pred>
		void sort(Pred pred, u32 index = 0, u32 count = u32Max) {
			// Czy indeks jest w zasi�gu tablicy?
			if(index >= _size)
				return;

			// Przytnij ilo�� element�w
			count = __min(_size - index, count);

			// TODO: Posortuj tablic�
		}
		template<typename Pred>
		void usort(Pred pred, u32 index = 0, u32 count = u32Max) {
			// Czy indeks jest w zasi�gu tablicy?
			if(index >= _size)
				return;

			// Przytnij ilo�� element�w
			count = __min(_size - index, count);

			// Posortuj tablic�
			qsort(&_array[index], count, sizeof(Type), (int(*)(const void*, const void*))pred);
		}

		// Serializer
	public:
		friend class File &operator << (class File &s, Array<Type> &a) {
			u32 size;
				
			if(s.loading()) {
				// Zniszcz star� tablic�
				a.clear();

				// Wczytaj ilo�� element�w
				s << size;

				// Zarezerwuj miejsce
				a.resize(size);

				// Wczytaj ka�dy element
				for(u32 i = 0; i < size; i++)
					s << a._array[i];
			}
			else {
				// Pobierz ilo�� element�w
				size = a.size();

				// Zapisz ilo�� element�w
				s << size;

				// Zapisz ka�dy element
				for(u32 i = 0; i < size; i++)
					s << a._array[i];
			}
			return s;
		}
	};
#define ArrayEach(Type, Itor, Arr) \
	for(Array<Type>::Iterator Itor = (Arr).begin(); Itor < (Arr).end(); Itor++)

#define ArrayEachr(Type, Itor, Arr)	\
	for(Array<Type>::Iterator Itor = (Arr).end(); Itor-- > (Arr).begin(); )
};
