//------------------------------------//
//
// Allocator.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-1
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// Allocator class

	template<typename Type>
	class Allocator
	{
		// Functions
	public:
		static Type* alloc(u32 count) {
			return (Type*)ayuine::alloc(sizeof(Type) * count);
		}
		static void free(Type *&data) {
			if(data) {
				ayuine::free(data);
				data = nullptr;
			}
		}
		static void ctor(Type *value, u32 count = 1) {
			while(count-- > 0) {
				new(value++) Type();
			}
		}
		static void ctor(Type *value, const Type *copy, u32 count = 1) {
			while(count-- > 0) {
				new(value++) Type(*copy++);
			}
		}
		template<typename CtorType>
		static void ctor1(Type *value, CtorType copy, u32 count = 1) {
			while(count-- > 0) {
				new(value++) Type(copy);
			}
		}
		static void dtor(Type *value, u32 count = 1) {
			while(count-- > 0) {
				value++->~Type();
			}
		}
		static void cdtor(Type *value, Type *copy, u32 count = 1) {
			if(value < copy) {
				// Iteruj od pocz�tku
				while(count-- > 0) {
					new(value++) Type(*copy);
					copy++->~Type();
				}
			}
			else {
				// Przesu� na koniec
				value += count;
				copy += count;

				// Iteruj od ko�ca
				while(count-- > 0) {
					new(--value) Type(*--copy);
					copy->~Type();
				}
			}
		}
	};
};
