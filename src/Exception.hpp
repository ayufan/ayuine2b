//------------------------------------//
//
// Exception.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-1
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// ExceptionTrace class

	class ExceptionTrace
	{
		ValueType(ExceptionTrace, 0);

		// Fields
	public:
		const c8*	Function;
		const c8*	File;
		u32				Line;

		// Constructor
	public:
		ExceptionTrace() {
			Line = 0;
		}
		ExceptionTrace(const c8* function, const c8* file, u32 line) {
			Function = function;
			File = file;
			Line = line;
		}
	};

	//------------------------------------//
	// Exception class

	class Exception
	{
		BaseObjectType(Exception, 0);

		// Fields
	public:
		ExceptionTrace					Trace;
		c8											Message[512];
		Array<ExceptionTrace>		CallStack;

		// Constructor
	public:
		AYUAPI Exception(const string &message = "");
		
		// Destructor
	public:
		AYUAPI virtual ~Exception();

		// Functions
	public:
		template<typename ExceptionType>
		static void ThrowException(const ExceptionType &exception, const ExceptionTrace &trace) {
			// Ustaw miejsce rzucenia wyj�tku
			const_cast<ExceptionType&>(exception).Trace = trace;

			// Loguj :)
			if(Engine::Log)
				Engine::Log->debug("%s at %s in %s (%i)", exception.Message, trace.Function, trace.File, trace.Line);

			// Rzu� wyj�tek
			throw exception;
		}
	};

	//------------------------------------//
	// UnhandledException class

	class UnhandledException : public Exception
	{
		ObjectType(UnhandledException, Exception, 0);

		// Constructor
	public:
		AYUAPI UnhandledException(const string &message = "");
	};

	//------------------------------------//
	// AssertException class

	class AssertException : public Exception
	{
		ObjectType(AssertException, Exception, 0);

		// Constructor
	public:
		AYUAPI AssertException(const string &message = "");
	};
};
