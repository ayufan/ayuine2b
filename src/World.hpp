//------------------------------------//
//
// World.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// WorldConsts enumeration

	enum WorldConsts
	{
		wcSize	= 1000,
	};

	//------------------------------------//
	// World class

	class World : public Actor
	{
		ObjectType(World, Actor, 0);

		// Classes
	private:
		struct BspZone;
		struct BspPortal;

		struct BspNode
		{
			// Fields
			ayuine::Axis			Axis;
			f32								Split;
			u32								Childs[2];
			Array<u32>				Zones;

			// Constructor
			BspNode() {
				Axis = aNonAxial;
				Split = 0;
				Childs[0] = Childs[1] = u32Max;
			}

			// Properties
			bool leaf() const {
				return Axis == aNonAxial;
			}
		};

		struct BspPortal
		{
			// Fields
			bbox		Box;
			u32			OtherZone;

			// Constructor
			BspPortal() {
				OtherZone = u32Max;
			}
			BspPortal(const bbox &box, u32 otherZone) {
				OtherZone = otherZone;
			}
		};

		struct BspZone
		{
			// Fields
			bbox								Box;
			Array<BspPortal>		Portals;
			Array<Actor*>				Actors;
			u32									Frame;
			u32									Visible;
			rect								VisibleRect;
			BspZone*						Next;

			// Constructor
			BspZone() {
				Frame = 0;
				Visible = 0;
			}
		};

		// State Fields
	public:
		vec4									Ambient;
		vec3									Gravity;
		f32										Time;

		// Precompiled Fields
	private:
		Array<BspNode>				_nodes;
		Array<BspZone>				_zones;
		u32										_root;
		u32										_frame;

		// World Fields
	private:
		NewtonWorld*					_worldPhysics;
		Mesh*									_worldMesh;
		Array<AMesh*>					_worldObjects;
		f32										_leftTime;
		
		// Constructor
	public:
		AYUAPI World();

		// Destructor
	public:
		AYUAPI virtual ~World();

		// Actor Methods
	public:
		AYUAPI virtual void invalidate(const bbox &box);
		AYUAPI virtual void update(f32 dt);
		AYUAPI virtual void prepare(RenderFrame &frame);

		// World Methods
	public:
		AYUAPI void load(const string &name);
		AYUAPI void unload();

		// Visbility Methods
	public:
		AYUAPI BspZone* ensureVisbility(const vec3 &origin, const frustum &frustum, const rect &clip = rect(vec2(-1, -1), vec2(1, 1)));

	private:
		void buildBspTree();
		u32	 buildBspTree_r(Array<struct BspBox> &boxes);
		u32	 findNode(const vec3 &origin);
		void findZones(Actor *actor);
		void ensureVisbility_r(u32 zone, const rect &clip, const frustum &f, Array<u32> &seen, BspZone* &zones);

		// Friends
		friend class Collider;
		friend class BoxCollider;
		friend class SphereCollider;
		friend class ConvexCollider;
		friend class StaticCollider;
		friend class CompoundCollider;
		friend class NullCollider;
		friend class RigidBody;
		friend class Joint;
		friend class BallJoint;
		friend class HingeJoint;
		friend class SliderJoint;
		friend class UpJoint;
	};
};
