//------------------------------------//
//
// VFS.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// FileType enumeration

	enum FileType
	{
		ftNone,

		ftMesh,
		ftTexture,
		ftMaterial,
		ftShader,
		ftFont,
		ftWorld,
		ftSound,
		ftMusic,

		ftMax
	};

	//------------------------------------//
	// VFS class

	class VFS
	{
		ValueType(VFS, 0);

		// Fields
	private:
		Array<string>					_fileTypes;

		// Constructor
	public:
		VFS();

		// Destructor
	public:
		~VFS();

		// Methods
	public:
		AYUAPI File*	loadFile(const string &name, FileType ft = ftNone);
		AYUAPI File*	saveFile(const string &name, FileType ft = ftNone);
		AYUAPI string	file(const string &name, FileType ft = ftNone);
		AYUAPI void		file(const string &name, const string &context, FileType ft = ftNone);
		AYUAPI u32		find(Array<string> &files, const string &pattern, FileType ft);
		AYUAPI bool		exists(const string &name, FileType ft = ftNone);
		AYUAPI string	fileType(FileType ft = ftNone);
		AYUAPI void		fileType(FileType ft, const string &name);
	};
};
