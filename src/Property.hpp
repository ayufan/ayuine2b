//------------------------------------//
//
// Property.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-1
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//#define DefineType(ObjectType)									public: AYUAPI static u32 id() { static u32 hash = ayuine::hash(#ObjectType); return hash; }
	//#define DefineObject(ObjectType, BaseType)			protected: AYUAPI virtual u32 idOf_() { return id(); } AYUAPI virtual bool isKindOf_(u32 hash) { return hash != id() ? BaseType::isKindOf_(hash) : true; }	DefineType(ObjectType)
	#define	DefineProperties(ObjectType, BaseType)	private: AYUAPI static Property _properties[]; AYUAPI static u32 propertyCount() { static u32 count = Property::count(_properties); return count; } protected: AYUAPI virtual u32 properties_() const { return propertyCount() + BaseType::properties_(); } AYUAPI virtual const Property *property_(u32 index) const { return index < propertyCount() ? &_properties[index] : property_(index - propertyCount()); }

	//------------------------------------//
	// PropertyType enumeration

	enum PropertyType 
	{
		ptNone,

		ptBoolean,
		ptInteger,
		ptValue,
		ptString,
		ptVec3,
		ptVec4,
		ptMat4,
		ptResource
	};

	//------------------------------------//
	// EnumValue class

	class EnumValue
	{
		ValueType(EnumValue, 0);

		// Fields
	public:
		string			Name;
		u32					Value;
	};

	//------------------------------------//
	// PropertyFlags enumeration

	enum PropertyFlags
	{
		pfNone,
		pfReadOnly
	};

	//------------------------------------//
	// PropertyCallback type

	typedef bool (*PropertyCallback)(const class Property &property, void *object, void *dest, void *value);

	//------------------------------------//
	// Property class

	class Property
	{
		ValueType(Property, 0);

		// Fields
	public:
		u32								Offset;
		PropertyType			Type;
		EnumValue*				Enums;
		Property*					Properties;
		string						Name;
		u32								Flags;
		PropertyCallback	Callback;

		// Constructor
	public:
		Property(u32 offset, PropertyType type, const string &name, u32 flags = pfNone, PropertyCallback callback = nullptr) {
			Offset = offset;
			Type = type;
			Enums = nullptr;
			Properties = nullptr;
			Name = name;
			Flags = flags;
			Callback = callback;
		}
		Property(u32 offset, Property* properties, const string &name, u32 flags = pfNone, PropertyCallback callback = nullptr) {
			Offset = offset;
			Type = ptNone;
			Enums = nullptr;
			Properties = properties;
			Name = name;
			Flags = flags;
			Callback = callback;
		}
		Property(u32 offset, EnumValue *enums, const string &name, u32 flags = pfNone, PropertyCallback callback = nullptr) {
			Offset = offset;
			Type = ptNone;
			Enums = enums;
			Properties = nullptr;
			Name = name;
			Flags = flags;
			Callback = callback;
		}
		Property() {
			Offset = 0;
			Type = ptNone;
			Enums = nullptr;
			Properties = nullptr;
			Flags = 0;
			Callback = nullptr;
		}

		// Properties
	public:
		bool end() const {
			return Type == ptNone && Name.empty();
		}

		// Functions
	public:
		AYUAPI static u32 count(Property *properties);
	};
	
	////------------------------------------//
	//// Object class

	//class Object
	//{
	//	DefineType(Object);

	//	// Protected methods
	//protected:
	//	AYUAPI virtual u32							idOf_() = 0;
	//	AYUAPI virtual bool							isKindOf_(u32 hash) = 0;
	//	AYUAPI virtual u32							properties_() const;
	//	AYUAPI virtual const Property*	property_(u32 index) const;

	//	// Public methods
	//public:
	//	AYUAPI u32							idOf();
	//	AYUAPI bool							isKindOf(u32 hash);
	//	AYUAPI u32							properties() const;
	//	AYUAPI const Property*	property(u32 index) const;

	//	// Public Template methods
	//public:
	//	template<typename Type>
	//	bool isKindOfT() {
	//		return this != nullptr && isKindOf_(TypeOf(Type));
	//	}

	//	// Destructor
	//public:
	//	AYUAPI virtual ~Object();
	//};
};
