//------------------------------------//
//
// MeshUni.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-13
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	struct UniImporter
	{
		ValueType(UniImporter, 0);
	public:
		enum {
			MESH_MAGIC = ('m' | 'i' << 8 | '0' << 16 | '4' << 24),
		};

		struct Vertex {
			ValueType(UniImporter::Vertex, 0);
		public:
			vec3 xyz;
			vec3 normal;
			vec2 coords[2];

			friend File &operator << (File &s, Vertex &v) {
				s.read(&v, sizeof(Vertex));
				return s;
			}
		};

		struct Surface {
			ValueType(UniImporter::Surface, 0);
		public:
			char name[128];								// surface name
			Array<Vertex> vertex;
			int num_triangles;
			Array<int> indices;
			bbox box;
			bsphere sphere;

			friend File &operator << (File &s, Surface &v) {
				s.read(v.name, sizeof(v.name));
				s << v.vertex;
				s << v.num_triangles;
				if(v.num_triangles) {
					v.indices.resize(v.num_triangles * 3);
					s.read(v.indices, sizeof(int) * v.num_triangles * 3);
				}
				if(v.vertex.size()) {
					v.box = bbox(&v.vertex[0].xyz, v.vertex.size(), sizeof(Vertex));
					v.sphere = bsphere(&v.vertex[0].xyz, v.vertex.size(), sizeof(Vertex));
				}
				return s;
			}
		};
		
		Array<Surface> surfaces;

		void load(const string &name) {
			SmartPtr<File> s(Engine::VFS->loadFile(va("%s.mesh", name.c_str()), ftMesh));
			int magic;

			// Pobierz nag��wek
			*s << magic;

			// Sprawd� nag��wek
			Assert("Invalid format" && magic == MESH_MAGIC);

			// Wczytaj powierzchnie
			*s << surfaces;
		}
	};

	void Mesh::loadMeshU(const string &name) {
		UniImporter importer;

		// Wczytaj mesha
		importer.load(name);

		// Dodaj wszystkie powierzchnie
		for(u32 ii = 0; ii < importer.surfaces.size(); ii++) {
			MeshObject object;
			MeshSurface surface;
			vec3 avgNormal;
			UniImporter::Surface *i = &importer.surfaces.at(ii);
					
			// Ustaw parametry obiektu
			object.Name = i->name;
			if(PRT) {
				object.PRT = va("%s/prt_%s", name.c_str(), i->name);
			}
			object.VertexStart = Vertices.size();
			object.IndexStart = Indices.size();
			object.Box = i->box;
			object.Sphere = i->sphere;

			// Ustaw pocz�tkowe parametry powierzchnii
			surface.Material = va("%s/%s", name.c_str(), i->name);
			surface.IndexStart = Indices.size();
			surface.Box = i->box;
			surface.Sphere = i->sphere;

			// Zadeklaruj tablic� wierzcho�k�w
			Array<MeshVertex> vertices(i->indices.size());

			//// Dump data (Create C++ code :P)
			//foreach(vector<UniImporter::Vertex>, j, i->vertex) {
			//	OutputDebugStringA(va("vec3(%ff, %ff, %ff), \n", j->xyz.X, j->xyz.Y, j->xyz.Z).c_str());
			//}
			//for(u32 j = 0; j < i->indices.size(); j++) {
			//	OutputDebugStringA(va("%i, ", i->indices.at(j)).c_str());
			//	if(j % 12 == 11)
			//		OutputDebugStringA("\n");
			//}

			// Dodaj wszystkie tr�jk�ty
			for(u32 j = 0; j < i->num_triangles; j++) {
				// Pobierz wierzcho�ki
				UniImporter::Vertex v[3] = {
					i->vertex.at(i->indices.at(j * 3 + 2)), 
					i->vertex.at(i->indices.at(j * 3 + 1)),
					i->vertex.at(i->indices.at(j * 3 + 0))};
				MeshVertex *vv = &vertices.at(j * 3);
				vec3 vectors[3];

				// Oblicz tangent
				tangentVectors::calculateVectors(
					v[0].xyz, v[1].xyz, v[2].xyz,
					v[0].coords[0], v[1].coords[0], v[2].coords[0], vectors);

				// Utw�rz wierzcho�ki
				for(u32 k = 0; k < 3; k++) {
					vv[k].Origin = v[k].xyz;
					vv[k].Coords[0] = v[k].coords[0];
					vv[k].Coords[1] = v[k].coords[1];
					vv[k].Normal = vec4::compress(vec4(v[k].normal, 1.0f));
					vv[k].Tangent = vec4::compress(tangentVectors::calculateTangentFlag(v[k].normal, vectors));
				}
				avgNormal += vectors[2];
			}

			// Wyznacz indeksy
			Array<u16> indices = optimize<u16>(vertices);

			// Loguj
			Engine::Log->print("MeshU: %s::%s has %i (%i) indices and %i (%i) vertices", name.c_str(), i->name, indices.size(), i->indices.size(), vertices.size(), i->vertex.size());

			// Dodaj wierzcho�ki i indeksy
			Vertices.push(vertices, vertices.size());
			Indices.push(indices, indices.size());

			// Znormalizuj wektor
			surface.Plane = vec4(avgNormal.normalize(), i->sphere.Center);

			// Ustaw ko�cowe parametry powierzchnii i obiektu
			surface.IndexCount = indices.size();
			object.IndexCount = indices.size();
			object.VertexCount = vertices.size();

			// Dodaj powierzchni� do obiektu
			object.Surfaces.push(surface);
			
			// Dodaj obiekt do mesha
			Objects.push(object);
		}
		_loaded = true;
	}
};
