//------------------------------------//
//
// Time.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	class Time
	{
		ValueType(Time, 0);

		// Fields
	private:
		/// Czas od uruchomenia aplikacji
		f64		_current;

		/// Czas trwania ostatniej klatki
		f64		_delta;
		
		/// Ostatnia zarejestrowana liczba cykli
		u64		_last;

		/// Pocz�tkowa liczba cykli
		u64		_first;

		/// Cz�stotliwo�� timera
		u64		_freq;

		/// Ilo�� klatek na sekund�
		f32		_fps;

		/// Startowa ilo�� cykli dla licznika klatek
		u64		_fpsStart;

		/// Ilo�� zarejestrowanych klatek
		u64		_fpsCount;

		// Constructor
	public:
		Time();

		// Destructor
	public:
		~Time();

		// Methods
	public:
		/// Zeruje liczniki czasu
		AYUAPI void reset();

		/// Uaktualnia liczniki czasu (co klatk�)
		AYUAPI void update();

		/// Zwraca ilo�� cykli od uruchomienia aplikacji
		AYUAPI u64 cycles();

		/// Zwraca czas (s) od uruchomenia aplikacji
		AYUAPI f32 current();

		/// Zwraca czas (s) trwania ostatniej klatki
		AYUAPI f32 delta();

		/// Zwraca ilo�� klatek na sekund�
		AYUAPI f32 fps();
	};
};
