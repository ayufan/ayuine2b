//------------------------------------//
//
// DeviceObject.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-14
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// DeviceObject: Constructor

	DeviceObject::DeviceObject() : _itor(this) {
		// Dodaj do listy obiekt�w
		if(Engine::Device) {
			// Podepnij list�
			_itor.linkFront(Engine::Device->_deviceObjects);
		}
	}

	//------------------------------------//
	// DeviceObject: Destructor

	DeviceObject::~DeviceObject() {
	}

	//------------------------------------//
	// DeviceObject: Methods

	u32 DeviceObject::type() {
		return 0;
	}

	void DeviceObject::deviceReset(bool theBegin) {

	}

	void DeviceObject::deviceLost(bool theEnd) {

	}
};
