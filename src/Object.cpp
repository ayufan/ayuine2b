//------------------------------------//
//
// Object.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-9-1
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// Object: Constructor

	Object::Object() {
	}

	Object::Object(const Object &other) {
	}

	//------------------------------------//
	// Object: Destructor

	Object::~Object() {
		// Wywo�aj zdarzenie
		OnDispose(this);
	}
};
