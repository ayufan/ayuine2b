//------------------------------------//
//
// Engine.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-31
//
//------------------------------------//

#pragma once

namespace ayuine
{
	//------------------------------------//
	// DirectXException class

	class DirectXException : public Exception
	{
		ObjectType(DirectXException, Exception, 0);

		// Fields
	public:
		HRESULT		Result;

		// Constructor
	public:
		AYUAPI DirectXException(HRESULT hr)
			: Exception(DXGetErrorStringA(hr)) {
			Result = hr;
		}
	};

	//------------------------------------//
	// Device Handling

	static void dxe_(HRESULT hr, const ExceptionTrace &trace) {
#ifdef _DEBUG
		if(FAILED(hr))
			Exception::ThrowException(DirectXException(hr), trace);
#endif
	}
	#define dxe(x)		dxe_(x, newExceptionTrace())

	//------------------------------------//
	// EngineFlags enumeration

	enum EngineFlags
	{
		efNone					= 0,
		efCamera				= 0,
		efConfig				= 0,
		efLog						= 0,
		efTime					= 0,
		efVFS						= 1,
		efStats					= 8,
		efInput					= 32 | efTime,
		efWorld					= 64 | efVFS | efTime | efCamera,
		efSound					= 128 | efVFS | efTime,
		efWindow				= 256 | efTime,
		efDevice				= 512 | efTime | efWindow | efStats,
		efDraw					= 1024 | efTime | efWindow | efDevice  | efStats,
		efRender				= 2048 | efTime | efWindow | efDevice  | efStats | efDraw | efVFS | efCamera,
		efAll						= 0xFFFFFFFF
	};

	//------------------------------------//
	// Engine class

	class Engine
	{
		ValueType(Engine, 0);

		// Statc Fields
	public:
		AYUAPI static u32											Flags;
		AYUAPI static ayuine::Input*					Input;
		AYUAPI static ayuine::World*					World;
		AYUAPI static ayuine::Sound*					Sound;
		AYUAPI static ayuine::Window*					Window;
		AYUAPI static ayuine::Device*					Device;
		AYUAPI static ayuine::Draw*						Draw;
		AYUAPI static ayuine::Render*					Render;
		AYUAPI static ayuine::Stats*		Stats;
		AYUAPI static ayuine::VFS*						VFS;
		AYUAPI static ayuine::Log*						Log;
		AYUAPI static ayuine::Time*						Time;
		AYUAPI static ayuine::Camera*					Camera;
		AYUAPI static ayuine::Config*					Config;

		// Engine Methods
	public:
		AYUAPI static void init(u32 flags = efAll);
		AYUAPI static bool done();
		AYUAPI static bool update();
		AYUAPI static void render();
		AYUAPI static void renderView(ayuine::Camera* camera, RenderMode mode = rmShaded, u32 flags = rfNone, class Viewport* viewport = nullptr);
		AYUAPI static void run();
		AYUAPI static void release();
	};
};
