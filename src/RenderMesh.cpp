//------------------------------------//
//
// RenderMesh.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-14
//
//------------------------------------//

#include "StdAfx.hpp"

namespace ayuine
{
	//------------------------------------//
	// RenderMesh: Constructor

	RenderMesh::RenderMesh() {
		VertexType = nullptr;
		VertexBuffer = nullptr;
		IndexBuffer = nullptr;
		Offset = 0;
	}

	//------------------------------------//
	// RenderMesh: Methods

	void RenderMesh::render(RenderState &state) {
		// Ustaw lustro
		if(Mirror) {
			// Je�li ju� jest wy�wietlane lustro to nie wy�wietlamy luster
			if(state.Level)
				return;
			state.Reflection = Mirror->Texture;
			state.bindReflection();
		}

		// Ustaw ramk�
		state.Object = Transform;
		state.ObjectPRT = PRT;

		// Podepnij obiekt
		state.bindObject();

		// Ustaw bufory
		VertexType->bind();
		VertexBuffer->bind(0, Offset);
		IndexBuffer->bind();

		// Wy�wietl mesha
		for(u32 i = 0; i < Drawers.size(); i++)
			Drawers.at(i).draw();

		// Wy��cz bufory
		IndexBuffer->unbind();
		VertexBuffer->unbind(0);

		// Odepnij obiekt
		state.unbindObject();

		// Odepnij lustro
		if(Mirror) {
			state.unbindReflection();
		}
	}
};
