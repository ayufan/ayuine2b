//------------------------------------//
//
// common.fx
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-27
//
//------------------------------------//

//------------------------------------//
// Material Consts (ps/vs)

float4		sdtColor					: register(c0);
float4		sdtParam					: register(c1);
float4		sdtFrame					: register(c2);		// (1.0f / Width, 1.0f / Height, Time)

//------------------------------------//
// Light Consts (ps/vs)

float4		sdtLightColor			: register(c3);
float4		sdtLightParam			: register(c4);

//------------------------------------//
// Transform Consts (vs)

float4x4	sdtTransform			: register(c5);
float4x3	sdtObject					: register(c9);

//------------------------------------//
// Light Consts (vs)

float4x4	sdtLightTransform	:	register(c12);
float4		sdtLightOrigin		: register(c16);
float3		sdtLightDir				: register(c17);

//------------------------------------//
// Camera Consts (vs)

float4		sdtCameraOrigin		: register(c18);
float3		sdtCameraAt				: register(c19);
float3		sdtCameraUp				: register(c20);
float3		sdtCameraRight		: register(c21);

//------------------------------------//
// Skinning Consts (vs)

float4x3	sdtSkinning[16]		: register(c22);

//------------------------------------//
// PRT Consts (ps)

float4		sdtPCA[4]					: register(c5);

//------------------------------------//
// Bloom Consts (ps)

float4		sdtTaps[15]				: register(c9);

//------------------------------------//
// Material Samplers

sampler		sdtTexture0			: register(s0);
sampler		sdtTexture1			: register(s1);
sampler		sdtTexture2			: register(s2);
sampler		sdtTexture3			: register(s3);

//------------------------------------//
// Ambient/Light/Refraction Samplers

sampler		sdtPRT					: register(s4);
sampler		sdtShadow				: register(s5);
sampler		sdtProjection		: register(s6);
sampler		sdtRefractive		: register(s6);
sampler		sdtReflective		: register(s7);

//------------------------------------//
// PostProcess Samplers

sampler		sdtScene				: register(s6);
sampler		sdtBlur					: register(s7);

//------------------------------------//
// Vertex Format

struct SkyVertex
{
	float3	Dir				: POSITION0;
};

struct ParticleVertex
{
	float3	Origin		: POSITION0;
	float4	Coords		: TEXCOORD0;
	float4	Color			: COLOR0;
};

struct MeshVertex 
{
	float3	Origin		: POSITION0;
	float4	Coords		: TEXCOORD0;
	float3	Normal		: NORMAL0;
	float4	Tangent		: TANGENT0;
};

struct ScreenVertex
{
	float3	Screen		: POSITION0;
};
