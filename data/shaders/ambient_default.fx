//------------------------------------//
//
// ambient_default.fx
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: Shaders
// Date: 2006-8-14
//
//------------------------------------//

#include "common.fx"

//#define NO_TRANSFORM

struct AmbientData
{
	float4 Coords : TEXCOORD0;
	float3 Normal : TEXCOORD1;
	float3 LightDir : TEXCOORD2;
};

//------------------------------------//
// Ambient Shader

float4 VertexShader(in MeshVertex In, out AmbientData Out) : POSITION0 {
	float3 origin;
	
	// Oblicz pozycje wierzchołka w świecie
#ifdef NO_TRANSFORM
	origin = In.Origin;
#else
	origin = mul(float4(In.Origin, 1.0f), sdtObject).xyz;
#endif

	// Zapisz podstawowe parametry
	Out.Coords.xy = In.Coords.xy;
	Out.Coords.zw = In.Coords.zw * 0.5f;
	
	// Oblicz wektor normalny
	Out.Normal = mul(In.Normal.xyz * 2.0f - 1.0f, sdtObject).xyz;
	Out.LightDir = sdtLightDir;

	// Przetransformuj wierzchołek na ekran
	return mul(float4(origin, 1.0f), sdtTransform);	
}

float4 PixelShader(in AmbientData In) : COLOR0 {
#ifdef PRT
	float shadow = saturate(
		dot(tex2D(sdtPRT, In.Coords.zw + float2(0.0f, 0.0f)) * 2.0f - 1.0f, sdtPCA[0]) + 
		dot(tex2D(sdtPRT, In.Coords.zw + float2(0.5f, 0.0f)) * 2.0f - 1.0f, sdtPCA[1]) + 
		dot(tex2D(sdtPRT, In.Coords.zw + float2(0.0f, 0.5f)) * 2.0f - 1.0f, sdtPCA[2]) + 
		dot(tex2D(sdtPRT, In.Coords.zw + float2(0.5f, 0.5f)) * 2.0f - 1.0f, sdtPCA[3]));
	float4 color = saturate(shadow * sdtLightColor);
#else	
	#ifdef SUN
		float4 color = saturate(sdtColor + sdtLightColor * saturate(dot(In.LightDir, normalize(In.Normal))));
	#else
		float4 color = sdtColor;
	#endif
#endif

	return tex2D(sdtTexture0, In.Coords.xy) * color;
}

