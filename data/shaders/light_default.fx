//------------------------------------//
//
// light_default.fx
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: Shaders
// Date: 2006-8-14
//
//------------------------------------//

#include "common.fx"

#define NO_SPECULAR
//#define NO_TRANSFORM

struct LightData
{
	float4 Coords : TEXCOORD0;
	float3 LDir : TEXCOORD1;
#ifndef NO_SPECULAR	
	float3 HDir : TEXCOORD2;
#endif
	float3 SDir : TEXCOORD3;
};

//------------------------------------//
// Light Shader

float4 VertexShader(in MeshVertex In, out LightData Out) : POSITION0 {
	float3 origin, normal, tangent, binormal;
	
	// Oblicz podstawowe parametry
#ifdef NO_TRANSFORM
	origin = In.Origin;
	normal = In.Normal * 2.0f - 1.0f;
	float4 temp = In.Tangent * 2.0f - 1.0f;
	tangent = temp.xyz;
	binormal = cross(normal, tangent) * temp.w;
#else
	origin = mul(float4(In.Origin, 1.0f), sdtObject).xyz;
	normal = mul(In.Normal * 2.0f - 1.0f, sdtObject).xyz;
	float4 temp = In.Tangent * 2.0f - 1.0f;
	tangent = mul(temp.xyz, sdtObject).xyz;
	binormal = cross(normal, tangent) * temp.w;
#endif

	// Oblicz dodatkowe dane
	Out.Coords.xy = In.Coords.xy;
	Out.Coords.zw = In.Coords.zw * 0.5f;
	
	// Oblicz podstawowe parametry do o�wietlenia
	float3 lightDir = (origin - sdtLightOrigin.xyz);
	float3 lightDirN = normalize(lightDir);
	float3 cameraDir = (origin - sdtCameraOrigin);
	float3 cameraDirN = normalize(cameraDir);
	
	// Oblicz wektor �wiat�a w przestrzenii stycznej
	Out.LDir = lightDir;
	Out.LDir = float3(dot(Out.LDir, tangent), dot(Out.LDir, binormal), dot(Out.LDir, normal));
	
	// Oblicz wektor po��wkowy
#ifndef NO_SPECULAR
	Out.HDir = lightDirN + cameraDirN;
	Out.HDir = float3(dot(Out.HDir, tangent), dot(Out.HDir, binormal), dot(Out.HDir, normal));
#endif
	
	// Oblicz wektor przekszta�cony do przestrzenii cienia :)
	Out.SDir = (origin - sdtLightOrigin.xyz) * sdtLightOrigin.w;
	
	// Oblicz pozycj� wierzcho�ka na ekranie
	return mul(float4(origin, 1.0f), sdtTransform);	
}

float4 PixelShader(in LightData In) : COLOR0 {
	float3 normal = tex2D(sdtTexture1, In.Coords).xyz * 2.0f - 1.0f;
	
	// Oblicz g��boko��
	float3 dir = In.SDir.xyz;
	float dist = length(dir);
	float idist = 1.0f / dist;
	
	// Rozproszenie
#ifdef NO_DIFFUSE
	float3 diffuse = 0;
	float opacity = 0;
#else
	float4 diffColor = tex2D(sdtTexture0, In.Coords);
	float dotLN = saturate(-dot(normalize(In.LDir), normal));
	float3 diffuse = dotLN * diffColor;
	float opacity = diffColor.x;
#endif
	
	// Odbicie
#ifdef NO_SPECULAR
	float3 specular = 0;
#else
	float4 specColor = tex2D(sdtTexture2, In.Coords);
	float dotHN = saturate(-dot(normalize(In.HDir), normal));
	float3 specular = dotHN * pow(specColor, 16.0f);
#endif	

	// Zanikanie
#ifdef NO_ATTN
	float attn = 1.0f;
#else
	// Oblicz zanikanie
	float attn = saturate(1.0f - dist);
#endif

	// Cienie
#if SHADOWS > 0
	float4 shadowColor = texCUBE(sdtShadow, -dir);
	float2 moments = shadowColor.xy + shadowColor.zw;
	
	// Oblicz bias
	float slope = 0.0f; // 1-dot(normal, dir)
	float bias = 0.0005 + 0.010 * slope;
	dist -= bias;
	
	#if SHADOWS > 1
		#if SHADOWS > 2
			float2 poisson[12] = {
				float2(-0.326212f, -0.40581f),
				float2(-0.840144f, -0.07358f),
				float2(-0.695914f, 0.457137f),
				float2(-0.203345f, 0.620716f),
				float2(0.96234f, -0.194983f),
				float2(0.473434f, -0.480026f),
				float2(0.519456f, 0.767022f),
				float2(0.185461f, -0.893124f),
				float2(0.507431f, 0.064425f),
				float2(0.89642f, 0.412458f),
				float2(-0.32194f, -0.932615f),
				float2(-0.791559f, -0.59771f)
			};
			
			#if SHADOWS > 3
				// 13 samples
				for(int i = 0; i < 12; i++) {
					shadowColor = texCUBE(sdtShadow, -dir + float3(poisson[i].xy, poisson[11-i].x) * idist * 0.005f);
					moments.xy += shadowColor.xy + shadowColor.zw;
				}
				moments.xy /= 13;
			#else
				// 5 samples
				for(int i = 0; i < 4; i++) {
					shadowColor = texCUBE(sdtShadow, -dir + float3(poisson[i * 3].xy, poisson[11 - i * 3].x) * idist * 0.005f);
					moments.xy += shadowColor.xy + shadowColor.zw;
				}
				moments.xy /= 5;
			#endif
		#endif
		
		// Variance Shadow Maps
		float e2 = moments.y;
		float ex2 = moments.x * moments.x;
		float variance = min(max(e2 - ex2, 0) + 0.00003, 1.0);
		float md = moments.x - dist;
		float shadow = max(dist <= moments.x, variance / (variance + md * md));
	#else
		// Hard Shadow Maps
		float shadow = dist < moments.x ? 1 : 0;
	#endif
#else
	float shadow = 1.0f;
#endif

	// PRT
#ifdef PRT
	float shadowPRT = saturate(
		dot(tex2D(sdtPRT, In.Coords.zw + float2(0.0f, 0.0f)) * 2.0f - 1.0f, sdtPCA[0]) + 
		dot(tex2D(sdtPRT, In.Coords.zw + float2(0.5f, 0.0f)) * 2.0f - 1.0f, sdtPCA[1]) + 
		dot(tex2D(sdtPRT, In.Coords.zw + float2(0.0f, 0.5f)) * 2.0f - 1.0f, sdtPCA[2]) + 
		dot(tex2D(sdtPRT, In.Coords.zw + float2(0.5f, 0.5f)) * 2.0f - 1.0f, sdtPCA[3]));
	shadow = saturate(shadow * sdtLightParam.x + shadowPRT * sdtLightParam.y);
#endif
	
	// Oblicz finalny color
	return float4((diffuse + specular) * attn * shadow * sdtLightColor * sdtLightColor.w, opacity);
}
