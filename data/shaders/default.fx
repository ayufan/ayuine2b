//------------------------------------//
//
// default.fx
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: Miscellaneous Files
// Date: 2006-8-1
//
//------------------------------------//

#include "common.fx"

struct AmbientData
{
	float2 Coords : TEXCOORD0;
	float3 Normal : TEXCOORD1;
};

struct LightData
{
	float2 Coords : TEXCOORD0;
	float3 LDir : TEXCOORD1;
	float3 HDir : TEXCOORD2;
	float3 SDir : TEXCOORD3;
};

//------------------------------------//
// Ambient Shader

float4 AmbientVertex(in MeshVertex In, out AmbientData Out) : POSITION0 {
	float3 origin;
	
	// Oblicz pozycje wierzcho�ka w �wiecie
	origin = mul(float4(In.Origin, 1.0f), sdtObject).xyz;
	
	// Zapisz podstawowe parametry
	Out.Coords = In.Coords;
	
	// Oblicz wektor normalny
	float3 normal = normalize(mul(In.Normal.xyz / 32767.0f, sdtObject).xyz);
	float3 tangent = float3(normal.z, -normal.x, normal.y);
	tangent = normalize(tangent - normal * dot(normal, tangent));
	float3 binormal = normalize(cross(tangent, normal));
	Out.Normal = binormal;
	
	// Przetransformuj wierzcho�ek na ekran
	return mul(float4(origin, 1.0f), sdtTransform);	
}

float4 AmbientPixel(in AmbientData In) : COLOR0 {
	//return float4(normalize(In.Normal) * 0.5f + 0.5f, 1.0f);
	return tex2D(sdtTexture0, In.Coords) * sdtColor;
}

//------------------------------------//
// Light Shader

float4 LightVertex(in MeshVertex In, out LightData Out) : POSITION0 {
	float3 origin, normal, tangent, binormal;
	
	// Oblicz podstawowe parametry
	origin = mul(float4(In.Origin, 1.0f), sdtObject).xyz;
	normal = normalize(mul(In.Normal.xyz / 32767.0f, sdtObject).xyz);
	tangent.xy = In.Tangent.xy / 32767.0f;
	tangent.z = sqrt(1.0f - dot(tangent.xy, tangent.xy));
	tangent = normalize(mul(tangent, sdtObject).xyz);
	binormal = cross(normal, tangent) * In.Normal.w;
	
	//tangent = float3(normal.z, -normal.x, normal.y);
	//tangent = normalize(tangent - normal * dot(normal, tangent));
	//binormal = normalize(cross(tangent, normal));
	
	// Oblicz dodatkowe dane
	Out.Coords = In.Coords;
	
	// Oblicz podstawowe parametry do o�wietlenia
	float3 lightDir = (origin - sdtLightOrigin.xyz);
	float3 lightDirN = normalize(lightDir);
	float3 cameraDir = (origin - sdtCameraOrigin);
	float3 cameraDirN = normalize(cameraDir);
	
	// Oblicz wektor �wiat�a w przestrzenii stycznej
	Out.LDir = lightDir;
	Out.LDir = float3(dot(Out.LDir, tangent), dot(Out.LDir, binormal), dot(Out.LDir, normal));
	
	// Oblicz wektor po��wkowy
	Out.HDir = lightDirN + cameraDirN;
	Out.HDir = float3(dot(Out.HDir, tangent), dot(Out.HDir, binormal), dot(Out.HDir, normal));
	
	// Oblicz wektor przekszta�cony do przestrzenii cienia :)
	Out.SDir = (origin - sdtLightOrigin.xyz);
	
	// Oblicz pozycj� wierzcho�ka na ekranie
	return mul(float4(origin, 1.0f), sdtTransform);	
}

float4 LightPixel(in LightData In) : COLOR0 {
	float3 normal = tex2D(sdtTexture1, In.Coords).xyz * 2.0f - 1.0f;
	
	// Oblicz g��boko��
	float3 dir = In.SDir.xyz;
	float dist = sdtLightOrigin.w * length(dir);
	
	// Rozproszenie
#ifdef NO_DIFFUSE
	float3 diffuse = 0;
	float opacity = 0;
#else
	float4 diffColor = tex2D(sdtTexture0, In.Coords);
	float dotLN = saturate(-dot(normalize(In.LDir), normal));
	float3 diffuse = dotLN * diffColor;
	float opacity = diffColor.x;
#endif
	
	// Odbicie
#ifdef NO_SPECULAR
	float3 specular = 0;
#else
	float4 specColor = tex2D(sdtTexture2, In.Coords);
	float dotHN = saturate(-dot(normalize(In.HDir), normal));
	float3 specular = dotHN * pow(specColor, 16.0f);
#endif	
	specular = 0;

	// Zanikanie
#ifdef NO_ATTN
	float attn = 1.0f;
#else
	// Oblicz zanikanie
	float attn = saturate(1.0f - dist);
#endif

	// Cienie
#if SHADOWS > 0
	float4 shadowColor = tex3D(sdtShadowMap, -dir);
	float2 moments = shadowColor.xy + shadowColor.zw;
	
	// Oblicz bias
	float slope = 0.0f; // 1-dot(normal, dir)
	float bias = 0.0005 + 0.010 * slope;
	dist -= bias;
	
	#if SHADOWS > 1
		#if SHADOWS > 2
			float2 poisson[12] = {
				float2(-0.326212f, -0.40581f),
				float2(-0.840144f, -0.07358f),
				float2(-0.695914f, 0.457137f),
				float2(-0.203345f, 0.620716f),
				float2(0.96234f, -0.194983f),
				float2(0.473434f, -0.480026f),
				float2(0.519456f, 0.767022f),
				float2(0.185461f, -0.893124f),
				float2(0.507431f, 0.064425f),
				float2(0.89642f, 0.412458f),
				float2(-0.32194f, -0.932615f),
				float2(-0.791559f, -0.59771f)
			};
			
			#if SHADOWS > 3
				// 13 samples
				for(int i = 0; i < 12; i++) {
					shadowColor = tex3D(sdtShadowMap, -dir + float3(poisson[i].xy, poisson[11-i].x) / 10);
					moments.xy += shadowColor.xy + shadowColor.zw;
				}
				moments.xy /= 13;
			#else
				// 5 samples
				for(int i = 0; i < 4; i++) {
					shadowColor = tex3D(sdtShadowMap, -dir + float3(poisson[i * 3].xy, poisson[11 - i * 3].x) / 10);
					moments.xy += shadowColor.xy + shadowColor.zw;
				}
				moments.xy /= 5;
			#endif
		#endif
		
		// Variance Shadow Maps
		float e2 = moments.y;
		float ex2 = moments.x * moments.x;
		float variance = min(max(e2 - ex2, 0) + 0.00003, 1.0);
		float md = moments.x - dist;
		float shadow = max(dist <= moments.x, variance / (variance + md * md));
	#else
		// Hard Shadow Maps
		float shadow = dist < moments.x ? 1 : 0;
	#endif
#else
	float shadow = 1.0f;
#endif
	
	// Oblicz finalny color
	return float4((diffuse + specular) * attn * shadow * sdtColor * sdtColor.w, opacity);
}
